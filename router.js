define([
	"initRouter"
],
function(initRouter) {
"use strict";
	console.log("进入...router");
	var router = new initRouter();
	router.get("login/", function(req) {
		app.setCurrentView("login");
	});
	//router.navigate("email/"+123);产品
	router.get("product/:key", function(req) {
		vp.registerFn.writeEmailGo = true;
		console.log("indexPage参数：",req.params.key);
		app.setCurrentView("product");
	});
	//产品
	router.get("product", function(req) {
		vp.registerFn.writeEmailGo = true;
		console.log("indexPage参数：",req.params.key);
		app.setCurrentView("product");
	});
	//router.navigate("customer/"+123);客户
	router.get("customer/:key", function(req) {
		vp.registerFn.writeEmailGo = true;
		console.log("customer参数：",req.params.key);
		app.setCurrentView("customer");
	});
	//客户
	router.get("customer", function(req) {
		vp.registerFn.writeEmailGo = true;
		console.log("customer参数：",req.params.key);
		app.setCurrentView("customer");
	});
	//客户
	router.get("customer/", function(req) {
		vp.registerFn.writeEmailGo = true;
		console.log("customer参数：",req.params.key);
		app.setCurrentView("customer");
	});
	//邮件
	router.get("email/:key", function(req) {
		vp.registerFn.writeEmailGo = true;
		console.log("email参数：",req.params.key);
		app.setCurrentView("email");
	});
	//邮件
	router.get("email", function(req) {
		vp.registerFn.writeEmailGo = true;
		console.log("email参数：",req.params.key);
		app.setCurrentView("email");
	});
	router.get("email/", function(req) {
		vp.registerFn.writeEmailGo = true;
		console.log("email参数：",req.params.key);
		app.setCurrentView("email");
	});
	//router.navigate("mine/"+123);我的
	router.get("mine/:key", function(req) {
		console.log("mine参数：",req.params.key);
		app.setCurrentView("mine");
	});
	//我的
	router.get("mine/", function(req) {
		console.log("mine参数：",req.params.key);
		app.setCurrentView("mine");
	});
	//router.navigate("customer/"+123);单据
	router.get("bill/:key", function(req) {
		vp.registerFn.writeEmailGo = true;
		console.log("bill参数：",req.params.key);
		app.setCurrentView("bill");
	});
	//单据
	router.get("bill", function(req) {
		vp.registerFn.writeEmailGo = true;
		console.log("bill参数：",req.params.key);
		app.setCurrentView("bill");
	});
	return router;
})