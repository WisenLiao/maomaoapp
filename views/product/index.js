define([
    "fortext!templates/product/index.html",
    "views/writeEmail/index"
], function(html,writeEmail) {
    "use strict";
    return Vue.extend({
        template: 		html,
        props: ["mainObj","user",],
        data: function() {
            return {
                currentView:"writeEmail",
                attrArr:{},
                resList:{},//报价组合  数据组
                results:{},
                dataa:{},
                imagesArr:[]
            };
        },
        components: {
            "writeEmail":writeEmail
        },
        mounted: function() {
            if( vp.registerFn && vp.registerFn.productModule){
                vp.registerFn.productModule();
            }else{
                var scriptArr = [
                    './scripts/product/index.js'
                ];
                vp.util.loadScript(scriptArr);
            }
        },
        methods: {

        }
    });
})