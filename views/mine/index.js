define([
    "fortext!templates/mine/index.html"
], function(html) {
    "use strict";
    return Vue.extend({
        template: 		html,
        props: ["mainObj","user"],
        data: function() {
            return {
            };
        },
        mounted: function() {
            if(vp.registerFn&&vp.registerFn.mindModule){
                vp.registerFn.mindModule();
            }else{
                var scriptArr = [
                    './scripts/mine/index.js'
                ];
                vp.util.loadScript(scriptArr);
            }
        },
        methods: {

        }
    });
})