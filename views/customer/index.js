define([
    "fortext!templates/customer/index.html",
    "views/writeEmail/index"
], function(html,writeEmail) {
    "use strict";
    return Vue.extend({
        template: 		html,
        props: ["mainObj","user"],
        data: function() {
            return {
                currentView:"writeEmail"
            };
        },
        components: {
            "writeEmail":writeEmail
        },
        mounted: function() {
            if(vp.registerFn&&vp.registerFn.customerRegist){
                vp.registerFn.customerRegist();
            }else{
                console.log("--------./scripts/customer/index.js------------")
                var scriptArr = [
                  './scripts/customer/index.js'
                ];
                vp.util.loadScript(scriptArr);
            }
        },
        methods: {

        }
    });
})