define([
	"fortext!templates/login/index.html"
], function(html) {
"use strict";
	return Vue.extend({
		template: 		html,
		props: ["mainObj","user"],
		data: function() {
			return {

			};
		},
		mounted: function() {
			if(vp.registerFn.loginRegist){
				vp.registerFn.loginRegist();
			}else{
				var scriptArr = [
					'./scripts/login/index.js'
				];
				vp.util.loadScript(scriptArr);
			}
		},
		methods: {

		}
	});
})