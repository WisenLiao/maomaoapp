define([
    "fortext!templates/writeEmail/index.html"
], function(html) {
    "use strict";
    return Vue.extend({
        template: 		html,
        props: ["mainObj","user"],
        data: function() {
            return {

            };
        },
        mounted: function() {
            if(vp.registerFn.writeEmailGo){
                vp.registerFn.writeEmailGo=false;
                if(vp.registerFn&&vp.registerFn.writeEmail){
                    vp.registerFn.writeEmail();
                }else{
                    var scriptArr = [
                        './scripts/writeEmail/index.js'
                    ];
                    vp.util.loadScript(scriptArr);
                }

            }
        },
        methods: {

        }
    });
})