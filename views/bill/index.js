
define([
    "fortext!templates/bill/index.html",
    "views/writeEmail/index"
], function(html,writeEmail) {
    "use strict";
    return Vue.extend({
        template: 		html,
        props: ["mainObj","user"],
        data: function() {
            return {
                currentView:"writeEmail"
            };
        },
        components: {
            "writeEmail":writeEmail
        },
        mounted: function() {
            if(vp.registerFn&&vp.registerFn.billRegist){
                vp.registerFn.billRegist();
            }else{
                console.log("--------./scripts/bill/index.js------------")
                var scriptArr = [
                    './scripts/bill/index.js'
                ];
                vp.util.loadScript(scriptArr);
            }
        },
        methods: {

        }
    });
})