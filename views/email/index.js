define([
    "fortext!templates/email/index.html",
    "views/writeEmail/index"

], function(html,writeEmail) {
    "use strict";
    return Vue.extend({
        template: 		html,
        props: ["mainObj","user"],
        data: function() {
            return {
                currentView:"writeEmail"
            };
        },
        components: {
            "writeEmail":writeEmail
        },
        mounted: function() {
            //vp.registerFn.writeEmailGo = false;
            if(vp.registerFn&&vp.registerFn.emailRegist){
                vp.registerFn.emailRegist();
            }else{
                var scriptArr = [
                    './scripts/email/index.js'
                ];
                vp.util.loadScript(scriptArr);
            }
        },
        methods: {

        }
    });
})