define([
		"views/login/index",
		"views/email/index",
		"views/mine/index",
		"views/customer/index",
		"views/bill/index",
		"views/product/index"
	],
function(login,email,mine,customer,bill,product)
{
"use strict";
	console.log("进入...app");
	return Vue.extend({
		data: function() {
			return {
				mainObj:{},
				user:{},
				currentView :"login"
			};
		},
		components: {
			"login":login,
			"email":email,
			"mine":mine,
			"bill":bill,
			"customer":customer,
			"product":product
		},
		mounted: function() {
			FastClick.attach(document.body);
		},
		destroyed :function(){
			
		},
		methods: {
			home:function () {
				var url = {
					local:"data/testDemo/data.json",
					server:"",
					debug:true
				};
				vp.core.ajax(url,{},function(data){
					//成功
					console.log("----成功----------",data);
				},function(returnCode, returnMessage ,error){
					//错误
					console.log("----错误----------",returnCode,returnMessage,error);
				},function(){
					//失败
					console.log("----失败----------");
				});
			},
			setCurrentView:function(arg){
				this.currentView = arg;
			}
		}
	})
});