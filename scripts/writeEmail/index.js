;$(window).ready(function(){
    var writeEmail=function(){
        var mailType=5;
        var mailTitle="开发信";
        var mailToBack='';
        window.qunSendArr=[];
        vp.registerObj.mailMOdalBoxto='#menue-box';//邮件模板页退出指向页面
        vp.registerObj.sendMailToPage='menue-box';//发送邮件后显示的页面
        vp.registerObj.mailWritePage='write_mail';//正在写邮件的页面
        setTimeout(function(){
            $('#emailTempIndex').on('click','article .contentD .systemUl li',function(){
                var $this=$(this);
                var ClassName=$this.attr('class').split(' ')[0];
                var Pkid=$this.attr('data-id');
                mailToBack='模板';
                switch(ClassName){
                    case 'li0':
                        mailType=0;
                        mailTitle="开发信";
                        $('#singleEmailTemp').removeClass('hidden').siblings('#emailTempIndex').addClass('hidden');
                        break;
                    case 'li1':
                        mailType=1;
                        mailTitle="价格类";
                        $('#pricetypemodal').removeClass('hidden').siblings('#emailTempIndex').addClass('hidden');
                        break;
                    case 'li2':
                        mailType=2;
                        mailTitle="打样类";
                        $('#dayangtypemodal').removeClass('hidden').siblings('#emailTempIndex').addClass('hidden');
                        break;
                    case 'li3':
                        mailType=3;
                        mailTitle="跟进类";
                        $('#genjintypemodal').removeClass('hidden').siblings('#emailTempIndex').addClass('hidden');
                        break;
                    case 'li4':
                        mailType=4;
                        mailTitle="售后类";
                        $('#daikuanmodal').removeClass('hidden').siblings('#emailTempIndex').addClass('hidden');
                        break;
                    case 'li5':
                        mailType=5;
                        mailTitle="邀请类";
                        $('#invitetypemodal').removeClass('hidden').siblings('#emailTempIndex').addClass('hidden');
                        break;
                    case 'li6':
                        mailType=6;
                        mailTitle="节日类";
                        $('#Festivaltypemodal').removeClass('hidden').siblings('#emailTempIndex').addClass('hidden');
                        break;
                }
                getModalList(Pkid,ClassName);
            });
            //点击模板进入详情
            $('div[data-type="modaljihe"]').on('click','.singleTempUl li',function(){
                var $this=$(this);
                var titleName=$this.find('.title-name').html();
                var contentT=$this.find('b').html();
                $('div[data-type="modaljihe"]').addClass('hidden').siblings('#mail-modal-detail').removeClass('hidden');
                $('#mail-modal-detail').find('.Addressee-theme .Addressee .names .name').html(mailTitle);
                $('#mail-modal-detail').find('.Addressee-theme .Theme .content').html(titleName);
                $('#mail-modal-detail').find('.mail-subject .mail-number').html(contentT);
            });
            //点击返回 退出开发信模板 进入模板首页
            $('div[data-type="modaljihe"]').on('click','header .icon',function(){
                $('div[data-type="modaljihe"]').addClass('hidden');
                $('#emailTempIndex').removeClass('hidden');
            });
            //点击返回
            $('#mail-modal-detail').on('click','.header span',function(){
                var $this=$(this);
                var className=$this.attr('class').split(' ')[0];
                switch(className){
                    case 'head-left':
                        //取消
                        $('#mail-modal-detail').addClass('hidden');
                        switch(mailType){
                            case 0:
                                $('#emailTempIndex').removeClass('hidden');
                                break;
                            case 1:
                                $('#pricetypemodal').removeClass('hidden');
                                break;
                            case 2:
                                $('#dayangtypemodal').removeClass('hidden');
                                break;
                            case 3:
                                $('#genjintypemodal').removeClass('hidden');
                                break;
                            case 4:
                                $('#daikuanmodal').removeClass('hidden');
                                break;
                            case 5:
                                $('#invitetypemodal').removeClass('hidden');
                                break;
                            case 6:
                                $('#Festivaltypemodal').removeClass('hidden');
                                break;
                        }
                        break;
                    case 'head-right':
                        //使用
                        //使用模板默认写邮件
                        var mailWritePage=vp.registerObj.mailWritePage;
                        var subject=$('#mail-modal-detail .Addressee-theme .Theme .content').html();
                        var body=$('#mail-modal-detail .mail-subject .mail-number').html();
                        //$('#'+mailWritePage).find('.Addressee-theme .Theme textarea').val(subject);
                        $('#'+mailWritePage).find('.mail-subject .mail-number').val(body);
                        autosize(document.querySelectorAll('#'+mailWritePage+' .mail-subject .mail-number'));
                        switch(mailWritePage){
                            case 'write_mail':
                                $('#mail-modal-detail').addClass('hidden').siblings('#'+mailWritePage).removeClass('hidden');
                                $('#'+mailWritePage).find('.mail-subject .mail-number').focus();
                                break;
                            case 'caogao-edit':
                                $('#mail-modal-detail').addClass('hidden').closest('.writeEmailmodal').addClass('hidden').siblings('#'+mailWritePage).removeClass('hidden');
                                $('#'+mailWritePage).find('.mail-subject .mail-number').focus();
                                break;
                            case 'reply-Mail':
                                $('#mail-modal-detail').addClass('hidden').siblings('#'+mailWritePage).removeClass('hidden');
                                $('#'+mailWritePage).find('.mail-subject .mail-number').focus();
                                break;
                            case 'zaici-edit':
                                $('#mail-modal-detail').addClass('hidden').siblings('#'+mailWritePage).removeClass('hidden');
                                $('#'+mailWritePage).find('.mail-subject .mail-number').focus();
                                break;
                            case 'customer-writeMail':
                                $('#mail-modal-detail').addClass('hidden').siblings('#'+mailWritePage).removeClass('hidden');
                                $('#'+mailWritePage).find('.mail-subject .mail-number').focus();
                                break;
                        }
                        break;
                }
            });
            //写邮件首页点击进入写邮件页面
            $('.email-main').on('click','#menue-box>.add',function(){
                vp.registerObj.mailWritePage='write_mail';//正在写邮件的页面
                vp.registerObj.sendMailToPage='menue-box';//发送邮件后显示的页面
                mailToBack='邮件首页';
                $('#menue-box').addClass('hidden').siblings('.writeEmailmodal').removeClass('hidden').children().addClass('hidden')
                    .siblings('#write_mail').removeClass('hidden');
                $('#write_mail .mail-subject .mail-number').focus();
            });
            //点击取消返回模板详情页
            $('#write_mail').on('click','.header span',function(){
                //var $this=$(this);
                //var ClasssName=$this.attr('class').split(' ')[0];
                //switch(ClasssName){
                //    case 'head-left':
                //        switch(mailToBack){
                //            case '模板':
                //                $('#write_mail').addClass('hidden').siblings('#mail-modal-detail').removeClass('hidden');
                //                break;
                //            case '邮件首页':
                //                $('#write_mail').addClass('hidden');
                //                $('#menue-box').removeClass('hidden');
                //                $('.writeEmailmodal').addClass('hidden');
                //                break;
                //        }
                //        break;
                //    case 'head-right':
                //        break;
                //}
            });
            //点击返回邮件首页
            $('#emailTempIndex').on('click','.icon',function(){
                //邮件模板出入口
                var comeTo=vp.registerObj.mailMOdalBoxto;
                if(comeTo=='#menue-box'){
                    $('#emailTempIndex').addClass('hidden').closest('.writeEmailmodal').addClass('hidden').siblings('#menue-box').removeClass('hidden')
                }else if(comeTo=='#caogao-edit'){
                    $('#emailTempIndex').addClass('hidden').closest('.writeEmailmodal').siblings(comeTo).removeClass('hidden')
                }else{
                    $('#emailTempIndex').addClass('hidden').siblings(comeTo).removeClass('hidden');
                }
            });
            //进入选择客户或选择联系人
            $('.writeEmailPage').on('click','#slect-Box0 .content .list',function(e){
                $('#slect-Box0').remove();
                $('#person-box1 .list-content .list').remove();
                $('#personal-seleact1 .list-content .list').remove();
                var $this=$(this);
                var ClassName=$this.attr('class').split(' ')[1];
                var writePage=vp.registerObj.mailWritePage;
                $('#'+writePage).addClass('hidden');
                kehuUpload();
                switch(ClassName){
                    case 'li0':
                        if(writePage=='write_mail'){
                            $('#personal-seleact1').removeClass('hidden');
                        }else{
                            $('#'+writePage).siblings('.writeEmailmodal').removeClass('hidden').children()
                                .addClass('hidden').siblings('#personal-seleact1').removeClass('hidden');
                        }
                        break;
                    case 'li1':
                        if(writePage=='write_mail'){
                            $('#person-box1').removeClass('hidden');
                        }else{
                            $('#'+writePage).siblings('.writeEmailmodal').removeClass('hidden').children()
                                .addClass('hidden').siblings('#person-box1').removeClass('hidden');
                        }
                        break;
                }
            });
            //加载更多客户发邮件
            $('#personal-seleact1').on('click','.content .more',function(){
                var len=$('#personal-seleact1 .list-content .list').length+1;
                var pageno=parseInt(len/10)+1;
                getMorekehu(len,pageno)
            });
            $('#person-box1').on('click','.content .more',function(){
                $('#personal-seleact1 .content .more').click();
            });
            //点击进入选择联系人详情
            $('#person-box1').on('click','.content .list-content .list',function(){
                vp.registerObj.getMailActiveCount=1;
                var $this=$(this);
                var domain=$this.find('.website').html();
                var pkid=$this.attr('data-id');
                var title=$this.find('.title .text').html();
                $('#personal-ren-list-de .active').removeClass('active');
                $('#person-box1').addClass('hidden').siblings('#personal-ren-list-de').removeClass('hidden');
                $('#personal-ren-list-de .top .text-center .text').html(title)
                getSendMailDetail2(domain,pkid);
                $('#personal-ren-list-de .one-send-box .more').attr('data-id',pkid);
            });
            //点击加载更多有效联系人
            $('#personal-ren-list-de').on('click','.one-send-box .more',function(){
                var $this=$(this);
                var pkId=$this.attr('data-id');
                var len=parseInt($('#personal-ren-list-de .one-send-box .list').length)+1;
                var pageNo=parseInt(len/10)+1;
                getMoreActiveList(len,pkId,pageNo);
            });
            //点击返回选择列表
            $('#personal-ren-list-de').on('click','.top .icon',function(){
                $('#personal-ren-list-de').addClass('hidden').siblings('#person-box1').removeClass('hidden');
            });
        //取消选择客户或联系人返回写邮件页面
        $('#person-box1').on('click','.top .canselbtn',function(){
            var writePage=vp.registerObj.mailWritePage;
            $('#person-box1').addClass('hidden');
            $('#'+writePage).removeClass('hidden');
        });
        //取消选择客户或联系人返回写邮件页面
        $('#personal-seleact1').on('click','.top .canselbtn',function(){
            var writePage=vp.registerObj.mailWritePage;
            $('#personal-seleact1').addClass('hidden');
            $('#'+writePage).removeClass('hidden');
        });
        //选择收件客户（公司）
        $('#personal-seleact1').on('click','.content .list-content .list',function(){
            console.log(1234567894561125412254252841254128);
            var $this=$(this);
            var len=$('#personal-seleact1 .content .list-content .active').length;
            if(len>0){
                if(len<10){
                    $this.toggleClass('active');
                    len=$('#personal-seleact1 .content .list-content .active').length;
                    if(len>0){
                        $('#personal-seleact1 .top .send-all').addClass('active');
                    }else{
                        $('#personal-seleact1 .top .send-all').removeClass('active');
                    }
                }else{
                    if($this.hasClass('active')){
                        $this.removeClass('active');
                    }else{
                        vp.ui.pop('icon-err','最多选10个')
                    }
                }
            }else{
                $this.toggleClass('active');
                $('#personal-seleact1 .top .send-all').addClass('active');
            }
        });
        //选择联系人群发邮件
        //选择单一客户后选择联系人发邮件
        $('#personal-ren-list-de').on('click','.content .lianxiren-houxuan',function(){
            console.log(1234567894561125412254252841254128)
            var $this=$(this);
            var dataType=$this.attr('data-type');
            var num=$('#personal-ren-list-de .wait .text .num').html();
            switch(dataType){
                case '1':
                    //所有联系人
                    $this.toggleClass('active').siblings('.active').removeClass('active');
                    $('#personal-ren-list-de .content .ren-box .one-send-box .active').removeClass('active');
                    break;
                case '2':
                    //待开发联系人
                    if($this.hasClass('active')){}else{
                        if(num=='0'){
                            vp.ui.pop('icon-err','联系人不能为空');
                            return false;
                        }
                    }
                    $this.toggleClass('active').siblings('.active').removeClass('active');
                    $('#personal-ren-list-de .content .ren-box .one-send-box .active').removeClass('active');
                    break;
                case '3':
                    $this.toggleClass('active');
                    $('#personal-ren-list-de .content .ren-box .all-nav .active').removeClass('active');
                    //有效联系人
                    break;
            }
        });
        //点击群发邮件一个公司内的人
        $('#personal-ren-list-de').on('click','.top .rt',function(){
            var pkid= $('#personal-ren-list-de .one-send-box .more').attr('data-id');
            var writepageNow=vp.registerObj.mailWritePage;
            var str='';
            var text='';
            var len='';
            $('#'+writepageNow).attr('data-sendPkId',pkid);
            if($('#personal-ren-list-de .lianxiren-houxuan[data-type="1"]').hasClass('active')){
                $('#personal-ren-list-de').addClass('hidden');
                text=$('#personal-ren-list-de .top .text-center .text').html();
                len=text.length;
                if(len>20){
                    text= $.trim(text.slice(0,20))+'...'
                }
                str='<li>'+text+'</li>';
                $('#'+writepageNow).removeClass('hidden').find('ul.names').html(str);
                $('#'+writepageNow).attr('data-send','allLianxiren');
            }else  if($('#personal-ren-list-de .lianxiren-houxuan[data-type="2"]').hasClass('active')){
                $('#personal-ren-list-de').addClass('hidden');
                text=$('#personal-ren-list-de .top .text-center .text').html();
                len=text.length;
                if(len>20){
                    text= $.trim(text.slice(0,20))+'...'
                }
                str='<li>'+text+"(待开发联系人)"+'</li>';
                $('#'+writepageNow).removeClass('hidden').find('ul.names').html(str);
                $('#'+writepageNow).attr('data-send','daikaifa');
            }else  if($('#personal-ren-list-de .lianxiren-houxuan[data-type="3"]').hasClass('active')){
                $('#personal-ren-list-de').addClass('hidden');
                $('#'+writepageNow).removeClass('hidden');
                $('#'+writepageNow).attr('data-send','youxiao');
                var arrActive=$('#personal-ren-list-de .one-send-box .list.active');
                var alen=arrActive.length;
                var str='';
                var arr=[];
                for(var i=0;i<alen;i++){
                    var name=$(arrActive[i]).find('.text').html();
                    var dataID=$(arrActive[i]).attr('data-pkid');
                    arr.push(dataID);
                    str+='<li>'+name+'</li>';
                }
                if(writepageNow=='write_mail'){

                }else{
                    $('.writeEmailmodal').addClass('hidden').children().addClass('hidden');
                }
                arr=arr.join(',');
                $('#'+writepageNow).attr('data-youxiaoid',arr);
                $('#'+writepageNow).find('ul.names').html(str);
            }
        });
            //选择公司确定
        $('#personal-seleact1').on('click','.top .send-all.active',function(){
                var $this=$(this);
                var seandAllCompany=[];
                var writePage=vp.registerObj.mailWritePage;
                var activeLIst=$('#personal-seleact1 .list.active');
                var len=activeLIst.length;
                var ul= $('#'+writePage).find('.Addressee-theme .Addressee ul.names');
                window.qunSendArr=[];
                $this.removeClass('active');
                ul.empty();
                for(var i=0;i<len;i++){
                    var website=$(activeLIst[i]).find('.website').html();
                    var index=website.indexOf('.');
                    var pkid=$(activeLIst[i]).attr('data-id');
                    window.qunSendArr[i]=pkid;
                    website=website.slice(0,index);
                    seandAllCompany.push(website);
                    ul.append('<li>'+website+'</li>');
                    console.log(website)
                    console.log(qunSendArr);
                }
                window.qunSendArr=qunSendArr.join(',');
                $('#personal-seleact1').addClass('hidden');
                $('#'+writePage).removeClass('hidden').attr('data-send','Allcompany');
                activeLIst.removeClass('active');
                $('#personal-seleact1 .top .active').removeClass('hidden');
            });
            //点击报价发邮件
            $('#quotation-modify').on('click','#Mask-pop .save-quotation .send-email',function(){
                var $this=$(this);
                var boxId=$this.closest('.writemailbox').find('.indexClass').attr('id');
                $('#quotation-modify').addClass('hidden');
                $('#Mask-pop').hide().find('.save-quotation').hide();
                $('.writeEmailmodal').removeClass('hidden');
                $('#write_mail').removeClass('hidden');
                vp.registerObj.mailMOdalBoxto='#write_mail';//邮件模板页退出指向页面
                vp.registerObj.mailWritePage='write_mail';//正在写邮件的页面
                if(boxId=='product_module'){
                    vp.registerObj.sendMailToPage='product_module';//发送邮件后显示的页面
                }else{
                    vp.registerObj.sendMailToPage='Bill';//发送邮件后显示的页面
                }
            });
            //说明
            $('#personal-ren-list-de').on('click','.ren-box .shuoming',function(){
                //$('#personal-ren-list-de').addClass('hidden');
                $('#email-explain').removeClass('hidden');
            });
            //退出说明
            $('#email-explain').on('click','.header .icon',function(){
                $('#email-explain').addClass('hidden');
                //$('#personal-ren-list-de').removeClass('hidden');
            });
            //关闭选择联系人或客户发邮件
            $(document).on('click','#slect-Box0',function(){
                $('#slect-Box0').remove();
            });
            $(document).on('click','#slect-Box0 .content',function(){
                return false;
            })
        },500);
        //加载私海客户
        function kehuUpload(){
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Sea/GetPrivateSeaList",
                debug:false,
                loadFlag:true
            };
            var data = {pageNo:1,pageSize:10};
            vp.core.ajax(url,data,function(e){
                var str='';
                var msg='';
                if(e.IsSuccess){
                    if(e.Data.Data==null||e.Data.Data==''||e.Data.Data==undefined||e.Data.Data==[]){
                        //为空
                        vp.ui.pop('icon-err','数据为空')
                    }else{
                        var data= e.Data.Data;
                        for(var i=0;i<data.length;i++){
                            str+='<div class="list" data-id="'+data[i].PkId+'">';
                            str+=   '<div class="iconleft-box">';
                            str+=       '<span class="iconleft">'+ $.trim(data[i].Title).toUpperCase()[0]+'</span>';
                            str+=   '</div>';
                            str+=   '<h3 class="title"><span class="text">'+$.trim(data[i].Title)+'</span></h3> ';
                            str+=   '<p class="content">'+$.trim(data[i].Descripte)+'</p>';
                            str+=   '<span class="icon-to-right icon"></span>';
                            str+=   '<span style="display:none" class="website">'+$.trim(data[i].Domain)+'</span>';
                            str+='</div>';
                        }
                        for(var i=0;i<data.length;i++){
                            msg+='<div class="list" data-id="'+data[i].PkId+'">';
                            msg+=   '<div class="iconleft-box">';
                            msg+=       '<span class="iconleft icon-right-null"></span>';
                            msg+=   '</div>';
                            msg+=   '<h3 class="title"><span class="text">'+$.trim(data[i].Title)+'</span></h3> ';
                            msg+=   '<p class="content">'+$.trim(data[i].Descripte)+'</p>';
                            //msg+=   '<span class="icon-to-right icon"></span>';
                            msg+=   '<span style="display:none" class="website">'+$.trim(data[i].Domain)+'</span>';
                            msg+='</div>';
                        }
                        $('#person-box1 .content .list-content').html(str);
                        $('#personal-seleact1 .content .list-content').html(msg);
                    }
                }else{

                }
            },function(e){
                vp.ui.pop('icon-err','操作失败')
            },function(){

            });


            // console.log( vp.registerObj.customerTest);
        }
        //加载更多私海客户
        function getMorekehu(len,pageno){
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Sea/GetPrivateSeaList",
                debug:false,
                loadFlag:true
            };
            var data = {pageNo:len,pageSize:1};
            vp.core.ajax(url,data,function(e){
                if(e.IsSuccess){
                    if(e.Data.Data==null||e.Data.Data==''||e.Data.Data==undefined||e.Data.Data==[]){
                        //为空
                        vp.ui.pop('icon-err','没有更多了');
                        return false;
                    }else{
                        var url = {
                            local:"data/testDemo/data.json",
                            type:"get",
                            server:"/v1/Sea/GetPrivateSeaList",
                            debug:false,
                            loadFlag:true
                        };
                        var data = {pageNo:pageno,pageSize:10};
                        vp.core.ajax(url,data,function(e){
                            var str='';
                            var msg='';
                            if(e.IsSuccess){
                                if(e.Data.Data==null||e.Data.Data==''||e.Data.Data==undefined||e.Data.Data==[]){
                                    //为空

                                }else{
                                    var data= e.Data.Data;
                                    for(var i=0;i<data.length;i++){
                                        str+='<div class="list" data-id="'+data[i].PkId+'">';
                                        str+=   '<div class="iconleft-box">';
                                        str+=       '<span class="iconleft">'+ $.trim(data[i].Title).toUpperCase()[0]+'</span>';
                                        str+=   '</div>';
                                        str+=   '<h3 class="title"><span class="text">'+$.trim(data[i].Title)+'</span></h3> ';
                                        str+=   '<p class="content">'+$.trim(data[i].Descripte)+'</p>';
                                        str+=   '<span class="icon-to-right icon"></span>';
                                        str+=   '<span style="display:none" class="website">'+$.trim(data[i].Domain)+'</span>';
                                        str+='</div>';
                                    }
                                    for(var i=0;i<data.length;i++){
                                        msg+='<div class="list" data-id="'+data[i].PkId+'">';
                                        msg+=   '<div class="iconleft-box">';
                                        msg+=       '<span class="iconleft icon-right-null"></span>';
                                        msg+=   '</div>';
                                        msg+=   '<h3 class="title"><span class="text">'+$.trim(data[i].Title)+'</span></h3> ';
                                        msg+=   '<p class="content">'+$.trim(data[i].Descripte)+'</p>';
                                        msg+=   '<span class="icon-to-right icon"></span>';
                                        msg+=   '<span style="display:none" class="website">'+$.trim(data[i].Domain)+'</span>';
                                        msg+='</div>';
                                    }
                                    $('#person-box1 .content .list-content').append(str);
                                    $('#personal-seleact1 .content .list-content').append(msg);
                                }
                            }else{

                            }
                        },function(e){
                            vp.ui.pop('icon-err','操作失败')
                        },function(){

                        });
                    }
                }else{

                }
            },function(e){
                vp.ui.pop('icon-err','操作失败')
            },function(){

            });
        }
        //群发发邮件
        function sendEmail(pkid,SendType,fujiannum,fujianARR,AfterSendTo,esubect,ebody){
            var url = {
                local:"data/testDemo/data.json",
                type:"post",
                server:"/v1/UserEmail/SendMessageGroup",
                debug:false,
                loadFlag:true
            };
            var data={
                "SeaInnerPkIds": pkid,
                "Subject": esubect,
                "Body": ebody,
                "SendType": SendType,
                "Attachments": fujianARR
            }
            if(fujiannum<1){
                data={
                    SeaInnerPkIds: pkid,
                    Subject: esubect,
                    Body:ebody,
                    SendType: SendType//SendType (integer): 发送类型(0=群发公司; 1=群发单个公司所有(单个公司存seainnerpkids) 2=群发单个公司未联系的(单个公司存seainnerpkids))(必填)
                }
            }

            vp.core.ajax(url,data,function(e){
                vp.ui.pop('icon-right','操作成功');
                var writePage=vp.registerObj.mailWritePage;
                $('#'+writePage).find('textarea').val('');
                $('#'+writePage).find('ul.names').empty();
                $('#'+writePage).find('.mail-picture').empty();
                $('#'+writePage).find('.footer input').val('');
                $('#'+writePage).find('.quotation-box').empty();
                $('#'+writePage).addClass('hidden');
                $('.writeEmailmodal').addClass('hidden');
                if(AfterSendTo=='menue-box'){
                    $('.writeEmailPage').addClass('hidden').closest('.writeEmailmodal').addClass('hidden');
                    $('#menue-box').removeClass('hidden');
                }else{
                    $('#'+AfterSendTo).removeClass('hidden')
                }
            },function(e){
                vp.ui.pop('icon-err','操作失败')
            },function(){

            });
        }
        //发送邮件
        function sendOneEmail(aPKID,subject,contentText,fujiannum,fujian,AfterSendTo){
            var url = {
                local:"data/testDemo/data.json",
                type:"post",
                server:"/v1/UserEmail/SendMessage",
                debug:false,
                loadFlag:true
            };
            var data={
                "ContactPkIds": aPKID,//收件人ID集合
                "ESubject": subject,
                "EBody": contentText,
                "AttachmentCount": fujiannum,//附件数量
                "Attachments": fujian//[{"PkId": fujianID, "OssPath":oss}]
            }
            if(fujiannum==0){
                data={
                    "ContactPkIds": aPKID,//收件人ID集合
                    "ESubject": subject,
                    "EBody": contentText,
                    "AttachmentCount": fujiannum//附件数量
                }
            }

            vp.core.ajax(url,data,function(e){
                vp.ui.pop('icon-right','操作成功');
                var writePage=vp.registerObj.mailWritePage;
                $('#'+writePage).find('textarea').val('');
                $('#'+writePage).find('ul.names').empty();
                $('#'+writePage).find('.mail-picture').empty();
                $('#'+writePage).find('.footer input').val('');
                $('#'+writePage).find('.quotation-box').empty();
                $('#'+writePage).addClass('hidden');
                $('.writeEmailmodal').addClass('hidden');
                if(AfterSendTo=='menue-box'){
                    $('.writeEmailPage').addClass('hidden').closest('.writeEmailmodal').addClass('hidden');
                    $('#menue-box').removeClass('hidden');
                }else{
                    $('#'+AfterSendTo).removeClass('hidden')
                }
            },function(e){
                vp.ui.pop('icon-err','操作失败')
            },function(){

            });
        }
        //发邮件选择联系人进入详情页
        function getSendMailDetail2(domain,pkid){
            var pkId=pkid;
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Sea/GetSeaDetailByDomain",
                debug:false,
                loadFlag:true
            };

            var data={
                domain:domain,
                seaType:1
            };

            vp.core.ajax(url,data,function(e){
                if(e.IsSuccess){
                    var a=parseInt(e.Data.ContactsNumber);
                    var num=returnNumBerType(a);
                    var youxiaonum=parseInt(e.Data.FollowUpRecord.ActiveNunber);
                    var waiNUm=returnNumBerType(a-youxiaonum);
                    $('#personal-ren-list-de .ren-box .all-nav .all-customer .num').html(num);
                    $('#personal-ren-list-de .ren-box .all-nav .wait .num').html(waiNUm);
                    $('#personal-ren-list-de .ren-box .one-send-box .title .num').html(youxiaonum);
                    if(a==0){
                        $('#personal-ren-list-de .ren-box').addClass('hidden').siblings('.no-count').removeClass('hidden');
                    }else{
                        $('#personal-ren-list-de .ren-box').removeClass('hidden').siblings('.no-count').addClass('hidden');
                        var url = {
                            local:"data/testDemo/data.json",
                            type:"get",
                            server:"/v1/Sea/GetActiveContacts",
                            debug:false,
                            loadFlag:true
                        };

                        var data={
                            pageNo:1,
                            pageSize:10,
                            seaPkId:pkId
                        };

                        vp.core.ajax(url,data,function(e){
                            if(e.IsSuccess){
                                $('#personal-ren-list-de .content .one-send-box .lianxiren-houxuan').remove();
                                if(e.Data.Data==null||e.Data.Data==''||e.Data.Data==undefined||e.Data.Data==[]){
                                    //为空
                                    $('#personal-ren-list-de .content .one-send-box .more').addClass('hidden')
                                }else{
                                    var data= e.Data.Data;
                                    var str='';
                                    for(var i=0;i<data.length;i++){
                                        str+='<div data-type="3" class="list lianxiren-houxuan" data-pkid="'+data[i].PkId+'" data-email="'+data[i].Email+'">';
                                        str+=   '<span class="icon-right-null lf iconlft"></span>';
                                        str+=   '<span class="text">'+data[i].ContactsName+'</span>';
                                        str+=   '<span class="box rt">';
                                        str+=       '<span class="time">'+data[i].LastContactsDate+'</span>';
                                        str+=       '<span class="icon-to-right icon hidden"></span>';
                                        str+=   '</span>';
                                        str+='</div>';
                                    }
                                    $('#personal-ren-list-de .content .one-send-box .more').removeClass('hidden');
                                    $('#personal-ren-list-de .content .one-send-box .more').before(str);
                                }
                            }else{

                            }
                        },function(e){
                            vp.ui.pop('icon-err','操作失败')
                        },function(){

                        });
                    }
                }
            },function(e){
                vp.ui.pop('icon-err','操作失败')
            },function(){

            });
        }
        function returnNumBerType(v){
            if(v==0){
                return 0
            }else if(v<10){
                return v;
            }else{
                v=parseInt(v/10);
                return v+'0+';
            }
        }
        //加载更多有效联系人
        function getMoreActiveList(len,pkId,pageNo){
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Sea/GetActiveContacts",
                debug:false,
                loadFlag:true
            };

            var data={
                pageNo:len,
                pageSize:1,
                seaPkId:pkId
            };

            vp.core.ajax(url,data,function(e){
                if(e.IsSuccess){
                    if(e.Data.Data==null||e.Data.Data==''||e.Data.Data==undefined||e.Data.Data==[]){
                        //为空
                        $('#personal-ren-list-de .content .one-send-box .more').addClass('hidden');
                        vp.ui.pop('icon-err','没有更多数据了');
                        return false;
                    }else{

                        var url = {
                            local:"data/testDemo/data.json",
                            type:"get",
                            server:"/v1/Sea/GetActiveContacts",
                            debug:false,
                            loadFlag:true
                        };

                        var data={
                            pageNo:pageNo,
                            pageSize:10,
                            seaPkId:pkId
                        };

                        vp.core.ajax(url,data,function(e){
                            if(e.IsSuccess){
                                if(e.Data.Data==null||e.Data.Data==''||e.Data.Data==undefined||e.Data.Data==[]){
                                    //为空

                                }else{
                                    var data= e.Data.Data;
                                    var str='';
                                    for(var i=0;i<data.length;i++){
                                        str+='<div data-type="3" class="list lianxiren-houxuan" data-pkid="'+data[i].PkId+'" data-email="'+data[i].Email+'">';
                                        str+=   '<span class="icon-right-nul lf iconlft"></span>';
                                        str+=   '<span class="text">'+data[i].ContactsName+'</span>';
                                        str+=   '<span class="box rt">';
                                        str+=       '<span class="time">'+data[i].LastContactsDate+'</span>';
                                        str+=       '<span class="icon-to-right icon hidden"></span>';
                                        str+=   '</span>';
                                        str+='</div>';
                                    }
                                    $('#personal-ren-list-de .content .one-send-box .more').before(str);
                                }
                            }else{

                            }
                        },function(e){
                            vp.ui.pop('icon-err','操作失败')
                        },function(){

                        });


                    }
                }else{

                }
            },function(e){
                vp.ui.pop('icon-err','操作失败')
            },function(){

            });
        }

        //获取模板分类
        function GETMailModalBox(){
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/UserEmail/GetTemplateClass",
                debug:false,
                loadFlag:true
            };
            var data = {};
            vp.core.ajax(url,data,function(e){
                //成功
                var data= e.Data;
                var str='';
                for(var i=0;i<data.length;i++){
                    var Type=data[i].PkId;
                    switch(Type){
                        case 1:
                            str+='<li class="li0" data-id="'+data[i].PkId+'">';
                            str+=    '<span class="icon-nav iconl"></span>';
                            str+=    '<b>开发信</b>';
                            str+=    '<span class="icon-to-right"></span>';
                            str+='</li>';
                            break;
                        case 2:
                            str+='<li class="li1" data-id="'+data[i].PkId+'">';
                            str+=    '<span class="icon-rmb-cir iconl"></span>';
                            str+=    '<b>价格类</b>';
                            str+=    '<span class="icon-to-right"></span>';
                            str+='</li>';
                            break;
                        case 3:
                            str+='<li class="li2" data-id="'+data[i].PkId+'">';
                            str+=    '<span class="icon-zhuanshi iconl"></span>';
                            str+=    '<b>打样类</b>';
                            str+=    '<span class="icon-to-right"></span>';
                            str+='</li>';
                            break;
                        case 4:
                            str+='<li class="li3" data-id="'+data[i].PkId+'">';
                            str+=    '<span class="icon-write-cir iconl"></span>';
                            str+=    '<b>跟进类</b>';
                            str+=    '<span class="icon-to-right"></span>';
                            str+='</li>';
                            break;
                        case 5:
                            str+='<li class="li5" data-id="'+data[i].PkId+'">';
                            str+=    '<span class="icon-maoduo11 iconl"></span>';
                            str+=    '<b>售后类</b>';
                            str+=    '<span class="icon-to-right"></span>';
                            str+='</li>';
                            break;
                        case 6:
                            str+='<li class="li4" data-id="'+data[i].PkId+'">';
                            str+=    '<span class="icon-message-in iconl"></span>';
                            str+=    '<b>邀请类</b>';
                            str+=    '<span class="icon-to-right"></span>';
                            str+='</li>';
                            break;
                        case 7:
                            str+='<li class="li6" data-id="'+data[i].PkId+'">';
                            str+=    '<span class="icon-qiqiu iconl"></span>';
                            str+=    '<b>节日类</b>';
                            str+=    '<span class="icon-to-right"></span>';
                            str+='</li>';
                            break;
                    }
                }
                $('#emailTempIndex ul').html(str);
            },function(e){
                console.log(e);
            },function(){

            });
        }
        //获取模板列表
        function getModalList(Pkid,type){
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/UserEmail/GetTemplates",
                debug:false,
                loadFlag:true
            };

            var data={
                classpkid:Pkid,
                pageindex:1,
                pagesize:30
            };

            vp.core.ajax(url,data,function(e){
                var data= e.Data.Data;
                if(data==null||data==undefined||data==''||data==[]){
                    vp.ui.pop('icon-err','数据为空');
                    $('div[data-type="modaljihe"] ul.singleTempUl').html('');
                }else{
                    var str='';
                    for(var i=0;i<data.length;i++){
                        //var ClassName=data[i].className;
                        str+='<li>';
                        switch(type){
                            case 'li0':
                                str+=   '<span class="icon-nav iconl" style="color: #5094e2;"></span>';
                                break;
                            case 'li1':
                                str+=   '<span class="icon-rmb-cir iconl" style="color: #F7A857;"></span>';
                                break;
                            case 'li2':
                                str+=   '<span class="icon-zhuanshi iconl" style="color: #F7A857;"></span>';
                                break;
                            case 'li3':
                                str+=   '<span class="icon-write-cir iconl" style="color: #E27650;"></span>';
                                break;
                            case 'li4':
                                str+=   '<span class="icon-maoduo11 iconl" style="color: #5094E2;"></span>';
                                break;
                            case 'li5':
                                str+=   '<span class="icon-message-in iconl" style="color: #5094e2;"></span>';
                                break;
                            case 'li6':
                                str+=   '<span class="icon-qiqiu iconl" style="color: #7767E0;"></span>';
                                break;
                        }
                        str+=   '<h4 class="title-name">'+data[i].Subject+'</h4>';
                        str+=   '<b>'+data[i].Body+'</b>';
                        str+=   '<span class="icon-to-right"></span>';
                        str+='</li>';
                    }
                    $('div[data-type="modaljihe"] ul.singleTempUl').html(str)
                }
            },function(e){
                vp.ui.pop('icon-err','操作失败')
            },function(){

            });
        }

        /****************************写邮件上传图片**********************************************************************/
        function WritemailUpImg(){
            $('.writeEmailPage').on('click','.picture-box .icon',function(){
                $(this).closest('.picture-box').remove();
                $('.writeEmailPage .footer .caogaobtn').val('');
            });
            $('.writeEmailPage .footer').on('change','.caogaobtn',function(){
                vp.ui.uploadPictures($(this)[0],$('.picture-box'),showPreview);
                //showPreview(this);
            });
            function showPreview(source){
            console.log(source);
                var writePage=vp.registerObj.mailWritePage;
                var len= $('#'+writePage).find('.mail-picture').find('img').length;
                var Filename=$('#'+writePage).find('input').val();
                if(len<15){
                    //只允许上传图片
                    var str='';
                    str+='<div class="picture-box" data-type="'+Filename+'">';
                    str+=   '<img src="'+source.base64+'">';
                    str+=   '<span class="icon" data-type="">×</span>';
                    str+='</div>';
                    $('#'+writePage).find('.mail-picture').append(str);
                    uploadFujian(Filename,source.base64,writePage);
                }
            }
        }
        WritemailUpImg();
        //上传报价单附件
        function uploadBaoJia(pkid){
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/UserEmail/UploadQuotationAttachMent",
                debug:false,
                loadFlag:true
            };
            var data = {CurrentCurrency:1,OrderQuotationPkId:pkid};
            vp.core.ajax(url,data,function(e){
                //成功
                var data= e.Data;
                var writePage=vp.registerObj.mailWritePage;
                var str='';
                var count=fuJianChange(data.AttachmentContentLength);
                str+='<div class="quotations" data-id="'+data.PkId +'">';
                str+=   '<i class="icon-price-dan icon0"></i>';
                str+=   '<span class="title">';
                str+=       '<h2 class="names">'+data.AttachmentPath +'</h2>';
                str+=       '<p class="sizes">'+count+'</p>';
                str+=   '</span>';
                str+=   '<span class="icon-mistake icon1"></span>';
                str+='</div>';
                $('#'+writePage).find('.mail-subject .quotation-box').append(str);
            },function(e){
                vp.ui.pop('icon-err','操作失败')
            },function(){
                vp.ui.pop('icon-err','操作失败')
            });
        }
        //上传图片
        function uploadFujian(FileName,FileStr,writePage){
            var url = {
                local:"data/testDemo/data.json",
                type:"post",
                server:"/v1/UserEmail/UploadAttachMentStr",
                debug:false,
                loadFlag:true
            };
            var data = {FileName:FileName,FileStr:FileStr};
            vp.core.ajax(url,data,function(e){
                //成功
                var imgArr=$('#'+writePage).find('.picture-box');
                var len=imgArr.length;
                if(e.IsSuccess){
                    $(imgArr[len-1]).attr('data-id',e.Data.PkId).attr('data-oss',e.Data.AttachmentOss);
                }else{
                    $(imgArr[len-1]).remove();
                    $('.writeEmailPage input').val('');
                }
            },function(e){
                var imgArr=$('#'+writePage).find('.picture-box');
                var len=imgArr.length;
                vp.ui.pop('icon-err','上传失败');
                $(imgArr[len-1]).remove();
                $('.writeEmailPage input').val('');
            },function(){
                var imgArr=$('#'+writePage).find('.picture-box');
                var len=imgArr.length;
                vp.ui.pop('icon-err','上传失败');
                $(imgArr[len-1]).remove();
                $('.writeEmailPage input').val('');
            });
        }
        /****************************写邮件上传图片**********************************************************************/

        /********************************写邮件选择客户或联系人*******************************************/
        function getCustomerSelect(){
            $('#qw1').on('click','.quotation-title .active',function(){
                var writePage=vp.registerObj.mailWritePage;
                var pkid=quotationPkidList;
                $('#qw1').addClass('hidden');
                //console.log(writePage)
                switch (writePage){
                    case 'write_mail':
                        $('#write_mail').removeClass('hidden');
                        break;
                    case 'zaici-edit':
                        $('#zaici-edit').removeClass('hidden');
                        break;
                    case 'reply-Mail':
                        $('#reply-Mail').removeClass('hidden');
                        break;
                    case 'caogao-edit':
                        $('.writeEmailmodal').addClass('hidden');
                        $('#caogao-edit').removeClass('hidden');
                        break;
                    case 'customer-writeMail':
                        $('#customer-writeMail').removeClass('hidden');
                        break;
                }
                $('#qw1 .quotation-list-item').empty();
                uploadBaoJia(pkid)
            });
            //报价单返回
            $('#qw1').on('click','.quotation-title .back-border',function(){
                var writePage=vp.registerObj.mailWritePage;
                /*
                $('#qw1').addClass('hidden');
                $('.writeEmailPage ul.names').empty();
                $('.writeEmailPage textarea').val('');
                $('.writeEmailPage .mail-picture').empty();
                $('.writeEmailPage .footer input').val('');
                $('#qw1 .quotation-list-item').empty();
                */
                switch (writePage){
                    case 'write_mail':
                        $('#write_mail').removeClass('hidden');
                        $('#qw1').addClass('hidden');
                        break;
                    case 'caogao-edit':
                        $('#caogao-edit').removeClass('hidden');
                        $('.writeEmailmodal').addClass('hidden');
                        $('#qw1').addClass('hidden');
                        break;
                    case 'reply-Mail':
                        $('#reply-Mail').removeClass('hidden');
                        $('#qw1').addClass('hidden');
                        break;
                    case 'zaici-edit':
                        $('#zaici-edit').removeClass('hidden');
                        $('#qw1').addClass('hidden');
                        break;
                    case 'customer-writeMail':
                        $('#customer-writeMail').removeClass('hidden');
                        $('#qw1').addClass('hidden');
                        break;
                }
            });
            //选择报价单
            $('#qw1').on('click','.quotation-body-border .quotation-list-item .quotation-item',function(){
                $(this).find('.item-head .pic-border .icon-right-null').toggleClass('selected');
                $(this).siblings().find('.item-head .pic-border .icon-right-null').removeClass('selected');
                if($(this).find('.item-head .pic-border .icon-right-null').hasClass('selected')){
                    window.quotationPkidList=($(this).attr('data-pkid'));
                    $('.edite-border').addClass('active')
                }
                else{
                    window.quotationPkidList='';
                    $('.edite-border').removeClass('active')
                }
            });
            $('#qw1').on('click','.See-more',function(){
                var len=$('#qw1 .quotation-list-item .quotation-item').length;
                var pageNo='';
                if(len%10==0){
                    pageNo=parseInt(len/10)+1;
                }else{
                    pageNo=parseInt(len/10)+2;
                }
                getBaoJia(pageNo)
            });

            $('.writeEmailPage').on('click','.footer .footer-right .icondan',function(e){
                var $this=$(this);
                var writePage=vp.registerObj.mailWritePage;
                var len=$('#'+writePage).find('.quotation-box .quotations').length;
                if(len>=10){
                    return false
                }
                if(writePage=='write_mail'){
                    $this.closest('.writeEmailPage').addClass('hidden').siblings('#qw1').removeClass('hidden');
                }else if(writePage=='zaici-edit'){
                    $this.closest('.writeEmailPage').addClass('hidden').siblings('#qw1').removeClass('hidden');
                }else if(writePage=='reply-Mail'){
                    $this.closest('.writeEmailPage').addClass('hidden').siblings('#qw1').removeClass('hidden');
                }else if(writePage=='customer-writeMail'){
                    $this.closest('.writeEmailPage').addClass('hidden').siblings('#qw1').removeClass('hidden');
                }else{
                    $this.closest('.writeEmailPage').addClass('hidden').siblings('.writeEmailmodal').removeClass('hidden').find('#qw1').removeClass('hidden');
                }
                getBaoJia(1);
            });

            $('.writeEmailPage').on('click','.Addressee .icon',function(){
                var writePage=vp.registerObj.mailWritePage;
                vp.ui.selectCustomer(writePage);
            });
            //发邮件
            $('.writeEmailPage').on('click','.header .head-right',function(){
                var writePage=vp.registerObj.mailWritePage;
                var AfterSendTo=vp.registerObj.sendMailToPage;
                var pkid=qunSendArr;//群发PKID
                var type=$('#'+writePage).attr('data-send');//发送类型 youxiao==>有效联系 allLianxiren==>全部联系 daikaifa==>待开发 Allcompany==》公司
                var pePkid=$('#'+writePage).attr('data-sendPkId');//公司PKID
                var valueSbject= $.trim($('#'+writePage).find('.Theme textarea').val());//发件标题
                var valueBody= $.trim($('#'+writePage).find('.mail-subject textarea').val());//发件内容
                var ulName=$('#'+writePage).find('ul.names li').length;//前端验证收件人是否为空
                var PKID=$('#'+writePage).attr('data-youxiaoid');//效联系人id集合
                var imgarr=$('#'+writePage).find('.picture-box');//图片数量
                var Imglen=imgarr.length;//图片数量
                var baojia=$('#'+writePage).find('.quotation-box .quotations');
                var BaojiaLen=baojia.length;
                var len=BaojiaLen+Imglen;
                var imgARR=[];
                var baojiaArr=[];
                var fuJIanI=[];//附件参数
                for(var i=0;i<Imglen;i++){
                    var fujianO={};
                    fujianO.PkId=$(imgarr[i]).attr('data-id');
                    fujianO.OssPath=$(imgarr[i]).attr('data-oss');
                    imgARR.push(fujianO);
                }
                for(var j=0;j<BaojiaLen;j++){
                    var baojiaOBJ={};
                    baojiaOBJ.PkId=$(baojia[j]).attr('data-id');
                    baojiaOBJ.OssPath=$(baojia[j]).find('.icon0').attr('href');
                    baojiaArr.push(baojiaOBJ);
                }
                fuJIanI=imgARR.concat(baojiaArr);
                if(valueSbject==''||valueBody==''){
                    vp.ui.pop('icon-err','主题或内容不能为空');
                    return false;
                }else if(ulName==0){
                    vp.ui.pop('icon-err','收件人不能为空');
                    return false;
                }
                switch(type){
                    case 'allLianxiren':
                        sendEmail(pePkid,1,len,fuJIanI,AfterSendTo,valueSbject,valueBody)//发邮件一公司全部人
                        break;
                    case 'daikaifa':
                        sendEmail(pePkid,2,len,fuJIanI,AfterSendTo,valueSbject,valueBody);//发邮件一公司待开发联系人
                        break;
                    case 'youxiao':
                        sendOneEmail(PKID,valueSbject,valueBody,len,fuJIanI,AfterSendTo);
                        break;
                    case 'Allcompany':
                        sendEmail(pkid,0,len,fuJIanI,AfterSendTo,valueSbject,valueBody)//群发公司
                        break;
                }

            });
            //选择邮件模板
            $('.writeEmailPage').on('click','.Addressee-theme .Theme .icon',function(){
                var $this=$(this);
                var writePage=vp.registerObj.mailWritePage;
                GETMailModalBox();
                switch(writePage){
                    case 'write_mail':
                        vp.registerObj.mailMOdalBoxto='#write_mail';//邮件模板退出指向页面
                        $('#write_mail').addClass('hidden');
                        $('#emailTempIndex').removeClass('hidden');
                        break;
                    case 'caogao-edit':
                        vp.registerObj.mailMOdalBoxto='#caogao-edit';//邮件模板退出指向页面
                        $this.closest('.writeEmailPage').addClass('hidden').siblings('.writeEmailmodal').removeClass('hidden').children().addClass('hidden')
                            .siblings('#emailTempIndex').removeClass('hidden');
                        break;
                    case 'reply-Mail':
                        vp.registerObj.mailMOdalBoxto='#reply-Mail';
                        $('#reply-Mail').addClass('hidden');
                        $('#emailTempIndex').removeClass('hidden');
                        break;
                    case 'zaici-edit':
                        vp.registerObj.mailMOdalBoxto='#zaici-edit';
                        $('#zaici-edit').addClass('hidden');
                        $('#emailTempIndex').removeClass('hidden');
                        break;
                    case 'customer-writeMail':
                        vp.registerObj.mailMOdalBoxto='#customer-writeMail';
                        $('#customer-writeMail').addClass('hidden');
                        $('#emailTempIndex').removeClass('hidden');
                        break;
                }
                //邮件模板入口
            });
            //取消编写邮件返回前面页面
            $('.writeEmailPage').on('click','.header .head-left',function(){
                var canselComePage=vp.registerObj.sendMailToPage;//发送邮件后显示的页面
                var writePage=vp.registerObj.mailWritePage;//写邮件的页面
                $('#'+writePage).addClass('hidden');
                $('#'+writePage).find('ul.names').html('');
                $('#'+writePage).find('textarea').val('');
                $('#'+writePage).find('input').val('');
                $('#'+writePage).find('.mail-picture').empty();
                if (writePage=='caogao-edit'){

                }else{
                    $('.writeEmailmodal').addClass('hidden');
                    $('#'+canselComePage).removeClass('hidden')
                }
            });
            //点击选择订单页面
            //删除报价单
            $('.writeEmailPage').on('click','.quotation-box .quotations .icon1',function(){
                var $this=$(this);
                $this.closest('.quotations').remove();
            });
            //删除图片
            $('.writeEmailPage').on('click','.picture-box .icon',function(){
                var $this=$(this);
                $this.closest('.picture-box').remove();
                $('.writeEmailPage input').val('');
            });
            //点击收起或展开
            $('.writeEmailPage').on('click','.Addressee .clickType',function(){
                var $this=$(this);
                $this.toggleClass('active');
                if($this.hasClass('active')){
                    $this.html('展开');
                    $this.siblings('.names').addClass('active');
                }else{
                    $this.html('收起');
                    $this.siblings('.names').removeClass('active');
                }
            });
            //规则说明
            $('.writeEmailPage').on('click','.footer .footer-left',function(){
                $('#email-explain').removeClass('hidden');
            });
        }
        setTimeout(function(){
            getCustomerSelect();
        },500);
        /********************************写邮件选择客户或联系人*******************************************/
        /********************************写邮件选择报价单*******************************************/

        function getBaoJia(currentpage){
            $.ajax("https://www.mooddo.com/v1/Order/GetOrderQuotations?pageSize="+ 10 +"&pageIndex="+currentpage, {
                type:"post",
                headers:{vp_token:vp.core.token},
                success: function(data) {
                    console.log(data.Code);
                    $('#qw1 .no-count').addClass('hidden');
                    $('#qw1 .quotation-list-item').empty();
                    var pagecount = data.PageCount;
                    var quotationList = data.Data;
                    var $quotation = $('#qw1').find('#example-quotation .quotation-item');
                    //去掉加载动态图
                    $('.load_animation').addClass('hidden');
                    if(quotationList.length>0){
                        $.each(quotationList,function(i,quotation){
                            //PkId
                            var PkId = quotation.PkId;
                            //单据名
                            var quotationname = quotation.QuotationName;
                            //产品名
                            var productname = quotation.ProductName;
                            var ordersale = quotation.UsdOrderTotalSales.toFixed(2);
                            var imgurl = quotation.ProductMainImagePath;
                            var $quotationmodify = $quotation.clone();

                            //数据填充
                            $quotationmodify.attr('data-pkid',PkId);
                            $quotationmodify.find('.item-head .item-name').html(quotationname);
                            $quotationmodify.find('.item-body .pic-border').css('background-image','url('+ imgurl +')');
                            $quotationmodify.find('.item-body .product-introduce .product .product-name').html(productname);
                            $quotationmodify.find('.item-body .product-introduce .price .money-num').html(ordersale);
                            $('#qw1 .quotation-list-item').append($quotationmodify);
                        });
                        $('#qw1').find('.loading_more').removeClass('hidden');
                        if(pagecount==currentpage){
                            $('#qw1').find('.loading_more .No-more').removeClass('hidden');
                            $('#qw1').find('.loading_more .See-more').addClass('hidden');

                        }
                    }else{
                        $('#qw1 .no-count').removeClass('hidden');
                    }
                }
            })
            window.quotationPkidList = '';
            Array.prototype.removearr = function(id){
                var index = this.indexOf(id);
                if(index>-1){
                    this.splice(index,1);
                }
            }
        }
        /********************************写邮件选择报价单*******************************************/
        function fuJianChange(a){
            a=parseInt(a/1024)+1;
            if(a<1024){
                a=a+'K';
            }else{
                a=(a/1024).toFixed(2)+'M';
            }
            return a;
        }
        //写邮件
        autosize(document.querySelectorAll('.mail-number'));
    };
    writeEmail();
//草稿箱
    vp.registerFn.writeEmail = writeEmail;
});