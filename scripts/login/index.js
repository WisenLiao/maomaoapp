;(function(){
    vp.ui.isTrue = true;
    var login_main = $('#login_main'),   //首页
        login_phone = $('#login_phone'),  //手机登陆页
        retrieve_password = $('#retrieve_password'),  //找回密码
        retrieve_password_n = $('#retrieve_password_n'),  //找回密码后续
        register_name = $('#register_name'),         //注册
        perfect_material = $('#perfect_material'),    //完善资料
        hobby_product = $('#hobby_product'), //偏好产品
        addhobbyPage=$('#addhobbyPage');//编辑偏好产品
        amend_password = $('#amend_password'), //修改密码
        api_url = vp.core.api,
        $reg = /^[\@A-Za-z0-9\,\.\`\/\\\?\;\'\[\]\!\#\$\%\^\&\*\.\~]{6,16}$/,  //密码格式
        $pages = 0;              //上一个页面 首页0 登陆1 注册2 找回密码3 找回密码下一步 4
        $code = /^\d{6}\b/;     //验证码
    var $nikename = /^[\@\u4e00-\u9fa5aA-Za-z0-9\,\.\`\/\\\丶\?\;\'\[\]\!\#\$\%\^\&\*\.\~]{1,30}$/;  //用户昵称判断
    var $username = /^[\@A-Za-z0-9]{2,18}$/                //用户账号格式判断
    function shareClick() {
        window.webkit.messageHandlers.Camera.postMessage(null);
    }
    var layout = {
        //首页
        HomepageFun:function(){
            //登陆跳转
            login_main.find('.phone_login').on('click',function(e){
                e.preventDefault();
                $pages = 1;
                login_main.addClass('hidden');
                login_phone.removeClass('hidden');
            });
            //注册跳转
            login_main.find('.create_name').on('click',function(e){
                e.preventDefault();
                login_main.addClass('hidden');
                register_name.removeClass('hidden');
            });
        },
        //登陆页
        LoginFun:function(){
            var name = login_phone.find('input[name="username"]'),  //账号
                password  = login_phone.find('input[name="password"]'); //mima
            //后退
            login_phone.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                $pages=0;
                login_phone.addClass('hidden');
                login_main.removeClass('hidden');
            })
            //跳转至找回密码
            login_phone.find('.retrieve').on('click',function(e){
                e.preventDefault();
                $pages = 1;
                login_phone.addClass('hidden');
                retrieve_password.removeClass('hidden');
            })
            //跳转至注册
            login_phone.find('.register').on('click',function(e){
                e.preventDefault();
                $pages = 1;
                login_phone.addClass('hidden');
                register_name.removeClass('hidden');
            })
            //清空输入框
            login_phone.find('.icon-err').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                _this.prev().val('');
                login_phone.find('input[name="username"]').keyup(); //隐藏清空
                login_phone.find('input[name="password"]').keyup(); //隐藏清空
            })
            //username
            name.on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            });
            //password
            password.on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            });
            //格式判断激活按钮
            function testFun(_this){
                var name = login_phone.find('input[name="username"]').val().replace(/\s/g, ''),
                    password = login_phone.find('input[name="password"]').val().replace(/\s/g, '');
                if(_this.val().trim().length > 0){
                    _this.next().removeClass('hidden');
                }else{
                    _this.next().addClass('hidden');
                }
                if(check_mobile(name) && $reg.test(password)){
                    login_phone.find('#login').addClass('right');
                }else{
                    login_phone.find('#login').removeClass('right');
                }
            };
            //login
            login_phone.find('#login').on('click',function(e){
                e.preventDefault();
                var _this = $(this),
                    nameval = name.val().replace(/\s/g,''),
                    passwordval = password.val().replace(/\s/g,'');
                if(!(_this.hasClass('right'))){
                    vp.ui.pop('icon-err','账号或密码格式错误');
                    return ;
                }
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/Login",
                    debug:false,
                    loadFlag:true,
                };
                var data = {PhoneNo:nameval,Password:passwordval,LoginEquipment:getPhoneType()};
                vp.core.ajax(url,data,function(data){
                    console.log(data)
                    saveStorage(data);
                    //已认证直接跳转到产品， 未认证跳转到完善资料
                    if(data.Data.IsPerfection){
                        router.navigate("product/");
                    }else{
                        login_phone.addClass('hidden');
                        perfect_material.removeClass('hidden');
                    }
                },function(data){
                    //错误
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                },function(){
                    //失败
                });
            });
        },
        //注册页
        RegisterFun:function(){
            var Mask_modal = register_name.find('.Mask_modal'),//遮罩
                Message_Code = Mask_modal.find('.Message-Code'), //验证码
                name = register_name.find('input[name="username"]'),  //用户名
                code = register_name.find('input[name="code"]'),     //短信验证码
                code_pic = Message_Code.find('input[name="jianjiancode"]'),//图片验证码框
                password = register_name.find('input[name="password"]'),//密码
                Voice_Test = Mask_modal.find('.Voice-Test'),      //已发送电话验证码
                Message_Test = Mask_modal.find('.Message-Test');  //未收到验证码
            var condition = 0; //验证码条件
            //后退
            register_name.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                console.log($pages)
                if($pages == 1){
                    register_name.addClass('hidden');
                    login_phone.removeClass('hidden');
                }
                if($pages == 0){
                    register_name.addClass('hidden');
                    login_main.removeClass('hidden');
                }
            })
            //清空输入框
            register_name.find('.icon-err').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                _this.prev().val('');
                register_name.find('input[name="username"]').keyup();
            })
            //username
            register_name.find('input[name="username"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            })
            //usercode
            register_name.find('input[name="code"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            })
            //password
            register_name.find('input[name="password"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            })
            //格式判断激活按钮
            function testFun(_this){
                var name = register_name.find('input[name="username"]').val().replace(/\s/g, ''),
                    code = register_name.find('input[name="code"]').val().replace(/\s/g, ''),
                    password = register_name.find('input[name="password"]').val().replace(/\s/g, '');
                if(_this.val().trim().length > 0){
                    _this.next('.icon-err').removeClass('hidden');
                }else{
                    _this.next('.icon-err').addClass('hidden');
                }
                //获取高亮
                if(check_mobile(name)){
                    register_name.find('.obtain').addClass('right');
                }else{
                    register_name.find('.obtain').removeClass('right');
                }
                //显示密码高亮
                if($reg.test(password)){
                    register_name.find('#reveal').addClass('right');
                }else{
                    register_name.find('#reveal').removeClass('right');
                }
                //下一步高亮
                if( $reg.test(password) && check_mobile(name) && $code.test(code) ){
                    register_name.find('.next').addClass('right');
                }else{
                    register_name.find('.next').removeClass('right');
                }
            }
            //获取图片验证码
            register_name.find('.obtain').on('click',function(e){
                e.preventDefault();
                var _this = $(this),
                    nameval = name.val().replace(/\s/g, ''),
                    codeval = code.val().replace(/\s/g, '');
                if(!_this.hasClass('right')){
                    vp.ui.pop('icon-err','手机号格式错误');
                    return;
                }
                if (register_name.find('.obtain').text() !== '获取') {
                    return;
                }
                code_pic.val(''); //清空验证框
                condition =0;  //验证码条件1
                Mask_modal.removeClass('hidden');
                Message_Code.removeClass('hidden');
                register_name.find('.codesimg').attr('src','images/loadcode.gif');
                var codeimg = new Image();
                codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                $(codeimg).load(function(){
                    register_name.find('.codesimg').attr('src','')
                    register_name.find('.codesimg').attr('src',codeimg.src);
                })
            })
            //获取短信验证码
            Message_Code.find('.get-message').on('click',function(e){
                e.preventDefault();
                var _this = $(this),
                    timer = null,
                    time = 60,
                    nameval = name.val().replace(/\s/g, ''),
                    codeval = code.val().replace(/\s/g, ''),
                    code_picval = code_pic.val().replace(/\s/g, '');
                if(code_pic.val().replace(/\s/g,'')==""){vp.ui.pop('icon-err','请输入验证码');return};
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/IsRightImageVerifyCode",
                    debug:false,
                    loadFlag:true,
                };
                var data = {PhoneNo:nameval,VerifyCode:code_picval};
                vp.core.ajax(url,data,function(data){
                    //成功
                    var url = {
                        local:"data/testDemo/data.json",
                        type:"get",
                        server:"/v1/Users/GetSmsRegisterCode",
                        debug:false,
                        loadFlag:true,
                    };
                    var data = {phoneNo:nameval,imgVerifyCode:code_picval,sendsMode:condition};
                    vp.core.ajax(url,data,function(data){
                        if (condition == 2) {
                            Message_Code.addClass('hidden');
                            Voice_Test.removeClass('hidden');
                        } else {
                            Message_Code.addClass('hidden');
                            Mask_modal.addClass('hidden');
                            vp.ui.pop('icon-right', '验证码已发送');
                        }
                        register_name.find('.obtain').text('60s');
                        Mask_modal.find('.resend-code').text('60s');
                        Mask_modal.find('.voice-valid').text('60s');
                        timer = setInterval(
                            function () {
                                time -= 1;
                                register_name.find('.obtain').text(time + 's');
                                Mask_modal.find('.resend-code').text(time + 's');
                                Mask_modal.find('.voice-valid').text(time + 's');
                            }, 1000
                        );
                        setTimeout(
                            function () {
                                clearInterval(timer);
                                register_name.find('.obtain').text('获取');
                                Mask_modal.find('.resend-code').text('重新发送验证码');
                                Mask_modal.find('.voice-valid').text('语音验证');
                                time = 60;
                            }, 60000
                        )
                    },function(data){
                        vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                    },function(data){
                    });
                },function(data){
                    code_pic.val(''); //清空验证框
                    if(data.Code==-1){
                        vp.ui.pop('icon-err','验证码输入错误')
                    }else{
                        vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                    }
                    register_name.find('.codesimg').attr('src','images/loadcode.gif');
                    var codeimg = new Image();
                    codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                    $(codeimg).load(function(){
                        register_name.find('.codesimg').attr('src',codeimg.src);
                    })
                },function(data){
                });
            })
            //显示密码
            var state = 0;
            register_name.find('#reveal').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                if(_this.hasClass('right')) {
                    if (state == 0) {
                        register_name.find('input[name="password"]').attr('type', 'text').next().text('隐藏');
                        state = 1;
                    } else {
                        register_name.find('input[name="password"]').attr('type', 'password').next().text('显示');
                        state = 0;
                    }
                }
            })
            //下一步
            register_name.find('.next').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                var nameval = name.val().replace(/\s/g, ''),
                    codeval = code.val().replace(/\s/g, ''),
                    passwordval = password.val().replace(/\s/g,'');
                if(!_this.hasClass('right')){
                    vp.ui.pop('icon-err','请检查格式是否正确，密码不可有符号');
                    return;
                }
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/Register",
                    debug:false,
                    loadFlag:true,
                };
                var data = {PhoneNo:nameval,Password:passwordval,VerifyCode:codeval,Equipment:getPhoneType()};
                vp.core.ajax(url,data,function(data){
                    //验证成功跳转到完善资料
                    $pages=2;
                    register_name.addClass('hidden');
                    perfect_material.removeClass('hidden');
                },function(data){
                     vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                },function(tem){
                });
            })
            //未收到验证码
            register_name.find('.other').on('click',function(e){
                e.preventDefault();
                var name = register_name.find('input[name="username"]').val().replace(/\s/g, '');
                Mask_modal.removeClass('hidden');
                Message_Test.removeClass('hidden');
                if(!check_mobile(name)){
                    Mask_modal.find('.voice-valid').removeClass('right');
                    Mask_modal.find('.resend-code').removeClass('right');
                }else{
                    Mask_modal.find('.voice-valid').addClass('right');
                    Mask_modal.find('.resend-code').addClass('right');
                }
            })
            //未收到验证码关闭窗口
            Message_Test.find('.icon-close').on('click',function(){
                $(this).closest('.Message-Test').addClass('hidden');
                Mask_modal.addClass('hidden');
            })
            //重新发送验证码22222
            Mask_modal.find('.resend-code').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                var nameval = name.val().replace(/\s/g,'');
                if(!_this.hasClass('right')){return;}
                if (register_name.find('.obtain').text() !== '获取') {
                    return;
                }
                code_pic.val(''); //清空验证框
                condition = 1;  //获取验证码条件 2
                register_name.find('.codesimg').attr('src','images/loadcode.gif');
                var codeimg = new Image();
                codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                $(codeimg).load(function(){
                    register_name.find('.codesimg').attr('src',codeimg.src);
                })
                Message_Test.addClass('hidden');
                Message_Code.removeClass('hidden');
            });
            //语音验证33333
            Mask_modal.find('.voice-valid').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                var nameval = name.val().replace(/\s/g,'');
                if(!_this.hasClass('right')){return;}
                if (register_name.find('.obtain').text() !== '获取') {
                    return;
                }
                code_pic.val(''); //清空验证框
                condition = 2;  //获取验证码条件 语音
                register_name.find('.codesimg').attr('src','images/loadcode.gif');
                var codeimg = new Image();
                codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                $(codeimg).load(function(){
                    register_name.find('.codesimg').attr('src',codeimg.src);
                })
                Message_Test.addClass('hidden');
                Message_Code.removeClass('hidden');
            })
            //关闭与确认电话通知信息
            Voice_Test.find('.icon-close').on('click',function(e){
                e.preventDefault();
                $(this).closest('.Voice-Test').addClass('hidden');
                Mask_modal.addClass('hidden');
            })
            Voice_Test.find('.confirm').on('click',function(e){
                e.preventDefault();
                $(this).closest('.Voice-Test').addClass('hidden');
                Mask_modal.addClass('hidden');
            })
            //验证码窗口关闭
            Message_Code.find('.icon-close').on('click',function(e){
                e.preventDefault();
                Message_Code.addClass('hidden');
                Mask_modal.addClass('hidden');
            })
        },
        //找回密码
        RetrieveFun:function(){
            var name = retrieve_password.find('input[name="username"]'), //账号
                code = retrieve_password.find('input[name="usercode"]'),  //验证码
                Mask_modal = retrieve_password.find('.Mask_modal'), //模态框
                Message_Code = retrieve_password.find('.Message-Code'),  //验证码框
                code_pic = Message_Code.find('input[name="jianjiancode"]'),  //图片验证码
                Voice_Test = Mask_modal.find('.Voice-Test'),      //已发送电话验证码
                Message_Test = Mask_modal.find('.Message-Test');  //未收到验证码
            var condition = 0;
            //后退
            retrieve_password.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                if($pages == 1){
                    retrieve_password.addClass('hidden');
                    login_phone.removeClass('hidden');
                }
                if($pages == 0){
                    retrieve_password.addClass('hidden');
                    login_main.removeClass('hidden');
                }
            })
            //清空输入框
            retrieve_password.find('.icon-err').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                _this.prev().val('');
                name.keyup(); //隐藏清空
            })
            //username
            name.on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            })
            //usercode
            code.on('keyup change',function(e){
                var _this = $(this);
                testFun(_this)
            })
            //格式判断激活按钮
            function testFun(_this){
                var nameval = name.val().replace(/\s/g, ''),
                    codeval = code.val().replace(/\s/g, '');
                if(_this.val().trim().length > 0){
                    _this.next('.icon-err').removeClass('hidden');
                }else{
                    _this.next('.icon-err').addClass('hidden');
                }
                if(check_mobile(nameval)){
                    retrieve_password.find('.obtain').addClass('right');
                }else{
                    retrieve_password.find('.obtain').removeClass('right');
                };
                if(check_mobile(nameval) && $code.test(codeval)){
                    retrieve_password.find('.next').addClass('right');
                }else{
                    retrieve_password.find('.next').removeClass('right');
                }
            }
            //获取图片验证码
            retrieve_password.find('.obtain').on('click',function(e){
                e.preventDefault();
                var _this = $(this),
                    nameval = name.val().replace(/\s/g, ''),
                    codeval = code.val().replace(/\s/g, '');
                if(!_this.hasClass('right')){
                    vp.ui.pop('icon-err','手机号格式错误');
                    return;
                }
                if (retrieve_password.find('.obtain').text() !== '获取') {
                    return;
                }
                condition = 0;  //验证码条件1
                Mask_modal.removeClass('hidden');
                Message_Code.removeClass('hidden');
                Message_Code.find('.codesimg').attr('src','images/loadcode.gif');
                code_pic.val(''); //清空验证框
                var codeimg = new Image();
                codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                $(codeimg).load(function(){
                    Message_Code.find('.codesimg').attr('src',codeimg.src);
                })
            })
            //获取短信验证码
            Message_Code.find('.get-message').on('click',function(e){
                e.preventDefault();
                var _this = $(this),
                    nameval = name.val().replace(/\s/g,''),
                    code_picval = code_pic.val().replace(/\s/g,''),
                    timer = null,
                    time = 60,
                    codeval = code.val().replace(/\s/g, '');
                if(code_pic.val().replace(/\s/g,'')==""){vp.ui.pop('icon-err','请输入验证码');return};
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/IsRightImageVerifyCode",
                    debug:false,
                    loadFlag:true,
                };
                var data = { PhoneNo:nameval,VerifyCode:code_picval};
                vp.core.ajax(url,data,function(data){
                    //成功
                    var url = {
                        local:"data/testDemo/data.json",
                        type:"get",
                        server:"/v1/Users/GetSmsFindPasswordCode",
                        debug:false,
                        loadFlag:false,
                    };
                    var data = {phoneNo:nameval,imgVerifyCode:code_picval,sendsMode:condition};
                    vp.core.ajax(url,data,function(data){
                        //成功
                        if (condition == 2) {
                            Message_Code.addClass('hidden');
                            Voice_Test.removeClass('hidden');
                        } else {
                            Message_Code.addClass('hidden');
                            Mask_modal.addClass('hidden');
                            vp.ui.pop('icon-right', '验证码已发送');
                        }
                        retrieve_password.find('.obtain').text('60s');
                        Mask_modal.find('.resend-code').text('60s');
                        Mask_modal.find('.voice-valid').text('60s');
                        timer = setInterval(
                            function () {
                                time -= 1;
                                retrieve_password.find('.obtain').text(time + 's');
                                Mask_modal.find('.resend-code').text(time + 's');
                                Mask_modal.find('.voice-valid').text(time + 's');
                            }, 1000
                        );
                        setTimeout(
                            function(){
                                clearInterval(timer);
                                retrieve_password.find('.obtain').text('获取');
                                Mask_modal.find('.resend-code').text('重新发送验证码');
                                Mask_modal.find('.voice-valid').text('语音验证');
                                time = 60;
                            },60000
                        )
                    },function(data){
                        vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                    },function(tem){
                    });
                },function(data){
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                    code_pic.val(''); //清空验证框
                    Message_Code.find('.codesimg').attr('src','images/loadcode.gif');
                    var codeimg = new Image();
                    codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                    $(codeimg).load(function(){
                        Message_Code.find('.codesimg').attr('src',codeimg.src);
                    })
                },function(tem){
                });
            })
            //下一步
            retrieve_password.find('.next').on('click',function(e){
                e.preventDefault();
                var _this = $(this),
                nameval = name.val().replace(/\s/g,''),
                codeval =  code.val().replace(/\s/g,'');
                if(!_this.hasClass('right')){
                    vp.ui.pop('icon-err','请确认格式是否正确')
                    return;
                }
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/IsRightByFindPwVerifyCode",
                    debug:false,
                    loadFlag:true,
                };
                var data = {PhoneNo:nameval,VerifyCode:codeval};
                vp.core.ajax(url,data,function(data){
                    //成功
                    retrieve_password.addClass('hidden');
                    retrieve_password_n.removeClass('hidden');
                },function(data){
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                },function(tem){
                });
            })
            //关闭图片验证码
            Message_Code.find('.icon-close').on('click',function(e){
                e.preventDefault();
                Mask_modal.addClass('hidden');
                Message_Code.addClass('hidden');
            })
            //未收到验证码
            retrieve_password.find('.other').on('click',function(e){
                e.preventDefault();
                var name = retrieve_password.find('input[name="username"]').val().replace(/\s/g, '');
                Mask_modal.removeClass('hidden');
                Message_Test.removeClass('hidden');
                if(!check_mobile(name)){
                    Mask_modal.find('.voice-valid').removeClass('right');
                    Mask_modal.find('.resend-code').removeClass('right');
                }else{
                    Mask_modal.find('.voice-valid').addClass('right');
                    Mask_modal.find('.resend-code').addClass('right');
                }
            })
            //重新获取验证码222
            Mask_modal.find('.resend-code').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                var nameval = name.val().replace(/\s/g,'');
                if(!_this.hasClass('right')){return;}
                if (retrieve_password.find('.obtain').text() !== '获取') {
                    return;
                }
                condition = 1;  //获取验证码条件 2
                code_pic.val(''); //清空验证框
                Message_Code.find('.codesimg').attr('src','images/loadcode.gif');
                var codeimg = new Image();
                codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                $(codeimg).load(function(){
                    Message_Code.find('.codesimg').attr('src',codeimg.src);
                })
                Message_Code.removeClass('hidden');
                Message_Test.addClass('hidden');
            });
            //语音验证333
            Mask_modal.find('.voice-valid').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                var nameval = name.val().replace(/\s/g,'');
                if(!_this.hasClass('right')){return;}
                if (retrieve_password.find('.obtain').text() !== '获取') {
                    return;
                }
                condition = 2;  //获取验证码条件 语音
                code_pic.val(''); //清空验证框
                Message_Code.find('.codesimg').attr('src','images/loadcode.gif');
                var codeimg = new Image();
                codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                $(codeimg).load(function(){
                    Message_Code.find('.codesimg').attr('src',codeimg.src);
                })
                Message_Test.addClass('hidden');
                Message_Code.removeClass('hidden');
            })
            //关闭未收到验证
            Message_Test.find('.icon-close').on('click',function(){
                Message_Test.addClass('hidden');
                Mask_modal.addClass('hidden');
            })
            //关闭与确认电话通知信息
            Voice_Test.find('.icon-close').on('click',function(e){
                e.preventDefault();
                $(this).closest('.Voice-Test').addClass('hidden');
                Mask_modal.addClass('hidden');
            })
            Voice_Test.find('.confirm').on('click',function(e){
                e.preventDefault();
                $(this).closest('.Voice-Test').addClass('hidden');
                Mask_modal.addClass('hidden');
            })
        },
        //找回密码2
        Retrieve_nFun:function(){
            var password = retrieve_password_n.find('input[name="password"]'), //新密码
                password_new = retrieve_password_n.find('input[name="userpassword"]'),  //确认密码
                name = retrieve_password.find('input[name="username"]'), //账号
                code = retrieve_password.find('input[name="usercode"]');  //验证码
            //后退
            retrieve_password_n.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                retrieve_password_n.addClass('hidden');
                login_main.removeClass('hidden');
            })
            //清空输入框
            retrieve_password_n.find('.icon-err').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                _this.prev().val('');
                retrieve_password.find('input[name="username"]').keyup(); //隐藏清空
            })
            //password
            retrieve_password_n.find('input[name="password"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            })
            //password2
            retrieve_password_n.find('input[name="userpassword"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);

            })
            function testFun(_this){
                var password = retrieve_password_n.find('input[name="password"]').val().replace(/\s/g, ''),
                    passwords = retrieve_password_n.find('input[name="userpassword"]').val().replace(/\s/g, '');
                if(_this.val().trim().length > 0){
                    _this.next().removeClass('hidden');
                }else{
                    _this.next().addClass('hidden');
                }
                if( $reg.test(password) && $reg.test(passwords) && password === passwords){
                    retrieve_password_n.find('.complete').addClass('right');
                }else{
                    retrieve_password_n.find('.complete').removeClass('right');
                }
            }
            //完成
            retrieve_password_n.find('.complete').on('click',function(e){
                if(!$(this).hasClass('right')){
                    vp.ui.pop('icon-err','请确认格式是否正确');
                    return
                };
                var passwordval = password.val().replace(/\s/g,''),
                    password_newval = password_new.val().replace(/\s/g,''),
                    nameval = retrieve_password.find('input[name="username"]').val().replace(/\s/g,''), //账号
                    codeval = retrieve_password.find('input[name="usercode"]').val().replace(/\s/g,'');  //验证码
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/PutFindPossword",
                    debug:false,
                    loadFlag:true,
                };
                var data = {NewPassword:passwordval,ConfirmPassword:password_newval,PhoneNo:nameval,VerifyCode:codeval};
                vp.core.ajax(url,data,function(data){
                    vp.ui.pop('icon-right',"密码找回成功");
                    password.val("");
                    password_new.val("");
                    retrieve_password.find('input[name="username"]').val("");
                    retrieve_password.find('input[name="usercode"]').val("");
                    retrieve_password_n.addClass('hidden');
                    login_phone.removeClass('hidden');
                },function(data){
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                },function(tem){
                });
            })
        },
        //完善资料
        Perfect_materialFun:function(){
            var name = perfect_material.find('input[name="username"]'),
                nikename = perfect_material.find('input[name="nickname"]'),
                products = perfect_material.find('input[name="product"]');
            //清空表单
            perfect_material.find('.icon-err').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                _this.prev().val('');
                perfect_material.find('input[name="username"]').keyup();
                perfect_material.find('input[name="nickname"]').keyup();
            });
            //username
            perfect_material.find('input[name="username"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            });
            //nikename
            perfect_material.find('input[name="nickname"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            });
            //products
            perfect_material.find('input[name="product"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            })
            //格式判断激活按钮
            function testFun(_this){
                var nameval = name.val().replace(/\s/g, ''),
                    nicknameval = nikename.val().replace(/\s/g, ''),
                    productsval = products.val().replace(/\s/g, '');
                if(_this.val().trim().length > 0){
                    _this.next('icon-err').removeClass('hidden');
                }else{
                    _this.next('icon-err').addClass('hidden');
                }
                if( $username.test(nameval) && $nikename.test(nicknameval) && productsval != ''){
                    perfect_material.find('.complete').addClass('right');
                }else{
                    perfect_material.find('.complete').removeClass('right');
                }
            }
            //偏好产品跳转
            perfect_material.find('input[name="product"]').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                perfect_material.addClass('hidden');
                hobby_product.removeClass('hidden');
                //hobby_product.find('.label_box').eq(0).find('.keyword').focus();
                var keywords = hobby_product.find('.keyword');
                var nullstate = false;  //判断是否有为空的关键词
                if(keywords.length>=10){return;};
                keywords.each(function(){
                    var _this = $(this);
                    if(_this.val().replace(/\s/g,'')==""){
                        _this.focus();
                        nullstate=true;
                        return false;
                    }else{
                        nullstate=false;
                    }
                })
                if(!nullstate){
                    var len=$('#hobby_product').find('.label_box').length;
                    if(len<1){
                        hobby_product.find('.main-body').append(
                            '<div class="label_box">'+
                            '<div class="label">'+
                            '<input class="keyword" type="text" maxlength="100" placeholder="请添加偏好产品" onfocus="this.blur()"><span class="icon-err"></span>'+
                            '</div>'+
                            '</div>'
                        );
                    }


                    keywords = hobby_product.find('.keyword');
                    keywords[keywords.length-1].focus();
                }
            });
            //完成
            perfect_material.find('.complete').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                var nameval = name.val().replace(/\s/g, ''),
                    nicknameval = nikename.val().replace(/\s/g, ''),
                    productsval = [];
                if(!_this.hasClass('right')){
                    if(!$username.test(nameval)){
                        vp.ui.pop('icon-err','账号需2位字符以上且不可有符号');
                    }else{
                        vp.ui.pop('icon-err','请完善资料');
                    }
                    return;
                }
                hobby_product.find('.keyword').each(function(e){
                    var me = $(this);
                    if( me.val()=="" ){return;}
                    productsval.push( me.val() );
                })
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/MakePerfectUserInfo",
                    debug:false,
                    loadFlag:true,
                };
                var data = {AccountName:nameval,Nickname:nicknameval,PreferredProducts:productsval};
                vp.core.ajax(url,data,function(data){
                    //成功
                    router.navigate("product/");
                },function(data){
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                },function(tem){
                });
            });
        },
        //偏好产品
        Hobby_productFun:function(){
            var pageEdit='add';//==》add 卫添加  edit 为编辑
            //聚焦
            hobby_product.on('click','.keyword',function(e){
                e.preventDefault();
                var _this = $(this);
                _this.focus();
            });
            //取消
            hobby_product.find('.cancel').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                hobby_product.addClass('hidden');
                perfect_material.removeClass('hidden');
            });
            //保存
            hobby_product.find('.hold').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                var str = "";
                hobby_product.find('.keyword').each(function(e){
                    if($(this).val()==""){return};
                    str += ($(this).val()+"、");
                })
                perfect_material.find('input[name="product"]').val(str);
                hobby_product.addClass('hidden');
                perfect_material.removeClass('hidden');
                testFun();
            });
            //添加
            hobby_product.on('click','.add',function(){
                addhobbyPage.removeClass('hidden');
                hobby_product.addClass('hidden');
                pageEdit='add';
            });
            //编辑
            hobby_product.on('click','.keyword',function(){
                var $this=$(this);
                var value=$this.val();
                pageEdit='edit';
                hobby_product.find('active').removeClass('active');
                $this.addClass('active');
                addhobbyPage.removeClass('hidden');
                addhobbyPage.find('textarea').val(value);
                hobby_product.addClass('hidden');
            });
            //取消编辑
            addhobbyPage.on('click','.header .cansel',function(){
                addhobbyPage.find('textarea').val('');
                addhobbyPage.addClass('hidden');
                hobby_product.removeClass('hidden');
            });
            //完成编辑
            addhobbyPage.off('click','.header .submit');
            addhobbyPage.on('click','.header .submit',function(){
                var value= $.trim(addhobbyPage.find('textarea').val());
                var str='';
                var msg='';
                if(value==''){
                    return false;
                }
                if(pageEdit=='edit'){
                    //编辑
                    if( hobby_product.find('.active.keyword').hasClass('mod')){
                        hobby_product.find('.mod').removeClass('mod');
                    }else{hobby_product.find('.mod').remove();}
                    hobby_product.find('.active.keyword').val(value).removeClass('active').removeClass('mod');
                }else{
                    //添加新偏好产品
                    str+='<div class="label_box">';
                    str+=   '<div class="label">';
                    str+=      '<input class="keyword" type="text" maxlength="100" onfocus=this.blur() value="'+value+'"><span class="icon-err"></span>';
                    str+=   '</div>';
                    str+='</div>';
                    hobby_product.find('.mod').remove();
                }
                addhobbyPage.find('textarea').val('');
                addhobbyPage.addClass('hidden');
                hobby_product.removeClass('hidden');
                hobby_product.find('.main-body').append(str);
                msg+='<div class="label_box mod">';
                msg+=   ' <div class="label">';
                msg+=      '<input class="keyword mod" type="text" maxlength="100" onfocus=this.blur() placeholder="请添加偏好产品"><span class="icon-err"></span>';
                msg+=   '</div>';
                msg+='</div>';
                hobby_product.find('.main-body').append(msg);
                if(pageEdit=='edit'){
                    vp.ui.pop('icon-right','修改成功');
                }else{
                    vp.ui.pop('icon-right','添加成功');
                }
            });
            //格式判断激活按钮
            function testFun(_this){
                var nameval = perfect_material.find('input[name="username"]').val().replace(/\s/g, ''),
                    nicknameval =perfect_material.find('input[name="nickname"]').val().replace(/\s/g, ''),
                    productsval = perfect_material.find('input[name="product"]').val().replace(/\s/g, '');
                if( $username.test(nameval) && $nikename.test(nicknameval) && productsval != ''){
                    perfect_material.find('.complete').addClass('right');
                }else{
                    perfect_material.find('.complete').removeClass('right');
                }
            }
            //删除
            hobby_product.on('click','.icon-err',function(e){
                e.preventDefault();
                var _this = $(this);
                _this.closest('.label_box').remove();
            })
            //添加
            hobby_product.find('.footer').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                var keywords = hobby_product.find('.keyword');
                var nullstate = false;  //判断是否有为空的关键词
                if(keywords.length>=10){vp.ui.pop('icon-err','最多只能添加10个偏好产品'); return;}
                keywords.each(function(){
                    var _this = $(this);
                    if(_this.val().replace(/\s/g,'')==""){
                        _this.focus();
                        nullstate=true;
                        return false;
                    }else{
                        nullstate=false;
                    }
                })
                if(!nullstate){
                    hobby_product.find('.main-body').append(
                        '<div class="label_box">'+
                        '<div class="label">'+
                        '<input class="keyword" type="text" maxlength="10"><span class="icon-err"></span>'+
                        '</div>'+
                        '</div>'
                    )
                    keywords = hobby_product.find('.keyword');
                    keywords[keywords.length-1].focus();
                }
            })
        },
        //修改密码
        Amend_passwordFun:function(){
            //后退
            amend_password.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                amend_password.addClass('hidden');
                login_main.removeClass('hidden');
            })
            //清空input
            amend_password.find('.icon-err').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                _this.prev().val('');
                amend_password.find('input[name="old-password"]').keyup(); //隐藏清空
                amend_password.find('input[name="password"]').keyup(); //隐藏清空
                amend_password.find('input[name="new-password"]').keyup(); //隐藏清空
            })
            //old-password
            amend_password.find('input[name="old-password"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            })
            //new-password
            amend_password.find('input[name="new-password"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            })
            //password
            amend_password.find('input[name="password"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            })
            function testFun(_this){
                var oldpassword = amend_password.find('input[name="old-password"]').val().replace(/\s/g, ''),
                    password = amend_password.find('input[name="password"]').val().replace(/\s/g, ''),
                    newpassword = amend_password.find('input[name="new-password"]').val().replace(/\s/g, '');
                if(_this.val().trim().length > 0){
                    _this.next().removeClass('hidden');
                }else{
                    _this.next().addClass('hidden');
                }
                if( $reg.test(oldpassword) && $reg.test(newpassword) && newpassword===password){
                    amend_password.find('.complete').addClass('right');
                }else{
                    amend_password.find('.complete').removeClass('right');
                }
            }
        },
        //隐私政策服务条款
        windowPopup:function(){
            var Agreement = $('.Agreement');  //兼职协议
            var Privacy_policy = $('.Privacy-policy');  //隐私政策
            Agreement.on('click',function(e){
                e.preventDefault();
                $('#Agreement_pop').removeClass('hidden');
            })
            Privacy_policy.on('click',function(e){
                e.preventDefault();
                $('#Privacy_policy_pop').removeClass('hidden');
            })

            //关闭条款框;
            $('#mainbox').find('.illustrate').find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                $(this).closest('.illustrate').addClass('hidden');
            })
        },
    }
    layout.HomepageFun();  //首页'
    layout.LoginFun();     //登录页
    layout.RegisterFun();  //注册页
    layout.RetrieveFun();  //找回密码
    layout.Retrieve_nFun();  //找回密码2
    layout.Perfect_materialFun(); //完善资料
    layout.Hobby_productFun(); //偏好产品
    layout.Amend_passwordFun();  //修改密码
    layout.windowPopup();   //条约
})(window)

//提示信息
//判断机型
function getPhoneType(){
//正则,忽略大小写
    var pattern_phone = new RegExp("iphone","i");
    var pattern_android = new RegExp("Android","i");
    var userAgent = navigator.userAgent.toLowerCase();
    var isAndroid = pattern_android.test(userAgent);
    var isIphone = pattern_phone.test(userAgent);
    var phoneType="phoneType";
    if(isAndroid){
        var zh_cnIndex = userAgent.indexOf("-");
        var spaceIndex = userAgent.indexOf("build",zh_cnIndex+4);
        var fullResult = userAgent.substring(zh_cnIndex,spaceIndex);
        phoneType=fullResult.split(";")[1];
    }else if(isIphone){
        var wigth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if(wigth>820){
            phoneType = "iphone6 plus or iphone7 plus";
        }else if(wigth>740){
            phoneType = "iphone6 or iphone7";
        }else if(wigth>630){
            phoneType = "iphone5 or iphone5s";
        }else{
            phoneType = "iphone 4s";
        }
    }else{
        phoneType = "***????***";
    }
    return phoneType;
}

//验证邮箱或者手机号是否正确
function check_email_mobile(val){
    var $reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    var $phone = /^0?(13[0-9]|15[012356789]|17[01678]|18[0-9]|14[57])[0-9]{8}$/;   //判断手机号
    if($reg.test(val)||$phone.test(val)){
        return true;
    }else{
        return false;
    }
}
//验证邮箱
function check_email(val){
    var $reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if($reg.test(val)){
        return true;
    }else{
        return false;
    }
}
//验证手机
function check_mobile(val){
    var $phone = /^0?(13[0-9]|15[012356789]|17[01678]|18[0-9]|14[57])[0-9]{8}$/;   //判断手机号
    if($phone.test(val)){
        return true;
    }else{
        return false;
    }
}

//用户自动登录信息
function saveStorage(data){
    localStorage.setItem('token',data.Data.Token);
}
$(function(){
    vp.ui.loading();
    $.ajax({
        type: 'POST',
        url:vp.core.api+'/v1/Users/IsLogin',
        cache: false,
        headers: {vp_token: vp.core.token},
        contentType: "application/json; charset=utf-8",
        data: {},
        dataType: 'json',
        timeout: 19900,
        success: function (data) {
            vp.ui.removeLoading();
            if(data.IsSuccess) {
                router.navigate("product/");
            }else{
                if(vp.ui.isTrue){
                    router.navigate("login/");
                }
            }
        },
        error: function (data) {

        }
    })
})
