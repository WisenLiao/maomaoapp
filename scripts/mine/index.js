;(function(w){
    function mindModule() {
    //*******我的*******
    var mine_module = $('#mine0'),                    //首页
    personal_message0 = $('#personal-message0'),      //个人信息页
    amend_password = $('#changepwd'),                 //修改密码页
    retrieve_password = $('#retrieve_password'),      //找回密码
    retrieve_password_n = $('#retrieve_password_n'),  //找回密码2
    real_namebind = $('#real_namebind'),       //实名认证页
    deposit_list = $('#deposit-list'),         //提现明细列表页
    withdraw_deposit = $('#withdraw_deposit'), //更换银行卡页
    Mention_account = $('#Mention_account'),   //提现账号 当前银行卡页面
    deposit_succ = $('#deposit-succ'),         //提现成功页面
    under_review = $('#under_review'),         //银行卡审核页
    Account_Detail = $('#Account-Detail'),     //账号余额以及提现页面
    market = $('#market'),                     //销售收入与提成明细
    newsCenter = $('#newsCenter'),             //消息中心页面
    applyProduct  = $('#applyProduct'),        //申请产品页面
    mySetting = $('#mySetting'),               //设置页
    help_center = $('#help-center'),           //帮助中心
    feedback_page = $('#feedback'),            //意见反馈页
    Sea_all = $('#Sea_all'),                   //大海说明页
    Quotation_note = $('#Quotation_note'),     //报价说明页
    Mail_rule = $('#Mail_rule'),               //邮件说明页
    certification = $('#certification'),       //实名认证说明
    api_url = vp.core.api,
    $reg = /^[\@A-Za-z0-9\,\.\`\/\\\?\;\'\[\]\!\#\$\%\^\&\*\.\~]{6,16}$/,
    $numreg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/,    //身份证号
    $chinese = /^[\u4E00-\u9FA5]{2,5}$/,                     //汉字
    $money = /^\d*(?:\.\d{0,2})?$/,                         //金额
    $card = /^\d{16}|\d{19}$/,                              //银行卡号
    Isconfirm = false,    //是否认证
    //首页
    layout_module = {
       //布局和获取基本信息  （包括个人详细页面）
       getDatafun:function(){
           var photo = mine_module.find('.photo img'),  //头像
               count = mine_module.find('.customer-num'), //客户数
               Real = mine_module.find('.is-valid span') //是否认证
               order = mine_module.find('.order-num'),     //订单数
               nickname = mine_module.find('.nickname'),   //用户昵称
               volume = mine_module.find('.volume-num'),  //销售额
               commission = mine_module.find('.commission-num'), //提成
               balancemodule = mine_module.find('.account-num').find('.balancemodule');   //余额
           //账号余额
           var url = {
               local:"data/testDemo/data.json",
               type:"post",
               server:"/v1/Users/GetUserHomepage",
               debug:false,
               loadFlag:true,
           };
           var data = {};
           vp.core.ajax(url,data,function(data){
               //成功
               console.log(data);
               if(data.Data.LogoPathOss!==null){
                   photo.attr('src', data.Data.LogoPathOss);//头像更改
               }
               nickname.text(data.Data.Nickname);     //昵称
               count.text(data.Data.CustomerCount);   //客户数
               order.text(data.Data.OrderCount);      //订单数
               if (data.Data.IsRealName) {
                   Real.text('已认证');       //是否实名认真
                   Isconfirm = true;
               } else {
                   Real.text('未认证');
               }
               Account_Detail.find('.account-money').find('.balance').text( '¥'+numModify(data.Data.CurrentBalance) ) //账号余额
               volume.children().eq(1).text('¥'+numModify(data.Data.TotalSales));          //销售额
               commission.children().eq(1).text('¥'+numModify(data.Data.TotalCommission));      //提成
               balancemodule.text('¥'+numModify(data.Data.CurrentBalance));   //余额
           },function(err){
           },function(tem){
           });
       },
       layoutFun: function () {
            //头部跳转消息中心
           mine_module.find('.message').on('click',function(e){
               e.preventDefault();
               mine_module.addClass('hidden');
               newsCenter.removeClass('hidden');
           })
           //跳转到个人信息
           mine_module.find('.photo').on('click',function(e){//个人信息
               e.preventDefault();
               mine_module.addClass('hidden');
               personal_message0.removeClass('hidden');
           })
           //跳转到账号余额
           mine_module.on('click','.mine-items .money .account',function(e){//账户余额
               e.preventDefault();
               mine_module.addClass('hidden');
               Account_Detail.removeClass('hidden');
           })
           //跳转到销售收入
           mine_module.on('click','.mine-items .money .sale-income',function(e){//销售收入
               e.preventDefault();
               mine_module.addClass('hidden');
               market.removeClass('hidden');
           })
           //跳转到消息中心
           mine_module.find('.center').find('.message').on('click',function(e){
               e.preventDefault();
               mine_module.addClass('hidden');
               newsCenter.removeClass('hidden');
           })
           //跳转到申请产品
           mine_module.find('.center').find('.product').on('click',function(e){
               e.preventDefault();
               mine_module.addClass('hidden');
               applyProduct.removeClass('hidden');
           })
           //跳转到帮助中心
           mine_module.find('.center').find('.help').on('click',function(e){
               e.preventDefault();
               mine_module.addClass('hidden');
               help_center.removeClass('hidden');
           })
           //跳转到设置
           mine_module.find('.setting').on('click',function(e){
               e.preventDefault();
               mine_module.addClass('hidden');
               mySetting.removeClass('hidden');
           })
       },
    },
    //个人信息
    layout_personal = {
        //获取基本信息
        layoutFun:function(){
           //个人详细页面
           var photo_person = personal_message0.find('#upload_picture'), //个人详细页头像
               num_person = personal_message0.find('.id-num').find('.num'),  //账号
               nikeput = personal_message0.find('input[name="nikename"]'),     //昵称输入框
               result_person = personal_message0.find('.result');  //认证信息
           //后退
           personal_message0.find('.iconleft-box').on('click',function(e){
               e.preventDefault();
               personal_message0.addClass('hidden');
               mine_module.removeClass('hidden');
           })
           var url = {
               local:"data/testDemo/data.json",
               type:"post",
               server:"/v1/Users/GetUserPersonalInfo",
               debug:false,
               loadFlag:true,
           };
           var data = {};
           vp.core.ajax(url,data,function(data){
               //成功
               //console.log(data)
               if (data.Data.IsRealName) {
                   result_person.text('已认证');       //是否实名认真
                   Account_Detail.find('.account-message').children('.card-detail').children('.bank').text( data.Data.BankOfDeposit ) //账户余额银行信息
                   Account_Detail.find('.account-message').children('.card-detail').find('.name').text( data.Data.BankAccountName )  //银行卡姓名
                   Account_Detail.find('.account-message').children('.card-detail').find('.card-num').text( data.Data.BankCardNo ) //银行卡号
                   Mention_account.find('.banks').text( data.Data.BankOfDeposit ); //提现账号银行卡姓名
                   Mention_account.find('.names').text( data.Data.BankAccountName ); //提现账号银行户名
                   Mention_account.find('.cards').text( data.Data.BankCardNo );  //银行卡号
                   Mention_account.find('.picture').attr('src',data.Data.FrontImagePath);
               } else {
                   result_person.text('未认证');
               }
               if(data.Data.LogoPathOss!==null){
                   photo_person.attr('src',data.Data.LogoPathOss);  //个人页头像
               }
               num_person.text( data.Data.AccountName+'@mooddo.biz'  );  //账号
               nikeput.val( data.Data.Nickname ) //昵称输入框
               //如果认证成功
               var combind = $('#real_namebind').find('.complete-bind'),
                   defeated = $('#real_namebind').find('.defeated'),
                   material = $('#real_namebind').find('.material'),
                   information = $('#real_namebind').find('.Submit-information'),
                   header = $('#real_namebind').find('.header'),
                   ulo = header.find('.line-level'),  //圆点
                   ult = header.find('.classify-type');  //三角
               //未认证的状态
               if(data.Data.CheckState==-100){
                   information.removeClass('hidden');
                   Account_Detail.find('.add-account').removeClass('hidden');    //账户余额提现账号
                   Account_Detail.find('.account-title').off('click');
                   Account_Detail.find('.account-title').on('click',function(e){
                       Account_Detail.addClass('hidden');   //未认证呼出实名认证
                       real_namebind.removeClass('hidden');
                   })
               }
               //已认证
               if(data.Data.CheckState==1){
                   combind.removeClass('hidden');
                   combind.find('.succeed').removeClass('hidden');
                   combind.find('.monicker').children('.name').text( data.Data.RealName );   //姓名
                   combind.find('.idnumber').children('.cardnum').text( data.Data.IdCardNo ); //身份证
                   combind.find('.bank-number').children('.Bank').text( data.Data.BankOfDeposit );  //户名
                   combind.find('.bank-number').children('.banknum').text( data.Data.BankCardNo );  //卡号
                   ulo.children('li').eq(1).removeClass('nond-rde').addClass('icon-right');
                   ulo.children('li').eq(2).addClass('icon-right').next().addClass('icon-right');
                   ult.children('li').eq(1).children('.undesanj').addClass('hidden');
                   ult.children('li').eq(3).children('.undesanj').removeClass('hidden');
                   //提现账号
                   Account_Detail.find('.account-message').removeClass('hidden');  //账户余额提现账号
                   Account_Detail.find('.account-title').off('click');
                   Account_Detail.find('.account-title').on('click',function(e){
                       Account_Detail.addClass('hidden');   //认证呼出更改银行卡绑定
                       Mention_account.removeClass('hidden');
                   })
               }
               //正在认证
               if(data.Data.CheckState==0){
                   material.removeClass('hidden');
                   combind.removeClass('hidden');
                   ulo.children('li').eq(1).removeClass('nond-rde').addClass('icon-right');
                   ulo.children('li').eq(2).addClass('icon-right').next().addClass('nond-rde');
                   ult.children('li').eq(1).children('.undesanj').addClass('hidden');
                   ult.children('li').eq(3).children('.undesanj').removeClass('hidden');
                   //提现账号
                   Account_Detail.find('.add-account').removeClass('hidden');  //账户余额提现账号
                   Account_Detail.find('.account-title').off('click');
                   Account_Detail.find('.account-title').on('click',function(e){
                       Account_Detail.addClass('hidden');   //认证呼出更改银行卡绑定
                       under_review.removeClass('hidden');
                   })
               }
               //认证失败
               if(data.Data.CheckState==-1){
                   combind.removeClass('hidden');
                   defeated.removeClass('hidden');
                   ulo.children('li').eq(1).removeClass('nond-rde').addClass('icon-right');
                   ulo.children('li').eq(2).addClass('icon-right').next().addClass('nond-rde');
                   ult.children('li').eq(1).children('.undesanj').addClass('hidden');
                   ult.children('li').eq(3).children('.undesanj').removeClass('hidden');
                   Account_Detail.find('.add-account').removeClass('hidden');    //账户余额提现账号
                   Account_Detail.find('.account-title').off('click');
                   Account_Detail.find('.account-title').on('click',function(e){
                       Account_Detail.addClass('hidden');   //未认证呼出实名认证
                       real_namebind.removeClass('hidden');
                   })
               }
           },function(err){
           },function(tem){
           });
       //跳转至修改密码
           personal_message0.find('.change-password').off('click');
           personal_message0.find('.change-password').on('click',function(){
               personal_message0.addClass('hidden');
               amend_password.removeClass('hidden');
           })
       //跳转至实名认证
           personal_message0.find('.pass-name').on('click',function(){
               personal_message0.addClass('hidden');
               real_namebind.removeClass('hidden');
           })
       },
        //更换头像
        photoFun:function(){
            var choose = document.getElementById('upload_photo');
            choose.onchange = function(e){
                showPreview(this);
            }
            function showPreview(source) {
                var file = source.files[0];
                //只允许上传图片
                if(!/image\/\w+/.test(file.type)){
                    //alert("请确保文件为图像类型");
                    return;
                }
                //检查浏览器支持性
                if(window.FileReader) {
                    var fr = new FileReader();  //实例化
                    fr.onloadend = function(e) {
                        $('#photo_module').removeClass('hidden');
                        new AlloyCrop({
                            image_src:e.target.result,
                            box:$('#photo_module').get(0),
                            circle:false, // optional parameters , the default value is false
                            width: 400,
                            height: 400,
                            ok:function (base64, canvas) {
                                document.getElementById("upload_picture").src = base64;   //成功读取返回值
                                mine_module.find('.main_box').children('.photo').children('img').attr('src',base64) //主页头像更换
                                updata( base64 );
                                $('#photo_module').addClass('hidden');
                            },
                            cancel:function(){
                                $('#photo_module').addClass('hidden');
                            },
                            ok_text: "完成",
                            cancel_text: "取消"
                        });
                    };
                    fr.readAsDataURL(file);  //读取为dataURL  readAsBinaryString//二进制
                }
                //更换头像
                function updata(src){
                    var url = {
                        local:"data/testDemo/data.json",
                        type:"post",
                        server:"/v1/Users/PutUploadUserLogo",
                        debug:false,
                        loadFlag:false,
                    };
                    var data = {LogoBase64: src};
                    vp.core.ajax(url,data,function(data){
                        //成功
                        console.log(data)
                    },function(err){
                    },function(tem){
                    });
                }
            }
        },
        //更换昵称
        nicknameFun:function(){
           var nikeput = personal_message0.find('input[name="nikename"]');     //昵称输入框
           nikeput.on('change',function(){
               var _val = $(this).val().replace(/\s/g,'');
               var url = {
                   local:"data/testDemo/data.json",
                   type:"post",
                   server:"/v1/Users/PutChangeNickName",
                   debug:false,
                   loadFlag:false,
               };
               var data = {NickName:_val};
               vp.core.ajax(url,data,function(data){
                   //成功
                   mine_module.find('.main_box').children('.nickname').text( _val );
               },function(err){
               },function(tem){
               });
            })
        },
    },
    //修改密码&&找回密码
    layout_changepwd = {
        //修改密码
        layoutFun:function(){
            var oldpwd = amend_password.find('input[name="old-password"]'), //旧密码
                newpwd = amend_password.find('input[name="new-password"]'),//新密码
                pwd = amend_password.find('input[name="password"]'),   //确认密码
                otherpwd = amend_password.find('.other'),          //忘记旧密码
                complete = amend_password.find('.complete');  //完成
            //后退
            amend_password.find('.icon-mail_fanhui').on('click', function (e) {
                e.preventDefault();
                amend_password.addClass('hidden');
                personal_message0.removeClass('hidden');
            })
            //清空input
            amend_password.find('.icon-err').on('click', function (e) {
                e.preventDefault();
                var _this = $(this);
                _this.prev().val('');
                oldpwd.keyup(); //隐藏清空
                newpwd.keyup(); //隐藏清空
                pwd.keyup(); //隐藏清空
            })
            //old-password
            oldpwd.on('keyup change', function (e) {
                var _this = $(this);
                testFun(_this);
            })
            //new-password
            newpwd.on('keyup change', function (e) {
                var _this = $(this);
                testFun(_this);
            })
            //password
            pwd.on('keyup change', function (e) {
                var _this = $(this);
                testFun(_this);
            })
            function testFun(_this) {
                var oldpassword = oldpwd.val().replace(/\s/g, ''),
                    password = newpwd.val().replace(/\s/g, ''),
                    newpassword = pwd.val().replace(/\s/g, '');
                if (_this.val().trim().length > 0) {
                    _this.next().removeClass('hidden');
                } else {
                    _this.next().addClass('hidden');
                }
                if ($reg.test(oldpassword) && $reg.test(newpassword) && newpassword === password) {
                    amend_password.find('.complete').addClass('right');
                } else {
                    amend_password.find('.complete').removeClass('right');
                }
            }
            //完成提交
            complete.on('click',function(e){
                e.preventDefault();
                if(!$(this).hasClass('right')){
                    vp.ui.pop('icon-err','请确认格式是否正确');
                    return
                };
                var _this = $(this),
                    oldval = oldpwd.val().replace(/\s/g,''),
                    newval = newpwd.val().replace(/\s/g,''),
                    pwdval = pwd.val().replace(/\s/g,'');
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/PutChangePassword",
                    debug:false,
                    loadFlag:true,
                };
                var data = {NewPassword: newval,ConfirmPassword:pwdval,Password:oldval};
                vp.core.ajax(url,data,function(data){
                    vp.ui.pop('icon-right','修改成功');
                    pwd.val('');
                    pwd.next('.icon-err').addClass('hidden');
                    oldpwd.val('');
                    oldpwd.next('.icon-err').addClass('hidden');
                    newpwd.val('');
                    newpwd.next('.icon-err').addClass('hidden');
                    complete.removeClass('right');
                    amend_password.addClass('hidden');
                    personal_message0.removeClass('hidden');
                },function(data){
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                },function(tem){
                });
            })
            //忘记旧密码跳转至修改密码
            otherpwd.on('click',function(e){
                e.preventDefault();
                amend_password.addClass('hidden');
                retrieve_password.removeClass('hidden');
            })
        },
        //找回密码
        RetrieveFun:function(){
            var name = retrieve_password.find('input[name="username"]'), //账号
                code = retrieve_password.find('input[name="usercode"]'),  //验证码
                Mask_modal = retrieve_password.find('.Mask_modal'), //模态框
                Message_Code = retrieve_password.find('.Message-Code'),  //验证码框
                code_pic = Message_Code.find('input[name="jianjiancode"]'),  //图片验证码
                Voice_Test = Mask_modal.find('.Voice-Test'),      //已发送电话验证码
                Message_Test = Mask_modal.find('.Message-Test');  //未收到验证码
            var condition = 0;
            //后退
            retrieve_password.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                retrieve_password.addClass('hidden');
                amend_password.removeClass('hidden');
            })
            //清空输入框
            retrieve_password.find('.icon-err').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                _this.prev().val('');
                name.keyup(); //隐藏清空
            })
            //username
            name.on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            })
            //usercode
            code.on('keyup change',function(e){
                var _this = $(this);
                testFun(_this)
            })
            //格式判断激活按钮
            function testFun(_this){
                var nameval = name.val().replace(/\s/g, ''),
                    codeval = code.val().replace(/\s/g, '');
                if(_this.val().trim().length > 0){
                    _this.next('.icon-err').removeClass('hidden');
                }else{
                    _this.next('.icon-err').addClass('hidden');
                }
                if(check_mobile(nameval)){
                    retrieve_password.find('.obtain').addClass('right');
                }else{
                    retrieve_password.find('.obtain').removeClass('right');
                };
                if(check_mobile(nameval) && $code.test(codeval)){
                    retrieve_password.find('.next').addClass('right');
                }else{
                    retrieve_password.find('.next').removeClass('right');
                }
            }
            //获取图片验证码
            retrieve_password.find('.obtain').on('click',function(e){
                e.preventDefault();
                var _this = $(this),
                    nameval = name.val().replace(/\s/g, ''),
                    codeval = code.val().replace(/\s/g, '');
                if(!_this.hasClass('right')){
                    vp.ui.pop('icon-err','手机号格式错误');
                    return;
                }
                if (retrieve_password.find('.obtain').text() !== '获取') {
                    return;
                }
                condition = 0;  //验证码条件1
                Mask_modal.removeClass('hidden');
                Message_Code.removeClass('hidden');
                code_pic.val(''); //清空验证框
                Message_Code.find('.codesimg').attr('src','images/loadcode.gif');
                var codeimg = new Image();
                codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                $(codeimg).load(function(){
                    Message_Code.find('.codesimg').attr('src',codeimg.src);
                })
            })
            //获取短信验证码
            Message_Code.find('.get-message').on('click',function(e){
                e.preventDefault();
                var _this = $(this),
                    nameval = name.val().replace(/\s/g,''),
                    code_picval = code_pic.val().replace(/\s/g,''),
                    timer = null,
                    time = 60,
                    codeval = code.val().replace(/\s/g, '');
                if(code_picval==""){vp.ui.pop('icon-err','请输入验证码'); return};
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/IsRightImageVerifyCode",
                    debug:false,
                    loadFlag:true,
                };
                var data = { PhoneNo:nameval,VerifyCode:code_picval};
                vp.core.ajax(url,data,function(data){
                    //成功
                    var url = {
                        local:"data/testDemo/data.json",
                        type:"get",
                        server:"/v1/Users/GetSmsFindPasswordCode",
                        debug:false,
                        loadFlag:false,
                    };
                    var data = {phoneNo:nameval,imgVerifyCode:code_picval,sendsMode:condition};
                    vp.core.ajax(url,data,function(data){
                        //成功
                        if (condition == 2) {
                            Message_Code.addClass('hidden');
                            Voice_Test.removeClass('hidden');
                        } else {
                            Message_Code.addClass('hidden');
                            Mask_modal.addClass('hidden');
                            vp.ui.pop('icon-right', '验证码已发送');
                        }
                        retrieve_password.find('.obtain').text('60s');
                        Mask_modal.find('.resend-code').text('60s');
                        Mask_modal.find('.voice-valid').text('60s');
                        timer = setInterval(
                            function () {
                                time -= 1;
                                retrieve_password.find('.obtain').text(time + 's');
                                Mask_modal.find('.resend-code').text(time + 's');
                                Mask_modal.find('.voice-valid').text(time + 's');
                            }, 1000
                        );
                        setTimeout(
                            function () {
                                clearInterval(timer);
                                retrieve_password.find('.obtain').text('获取');
                                Mask_modal.find('.resend-code').text('重新发送验证码');
                                Mask_modal.find('.voice-valid').text('语音验证');
                                time = 60;
                            }, 60000
                        )
                    },function(data){
                        vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                    },function(tem){
                    });
                },function(data){
                    code_pic.val(''); //清空验证框
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                    Message_Code.find('.codesimg').attr('src','images/loadcode.gif');
                    var codeimg = new Image();
                    codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                    $(codeimg).load(function(){
                        Message_Code.find('.codesimg').attr('src',codeimg.src);
                    })
                },function(tem){
                });
            })
            //下一步
            retrieve_password.find('.next').on('click',function(e){
                e.preventDefault();
                var _this = $(this),
                    nameval = name.val().replace(/\s/g,''),
                    codeval =  code.val().replace(/\s/g,'');
                if(!_this.hasClass('right')){
                    vp.ui.pop('icon-err','请确认格式是否正确')
                    return;
                }
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/IsRightByFindPwVerifyCode",
                    debug:false,
                    loadFlag:true,
                };
                var data = {PhoneNo:nameval,VerifyCode:codeval};
                vp.core.ajax(url,data,function(data){
                    //成功
                    retrieve_password.addClass('hidden');
                    retrieve_password_n.removeClass('hidden');
                },function(data){
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                },function(tem){
                });
            })
            //关闭图片验证码
            Message_Code.find('.icon-close').on('click',function(e){
                e.preventDefault();
                Mask_modal.addClass('hidden');
                Message_Code.addClass('hidden');
            })
            //未收到验证码
            retrieve_password.find('.other').on('click',function(e){
                e.preventDefault();
                var name = retrieve_password.find('input[name="username"]').val().replace(/\s/g, '');
                Mask_modal.removeClass('hidden');
                Message_Test.removeClass('hidden');
                if(!check_mobile(name)){
                    Mask_modal.find('.voice-valid').removeClass('right');
                    Mask_modal.find('.resend-code').removeClass('right');
                }else{
                    Mask_modal.find('.voice-valid').addClass('right');
                    Mask_modal.find('.resend-code').addClass('right');
                }
            })
            //重新获取验证码222
            Mask_modal.find('.resend-code').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                var nameval = name.val().replace(/\s/g,'');
                if(!_this.hasClass('right')){return;}
                if (retrieve_password.find('.obtain').text() !== '获取') {
                    return;
                }
                condition = 1;  //获取验证码条件 2
                code_pic.val(''); //清空验证框
                Message_Code.find('.codesimg').attr('src','images/loadcode.gif');
                var codeimg = new Image();
                codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                $(codeimg).load(function(){
                    Message_Code.find('.codesimg').attr('src',codeimg.src);
                })
                Message_Code.removeClass('hidden');
                Message_Test.addClass('hidden');
            });
            //语音验证333
            Mask_modal.find('.voice-valid').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                var nameval = name.val().replace(/\s/g,'');
                if(!_this.hasClass('right')){return;}
                if (retrieve_password.find('.obtain').text() !== '获取') {
                    return;
                }
                condition = 2;  //获取验证码条件 语音
                code_pic.val(''); //清空验证框
                Message_Code.find('.codesimg').attr('src','images/loadcode.gif');
                var codeimg = new Image();
                codeimg.src=api_url+"/v1/Users/GetImageVerifyCode?phoneNo="+nameval+"&rnd="+new Date().getTime();
                $(codeimg).load(function(){
                    Message_Code.find('.codesimg').attr('src',codeimg.src);
                })
                Message_Test.addClass('hidden');
                Message_Code.removeClass('hidden');
            })
            //关闭未收到验证
            Message_Test.find('.icon-close').on('click',function(){
                Message_Test.addClass('hidden');
                Mask_modal.addClass('hidden');
            })
            //关闭与确认电话通知信息
            Voice_Test.find('.icon-close').on('click',function(e){
                e.preventDefault();
                $(this).closest('.Voice-Test').addClass('hidden');
                Mask_modal.addClass('hidden');
            })
            Voice_Test.find('.confirm').on('click',function(e){
                e.preventDefault();
                $(this).closest('.Voice-Test').addClass('hidden');
                Mask_modal.addClass('hidden');
            })
        },
        //找回密码2
        Retrieve_nFun:function(){
            var password = retrieve_password_n.find('input[name="password"]'), //新密码
                password_new = retrieve_password_n.find('input[name="userpassword"]'),  //确认密码
                name = retrieve_password.find('input[name="username"]'), //账号
                code = retrieve_password.find('input[name="usercode"]');  //验证码
            //后退
            retrieve_password_n.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                retrieve_password_n.addClass('hidden');
                amend_password.removeClass('hidden');
            })
            //清空输入框
            retrieve_password_n.find('.icon-err').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                _this.prev().val('');
                retrieve_password.find('input[name="username"]').keyup(); //隐藏清空
            })
            //password
            retrieve_password_n.find('input[name="password"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);
            })
            //password2
            retrieve_password_n.find('input[name="userpassword"]').on('keyup change',function(e){
                var _this = $(this);
                testFun(_this);

            })
            //格式验证
            function testFun(_this){
                var password = retrieve_password_n.find('input[name="password"]').val().replace(/\s/g, ''),
                    passwords = retrieve_password_n.find('input[name="userpassword"]').val().replace(/\s/g, '');
                if(_this.val().trim().length > 0){
                    _this.next().removeClass('hidden');
                }else{
                    _this.next().addClass('hidden');
                }
                if( $reg.test(password) && $reg.test(passwords) && password === passwords){
                    retrieve_password_n.find('.complete').addClass('right');
                }else{
                    retrieve_password_n.find('.complete').removeClass('right');
                }
            }
            //完成
            retrieve_password_n.find('.complete').on('click',function(e){
                if(!$(this).hasClass('right')){
                    vp.ui.pop('icon-err','请确认格式是否正确')
                    return
                };
                var passwordval = password.val().replace(/\s/g,''),
                    password_newval = password_new.val().replace(/\s/g,''),
                    nameval = retrieve_password.find('input[name="username"]').val().replace(/\s/g,''), //账号
                    codeval = retrieve_password.find('input[name="usercode"]').val().replace(/\s/g,'');  //验证码
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/PutFindPossword",
                    debug:false,
                    loadFlag:true,
                };
                var data = {NewPassword:passwordval,ConfirmPassword:password_newval,PhoneNo:nameval,VerifyCode:codeval};
                vp.core.ajax(url,data,function(data){
                    vp.ui.pop('icon-right',"密码找回成功");
                    password.val("");
                    password_new.val("");
                    retrieve_password.find('input[name="username"]').val("");
                    retrieve_password.find('input[name="usercode"]').val("");
                    retrieve_password_n.addClass('hidden');
                    amend_password.removeClass('hidden');
                },function(data){
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                },function(tem){
                });
            })
        },
    },
    //提现明细列表
    layout_deposit = {
        pagesize:8,
        pageno:1,
        layoutFun:function(){
            var contentbox = deposit_list.find('.content');  //明细列表box
            var loading = deposit_list.find('.loading_more'), //加载框
                me = this,
            Seemore = loading.find('.deposit_listfind'), //加载更多
            Nomore = loading.find('.No-more');         //没有更多
            Seemore.on('click',function(e){
                e.preventDefault();
                me.pageno+=1;
                me.gaindatas(pageno);
            })
            me.gaindatas(1) //获取数据
            //添加滚动置顶
            vp.ui.scroll(deposit_list,contentbox);
        },
        //获取提现列表分页
        gaindatas:function(pageno){
            var contentbox = deposit_list.find('.deposit-box');  //明细列表box
            var loading = deposit_list.find('.loading_more'), //加载框
                me = this,
                Seemore = loading.find('.deposit_listfind'), //加载更多
                Nomore = loading.find('.No-more');         //没有更多
            //获取
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Enchashment/GetEnchashmentpageBack",
                debug:false,
                loadFlag:true,
            };
            var data = {pageNo:pageno,pageSize:me.pagesize,condition:{enchashmentState:-100,whetherSortByCreateDate:1}};
            vp.core.ajax(url,data,function(data){
                console.log(data);
                for(var i=0;i<data.Data.Data.length;i++){
                    //提现阶段
                    if(data.Data.Data[i].EnchashmentState==0){
                        contentbox.append(
                            '<div class="deposit-schedule">'+
                                '<div class="schedule-type" style="padding:0 1.4rem;">'+
                                    '<div class="list-type tijiao active">'+
                                        '<span class="icon-maoduo13 icon"></span>'+
                                        '<span class="text">提交申请</span>'+
                                    '</div>'+
                                    '<div class="list-type line active"></div>'+
                                    '<div class="list-type waite">'+
                                        '<span class="icon-maoduo2 icon"></span>'+
                                        '<span class="text">等待转账</span>'+
                                    '</div>'+
                                    '<div class="list-type line"></div>'+
                                    '<div class="list-type succ">'+
                                        '<span class="icon-maoduo12 icon"></span>'+
                                        '<span class="text">提现成功</span>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="schedule-title">'+
                                    '<div class="pass-name bott">'+
                                        '<span class="icon-maoduo14 icon lefticon lf"></span>'+
                                        '<div class="name-box lf">'+
                                            '<span class="num">¥'+data.Data.Data[i].EnchashmentConverAmount+'</span>'+
                                            '<h3 class="bank">'+data.Data.Data[i].BankOfDeposit+'</h3>'+
                                        '</div>'+
                                        '<span class="times rt">'+data.Data.Data[i].CreateDateStr+'</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'
                        )
                    }else if(data.Data.Data[i].EnchashmentState==1 || data.Data.Data[i].EnchashmentState==2){
                        contentbox.append(
                            '<div class="deposit-schedule">'+
                                '<div class="schedule-type">'+
                                    '<div class="schedule-title">'+
                                        '<div class="pass-name bott">'+
                                            '<span class="icon-maoduo14 icon lefticon lf"></span>'+
                                            '<div class="name-box lf">'+
                                                '<span class="num">¥'+data.Data.Data[i].EnchashmentConverAmount+'</span>'+
                                                '<h3 class="bank">'+data.Data.Data[i].BankOfDeposit+'</h3>'+
                                            '</div>'+
                                        '<span class="times rt">'+data.Data.Data[i].EnchashmentDateStr+'</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'
                        )
                    }else{
                        contentbox.append(
                            '<div class="deposit-schedule">'+
                                '<div class="schedule-type">'+
                                    '<div class="schedule-title">'+
                                        '<div class="pass-name bott">'+
                                            '<span class="icon-maoduo14 icon lefticon lf" style="color:#999"></span>'+
                                            '<div class="name-box lf">'+
                                                '<span class="num">¥'+data.Data.Data[i].EnchashmentConverAmount+'</span>'+
                                                '<h3 class="bank">'+data.Data.Data[i].BankOfDeposit+'</h3>'+
                                            '</div>'+
                                        '<span class="times rt">'+data.Data.Data[i].EnchashmentDateStr+'</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'
                        )
                    }
                }
                if(data.Data.Data.length == 0 && pageno==1){
                    loading.addClass('hidden');
                    deposit_list.find('.null-state').removeClass('hidden');
                }else{
                    loading.removeClass('hidden');
                    deposit_list.find('.null-state').addClass('hidden');
                    if(data.Data.Data.length < me.pagesize ){
                        Nomore.removeClass('hidden');
                        Seemore.addClass('hidden');
                    }else{
                        Seemore.removeClass('hidden');
                        Nomore.addClass('hidden');
                    }
                }
            },function(err){
            },function(tem){
            });
        },
    },
    //实名认证
    layout_namebind = {
        layoutFun:function(){
            var information = real_namebind.find('.Submit-information'),  //提交资料页
              bindbank = real_namebind.find('.Bind-bank-card'),         //绑定银行卡页
              complete_bind = real_namebind.find('.complete-bind'),    //完成认证页
            name =  information.find('input[name="username"]'),    //姓名
            usernum = information.find('input[name="usernumber"]'),   //身份证
            imgbox = information.find('.user_picture'),    //证件照
            user_positive = imgbox.children('.user_positive'),  //身份证正面
            user_back = imgbox.children('.user_back'),    //身份证反面
            user_hold = imgbox.children('.user_hold'),    //身份证手持
            next_step = information.find('.next-step'),  //下一步
            line_level = real_namebind.find('.line-level'),  //进程状态圆点
            classify_type = real_namebind.find('.classify-type');  //进程状态三角
            //后退
            real_namebind.find('.headfanhui').on('click',function(e){
              e.preventDefault();
              real_namebind.addClass('hidden');
              personal_message0.removeClass('hidden');
            });
            //实名认证说明
            real_namebind.find('.explain-text').on('click',function(e){
                e.preventDefault();
                certification.removeClass('hidden');
            })
            certification.find('.icon-mail_fanhui').on('click',function(){
                certification.addClass('hidden');
            })
            //用户姓名
            name.on('keyup change',function(e){
                e.preventDefault();
                textFun();
            });
            //身份证
            usernum.on('keyup change',function(e){
                e.preventDefault();
                textFun();
            });
            //上传正面图
            user_positive.children('input').on('change',function(e){
                vp.ui.uploadPictures( $(this)[0],$(this).prev('img'),textFun )
            })
            //上传手持图
            user_back.children('input').on('change',function(e){
                vp.ui.uploadPictures( $(this)[0],$(this).prev('img'),textFun )
            })
            //上传正面图
            user_hold.children('input').on('change',function(e){
                vp.ui.uploadPictures( $(this)[0],$(this).prev('img'),textFun )
            })
            //格式判断
            function textFun(){
                var nameval = name.val().replace(/\s/g,''),
                usernumval = usernum.val().replace(/\s/g,''),
                Positive = user_positive.children('img'),
                opposite = user_back.children('img'),
                hand = user_hold.children('img');
                if($chinese.test(nameval) && $numreg.test(usernumval) && Positive.attr('src')!=='' && opposite.attr('src')!=='' && hand.attr('src')!==''){
                    next_step.addClass('right');
                }else{
                    next_step.removeClass('right');
                }
            }
            //上一步
            real_namebind.find('.prevto').on('click',function(e){
                e.preventDefault();
                bindbank.addClass('hidden');
                information.removeClass('hidden');
                real_namebind.find('.headfanhui').removeClass('hidden');
                real_namebind.find('.prevto').addClass('hidden');
            })
            //下一步
            next_step.on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                if(!_this.hasClass('right')){return};
                information.addClass('hidden');
                bindbank.removeClass('hidden');
                line_level.children('li').eq(1).removeClass('nond-rde').addClass('icon-right');
                line_level.children('li').eq(2).addClass('nond-rde');
                classify_type.children('li').eq(1).children('.undesanj').addClass('hidden');
                classify_type.children('li').eq(2).children('.undesanj').removeClass('hidden');
                real_namebind.find('.headfanhui').addClass('hidden');
                real_namebind.find('.prevto').removeClass('hidden');
            })
            //绑定银行卡---------------------------------------------------------------------
            var cardname = bindbank.find('input[name="cardname"]'),     //开户姓名
                cardhnames = bindbank.find('input[name="cardhnames"]'), //开户行
                cardnumber = bindbank.find('input[name="cardnumber"]'), //银行卡号
                card_picture = bindbank.find('.card_picture'),  //银行卡图片
                card_complete = bindbank.find('.card-complete'),    //提交资料
                material = complete_bind.find('.material'),      //提交成功
                succeed =     complete_bind.find('.succeed'),    //认证成功
                defeated = complete_bind.find('.defeated'),     //认证失败
                completerok = material.find('.completer'),      //资料提交成功完成
                newly = complete_bind.find('.newly');           //重新认证
            //开户姓名
            cardname.on('keyup change',function(){
                cardtest();
            })
            //开户行
            cardhnames.on('keyup change',function(){
                cardtest();
            })
            //银行卡号
            cardnumber.on('keyup change',function(){
                cardtest();
            })
            //银行卡照片
            card_picture.children('input').on('change',function(){
                vp.ui.uploadPictures( $(this)[0],$(this).prev('img'),cardtest )
            })
            function cardtest() {
                var cardnameval = cardname.val().replace(/\s/g, ''),
                    cardhnamesval = cardhnames.val().replace(/\s/g, ''),
                    cardnumberval = cardnumber.val().replace(/\s/g, ''),
                    cardimg = card_picture.children('img');
                if ($chinese.test(cardnameval) && cardhnamesval!=='' && $card.test(cardnumberval) && card_picture.children('img').attr('src')!=='') {
                    card_complete.addClass('right');
                }else{
                    card_complete.removeClass('right');
                }
            }
            //提交资料
            card_complete.on('click',function(e){
                e.preventDefault();
                //身份信息
                var nameval = name.val().replace(/\s/g,''),
                    usernumval = usernum.val().replace(/\s/g,''),
                    Positive = user_positive.children('img').attr('src'),
                    opposite = user_back.children('img').attr('src'),
                    hand = user_hold.children('img').attr('src');
                //卡号信息
                var cardnameval = cardname.val().replace(/\s/g, ''),
                    cardhnamesval = cardhnames.val().replace(/\s/g, ''),
                    cardnumberval = cardnumber.val().replace(/\s/g, ''),
                    cardimg = card_picture.children('img').attr('src');
                var _this = $(this);
                if(!_this.hasClass('right')){return};
                //提交身份信息
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/PutUserRealName",
                    debug:false,
                    loadFlag:true,
                };
                var data = {
                    RealName: nameval,
                    IdCardNo:usernumval,
                    FrontImg:Positive,
                    BackImg:opposite,
                    HandheldImg:hand,
                    BCAccountName: cardnameval,
                    BCBankOfDeposit:cardhnamesval,
                    BCBankCardNo:cardnumberval,
                    BCFrontImg :cardimg
                };
                vp.core.ajax(url,data,function(data){
                    //成功
                    console.log(data)
                    bindbank.addClass('hidden');
                    complete_bind.removeClass('hidden');
                    material.removeClass('hidden')
                    line_level.children('li').eq(2).removeClass('nond-rde').addClass('icon-right');
                    line_level.children('li').eq(3).addClass('nond-rde');
                    classify_type.children('li').eq(2).children('.undesanj').addClass('hidden');  //进度显示
                    classify_type.children('li').eq(3).children('.undesanj').removeClass('hidden');
                    real_namebind.find('.headfanhui').removeClass('hidden');
                    real_namebind.find('.prevto').addClass('hidden'); //上一步
                },function(data){
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                },function(tem){
                });
            })
            //认证未通过,重新认证
            newly.on('click',function(e){
                e.preventDefault();
                complete_bind.addClass('hidden');
                defeated.addClass('hidden');
                information.removeClass('hidden');
                line_level.children().removeClass();
                line_level.children().eq(0).addClass('icon-right').next().addClass('nond-rde');
                classify_type.children().eq(3).children('.undesanj').addClass('hidden');
                classify_type.children().eq(1).children('.undesanj').removeClass('hidden');
            })
            //资料提交成功  完成选项
            completerok.on('click',function(e){
                e.preventDefault();
                real_namebind.addClass('hidden');
                personal_message0.removeClass('hidden');
            })
        },
    },
    //账户余额 &&&&&& 提现
    layout_Account = {
       layoutFun:function(){
           var withdrawal = Account_Detail.find('.withdrawal'), //申请提现
           money_count = Account_Detail.find('#money-count');    //金额输入
           balance = Account_Detail.find('.balance');           //余额
           money_count.on('keyup change',function(e){
               e.preventDefault();
               var _this = $(this);
               if(_this.val()!=="" && $money.test(_this.val()) && Isconfirm){
                   withdrawal.addClass('right');
               }else{
                   withdrawal.removeClass('right');
               }
           })
           $(window).resize(function(){
               var viewTop = $(window).scrollTop(),            // 可视区域顶部
                   viewBottom = viewTop + window.innerHeight;  // 可视区域底部
               var elementTop = $('#money-count').offset().top, // $element是保存的input
                   elementBottom = elementTop + $('#money-count').height()-viewBottom;
               $('#Account-Detail').scrollTop( elementBottom );
           })
           Account_Detail.on('click','.account-head .icon-mail_fanhui',function(){
               Account_Detail.addClass('hidden');
               $('#mine0').removeClass('hidden');
           })
           $('#deposit-list').on('click','.top .iconleft-box',function(){
               Account_Detail.removeClass('hidden');
               $('#deposit-list').addClass('hidden');
           })
           //账号余额跳转至明细
           Account_Detail.find('.detail-deal').on('click',function(e){
               e.preventDefault();
               Account_Detail.addClass('hidden');
               deposit_list.removeClass('hidden');
           })
           //提现 成功后跳转至提现成功页面
           withdrawal.on('click',function(e){
               e.preventDefault();
               var _this= $(this),
                   balanceVAL= balance.text().replace(/\s/g,''),  //余额
                   moneyVAL = money_count.val().replace(/\s/g,'');  //输入金额
               if(!_this.hasClass('right')){return};
               if(moneyVAL > balanceVAL){
                   vp.ui.pop('icon-err','提现金额输入错误','提现金额不能大于账户余额，请重新输入');
                   return;
               }
               var url = {
                   local:"data/testDemo/data.json",
                   type:"post",
                   server:"/v1/Enchashment/PutEnchashment",
                   debug:false,
                   loadFlag:true,
               };
               var data = {EnchashmentAmount:moneyVAL,Currency:0,};
               vp.core.ajax(url,data,function(data){
                   console.log(data);
                   Account_Detail.addClass('hidden');
                   deposit_succ.removeClass('hidden');
                   layout_module.getDatafun();//重新获取余额等信息
                   deposit_succ.find('.list.char-money').find('.num').text('¥'+data.Data.EnchashmentConverAmount  );
                   deposit_succ.find('.list.tixian-id').find('.bank').text( data.Data.BankOfDeposit );
                   deposit_succ.find('.list.tixian-id').find('.num').text( data.Data.BankCardNo );
                   deposit_list.find('.deposit-box').children().remove();
                   layout_deposit.pageno=1;
                   layout_deposit.gaindatas(1);
               },function(data){
                   vp.ui.pop('icon-err',vp.ui.showMessage(data.Code))
               },function(tem){
               });
           })
           //银行卡信息热区
           Account_Detail.find('.account-message').on('click',function(e){
               e.preventDefault();
               Account_Detail.find('.account-title').click();
           })
           Account_Detail.find('.add-account').on('click',function(e){
               e.preventDefault();
               Account_Detail.find('.account-title').click();
           })
           //提现成功完成 返回到账户余额页
           deposit_succ.find('.submit').on('click',function(e){
               e.preventDefault();
               deposit_succ.addClass('hidden');
               Account_Detail.removeClass('hidden');
           })
           //提现成功跳转至明细
           deposit_succ.find('.top').find('.list').on('click',function(e){
               e.preventDefault();
               deposit_succ.addClass('hidden');
               deposit_list.removeClass('hidden');
           })
       },
    },
    //提现账号，我的银行卡
    layout_Mention = {
        layoutFun:function(){
            //后退到账户余额
            Mention_account.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                Mention_account.addClass('hidden');
                Account_Detail.removeClass('hidden');
            })
            //更换银行卡跳转
            Mention_account.find('.complete').on('click',function(e){
                e.preventDefault();
                Mention_account.addClass('hidden');
                withdraw_deposit.removeClass('hidden');
            })
            //银行卡审核页 后退与完成
            under_review.find('.completer').on('click',function(e){
                e.preventDefault();
                under_review.addClass('hidden');
                Account_Detail.removeClass('hidden');
            })
            under_review.find('.icon-mail_fanhui').on('click',function(){
                under_review.find('.completer').click();
            })
        }
    },
    //更换银行卡
    layout_Replacement = {
        layoutFun:function(){
            var name = withdraw_deposit.find('input[name="username"]'),  //开户姓名
                bankAccount = withdraw_deposit.find('input[name="bankAccount"]'),  //开户行
                cardNo = withdraw_deposit.find('input[name="cardNo"]'),   //银行卡号
                complete = withdraw_deposit.find('.complete'),  //完成
                cancelhead = withdraw_deposit.find('.cancel-head'),  //取消
                pictures = withdraw_deposit.find('.upload-pictures').find('.picture');  //银行卡图片
            name.on('keyup change',function(){testFun();})
            cardNo.on('keyup change',function(){testFun();})
            pictures.on('keyup change',function(){testFun();})
            //取消
            cancelhead.on('click',function(e){
                e.preventDefault();
                withdraw_deposit.addClass('hidden');
                Mention_account.removeClass('hidden');
            })
            withdraw_deposit.find('.choose').on('change',function(e){
               e.preventDefault();
                vp.ui.uploadPictures($(this)[0],pictures,testFun)
            });
            //格式判断
            function testFun(){
                var nameVal = name.val().replace(/\s/g,''),
                    bankAccountVal = bankAccount.val().replace(/\s/g,''),
                    cardNoVal = cardNo.val().replace(/\s/g,''),
                    imgVal = pictures.attr('src');
                if($chinese.test(nameVal) && bankAccountVal!=="" && $card.test(cardNoVal) && imgVal!==""){
                    complete.addClass('right');
                }else{
                    complete.removeClass('right');
                }
            }
            complete.on('click',function(e){
                e.preventDefault();
                var nameVal = name.val().replace(/\s/g,''),
                    bankAccountVal = bankAccount.val().replace(/\s/g,''),
                    cardNoVal = cardNo.val().replace(/\s/g,''),
                    imgVal = pictures.attr('src');
                if(!$(this).hasClass('right')){return};
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Enchashment/PutAddBankCard",
                    debug:false,
                    loadFlag:true,
                };
                var data = {AccountName: nameVal,BankOfDeposit:bankAccountVal,BankCardNo:cardNoVal,FrontImg:imgVal};
                vp.core.ajax(url,data,function(data){
                    vp.ui.pop('资料提交成功');
                    withdraw_deposit.addClass('hidden');
                    under_review.removeClass('hidden');
                },function(data){
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code));
                },function(tem){
                });
            })
        },
    },
    //销售收入与提成
    layout_market = {
        pagesize:8,   //分页数量
        salespageno:1,  //销售分页
        deductpageno:1, //提现分页
        layoutFun:function(){
            var me=this,
                sales = market.find('.menue').children('.market'), //销售btn
                deduct = market.find('.menue').children('.deduct'),  //提成btn
                market_content = market.find('.market-content'),    //销售列表box
                deduct_content = market.find('.deduct-content');   //提成列表box
            sales.on('click',function(e){
                e.preventDefault();
                $(this).addClass('active').siblings().removeClass('active');
                market_content.removeClass('hidden').siblings('.deduct-content').addClass('hidden');
            })
            deduct.on('click',function(e){
                e.preventDefault();
                $(this).addClass('active').siblings().removeClass('active');
                deduct_content.removeClass('hidden').siblings('.market-content').addClass('hidden');
            })
          //后退
            market.find('.iconleft-box').on('click',function(e){
                e.preventDefault();
                market.addClass('hidden');
                mine_module.removeClass('hidden');
            })
            //右上角金额切换
            var marketstate = false;    //false代表当前为人民币  销售和提现明细切换
            market.find('.switchs').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                //销售明细切换
                if(marketstate){
                    _this.removeClass('icon-maoduo41');
                    _this.addClass('icon-maoduo42');
                    market_content.find('.num').each(function(e){
                        $(this).text('¥'+$(this).data('rmb') );
                        marketstate = false;
                    })
                    deduct_content.find('.num').each(function(e){
                        $(this).text('¥'+$(this).data('rmb') );
                        marketstate = false;
                    })
                }else{
                    _this.removeClass('icon-maoduo42');
                    _this.addClass('icon-maoduo41');
                    market_content.find('.num').each(function(e){
                        $(this).text('$'+$(this).data('doller') );
                        marketstate = true;
                    })
                    deduct_content.find('.num').each(function(e){
                        $(this).text('$'+$(this).data('doller') );
                        marketstate = true;
                    })
                }
            })
            //获取明细列表
            me.salesdatas(1,marketstate);
            me.deductdatas(1,marketstate);
            //查看更多加载事件
            //销售明细加载更多
            market_content.find('.See-more').on('click',function(e){
                e.preventDefault();
                me.salespageno+=1;
                me.salesdatas(me.salespageno,marketstate);
            })
            //提成明细加载更多
            deduct_content.find('.See-more').on('click',function(e){
                e.preventDefault();
                me.deductpageno+=1;
                me.deductdatas(me.deductpageno,marketstate);
            })
        },
        //获取销售明细
        salesdatas:function(pageno,ismoney){
            var me = this;
            var Total_sales = market.find('.market-content').find('.all').find('.num'); //销售总额
            var salesAll = market.find('.market-content').find('.list-menue');    //销售收入明细表
            var loading = market.find('.market-content').find('.loading_more');   //销售明细加载更多
            var url = {
                local:"data/testDemo/data.json",
                type:"post",
                server:"/v1/Users/GetUserSaleDetailPage",
                debug:false,
                loadFlag:true,
            };
            var data = {PageSize: me.pagesize,PageNo:pageno,NeedPage:true};
            vp.core.ajax(url,data,function(data){
                console.log(data);
                Total_sales.text('¥'+numModify(data.Data.TotalSales) );  //销售总额
                Total_sales.attr('data-rmb',numModify(data.Data.TotalSales));
                Total_sales.attr('data-doller',numModify(data.Data.DollarTotalSales));
                //销售列表
                if(data.Data.SalesList==null||undefined||""){data.Data.SalesList=[]}
                for(var i=0;i<data.Data.SalesList.length;i++){
                    salesAll.append(
                        '<div class="list">'+
                            '<span class="icon-me_text icon lefticon lf"></span>'+
                            '<div class="lf content-box">'+
                                '<p class="name">销售收入</p>'+
                                '<span class="time">'+data.Data.SalesList[i].CompleteTime+'</span>'+
                            '</div>'+
                            '<span class="num rt" data-doller='+numModify(data.Data.SalesList[i].DollarSalesAmount)+' data-rmb='+numModify(data.Data.SalesList[i].SalesAmount)+'>'+ismoneytype(ismoney)+'</span>'+
                        '</div>'
                    )
                }
                function ismoneytype(type){
                    if(type){
                        return '$'+numModify(data.Data.SalesList[i].DollarSalesAmount);
                    }else{
                        return '¥'+numModify(data.Data.SalesList[i].SalesAmount);
                    }
                }
                //判断是否显示没有更多 或者加载更多
                if(data.Data.SalesList.length== 0 && pageno==1){
                    loading.addClass('hidden');
                    market.find('.market-content').find('.null-state').removeClass('hidden');
                    market.find('.market-content').children('.all').addClass('hidden');
                }else{
                    loading.removeClass('hidden');
                    market.find('.market-content').children('.all').removeClass('hidden');
                    if(data.Data.SalesList.length < me.pagesize ){
                        loading.find('.No-more').removeClass('hidden');
                        loading.find('.See-more').addClass('hidden');
                    }else{
                        loading.find('.See-more').removeClass('hidden');
                        loading.find('.No-more').addClass('hidden');
                    }
                }
            },function(err){
            },function(tem){
            });
        },
        //获取提现明细
        deductdatas:function(pageno,ismoney){
            var me = this;
            var Total_deduct = market.find('.deduct-content').find('.all').find('.num');  //提成总额
            var deductAll = market.find('.deduct-content').find('.list-menue');    //提成明细表
            var loading = market.find('.deduct-content').find('.loading_more');   //销售明细加载更多
            var url = {
                local:"data/testDemo/data.json",
                type:"post",
                server:"/v1/Users/GetUserSaleDetailPage",
                debug:false,
                loadFlag:true,
            };
            var data = {PageSize: me.pagesize,PageNo:pageno,NeedPage:true};
            vp.core.ajax(url,data,function(data){
                console.log(data);
                //提成总额
                Total_deduct.text('¥'+numModify(data.Data.TotalCommission));  //销售总额
                Total_deduct.attr('data-rmb',numModify(data.Data.TotalCommission));
                Total_deduct.attr('data-doller',numModify(data.Data.DollarTotalCommission));
                //提成列表
                if(data.Data.CommissionList==null||undefined||""){data.Data.CommissionList=[]}
                for(var i=0;i<data.Data.CommissionList.length; i++){
                    deductAll.append(
                        '<div class="list">'+
                        '<span class="icon-me_text icon lefticon lf"></span>'+
                        '<div class="lf content-box">'+
                        '<p class="name">提成收入</p>'+
                        '<span class="time">'+data.Data.CommissionList[i].CompleteTime+'</span>'+
                        '</div>'+
                        '<span class="num rt" data-rmb='+numModify(data.Data.CommissionList[i].CommissionAmount)+' data-doller='+numModify(data.Data.CommissionList[i].DollarCommissionAmount)+'>'+ismoneytype(ismoney)+'</span>'+
                        '</div>'
                    )
                }
                function ismoneytype(type){
                    if(type){
                        return '$'+numModify(data.Data.CommissionList[i].DollarCommissionAmount);
                    }else{
                        return '¥'+numModify(data.Data.CommissionList[i].CommissionAmount);
                    }
                }
                //判断是否显示没有更多 或者加载更多
                if(data.Data.CommissionList.length== 0 && pageno==1){
                    loading.addClass('hidden');
                    market.find('.deduct-content').find('.null-state').removeClass('hidden');
                    market.find('.deduct-content').children('.all').addClass('hidden');
                }else{
                    loading.removeClass('hidden');
                    market.find('.deduct-content').children('.all').removeClass('hidden');
                    if(data.Data.SalesList.length < me.pagesize ){
                        loading.find('.No-more').removeClass('hidden');
                        loading.find('.See-more').addClass('hidden');
                    }else{
                        loading.find('.See-more').removeClass('hidden');
                        loading.find('.No-more').addClass('hidden');
                    }
                }
            },function(err){
            },function(tem){
            });
        },
    },
    //消息中心
    layout_newsCenter = {
        pagesize:8,
        pagenoL:1,
        pagenoR:1,
        layoutFun:function(){
            var $tradBtn = newsCenter.find('.tradBtn'),  //交易提醒
                $sysBtn = newsCenter.find('.sysNewsBtn'),  //系统提醒
                $tradUl = newsCenter.find(".tradingRemindUl"), //交易提醒表
                $sysUl = newsCenter.find(".systemNewsUl"),     //系统消息表
                $tradload = newsCenter.find('.transaction-alerts'),//交易提醒加载更多
                $sysload = newsCenter.find('.system-alert'),   //系统消息加载更多栏
                $jiaoyi = newsCenter.find('.transaction-alerts'), //交易信息box
                $xitong = newsCenter.find('.system-alert');      //系统消息box
            var me = this;
            //返回
            newsCenter.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                me.notread();   //返回获得剩下未读数量
                newsCenter.addClass('hidden');
                mine_module.removeClass('hidden');
            });
            $sysBtn.on("click",function(){
                $tradBtn.css({"border":"none","color":"#aaa"});
                $sysBtn.css({"border-bottom":"3px solid red","color":"#666"});
                $jiaoyi.addClass('hidden');
                $xitong.removeClass('hidden');
            });
            $tradBtn.on("click",function(){
                $sysBtn.css({"border":"none","color":"#aaa"});
                $tradBtn.css({"border-bottom":"3px solid red","color":"#666"});
                $xitong.addClass('hidden');
                $jiaoyi.removeClass('hidden');
            });
            //交易提醒明细绑定事件跳转和已读
            $tradUl.off('click','.business');
            $tradUl.on('click','.business',function(e){
                e.preventDefault();
                var _this = $(this);
                if(_this.find('.mstitle ').hasClass('hidden')){return;}
                _this.find('.mstitle').addClass('hidden');  //修改已读
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Message/UpMessageRead",
                    debug:false,
                    loadFlag:false,
                };
                var data = {pkId:_this.data('pkid')};
                vp.core.ajax(url,data,function(data){
                    console.log(data);
                },function(err){
                    console.log(err);
                },function(tem){
                });
            });
            //系统消息明细绑定事件跳转和已读
            $sysUl.off('click','li');
            $sysUl.on('click','li',function(e){
                e.preventDefault();
                var _this = $(this);
                if(_this.find('.mstitle ').hasClass('hidden')){return;}
                _this.find('.mstitle').addClass('hidden');  //修改已读
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Message/UpMessageRead",
                    debug:false,
                    loadFlag:false,
                };
                var data = {pkId:_this.data('pkid')};
                vp.core.ajax(url,data,function(data){
                    console.log(data);
                },function(err){
                    console.log(err);
                },function(tem){
                });
            });
            //添加滚动置顶
            vp.ui.scroll($jiaoyi,$jiaoyi);
            vp.ui.scroll($xitong,$xitong);
            //获取数据
            this.dealdatas(1);
            this.systemdatas(1);
            //绑定获取事件   交易提醒信息
            $tradload.find('.See-more').on('click',function(e){
                e.preventDefault();
                console.log('1');
                me.pagenoL+=1;
                me.dealdatas(me.pagenoL);
            })
            //绑定获取事件   系统消息
            $sysload.find('.See-more').on('click',function(e){
                e.preventDefault();
                console.log('1');
                me.pagenoR+=1;
                me.systemdatas(me.pagenoR);
            })
            //推送
            me.get_pushFun();
        },
        //得到网易权限获取推送  添加数据
        get_pushFun:function(){
            var nim = {},
                accid = "",
                message = "",
                appKey = "",
                me = this,
                TradeBox = newsCenter.find(".tradingRemindUl"), //交易提醒表
                SystemBox = newsCenter.find(".systemNewsUl");     //系统消息表
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Message/GetNeteaseApiToken",
                debug:false,
                loadFlag:false,
            };
            var data = {};
            vp.core.ajax(url,data,function(data){
                console.log(data);
                accid = data.Data.AccId;
                message = data.Data.Netease_Token;
                appKey = data.Data.AppKey;
                nim = NIM.getInstance({
                    appKey: appKey,
                    account: accid,
                    token: message,
                    onconnect: onConnect,
                    onwillreconnect: onWillReconnect,
                    ondisconnect: onDisconnect,
                    onerror: onError,
                    oncustomsysmsg: onCustomSysMsg
                });
            },function(err){
                console.log(err);
            },function(tem){
            });
            function onConnect() {}//"连接成功"
            function onError(error) {}//连接失败"
            function onDisconnect(error) {}//连接丢失
            function onWillReconnect(obj) {}//即将重连
            function onCustomSysMsg(sysMsg) {
                var data = $.parseJSON(sysMsg.content);
                console.log(data)
                me.notread(); //获取到推送后 修改全局消息数量;
                var isread="";
                var timetype=null;
                //交易信息
                if(data.Data.MessageType==1){
                    isread = data.Data.IsRead?'hidden':'';  //判断是否已读
                    TradeBox.prepend(
                        '<li data-pkid='+data.Data.PkId+' class="business">'+
                            '<ul class="detailNews">'+
                                '<li>'+
                                '<span class="icon-maoduo5"><i class="mstitle '+isread+'"></i></span>'+
                                '<em>'+data.Data.CreateDate+'</em>'+
                                '<h5>'+data.Data.Title+'</h5>'+
                                '<p>'+data.Data.Message+'</p>'+
                                '</li>'+
                            '</ul>'+
                        '</li>'
                    )
                }
                //系统信息
                if(data.Data.MessageType==2){
                    isread = data.Data.IsRead?'hidden':'';  //判断是否已读
                    timetype = data.Data.NoticeType==2?data.Data.StartTime:data.Data.CreateDate;
                    SystemBox.prepend(
                        '<li data-pkid='+data.Data.PkId+'>'+
                            '<span class="'+switchs(data.Data.NoticeType)+'"><i class="mstitle '+isread+'"></i></span>'+
                            '<em>'+timetype+'</em>'+
                            '<h5>'+me.messageType(data.Data.NoticeType)+'</h5>'+
                            '<p>'+data.Data.Message+'</p>'+
                        '</li>'
                    )
                }
                function switchs(type){
                    switch(type) {
                        case 1:
                            //服务条款
                            return "icon-maoduo17"
                            break;
                        case 2:
                            //关于我们
                            return "icon-maoduo4"
                            break;
                    }
                }
            }
            //添加数据
            // $.ajax({
            //     url:'https://www.mooddo.com/v1/Message/AddMessage',
            //     type:'post',
            //     headers:{vp_token:'k2ltIaYl5MeZAHK5FyCI2qrPu40='},
            //     contentType: "application/json; charset=utf-8",
            //     data:JSON.stringify({
            //         "MessageType": "1",
            //         "NoticeType": "3",
            //         "EventType": "0",
            //         "Link": "http://www.baidu.com",
            //         "Title": "123",
            //         "Message": "货物已经发往深圳湾港口",
            //         "StartTime": "2016-12-28T02:32:26.797Z",
            //         "EndTime": "2016-12-28T02:32:26.797Z",
            //         "Version": "3.1"
            //     }),
            //     success:function(data){
            //         console.log(data)
            //     }
            // })
        },
        //获取交易提醒分页
        dealdatas:function(pageno){
            var me = this;
            var transaction = newsCenter.find('.tradingRemindUl'),  //交易提醒
                systems = newsCenter.find('.systemNewsUl'),         //系统提醒
                $tradload = newsCenter.find('.transaction-alerts').children('.loading_more'),//交易提醒加载更多
                $sysload = newsCenter.find('.system-alert').children('.loading_more'),   //系统消息加载更多栏
                isread = '';  //判断是否已读
            //获取交易
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Message/GetMessageList",
                debug:false,
                loadFlag:true,
            };
            var data = {pageNo:pageno,pageSize:me.pagesize,messageType:1};
            vp.core.ajax(url,data,function(data){
                // data.Data.Data.length = 0;
                console.log(data);
                for(var i=0;i<data.Data.Data.length;i++){
                    isread = data.Data.Data[i].IsRead?'hidden':'';  //判断是否已读
                    transaction.append(
                        '<li data-pkid='+data.Data.Data[i].PkId+' class="business">'+
                            '<ul class="detailNews">'+
                                '<li>'+
                                '<span class="icon-maoduo5"><i class="mstitle '+isread+'"></i></span>'+
                                '<em>'+data.Data.Data[i].CreateDate+'</em>'+
                                //'<h5>'+me.messageType(data.Data.Data[i].NoticeType )+'</h5>'+
                                '<h5>'+data.Data.Data[i].Title+'</h5>'+
                                '<p>'+data.Data.Data[i].Message+'</p>'+
                                '</li>'+
                            '</ul>'+
                        '</li>'
                        )
                }
                if(data.Data.Data.length == 0 && pageno==1){
                    newsCenter.find('.transaction-alerts').find('.null-state').removeClass('hidden');
                    $tradload.addClass('hidden');
                }else{
                    $tradload.removeClass('hidden');
                    newsCenter.find('.transaction-alerts').find('.null-state').addClass('hidden');
                    if(data.Data.Data.length < me.pagesize ){
                        $tradload.find('.No-more').removeClass('hidden');
                        $tradload.find('.See-more').addClass('hidden');
                    }else{
                        $tradload.find('.See-more').removeClass('hidden');
                        $tradload.find('.No-more').addClass('hidden');
                    };
                }
            },function(err){
                console.log(err);
            },function(tem){
            });
        },
        //获取系统提醒分页
        systemdatas:function(pageno){
            var me = this;
            var transaction = newsCenter.find('.tradingRemindUl'),  //交易提醒
                systems = newsCenter.find('.systemNewsUl'),         //系统提醒
                $tradload = newsCenter.find('.transaction-alerts').children('.loading_more'),//交易提醒加载更多
                $sysload = newsCenter.find('.system-alert').children('.loading_more'),   //系统消息加载更多栏
                isread = '';  //判断你是否已读
                timetype = null;
            //系统消息
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Message/GetMessageList",
                debug:false,
                loadFlag:true,
            };
            var data = {pageNo:pageno,pageSize:me.pagesize,messageType:2};
            vp.core.ajax(url,data,function(data){
                console.log(data);
                for(var i=0;i<data.Data.Data.length;i++){
                    isread = data.Data.Data[i].IsRead?'hidden':'';  //判断是否已读
                    timetype = data.Data.Data[i].NoticeType==2?data.Data.Data[i].StartTime:data.Data.Data[i].CreateDate;
                    systems.append(
                        '<li data-pkid='+data.Data.Data[i].PkId+'>'+
                        '<span class='+switchs(data.Data.Data[i].NoticeType)+'><i class="mstitle '+isread+'"></i></span>'+
                        '<em>'+timetype+'</em>'+
                        '<h5>'+me.messageType(data.Data.Data[i].NoticeType)+'</h5>'+
                        '<p>'+data.Data.Data[i].Message+'</p>'+
                        '</li>'
                    )
                }
                if(data.Data.Data.length == 0 && pageno==1){
                    newsCenter.find('.system-alert').find('.null-state').removeClass('hidden');
                    $sysload.addClass('hidden');
                }else{
                    $sysload.removeClass('hidden');
                    newsCenter.find('.system-alert').find('.null-state').addClass('hidden');
                    if(data.Data.Data.length < me.pagesize ){
                        $sysload.find('.No-more').removeClass('hidden');
                        $sysload.find('.See-more').addClass('hidden');
                    }else{
                        $sysload.find('.See-more').removeClass('hidden');
                        $sysload.find('.No-more').addClass('hidden');
                    };
                }
            },function(err){
                console.log(err);
            },function(tem){
            });
            function switchs(type){
                switch(type) {
                    case 1:
                        //服务条款
                        return "icon-maoduo17"
                        break;
                    case 2:
                        //关于我们
                        return "icon-maoduo4"
                        break;
                }
            }
        },
        //得到未读消息
        notread:function(){
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Message/GetNoReadNumber",
                debug:false,
                loadFlag:false,
            };
            var data = {};
            vp.core.ajax(url,data,function(data){
                if(data.Data==0){
                    mine_module.find('.not-read-message').addClass('hidden');
                }else{
                    mine_module.find('.not-read-message').removeClass('hidden');
                    mine_module.find('.not-read-message').children('.notread').text( data.Data );  //
                }
                if(data.Data>0){
                    mine_module.find('.head').find('.read-title').removeClass('hidden');
                }else{
                    mine_module.find('.head').find('.read-title').addClass('hidden');
                }
            },function(err){
                console.log(err);
            },function(tem){
            });
        },
        //分类
        messageType:function(type){
            switch(type){
                case 1:
                    //服务条款
                    return "系统通知"
                    break;
                case 2:
                    //关于我们
                    return "活动通知"
                    break;
                case 3:
                    //意见反馈
                    return "客户回复"
                    break;
                case 4:
                    //意见反馈
                    return "物流信息"
                    break;
            }
        },
    },
    //申请产品
    layout_applyProduct = {
        layoutFun:function(){
            var isImg = false;  //判断是否有图片
            //返回按钮
            var $backBtn = applyProduct.find('.icon-mail_fanhui');
            $backBtn.on("click",function(){
                applyProduct.addClass("hidden");
                mine_module.removeClass("hidden");
            })
            var $picSelectDiv = applyProduct.find(".picSeletDiv");
            var $submitBtn = applyProduct.find('.product-obtain');
            //删除图片
            applyProduct.find('.picture_boxsize').on('click','.icon-maoduo0',function(e){
                e.preventDefault();
                $(this).closest('.imgsbox').remove();
                if(applyProduct.find('.picture_boxsize').children('.imgsbox').length<3){
                    $picSelectDiv.show();
                }
                picNum--;
            })
            //图片上传↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            var upload_photo = document.getElementById('inputFile');
            upload_photo.onchange = function(e){
                lrz(this.files[0])
                    .then(function (rst) {
                        console.log(rst)
                        var imgName = rst.origin.name;
                        var $newImg = $('<div class="imgsbox"><i class="icon-maoduo0"></i><img src="'+rst.base64+'" data-name="'+imgName+'" class="imgs"/></div>');
                        applyProduct.find('.picture_boxsize').prepend($newImg)
                        picNum++;
                        if(picNum == 3){
                            $picSelectDiv.hide();
                            $(".addProductPic img").css("top","0");
                        }else{
                            $picSelectDiv.show();
                            $(".addProductPic img").css("top","0");
                        }
                        isImg = true;
                        testFun();
                    })
                    .catch(function (err) {
                        // 处理失败会执行
                    })
                    .always(function () {
                        // 不管是成功失败，都会执行
                    });
            }
            var picNum = 0;
            var $inputProName = applyProduct.find('.inputProName');
            var $textProDisc = applyProduct.find('.product-describe');
            $inputProName.on('keyup change',function(){
                testFun();
            })
            $textProDisc.on('keyup change',function(){
                testFun();
            })
            //格式判断
            function testFun(){
                var prdocut_name = applyProduct.find('.inputProName').val(),
                    product_describe = applyProduct.find('.product-describe').val(),
                    imgs = applyProduct.find('img');
                if(prdocut_name!=="" && product_describe!=="" && isImg){
                    applyProduct.find('.product-obtain').addClass('right');
                }else{
                    applyProduct.find('.product-obtain').removeClass('right');
                }
            }
            //提交信息
            $submitBtn.on('click',function(e){
                e.preventDefault();
                var prdocut_name = applyProduct.find('.inputProName').val(),
                    product_describe = applyProduct.find('.product-describe').val(),
                    imgs = applyProduct.find('img');
                var _this = $(this);
                if(!_this.hasClass('right')){return};
                var imgsAll = [];
                for(var i=0;i<imgs.length;i++){
                    imgsAll[i] = {FileName:$(imgs[i]).attr('data-name'),Base64Str:imgs[i].src};
                }
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Product/ApplyProduct",
                    debug:false,
                    loadFlag:true,
                };
                var data = {ProductName:prdocut_name,ProductDescription:product_describe,Images:imgsAll};
                vp.core.ajax(url,data,function(data){
                    vp.ui.pop('icon-right','申请提交成功');
                    applyProduct.find('.inputProName').val(""); //提交成功后清空
                    applyProduct.find('.product-describe').val("") //提交成功后清空
                    picNum = 0;
                    _this.removeClass('hidden');
                    for(var i=0;i<imgs.length;i++){
                        applyProduct.find('.imgsbox').remove();
                    }
                    applyProduct.addClass('hidden');
                    mine_module.removeClass('hidden');
                },function(err){
                    console.log(err);
                },function(tem){
                });
            })
        },
    },
    //帮助中心
    layout_helpCenter = {
        layoutFun:function(){
            //后退
            help_center.children('.header').children('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                help_center.addClass('hidden');
                mine_module.removeClass('hidden');
            })
            //大海说明
            help_center.find('.sea-all').on('click',function(e){
                help_center.addClass('hidden');
                Sea_all.removeClass('hidden');
            });
            //报价说明
            help_center.find('.offer').on('click',function(e){
                help_center.addClass('hidden');
                Quotation_note.removeClass('hidden');
            });
            //邮件说明
            help_center.find('.mail-rules').on('click',function(e){
                help_center.addClass('hidden');
                Mail_rule.removeClass('hidden');
            });
            $('#Sea_all').find('.icon-mail_fanhui').on('click',function(e){
                $('#Sea_all').addClass('hidden');
                help_center.removeClass('hidden');
            })
            $('#Quotation_note').find('.icon-mail_fanhui').on('click',function(e){
                $('#Quotation_note').addClass('hidden');
                help_center.removeClass('hidden');
            })
            $('#Mail_rule').find('.icon-mail_fanhui').on('click',function(e){
                $('#Mail_rule').addClass('hidden');
                help_center.removeClass('hidden');
            })
        },
    },
    //设置
    layout_setup = {
        layoutFun:function(){
            var exitloginBox = mySetting.find('.exit-login'),  //是否确认退出选择框
                outits = exitloginBox.find('.exit'),    //退出
                notits = exitloginBox.find('.cancel-off');  //取消
            //后退
            mySetting.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                mySetting.addClass('hidden');
                mine_module.removeClass('hidden');
            })
            //退出账号
            mySetting.find('.exit-account').on('click',function(e){
                e.preventDefault();
                mySetting.find('.exit-login').removeClass('hidden');
            })
            //清除缓存
            mySetting.find('.c5').find('a').on('click',function(e){
                vp.ui.pop('icon-right','清除成功');
            })
            //确认退出
            outits.on('click',function(e){
                e.preventDefault();
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/LogOut",
                    debug:false,
                    loadFlag:true,
                };
                var data = {};
                vp.core.ajax(url,data,function(data) {
                    mySetting.addClass('hidden');
                    router.navigate("login/")  //跳转至登陆模块
                })
            })
            notits.on('click',function(e){
                e.preventDefault();
                exitloginBox.addClass('hidden');
            })
            //意见反馈
            mySetting.find('.c3').on('click',function(e){
                e.preventDefault();
                mySetting.addClass('hidden');
                feedback_page.removeClass('hidden');
            })
            var pattern_phone = new RegExp("iphone","i");
            var userAgent = navigator.userAgent.toLowerCase();
            var isIphone = pattern_phone.test(userAgent);
            if(isIphone) {
                mySetting.find('.c5').removeClass('hidden');
            }
        },
    },
    //所有弹窗及说明
    layout_window_pop = {
        layoutFun:function(){
            var box = $('#mine-main');
            //隐私政策
            mySetting.find('.c1').on('click',function(e){
                e.preventDefault();
                $('#Privacy_policy_pop').removeClass('hidden');
            })
            //兼职协议
            mySetting.find('.c4').on('click',function(e){
                e.preventDefault();
                $('#Part-time_job_pop').removeClass('hidden');
            })
            //关于我们
            mySetting.find('.c2').on('click',function(e){
                e.preventDefault();
                $('#About_us_pop').removeClass('hidden');
            })
            //单据说明
            help_center.find('.bill-explain').on('click',function(e){
                e.preventDefault();
                $('#bill_rule_pop').removeClass('hidden');
            })
            //实名认证说明
            help_center.find('.Real-name-explain').on('click',function(e){
                e.preventDefault();
                $('#certification').removeClass('hidden');
            })
            //货运说明
            help_center.find('.freight-explain').on('click',function(e){
                e.preventDefault();
                $('#freight_pop').removeClass('hidden');
            })
            //订单追踪说明
            help_center.find('.order-tracking-explain').on('click',function(e){
                e.preventDefault();
                $('#order_tracking_pop').removeClass('hidden');
            })
            //关闭条款框;
            box.find('.illustrate').find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                $(this).closest('.illustrate').addClass('hidden');
            })
        },
    },
    //意见反馈
    layout_feedback_page = {
        layoutFun:function(){
            var textarea = feedback_page.find('.feedback-content');
            //后退
            feedback_page.find('.iconleft-box').on('click',function(e){
                e.preventDefault();
                feedback_page.addClass('hidden');
                mySetting.removeClass('hidden');
            })
            //反馈输入框
            autosize(textarea.get(0));
            textarea.on('keyup change',function(e){
                if($(this).val().replace(/\s/g,'')==""){
                    feedback_page.find('.complete').removeClass('right')
                }else{
                    feedback_page.find('.complete').addClass('right')
                }
            })
            //完成上传
            feedback_page.find('.complete').on('click',function(e){
                e.preventDefault();
                var vals = textarea.val()
                if(!$(this).hasClass('right')){return};
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Users/RecordUserFeedback",
                    debug:false,
                    loadFlag:true,
                };
                var data = {FeedbackContent:vals};
                vp.core.ajax(url,data,function(data){
                    vp.ui.pop('icon-right','提交成功');
                    textarea.val('');
                    feedback_page.find('.complete').removeClass('right');
                },function(err){
                    console.log(err);
                },function(tem){
                });
            })
        },
    };
    //首页
    layout_module.layoutFun();  //事件
    layout_module.getDatafun();//获取
    //个人信息
    layout_personal.layoutFun(); //获取
    layout_personal.photoFun(); //更换头像
    layout_personal.nicknameFun();  //更改昵称
    //修改密码
    layout_changepwd.layoutFun(); //修改密码
    layout_changepwd.RetrieveFun(); //找回密码1
    layout_changepwd.Retrieve_nFun(); //找回密码2
    //提现明细
    layout_deposit.layoutFun();  //获取提现明细列表
    //实名认证
    layout_namebind.layoutFun(); //
    //账户余额
    layout_Account.layoutFun();
    //提现账号，我的银行卡
    layout_Mention.layoutFun();
    //销售收入
    layout_market.layoutFun();
    //更换银行卡
    layout_Replacement.layoutFun();
    //消息中心
    layout_newsCenter.layoutFun();
    //layout_newsCenter.get_pushFun();//得到网易权限获取推送
    layout_newsCenter.notread();   //得到未读消息
    //申请产品
    layout_applyProduct.layoutFun();
    //帮助中心
    layout_helpCenter.layoutFun();
    //设置
    layout_setup.layoutFun();
    //所有弹窗及说明
    layout_window_pop.layoutFun();
    //意见反馈
    layout_feedback_page.layoutFun();
    //footer栏跳转
    mine_module.on('click','.menue-bottom .bottom-list',function(){
        var $this=$(this);
        var ClassName=$this.attr('class').split(' ')[1];
        console.log(ClassName);
        switch(ClassName){
            case 'customer':
                router.navigate("customer/");
                break;
            case 'email':
                router.navigate("email/");
                break;
            case 'mime':
                router.navigate("mine/");
                break;
            case 'receipts':
                router.navigate("bill/");
                break;
            case 'product':
                router.navigate("product/");
                break;
            }
    })
        //保留两位处理
        function numModify(data){
            return Math.floor(data * 100) / 100;
        }
    }
    mindModule();
    vp.registerFn.mindModule = mindModule;
})(window)