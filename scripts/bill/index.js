/**
 * Created by VeryVP030 on 2016/12/12.
 */
var billRegist=function(){
    var costRate = 1.2;
    var rate =0;
//获得汇率
    getRate();
// 进入报价单
    $('#Bill').on('click','.item-menu .quotation-order',function(e){
        e.stopPropagation();
        $('#Bill').addClass('hidden');
        $('#quotation').removeClass('hidden');
    })
// 报价单返回到主页
    $('#quotation').on('click','.quotation-title .back-border',function(e){
        e.stopPropagation();
        $('#Bill').removeClass('hidden');
        $('#quotation').addClass('hidden');
    })

//进入报价单详情
    $('#quotation').on('click','.quotation-list-item .quotation-item',function(e){
        e.stopPropagation();
        if($(this).hasClass('abled')){
            $('#quotation-modify').removeClass('hidden');
            $('#quotation').addClass('hidden');
            /////分割线////
            var quotationDetailData = $(this).data('quotationDetailData');
            //console.error(quotationDetailData);
            ///报价单详情页数据渲染
            var productno = quotationDetailData.productno;
            var productname = quotationDetailData.productname;
            var ordertotalsales = quotationDetailData.ordertotalsales;
            var commissionproportion = quotationDetailData.commissionproportion;
            var quotationgroup = quotationDetailData.quotationgroup;
            var note = quotationDetailData.note;
            var quotationname = quotationDetailData.quotationname;
            var PkId = quotationDetailData.PkId;
            var ProductPkId = quotationDetailData.ProductPkId;
            var Logistics = quotationDetailData.Logistics;
            //console.log(Logistics);




            //储存下载报价单pkId
            $('#quotation-modify').find('.quotation-modify-body .order-download .download').data('pkId',PkId);

            $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('ProductPkId',ProductPkId);

            $('#quotation-modify').find('.product-order-num .product-num .productno').html(productno);
            $('#quotation-modify').find('.product-order-num .product-name').html(productname);
            limitText($('#quotation-modify').find('.product-order-num .product-name')[0]);
            $('#quotation-modify').find('.profit .account-balance .sale-volume .sale-num .money-count').html(numModify(ordertotalsales));
            $('#quotation-modify').find('.profit .account-balance .withdraw-deposit .deposit-num .money-count').html(numModify(commissionproportion));
            $('#quotation-modify').find('.order-name .order-word textarea').html(quotationname);

            //自定义信息

            if(!note){
                $('#quotation-modify').find('.note-modify').hide();
                $('#quotation-modify').find('.note-modify').removeClass('isShow');
                $('#quotation-modify').find('.modify-group .add-note').show();
            }
            else{
                $('#quotation-modify').find('.modify-group .add-note').hide();
                $('#quotation-modify').find('.note-modify').show();
                $('#quotation-modify').find('.note-modify').addClass('isShow');
                $('#quotation-modify').find('.note-modify .note-word').html(note);
            }

            //货运信息
            if(!Logistics){
                $('#quotation-modify').find('.freight-way').hide();
                $('#quotation-modify').find('.modify-group .add-address').show();
                $('#quotation-modify').find('.freight-way').removeClass('isShow');
            }
            else{
                var Logisticsmodify = {
                    CountryPkId: Logistics.CountryPkId,
                    DeliveryMethod: Logistics.DeliveryMethod,
                    FreightOriginalPrice: Logistics.UsdFreightOriginalPrice,
                    FreightCurrentPrice: Logistics.UsdFreightCurrentPrice
                }
                $('#quotation-modify').find('.modify-group .add-address').hide();
                $('#quotation-modify').find('.freight-way').show();
                $('#quotation-modify').find('.freight-way').addClass('isShow');
                //货运信息数据填充
                $('#quotation-modify').find('.freight-way .freight-head .modify').data('Logistics',Logistics);
                $('#quotation-modify').find('.freight-way .freight-head .modify').data('Logisticsmodify',Logisticsmodify);
                //console.info(Logistics);
                $('#quotation-modify').find('.freight-way .freight-body .freight-detail .country-detail').html(Logistics.CountryNameEn);
                $('#freight-information .freight-body .destination .country-select').data('CountryPkId',Logistics.CountryPkId);
                $('#quotation-modify').find('.freight-way .freight-body .freight-detail .freight-word .transway .way').html(Logistics.DeliveryMethod);
                $('#quotation-modify').find('.freight-way .freight-body .freight-detail .freight-way-tip span').html(Logistics.DeliveryName);
                $('#quotation-modify').find('.freight-way .freight-body .total-money .money').html(numModify(Logistics.UsdFreightCurrentPrice));
                $('#quotation-modify').find('.freight-way').data('cost',numModify(Logistics.UsdFreightOriginalPrice));

                if(Logistics.DeliveryMethod == 'IP'){
                    $('#freight-information').find('.freight-way-border .IPway .icon-right').addClass('selected');
                    $('#freight-information').find('.freight-way-border .IEway .icon-right').removeClass('selected');
                }
                else{
                    $('#freight-information').find('.freight-way-border .IPway .icon-right').removeClass('selected');
                    $('#freight-information').find('.freight-way-border .IEway .icon-right').addClass('selected');
                }
                //调用ip ie 是否支持接口
                var pkid = Logistics.CountryPkId;
                $.ajax('https://www.mooddo.com/v1/Logistics/GetLogisticsCountryById?CountryPkId='+pkid,{
                    type:"get",
                    headers:{vp_token:vp.core.token},
                    contentType:"application/json",
                    success: function(data){
                        //console.error(data);
                        if(data.IsSuccess){
                            $('#quotation-modify').find('.freight-way .freight-head .modify').data('transway',data.Data);
                        }
                    }
                })
            }

            var $quotationgroup = $('#quotation-modify').find('#quotation-modify-exmple .quotation-item');

            $.each(quotationgroup,function(i,group){

                var ProductAttrs = [];
                //成本
                var cost = group.UsdProductOriginalPrice;
                console.log('cost',cost);
                var groupcombox = {
                    "img": group.ProucetImagePaths,
                    "attr": group.ProductAttrs,
                    "ProductQty": group.ProductQty,
                    "ProductCurrentPrice": group.UsdProductCurrentPrice,
                    "TotalGroupCurrentPrice": group.UsdTotalGroupCurrentPrice,
                    "TotalGroupCurrentSinglePrice": group.UsdTotalGroupCurrentSinglePrice
                }
                var $quotationgroupmodify = $quotationgroup.clone();
                $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('OrderQuotationPkId',group.OrderQuotationPkId);
                $('#quotation-modify').find('.quotation-modify-body .order-download .download').data('OrderQuotationPkId',group.OrderQuotationPkId);
                $quotationgroupmodify.find('.total-money .money-count').html(numModify(group.UsdTotalGroupCurrentPrice));

                $quotationgroupmodify.find('.quotation-head .modify').data('groupcombox',groupcombox);
                if (group.ProucetImagePaths.length==0) {
                    $quotationgroupmodify.find('.pic-container').html('未添加图片');
                }
                $.each(group.ProucetImagePaths,function(j,img){
                    $quotationgroupmodify.find('.pic-container').append(
                        '<div class="pic-border" style="background-image: url('+ img +')"></div>'
                    );
                    //$quotationgroupmodify.find('.pic-container .pic-border').css('background-image','url('+ img +')');

                })
                $quotationgroupmodify.find('.product-detail').append(
                    '<div class="product-attr">'
                    +'<span>Unit price: </span>'
                    +'<span></span>'
                    +'<span class="money fund-change">'+ numModify(group.UsdProductCurrentPrice) +'</span>'
                    +'</div>'
                );
                $.each(group.ProductAttrs,function(q,attr){
                    cost += attr.UsdProductAttributeOriginalPrice;
                    console.log('cost',attr.UsdProductAttributeOriginalPrice);
                    $quotationgroupmodify.find('.product-detail').append(
                        '<div class="product-attr">'
                        +'<span>'+ attr.AttributeName +': </span>'
                        +'<span>'+ attr.ProductAttributeValue +'</span>'
                        +'<span class="money fund-change">'+ numModify(attr.UsdProductAttributeCurrentPrice) +'</span>'
                        +'</div>'
                    );

                    var attrObj = {
                        'AttributeName': attr.AttributeName,
                        'ProductAttributeValue': attr.ProductAttributeValue,
                        'ProductAttributePkId': attr.ProductAttributePkId,
                        'ProductAttributeValuePkId': attr.ProductAttributeValuePkId,
                        'ProductAttributeOriginalPrice': attr.UsdProductAttributeOriginalPrice,
                        'ProductAttributeCurrentPrice': attr.UsdProductAttributeCurrentPrice
                    }
                    ProductAttrs.push(attrObj);

                })
                $quotationgroupmodify.find('.product-detail').append(
                    '<div class="product-attr">'
                    +'<span>quantity: </span>'
                    +'<span>'+ group.ProductQty +'</span>'
                    +'<span class="money fund-change">'+ numModify(group.UsdTotalGroupCurrentPrice) +'</span>'
                    +'</div>'
                );
                //console.log(ProductAttrs);
                var ProductAttrsData = {
                    "ProductOriginalPrice": group.UsdProductOriginalPrice,
                    "ProductCurrentPrice": group.UsdProductCurrentPrice,
                    "ProductQty": group.ProductQty,
                    "ProductAttrs": ProductAttrs,
                    "ProucetImagePaths": group.ProucetImagePaths
                };
                cost = cost*group.ProductQty;
                //储存修改组合数据
                $quotationgroupmodify.data('cost',cost);
                $quotationgroupmodify.data('ProductAttrsData',ProductAttrsData);
                $('#quotation-modify').find('.quotation-modify-body .quotation-group').append($quotationgroupmodify);

            })

            //报价单报价组合数据渲染
            //PkId = 32;
            $.ajax("https://www.mooddo.com/v1/Product/GetProdctByPkId?pkId="+ProductPkId, {
                type:"get",
                headers:{vp_token:vp.core.token},
                contentType:"application/json",
                success: function(data) {
                    console.log(data);
                    var quotationData = data.Data;
                    var images = quotationData.Images;
                    var baseprice = quotationData.USDPrice;
                    var attribute = quotationData.Attributes;
                    //报价组合数据填充

                    $('#productProperties article .aboutProduct .productno').html(productno);
                    $('#productProperties article .aboutProduct .product-name').html(productname);
                    $('#productProperties article .quantityOfPro .prodBasePrice .changeBtn em').html(numModify(baseprice*costRate));
                    $('#productProperties article .quantityOfPro .qtyInputer .basePricePerPcs em').html(numModify(baseprice*costRate));
                    ////
                    //图片银行图片更新
                    $('#pictureBank article .picLists .cl').html('');
                    $.each(images,function(i,img){
                        $('#pictureBank article .picLists .cl').append(
                            '<li><span class="icon-right"></span> <img src="'+ img.DispalyLink +'" alt=""></li>'
                        )
                    })
                    ///
                    var $attrList = $('#productProperties').find('article #attribute-exmple .prodProperty');

                    $.each(attribute,function(i,attr){
                        var $attrListModify = $attrList.clone();
                        $attrListModify.find('.propertyHeader .propertyName').html(attr.AttributeName);
                        $attrListModify.find('.propertyHeader .changeBtn em').html(numModify(attr.Values[0].AttributeValuePrice*costRate));
                        $.each(attr.Values,function(j,obj){
                            $attrListModify.find('.detailList').append(
                                '<li class="cl" data-id="'+ obj.PkId +'" data-attr-id="'+ attr.PkId +'">'
                                +'<span class="icon icon-right"></span><em>'+ obj.AttributeValueName +'</em><span class="detailPrice">+$<b>'+ numModify(obj.AttributeValueUSDPrice*costRate) +'</b></span>'
                                +'</li>'
                            )
                        })

                        $('#productProperties article').find('.propertyList').append($attrListModify);
                    })
                    layout_productProperties.layoutFun();
                }
            })

        }

    })

//报价单详情返回
    $('#quotation-modify').on('click','.quotation-title .back-border',function(e){
        e.stopPropagation();
        billReset();
        layout_pictureBank.reset();
        //重置报价单详情
        $('#quotation-modify .quotation-modify-body .quotation-group').html('');
        //报价组合重置
        $('#productProperties .propertyList').html('');
        $('#quotation-modify').addClass('hidden');
        $('#quotation').removeClass('hidden');
    })

//报价单编辑按钮
    $('#quotation').on('click','.quotation-title .edite-border',function(e){
        e.stopPropagation();
        $('#quotation .delete').show();
        $('#quotation .quotation-body-border').css('padding-top','2.6668rem')
        $('#quotation').find('.quotation-item').removeClass('abled');
        $(this).closest('.quotation-title').find('.select-menu').show();
        $('#quotation').find('.quotation-item .pic-border .icon-right-null').show();
        $('#quotation').find('.quotation-item .pic-border .icon-maoduo28').hide();
        $('#quotation').find('.loading_more .See-more').removeClass('abled');
    })

//报价单编辑选择
    $('#quotation').on('click','.quotation-item .item-head .pic-border .icon-right-null',function(){
        $(this).toggleClass('selected');
        var num = $('#quotation').find('.quotation-item .item-head .pic-border').find('.selected').length;
        if(num>0){
            $('#quotation .delete').addClass('abled');
        }
        else{
            $('#quotation .delete').removeClass('abled');
            $('#quotation .quotation-title .select-menu .select-all').html('全选');
            $('#quotation .quotation-title .select-menu .select-all').removeClass('isSelectAll');
        }
        $('#quotation .select-menu .select-num .select-count').html(num);
    })
//报价单全选
    $('#quotation').on('click','.quotation-title .select-menu .select-all',function(){
        var num = $('#quotation .quotation-list-item .quotation-item .item-head .pic-border .icon-right-null').length;
        if($(this).hasClass('isSelectAll')){
            $('#quotation .quotation-list-item .quotation-item .item-head .pic-border .icon-right-null').removeClass('selected');
            $(this).html('全选')
            $('#quotation .select-menu .select-num .select-count').html(0);
        }
        else{
            $('#quotation .quotation-list-item .quotation-item .item-head .pic-border .icon-right-null').addClass('selected');
            $(this).html('取消全选');
            $('#quotation .select-menu .select-num .select-count').html(num);
        }
        $(this).toggleClass('isSelectAll');
        if($('#quotation .quotation-list-item .quotation-item .item-head .pic-border .selected').length>0){
            $('#quotation .delete').addClass('abled');
        }
        else{
            $('#quotation .delete').removeClass('abled');
        }
    })

//报价单删除
    $('#quotation').on('click','.delete',function(){
        var pkIds = [];
        if($(this).hasClass('abled')){
            console.info(123)
            //$('#quotation').find('.quotation-item .item-head .pic-border .selected').closest('.quotation-item').remove();
            $(this).hide();
            $('#quotation .quotation-body-border').css('padding-top','1.3334rem');
            $('#quotation').find('.quotation-item .pic-border .icon-right-null').hide();
            $('#quotation').find('.quotation-item .pic-border .icon-maoduo28').show();
            $('#quotation').find('.quotation-title .select-menu').hide();
            $('#quotation .delete').removeClass('abled');
            $('#quotation').find('.quotation-item').addClass('abled');
            $('#quotation .quotation-title .select-menu .select-all').html('全选');
            $('#quotation .quotation-title .select-menu .select-all').removeClass('isSelectAll');
            $('#quotation').find('.select-menu .select-num .select-count').html('0');
            $('#quotation').find('.loading_more .See-more').addClass('abled');
            $('#quotation').find('.quotation-item .item-head .pic-border .selected').closest('.quotation-item').each(function(){
                //console.log($(this).data('quotationDetailData').PkId);
                pkIds.push($(this).data('quotationDetailData').PkId);
            })
            $.ajax("https://www.mooddo.com/v1/Order/DeleteOrderQuotation", {
                type:"post",
                headers:{vp_token:vp.core.token},
                data:JSON.stringify({PkIds: pkIds}),
                dataType: "json",
                contentType:"application/json",
                success: function(data) {
                    if(data.IsSuccess){
                        quotationpage = 1;
                        $('#quotation').find('.quotation-item .item-head .pic-border .selected').closest('.quotation-item').remove();
                    }
                    else{
                        vp.ui.pop('.icon-err','删除失败')
                    }

                }
            })
        }

    })
//自定义信息删除
    $('#quotation-modify').on('click','.note-modify .note-title .name-delete',function(){
        $(this).closest('.note-modify').hide();
        $(this).closest('.note-modify').removeClass('isShow');
        console.log($(this).closest('.quotation-modify-body'))
        $(this).closest('.quotation-modify-body').find('.modify-group .add-note').show();
    })
//添加自定义信息
    $('#quotation-modify').on('click','.modify-group .add-note',function(){
        $('#quotation-modify').addClass('hidden');
        $('#own-information').removeClass('hidden');

    })
//自定义信息修改
    $('#quotation-modify').on('click','.note-modify .note-title .name-modify',function(){
        $('#quotation-modify').addClass('hidden');
        $('#own-information').removeClass('hidden');
    })


//报价单编辑取消
    $('#quotation').on('click','.quotation-title .select-menu .cancel',function(){
        $('#quotation').find('.quotation-item .pic-border .icon-right-null').removeClass('selected');
        $('#quotation').find('.quotation-item .pic-border .icon-right-null').hide();
        $('#quotation').find('.quotation-item .pic-border .icon-maoduo28').show();
        $(this).closest('.select-menu').hide();
        $('#quotation').find('.delete').hide();
        $('#quotation').find('.delete').removeClass('abled');
        $('#quotation .quotation-body-border').css('padding-top','1.3334rem');
        $('#quotation').find('.quotation-item').addClass('abled');
        $('#quotation .quotation-title .select-menu .select-all').html('全选');
        $('#quotation .quotation-title .select-menu .select-all').removeClass('isSelectAll');
        $('#quotation .quotation-title .select-menu .select-num .select-count').html('0');
        $('#quotation').find('.loading_more .See-more').addClass('abled');
    })

//订单列表页显示
    $('#Bill').on('click','.item-menu .order',function(e){
        e.stopPropagation();
        $('#Bill').addClass('hidden');
        $('#deal-order').removeClass('hidden');
    })
//订单详情返回
    $('#deal-order').on('click','.order-head .back',function(){
        $('#Bill').removeClass('hidden');
        $('#deal-order').addClass('hidden');
    })
//订单详情页进入
    $('#deal-order').on('click','.order-list .order-item-card',function(){
        $('#order-track').removeClass('hidden');
        $('#deal-order').addClass('hidden');
    })
//订单详情页返回
    $('#Order-Detail').on('click','.quotation-title .back-border',function(){
        $('#Order-Detail').addClass('hidden');
        $('#order-track').removeClass('hidden');
    })
//订单列表页进入追踪页
    $('#deal-order').on('click','.order-list .order-item-card',function(){
        vp.ui.loading();
        var orderno = $(this).data('orderno');
        var ordercreatedate = $(this).data('createdate');
        var productname = $(this).data('productname');
        var ordersale = $(this).data('ordersale');
        var imgurl = $(this).data('imgurl');
        var pkId = $(this).data('pkid');
        var productpkId = $(this).data('productpkId');
        var orderDetailData = $(this).data('orderDetailData');
        //订单pkid数据储存
        $('#order-track .order-stop .stop-trade').data('pkId',pkId);


        //////////////////////////////////////////////
        ///订单详情页数据渲染
        var productno = orderDetailData.productno;
        var productname = orderDetailData.productname;
        var salesvolume = orderDetailData.salesvolume;
        var rmbsalesvolume = orderDetailData.rmbsalesvolume;
        var commissionproportion = orderDetailData.commissionproportion;
        var rmbcommissionproportion = orderDetailData.rmbcommissionproportion;
        var quotationgroup = orderDetailData.quotationgroup;
        var note = orderDetailData.note;
        var Logistics = orderDetailData.Logistics;

        //定价单下载所需的pkid
        $('#Order-Detail').find('.quotation-body .order-download .download').data('pkId',pkId);
        $('#Order-Detail').find('.product-order-num .product-num .productno').html(productno);
        $('#Order-Detail').find('.product-order-num .product-name').html(productname);
        limitText($('#Order-Detail').find('.product-order-num .product-name')[0]);
        $('#Order-Detail').find('.profit .account-balance .sale-volume .sale-num .money-count').html(numModify(salesvolume));
        $('#Order-Detail').find('.profit .account-balance .sale-volume .sale-num .money-count').data('usdmoney',numModify(salesvolume));
        $('#Order-Detail').find('.profit .account-balance .sale-volume .sale-num .money-count').data('rmbmoney',numModify(rmbsalesvolume));

        $('#Order-Detail').find('.profit .account-balance .withdraw-deposit .deposit-num .money-count').html(numModify(commissionproportion));
        $('#Order-Detail').find('.profit .account-balance .withdraw-deposit .deposit-num .money-count').data('usdmoney',numModify(commissionproportion));
        $('#Order-Detail').find('.profit .account-balance .withdraw-deposit .deposit-num .money-count').data('rmbmoney',numModify(rmbcommissionproportion));
        if(!note){
            $('#Order-Detail').find('.own-note').hide();
        }
        else{
            $('#Order-Detail').find('.own-note').show();
            $('#Order-Detail').find('.own-note .note-content').html(note);
        }

        //货运信息
        if(!Logistics){
            $('#Order-Detail').find('.freight-way').hide();
        }
        else {

            $('#Order-Detail').find('.freight-way').show();

            //货运信息数据填充

            //console.info(Logistics);
            $('#Order-Detail').find('.freight-way .freight-body .freight-detail .country-detail').html(Logistics.CountryNameEn);
            $('#Order-Detail').find('.freight-way .freight-body .freight-detail .freight-word .transway .way').html(Logistics.DeliveryMethod);
            $('#Order-Detail').find('.freight-way .freight-body .freight-detail .freight-way-tip span').html(Logistics.DeliveryName);
            $('#Order-Detail').find('.freight-way .freight-body .total-money .money').html(numModify(Logistics.UsdFreightCurrentPrice));
            $('#Order-Detail').find('.freight-way .freight-body .total-money .money').data('usdmoney',numModify(Logistics.UsdFreightCurrentPrice));
            $('#Order-Detail').find('.freight-way .freight-body .total-money .money').data('rmbmoney',numModify(Logistics.FreightCurrentPrice));

            ///////////////////////////////////////////////
        }
        var $quotationgroup = $('#Order-Detail').find('#quotation-group-exmple .quotation-item');
        $.each(quotationgroup,function(i,group){

            var $quotationgroupmodify = $quotationgroup.clone();
            $quotationgroupmodify.find('.total-money .money-count').html(numModify(group.UsdTotalGroupCurrentPrice));
            $quotationgroupmodify.find('.total-money .money-count').data('usdmoney',numModify(group.UsdTotalGroupCurrentPrice));
            $quotationgroupmodify.find('.total-money .money-count').data('rmbmoney',numModify(group.TotalGroupCurrentPrice));
            if(group.ProucetImagePaths.length==0){
                group.ProucetImagePaths.push('images/quotation_blank.png');
            }
            $.each(group.ProucetImagePaths,function(j,img){
                $quotationgroupmodify.find('.pic-container').append(
                    '<div class="pic-border" style="background-image: url('+ img +')"></div>'
                );
                //$quotationgroupmodify.find('.pic-container .pic-border').css('background-image','url('+ img +')');

            })
            $quotationgroupmodify.find('.product-detail').append(
                '<div class="product-attr">'
                +'<span>Unit price: </span>'
                +'<span></span>'
                +'<span class="money fund-change" data-usdmoney="'+ numModify(group.UsdProductCurrentPrice) +'" data-rmbmoney="'+ numModify(group.ProductCurrentPrice) +'">'+ numModify(group.UsdProductCurrentPrice) +'</span>'
                +'</div>'
            );
            $.each(group.ProductAttrs,function(q,attr){
                $quotationgroupmodify.find('.product-detail').append(
                    '<div class="product-attr">'
                    +'<span>'+ attr.AttributeName +': </span>'
                    +'<span>'+ attr.ProductAttributeValue +'</span>'
                    +'<span class="money fund-change" data-usdmoney="'+ numModify(attr.UsdProductAttributeCurrentPrice) +'" data-rmbmoney="'+ numModify(attr.ProductAttributeCurrentPrice) +'">'+ numModify(attr.UsdProductAttributeCurrentPrice) +'</span>'
                    +'</div>'
                );
            })
            $quotationgroupmodify.find('.product-detail').append(
                '<div class="product-attr">'
                +'<span>quantity: </span>'
                +'<span>'+ group.ProductQty +'</span>'
                +'<span class="money fund-change" data-usdmoney="'+ numModify(group.UsdTotalGroupCurrentPrice) +'" data-rmbmoney="'+ numModify(group.TotalGroupCurrentPrice) +'">'+ numModify(group.UsdTotalGroupCurrentPrice) +'</span>'
                +'</div>'
            );
            $('#Order-Detail').find('.quotation-body .quotation-group').append($quotationgroupmodify);

        })
        /////////////////////////////////////////


        //订单追踪数据填充
        $('#order-track').find('.order-card .order-head .order-num .order-count').html(orderno);
        $('#order-track').find('.order-card .order-head .order-date').html(ordercreatedate);
        $('#order-track').find('.order-card .card-body .product-name-word').html(productname);
        $('#order-track').find('.order-card .card-body .product-money .price').html(numModify(salesvolume));
        if(imgurl){
            $('#order-track').find('.order-card .card-body .pic-border').css('background-image','url('+ imgurl +')');
        }

        ///订单追踪接口//////
        $.ajax("https://www.mooddo.com/v1/Order/GetOrderTracking", {
            type:"post",
            headers:{vp_token:vp.core.token},
            data:JSON.stringify({OrderPkId: pkId}),
            dataType: "json",
            contentType:"application/json",
            success: function(data) {
                vp.ui.removeLoading();
                console.log(data);
                var trackorderlist = data.Data;
                //trackorderlist. reverse();
                var $height = (trackorderlist.length-1)*1.9434+'rem';
                $('#order-track').find('.order-track-detail .dot-border .line').css('height',$height);
                $.each(trackorderlist,function(i,trackorder){
                    //控制订单追踪流程图
                    $('#order-track').find('.order-track-detail .dot-border').append('<div class="dot dot'+(i+1)+'"></div>');
                    $('#order-track').find('.order-track-detail .dot-border .dot'+(i+1)).css('top',i*1.9434+'rem');
                    //修改右边订单状态和时间
                    $('#order-track').find('.order-track-detail .order-track-border').append(
                        '<div class="track-state">'
                        +'<div class="word">'+ trackorder.Action +'</div>'
                        +'<span class="date">'+ trackorder.CreateDateChn +'</span>'
                        +'</div>'
                    )
                })
            }
        })
        //////
        $('#Bill').addClass('hidden');
        $('#order-track').removeClass('hidden');
    })
////

////
//单据首页进入追踪页
    $('#Bill').on('click','.trading-list .order-item-card',function(){
        var orderno = $(this).data('orderno');
        var ordercreatedate = $(this).data('createdate');
        var productname = $(this).data('productname');
        var ordersale = $(this).data('ordersale');
        var imgurl = $(this).data('imgurl');
        var pkId = $(this).data('pkid');
        var productpkId = $(this).data('productpkId');
        var orderDetailData = $(this).data('orderDetailData');
        //订单pkid数据储存
        $('#order-track .order-stop .stop-trade').data('pkId',pkId);


        //////////////////////////////////////////////
        ///订单详情页数据渲染
        var productno = orderDetailData.productno;
        var productname = orderDetailData.productname;
        var salesvolume = orderDetailData.salesvolume;
        var rmbsalesvolume = orderDetailData.rmbsalesvolume;
        var commissionproportion = orderDetailData.commissionproportion;
        var rmbcommissionproportion = orderDetailData.rmbcommissionproportion;
        var quotationgroup = orderDetailData.quotationgroup;
        var note = orderDetailData.note;
        var Logistics = orderDetailData.Logistics;
        //定价单下载所需的pkid
        $('#Order-Detail').find('.quotation-body .order-download .download').data('pkId',pkId);
        $('#Order-Detail').find('.product-order-num .product-num .productno').html(productno);
        $('#Order-Detail').find('.product-order-num .product-name').html(productname);

        $('#Order-Detail').find('.profit .account-balance .sale-volume .sale-num .money-count').html(numModify(salesvolume));
        $('#Order-Detail').find('.profit .account-balance .sale-volume .sale-num .money-count').data('usdmoney',numModify(salesvolume));
        $('#Order-Detail').find('.profit .account-balance .sale-volume .sale-num .money-count').data('rmbmoney',numModify(rmbsalesvolume));

        $('#Order-Detail').find('.profit .account-balance .withdraw-deposit .deposit-num .money-count').html(numModify(commissionproportion));
        $('#Order-Detail').find('.profit .account-balance .withdraw-deposit .deposit-num .money-count').data('usdmoney',numModify(commissionproportion));
        $('#Order-Detail').find('.profit .account-balance .withdraw-deposit .deposit-num .money-count').data('rmbmoney',numModify(rmbcommissionproportion));
        if(!note){
            $('#Order-Detail').find('.own-note').hide();
        }
        else{
            $('#Order-Detail').find('.own-note').show();
            $('#Order-Detail').find('.own-note .note-content').html(note);
        }

        //货运信息
        if(!Logistics){
            $('#Order-Detail').find('.freight-way').hide();
        }
        else {

            $('#Order-Detail').find('.freight-way').show();

            //货运信息数据填充

            //console.info(Logistics);
            $('#Order-Detail').find('.freight-way .freight-body .freight-detail .country-detail').html(Logistics.CountryNameEn);
            $('#Order-Detail').find('.freight-way .freight-body .freight-detail .freight-word .transway .way').html(Logistics.DeliveryMethod);
            $('#Order-Detail').find('.freight-way .freight-body .freight-detail .freight-way-tip span').html(Logistics.DeliveryName);
            $('#Order-Detail').find('.freight-way .freight-body .total-money .money').html(numModify(Logistics.UsdFreightCurrentPrice));
            $('#Order-Detail').find('.freight-way .freight-body .total-money .money').data('usdmoney',numModify(Logistics.UsdFreightCurrentPrice));
            $('#Order-Detail').find('.freight-way .freight-body .total-money .money').data('rmbmoney',numModify(Logistics.FreightCurrentPrice));

            ///////////////////////////////////////////////
        }
        var $quotationgroup = $('#Order-Detail').find('#quotation-group-exmple .quotation-item');
        $.each(quotationgroup,function(i,group){

            var $quotationgroupmodify = $quotationgroup.clone();
            $quotationgroupmodify.find('.total-money .money-count').html(numModify(group.UsdTotalGroupCurrentPrice));
            $quotationgroupmodify.find('.total-money .money-count').data('usdmoney',numModify(group.UsdTotalGroupCurrentPrice));
            $quotationgroupmodify.find('.total-money .money-count').data('rmbmoney',numModify(group.TotalGroupCurrentPrice));
            //console.log(group.ProucetImagePaths)
            if(group.ProucetImagePaths.length==0){
                group.ProucetImagePaths.push('images/quotation_blank.png');
            }
            $.each(group.ProucetImagePaths,function(j,img){

                $quotationgroupmodify.find('.pic-container').append(
                    '<div class="pic-border" style="background-image: url('+ img +')"></div>'
                );
                //$quotationgroupmodify.find('.pic-container .pic-border').css('background-image','url('+ img +')');

            })
            $quotationgroupmodify.find('.product-detail').append(
                '<div class="product-attr">'
                +'<span>Unit price: </span>'
                +'<span></span>'
                +'<span class="money fund-change" data-usdmoney="'+ numModify(group.UsdProductCurrentPrice) +'" data-rmbmoney="'+ numModify(group.ProductCurrentPrice) +'">'+ numModify(group.UsdProductCurrentPrice) +'</span>'
                +'</div>'
            );
            $.each(group.ProductAttrs,function(q,attr){
                $quotationgroupmodify.find('.product-detail').append(
                    '<div class="product-attr">'
                    +'<span>'+ attr.AttributeName +': </span>'
                    +'<span>'+ attr.ProductAttributeValue +'</span>'
                    +'<span class="money fund-change" data-usdmoney="'+ numModify(attr.UsdProductAttributeCurrentPrice) +'" data-rmbmoney="'+ numModify(attr.ProductAttributeCurrentPrice) +'">'+ numModify(attr.UsdProductAttributeCurrentPrice) +'</span>'
                    +'</div>'
                );
            })
            $quotationgroupmodify.find('.product-detail').append(
                '<div class="product-attr">'
                +'<span>quantity: </span>'
                +'<span>'+ group.ProductQty +'</span>'
                +'<span class="money fund-change" data-usdmoney="'+ numModify(group.UsdTotalGroupCurrentPrice) +'" data-rmbmoney="'+ numModify(group.TotalGroupCurrentPrice) +'">'+ numModify(group.UsdTotalGroupCurrentPrice) +'</span>'
                +'</div>'
            );
            $('#Order-Detail').find('.quotation-body .quotation-group').append($quotationgroupmodify);

        })
        /////////////////////////////////////////


        //订单追踪数据填充
        $('#order-track').find('.order-card .order-head .order-num .order-count').html(orderno);
        $('#order-track').find('.order-card .order-head .order-date').html(ordercreatedate);
        $('#order-track').find('.order-card .card-body .product-name-word').html(productname);
        $('#order-track').find('.order-card .card-body .product-money .price').html(numModify(salesvolume));
        if(imgurl){
            $('#order-track').find('.order-card .card-body .pic-border').css('background-image','url('+ imgurl +')');
        }
        ///订单追踪接口//////
        $.ajax("https://www.mooddo.com/v1/Order/GetOrderTracking", {
            type:"post",
            data:JSON.stringify({OrderPkId: pkId}),
            dataType: "json",
            headers:{vp_token:vp.core.token},
            contentType:"application/json",
            success: function(data) {
                console.log(data);
                var trackorderlist = data.Data;
                var $height = (trackorderlist.length-1)*1.9434+'rem';
                $('#order-track').find('.order-track-detail .dot-border .line').css('height',$height);
                $.each(trackorderlist,function(i,trackorder){
                    //控制订单追踪流程图
                    $('#order-track').find('.order-track-detail .dot-border').append('<div class="dot dot'+(i+1)+'"></div>');
                    $('#order-track').find('.order-track-detail .dot-border .dot'+(i+1)).css('top',i*1.9434+'rem');
                    //修改右边订单状态和时间
                    $('#order-track').find('.order-track-detail .order-track-border').append(
                        '<div class="track-state">'
                        +'<div class="word">'+ trackorder.Action +'</div>'
                        +'<span class="date">'+ trackorder.CreateDateChn +'</span>'
                        +'</div>'
                    )
                })
            }
        })
        //////
        $('#Bill').addClass('hidden');
        $('#order-track').removeClass('hidden');

    })

//追踪页面进入详情页
    $('#order-track').on('click','.order-card',function(){
        $('#order-track').addClass('hidden');
        $('#Order-Detail').removeClass('hidden');
        limitText($('#Order-Detail').find('.product-order-num .product-name')[0]);
    })

//追踪页面返回
    $('#order-track').on('click','.order-track-head .back',function(){
        //清空追踪页数据
        //追踪页数据清空
        $('#order-track').find('.order-track-detail .order-track-line .dot-border .line').siblings().remove();
        $('#order-track').find('.order-track-detail .order-track-line .order-track-border').html('');
        //订单详情页数据清空
        $('#Order-Detail').find('.quotation-body .quotation-group').html('');
        $('#order-track').addClass('hidden');
        $('#deal-order').removeClass('hidden');
    })

//订单终止按钮
    $('#order-track').on('click','.order-stop .stop-trade',function(){
        var pkId = $(this).data('pkId');
        $('#stop-order').find('.stop-head .submit').data('pkId',pkId);
        $('#order-track').addClass('hidden');
        $('#stop-order').removeClass('hidden');
    })

//中止申请取消
    $('#stop-order').on('click','.stop-head .cancel',function(){
        $('#order-track').removeClass('hidden');
        $('#stop-order').addClass('hidden');
    })

//中止提交
    $('#stop-order').on('click','.stop-head .submit',function(){
        var pkId = $(this).data('pkId');

        if($('#stop-order .reason-border textarea').val()){

            $.ajax("https://www.mooddo.com/v1/Order/SubmitOrderEndTransaction", {
                type:"post",
                headers:{vp_token:vp.core.token},
                data:JSON.stringify({
                    "OrderPkId": pkId,
                    "Status": "订单反馈",
                    "Reason": $('#stop-order .reason-border textarea').val()
                }),
                dataType: "json",
                contentType:"application/json",
                success: function(data) {
                    //console.log(data)
                    if(data.IsSuccess){
                        $('#order-track').removeClass('hidden');
                        $('#stop-order').addClass('hidden');
                    }else{
                        vp.ui.pop('icon-err','提交失败');
                    }
                }
            })
        }
        else{
            vp.ui.pop('icon-err','反馈内容不能为空','请填写');
        }

    })
//自定义信息确定
    $('#own-information').on('click','.stop-head .submit',function(){
        if($('#own-information .reason-border textarea').val()){
            $('#quotation-modify').removeClass('hidden');
            $('#own-information').addClass('hidden');
            $('#quotation-modify').find('.quotation-modify-body .note-modify .note-word').html($('#own-information .reason-border textarea').val());
            $('#quotation-modify').find('.modify-group .add-note').hide();
            $('#quotation-modify').find('.note-modify').show();
            $('#quotation-modify .note-modify').addClass('isShow');
        }
        else{
            vp.ui.pop('icon-err','自定义信息不能为空','请填写自定义信息');
        }

    })
//自定义信息取消按钮
    $('#own-information').on('click','.stop-head .cancel',function(){
        $('#quotation-modify').removeClass('hidden');
        $('#own-information').addClass('hidden');
        //$('#quotation-modify').find('.modify-group .add-note').hide();
        //$('#quotation-modify').find('.note-modify').show();
    })
//修改产品组合按钮
    $('#quotation-modify').on('click','.quotation-item .quotation-head .modify',function(){
        billReset();
        //给修改的组合做标记
        $(this).closest('.quotation-item').addClass('modify').siblings().removeClass('modify');
        //console.log($(this).data('groupcombox'));
        $('#productProperties footer .sureBtn').data('state',1);
        var data = $(this).data('groupcombox');
        console.log(data);
        //图片填充
        if(data.img.length>0){
            $('#productProperties .productPicList .picUl').html('');
        }else{
            $('#productProperties .productPicList .picUl').html('<li><img class="default" src="images/quotation_add.png" alt=""></li>');
        }
        $.each(data.img,function(i,img){
            $('#productProperties .productPicList .picUl').append(
                '<li><img src="'+ img +'"/></li>'
            )
        })

//修改报价组合时，数据修改
        $('#productProperties .qtyTitle .changeQtyBtn #product-num').html(data.ProductQty);
        $('#productProperties .prodBasePrice .changeBtn em').html(numModify(data.ProductCurrentPrice));
        $('#productProperties footer .pricePerPcs em').html(numModify(data.TotalGroupCurrentSinglePrice));
        $('#productProperties footer .salesNum em').html(numModify(data.TotalGroupCurrentPrice));
        $("#productProperties").removeClass('hidden');
        $('#quotation-modify').addClass('hidden');
        $('#productProperties article .propertyList .prodProperty').each(function(i,ele){
            //console.log(ele)
            console.log(i)
            console.log(data);
            var attrbuteid = data.attr[i].ProductAttributeValuePkId;

            $(ele).find('.propertyHeader .changeBtn em').html(numModify(data.attr[i].UsdProductAttributeCurrentPrice));
            if($(ele).find('.detailList').find('[data-id="'+ attrbuteid +'"]').length>0){
                $(ele).find('.detailList').find('[data-id="'+ attrbuteid +'"]').addClass('choosed');
            }
            else{
                $(ele).find('.detailList').find('.cl:first-child').addClass('choosed');
            }
            //console.error($(ele).find('.detailList').find('[data-id="'+ ProductAttributePkId +'"]'))
        })
        /////////

    })

//产品组合删除按钮
    $('#quotation-modify').on('click','.quotation-item .quotation-head .group-delete',function(){
        $(this).closest('.quotation-item').remove();
        //getSale();
        getTransFee(true);
    })
//添加产品组合
    $('#quotation-modify').on('click','.modify-group .add-product',function(){
        billReset();
        layout_pictureBank.reset();
        //用于区分产品组合保存时是新建还是修改
        $('#productProperties footer .sureBtn').data('state',0);
        $('#productProperties .propertyList .prodProperty .detailList li').removeClass('choosed');
        $('#productProperties .propertyList .prodProperty .detailList li:first-child').addClass('choosed');
        //初始化产品数量
        $('#productProperties .quantityOfPro .qtyTitle .changeQtyBtn .firstShow').html('请输入产品数量');
        $('#productProperties .quantityOfPro .qtyTitle .changeQtyBtn #product-num').html('');
        //初始化价格
        $('#productProperties .quantityOfPro .prodBasePrice .changeBtn em').html($('#productProperties .quantityOfPro .qtyInputer em').html());
        $("#productProperties .propertyList .propertyHeader .changeBtn em").each(function(){
            $(this).html($(this).closest('.prodProperty').find('.detailList li:first-child .detailPrice b').html());
        })
        //初始化图片框
        $("#productProperties .productPicList .picUl").html('<li><img class="default" src="images/quotation_add.png" alt=""></li>');
        $("#productProperties").removeClass('hidden');
        $('#quotation-modify').addClass('hidden');
    })

//订单下载
    $('#Order-Detail').on('click','.quotation-body .order-download .download',function () {

        var pkId = $(this).data('pkId');
        var currency = 0;
        if($('#quotation-modify .quotation-title .money-change-border .icon-maoduo41').hasClass('hidden')){
            currency = 0;
        }
        else{
            currency = 1;
        }
        var type = getPhoneType();

        if(type =='Iphone'){
            urltoken = encodeURIComponent (vp.core.token);
            window.open('https://www.mooddo.com/v1/Order/DownLoadOrder?OrderPkId='+ pkId +'&CurrentCurrency='+currency+'&vp_token='+urltoken)
        }
        else if(type == 'Android') {
            $.ajax('https://www.mooddo.com/v1/Order/GetOrderPath?CurrentCurrency='+ currency +'&OrderPkId=' + pkId,{
                type:"get",
                headers:{vp_token:vp.core.token},
                contentType:"application/json",
                success: function(data){
                    console.log(data);
                    if(data){
                        download(data);
                    }
                }
            })
        }


    })
//报价单下载
    $('#quotation-modify').on('click','.quotation-modify-body .order-download .download',function(){
        var pkId = $(this).data('OrderQuotationPkId');
        var currency = 0;
        if($('#quotation-modify .quotation-title .money-change-border .icon-maoduo41').hasClass('hidden')){
            currency = 0;
        }
        else{
            currency = 1;
        }
        var type = getPhoneType();

        if(type =='Iphone'){
            urltoken = encodeURIComponent (vp.core.token);
            window.open('https://www.mooddo.com/v1/Order/DownLoadQuotations?OrderQuotationPkId='+ pkId +'&CurrentCurrency='+currency+'&vp_token='+urltoken)
        }
        else if(type == 'Android') {
            $.ajax('https://www.mooddo.com/v1/Order/GetQuotationsPath?CurrentCurrency='+ currency +'&OrderQuotationPkId=' + pkId,{
                type:"get",
                headers:{vp_token:vp.core.token},
                contentType:"application/json",
                success: function(data){
                    console.log(data);
                    if(data){
                        download(data);
                    }
                }
            })
        }

    })
//产品组合页面
//产品属性组合
//数据格式化
    function numModify(data){
        return (Math.floor(data * 1000) / 1000).toFixed(2);
    }


    layout_productProperties = {
        layoutFun:function(){

            //初始化
            //header按钮
            $("#productProperties").find("header .cancelBtn").on("click",function(){
                $("#productProperties").addClass('hidden');
                $('#quotation-modify').removeClass('hidden');
                //选项选择重置
                $('#productProperties .propertyList .prodProperty .detailList .cl').removeClass('choosed');
                $("#productProperties .productPicList .picUl").html('');
            })
            $("#productProperties").find("footer .sureBtn").on("click",function(){
                //$("#productProperties").addClass('hidden');
                //$('#quotation-modify').removeClass('hidden');
            })
            //产品数量修改
            $('#productProperties').on('click','.quantityOfPro .qtyTitle .changeQtyBtn',function(){
                //$(this).find('.fee-count').hide();
                $(this).find('input').show();
                $(this).find('input').focus();
            })
            $('#productProperties').on('blur','#product-num-input',function(){

                $('#productProperties').one('click',function(e){
                    if($(e.target).closest('#product-num-border').length==0){
                        //$('#fee-modify').find('.fee-count').show();
                        $('#product-num-border').find('#product-num-input').hide();
                        if($('#product-num-input').val()&&parseInt($('#product-num-input').val())>0&&parseInt($('#product-num-input').val())<100000000){
                            $('#product-num-border').find('#product-num').html(parseInt($('#product-num-input').val()));
                            $('#product-num-border .firstShow').html('修改数量');
                            countSinglePrice();
                        }
                        $('#product-num-input').val('');
                    }
                })
            })

            //单项属性选择
            //var $propertyLists = $("#productProperties .propertyList");
            $("#productProperties").on('click','.propertyList .detailList li',function(e){
                e.stopPropagation();
                $("#productProperties").click();
                //$propertyLists.find("li").on("click",function(){
                var thisPrice = $(this).find("b").html();
                $(this).addClass("choosed").siblings("li").removeClass("choosed");
                $(this).parents(".prodProperty").find(".changeBtn em").html(thisPrice)
                countSinglePrice();
            })

            //修改报价
            //修改报价

            $("#productProperties").on("click",'.changeBtn',function(e){
                $(this).find('input').show();
                $(this).find('input').focus();
            })
            $("#productProperties").on('blur','.changeBtn input',function(){
                var $ele = $(this).closest('.changeBtn');
                $('#productProperties').one('click',function(e){
                    if(!$(e.target).closest('.changeBtn').is($ele)){
                        $ele.find('input').hide();
                        if($ele.find('input').val()&&parseFloat($ele.find('input').val())>=0&&parseFloat($ele.find('input').val())<100000000){
                            $ele.find('em').html(parseFloat($ele.find('input').val()));

                            countSinglePrice();
                        }
                        $ele.find('input').val('');
                    }

                })

                countSinglePrice();

            })

            //计算价格
            function countSinglePrice(){
                var $changeBtn = $("#productProperties .changeBtn");
                //计算单价
                var singlePrice = 0;
                var $em = $changeBtn.find("em");
                $em.each(function(i,ele){

                    singlePrice = singlePrice + parseFloat($(this).html());
                });
                $("#productProperties .pricePerPcs em").html(singlePrice.toFixed(2));
                //计算总价
                var productQuantity = $('#product-num').html();
                if(!isNaN(productQuantity)){
                    var saleMoney = singlePrice * productQuantity;
                    $("#productProperties .salesNum em").html(saleMoney.toFixed(2));

                }
            }
            countSinglePrice();
            //添加图片
            //var $productPicList = $("#productProperties article .productPicList");
            $('#productProperties').on('click','article .productPicList .picUl li',function(){
                //$productPicList.find("li").on("click",function(){
                $("#productProperties").addClass("hidden").siblings("#pictureBank").removeClass("hidden");
            })
        }
    };

    var layout_pictureBank = {
        picNum: 0,
        reset: function(){
            $("#pictureBank .picLists p em").html(0);
            $('#pictureBank .picLists li span').removeClass('spanShow');
            this.picNum = 0;
        },
        layoutFun:function(){
            //图片选择
            var $li = $("#pictureBank .picLists li"),
                $choosedNum = $("#pictureBank .picLists p em"),
                _this = this;
            $('#pictureBank').on('click','.picLists li',function(){
                //$li.on("click",function(){
                var $span = $(this).find("span");
                if($span.is(":visible") && _this.picNum <= 3){
                    $span.removeClass("spanShow");
                    _this.picNum--;
                }else if(_this.picNum < 3){
                    $span.addClass("spanShow");
                    _this.picNum++;
                }
                $choosedNum.html(_this.picNum);
            })
            //取消按钮
            $("#pictureBank").find("header .cancelBtn").on("click",function(){
                $("#productProperties").removeClass("hidden").siblings("#pictureBank").addClass("hidden");
            })
            //确定按钮
            $("#pictureBank").find("header .sureBtn").on("click",function(){
                $("#productProperties").removeClass("hidden").siblings("#pictureBank").addClass("hidden");
                //插入图片
                var $selectedImgs = $("#pictureBank .picLists li").find(".spanShow").siblings("img");
                var $srcArr = [];
                $selectedImgs.each(function(){
                    var src = $(this).attr("src");
                    $srcArr.push(src);
                })
                //console.log($srcArr);
                //var $insertImg = $("<li><img></img></li>");
                var imgLists = "";
                for(var i=0; i<$srcArr.length; i++){
                    imgLists = imgLists + '<li><img src="' + $srcArr[i] + '"/></li>';
                }
                console.error(imgLists)
                if(imgLists){
                    $("#productProperties").find(".productPicList .picUl").html(imgLists);
                }
                else{
                    $("#productProperties").find(".productPicList .picUl").html('<li><img class="default" src="images/quotation_add.png" alt=""></li>');
                }

                //$("#pictureBank .picLists li .icon-right-cir").removeClass('spanShow');
                //$choosedNum.html(0);
                //添加图片事件绑定
                var $productPicList = $("#productProperties article .productPicList");
                $productPicList.find("li").on("click",function(){
                    $("#productProperties").addClass("hidden").siblings("#pictureBank").removeClass("hidden");
                })
            })
        }
    }

    layout_pictureBank.layoutFun();
//layout_productProperties.layoutFun();
    var orderpage = 1;
//获取订单接口
    function getOrders(pagesize,currentpage,isFirstLoad,isFirstPage) {
        $.ajax("https://www.mooddo.com/v1/Order/GetOrders?pageSize="+ pagesize +"&pageIndex="+currentpage, {
            type:"post",
            headers:{vp_token:vp.core.token},
            success: function(data) {
                console.log(data);
                if(!isFirstPage){
                    orderpage++;
                }
                var pagecount = data.PageCount;
                if(pagecount==0){
                    $('#Bill .bill-body .no-data').show();
                    $('#deal-order .deal-order-border .no-data').show();
                    return false;
                }
                var orderList = data.Data;
                //订单列表循环
                var $order = $('#Bill').find('#example-order .order-item-card');
                //去掉加载动态图
                $('.load_animation').addClass('hidden');
                if(orderList.length>0){
                    $.each(orderList,function(i,order){
                        var createdate = order.CreateDateChn;
                        var productname = order.ProductName;
                        var ordersale = order.UsdOrderTotalSales;
                        var status = order.OrderStatus;
                        var imgurl = order.ProductMainImagePath;
                        var orderno = order.OrderNo;
                        var pkid = order.PkId;
                        //订单pkid
                        var productpkId = order.ProductPkId;
                        //自定义信息
                        var note = order.CustomInfo;
                        //订单详情
                        var quotationgroup = order.QuotationGroup;
                        //产品编号
                        var productno = order.ProductNo;

                        //销售额
                        var salesvolume = order.UsdOrderTotalSales;
                        var rmbsalesvolume = order.OrderTotalSales;
                        //提成金额
                        var commissionproportion = order.UsdCommissionProportion;
                        var rmbcommissionproportion = order.CommissionProportion;
                        //货运信息
                        var Logistics = order.Logistics;
                        //订单详情页
                        var orderDetailData ={
                            'note': note,
                            'pkid': pkid,
                            'productno': productno,
                            'productname': productname,
                            'salesvolume': salesvolume,
                            'rmbsalesvolume': rmbsalesvolume,
                            'commissionproportion': commissionproportion,
                            'rmbcommissionproportion': rmbcommissionproportion,
                            'quotationgroup': quotationgroup,
                            'Logistics': Logistics
                        }


                        //console.log($order[0])
                        var $ordermodify = $order.clone();
                        //后台返回数据填充
                        $ordermodify.find('.card-head .date').html(createdate);
                        if(imgurl){
                            $ordermodify.find('.card-body .pic-border').css('background-image','url('+ imgurl +')');
                        }

                        $ordermodify.find('.product-introduce .product .product-name').html(productname);
                        $ordermodify.find('.product-introduce .price .money-num').html(numModify(salesvolume));
                        $ordermodify.find('.card-foot .state').html(status);
                        //储存数据，供下一页使用
                        $ordermodify.data('orderno',orderno);
                        $ordermodify.data('createdate',createdate);
                        $ordermodify.data('productname',productname);
                        $ordermodify.data('ordersale',ordersale);
                        $ordermodify.data('imgurl',imgurl);
                        $ordermodify.data('pkid',pkid);
                        $ordermodify.data('productpkId',productpkId);
                        $ordermodify.data('orderDetailData',orderDetailData);
                        if(isFirstLoad){
                            $('#Bill').find('.trading-list').append($ordermodify);
                            $('#deal-order').find('.order-list').append($ordermodify.clone(true));
                        }
                        else{
                            if(isFirstPage){
                                $('#Bill').find('.trading-list').append($ordermodify);
                            }
                            else{
                                $('#deal-order').find('.order-list').append($ordermodify.clone(true));
                            }
                        }

                    })
                    if(isFirstLoad){
                        ////控制底部加载按钮的显示隐藏////
                        $('#Bill').find('.loading_more').removeClass('hidden');
                        $('#deal-order').find('.loading_more').removeClass('hidden');
                        if(pagecount==currentpage){
                            $('#Bill').find('.loading_more .No-more').removeClass('hidden');
                            $('#Bill').find('.loading_more .See-more').addClass('hidden');
                            $('#deal-order').find('.loading_more .No-more').removeClass('hidden');
                            $('#deal-order').find('.loading_more .See-more').addClass('hidden');
                        }else{
                            $('#Bill').find('.loading_more .No-more').addClass('hidden');
                            $('#Bill').find('.loading_more .See-more').removeClass('hidden');
                            $('#deal-order').find('.loading_more .No-more').addClass('hidden');
                            $('#deal-order').find('.loading_more .See-more').removeClass('hidden');
                        }
                    }
                    else{
                        if(isFirstPage){
                            $('#Bill').find('.loading_more').removeClass('hidden');
                            $('#Bill').find('.loading_more .No-more').removeClass('hidden');
                            $('#Bill').find('.loading_more .See-more').addClass('hidden');
                        }
                        else{

                            $('#deal-order').find('.loading_more').removeClass('hidden');
                            if(pagecount==currentpage){
                                $('#deal-order').find('.loading_more .No-more').removeClass('hidden');
                                $('#deal-order').find('.loading_more .See-more').addClass('hidden');
                            }
                        }
                    }


                }
                else{

                }

            }
        });
    }

    getOrders(10,1,true,true);
//
//单据首页点击加载更多
    $('#Bill').on('click','.See-more',function(){
        $('.load_animation').removeClass('hidden');
        $('#Bill').find('.loading_more').addClass('hidden');

        getOrders(10,2,false,true);
    })
//订单列表页点击加载更多
    $('#deal-order').on('click','.See-more',function(){
        $('.load_animation').removeClass('hidden');
        $('#deal-order').find('.loading_more').addClass('hidden');

        getOrders(10,orderpage,false,false);
    })
//获取报价单列表
    function getOrderQuotations(pagesize,currentpage){
        $.ajax("https://www.mooddo.com/v1/Order/GetOrderQuotations?pageSize="+ pagesize +"&pageIndex="+currentpage, {
            type:"post",
            headers:{vp_token:vp.core.token},
            success: function(data) {
                console.log(data);
                quotationpage++;
                var pagecount = data.PageCount;
                //没有数据时
                if(pagecount==0){
                    $('#quotation .quotation-body-border .no-data').show();
                    return false;
                }
                var quotationList = data.Data;
                var $quotation = $('#quotation').find('#example-quotation .quotation-item');
                //去掉加载动态图
                $('.load_animation').addClass('hidden');
                if(quotationList.length>0){
                    $.each(quotationList,function(i,quotation){
                        //PkId
                        var PkId = quotation.PkId;
                        var ProductPkId = quotation.ProductPkId;
                        //单据名
                        var quotationname = quotation.QuotationName;
                        //产品名
                        var productname = quotation.ProductName;
                        var ordersale = quotation.RmbOrderTotalSales;
                        var imgurl = quotation.ProductMainImagePath;
                        var $quotationmodify = $quotation.clone();
                        //产品编号
                        var productno = quotation.ProductNo;
                        //报价组合
                        var quotationgroup = quotation.QuotationGroup;
                        //自定义信息
                        var note = quotation.CustomInfo;
                        //销售额
                        var ordertotalsales = quotation.UsdOrderTotalSales;
                        //提成
                        var commissionproportion = quotation.UsdCommissionProportion;
                        //货运信息
                        var Logistics = quotation.Logistics;

                        //报价单详情页
                        var quotationDetailData = {
                            'PkId': PkId,
                            'ProductPkId': ProductPkId,
                            'productno': productno,
                            'productname': productname,
                            'note': note,
                            'ordertotalsales': ordertotalsales,
                            'commissionproportion': commissionproportion,
                            'quotationname': quotationname,
                            'quotationgroup': quotationgroup,
                            'Logistics': Logistics

                        }
                        //数据储存
                        $quotationmodify.data('quotationDetailData',quotationDetailData);
                        //数据填充
                        $quotationmodify.find('.item-head .item-name').html(quotationname);
                        if(imgurl){
                            $quotationmodify.find('.item-body .pic-border').css('background-image','url('+ imgurl +')');
                        }
                        $quotationmodify.find('.item-body .product-introduce .product .product-name').html(productname);
                        $quotationmodify.find('.item-body .product-introduce .price .money-num').html(numModify(ordertotalsales));
                        $('#quotation .quotation-list-item').append($quotationmodify);
                    })
                    $('#quotation').find('.loading_more').removeClass('hidden');
                    if(pagecount==currentpage){
                        $('#quotation').find('.loading_more .No-more').removeClass('hidden');
                        $('#quotation').find('.loading_more .See-more').addClass('hidden');

                    }
                }



            }
        });
    }
    var quotationpage = 1;
    getOrderQuotations(10,quotationpage);

//点击加载更多
    $('#quotation').on('click','.See-more',function(){
        if($(this).hasClass('abled')){
            $('.load_animation').removeClass('hidden');
            $('#quotation').find('.loading_more').addClass('hidden');

            getOrderQuotations(10,quotationpage);
        }

    })
//保存报价组合

    $('#productProperties').on('click','footer .sureBtn',function(){
        $('#productProperties').click();
        var ProductNo = $('#productProperties .aboutProduct .productno').html();
        var ProductName = $('#productProperties .aboutProduct .product-name').html();
        var imgArr = [];
        var cost = 0;
        if(!$('#productProperties .productPicList .picUl li img:first-child').hasClass('default')){

            $('#productProperties .productPicList .picUl li img').each(function(i,img){

                imgArr.push(img.src);
            })
        }


        var ProductQty = $('#productProperties #product-num').html();
        var ProductOriginalPrice = $('#productProperties .quantityOfPro .qtyInputer .basePricePerPcs em').html()/costRate;
        //计算成本价格
        cost = ProductOriginalPrice;
        var ProductCurrentPrice = $('#productProperties .quantityOfPro .prodBasePrice .changeBtn em').html();
        var ProductAttrs = [];
        var ProductAttrsmodify = [];
        $('#productProperties .propertyList .prodProperty').each(function(i,ele){
            var attrs = {};
            var AttributeName = $(ele).find('.propertyHeader .propertyName').html();
            var ProductAttributeCurrentPrice = $(ele).find('.propertyHeader .changeBtn em').html();
            var ProductAttributeValue = $(ele).find('.detailList .choosed em').html();
            var ProductAttributeOriginalPrice = $(ele).find('.detailList .choosed .detailPrice b').html()/costRate;
            cost += ProductAttributeOriginalPrice;
            var ProductAttributePkId = $(ele).find('.detailList .choosed').data('attr-id');
            var ProductAttributeValuePkId = $(ele).find('.detailList .choosed').data('id');
            attrs={
                'AttributeName': AttributeName,
                'UsdProductAttributeCurrentPrice': ProductAttributeCurrentPrice,
                'ProductAttributeValue': ProductAttributeValue,
                'UsdProductAttributeOriginalPrice': ProductAttributeOriginalPrice,
                'ProductAttributePkId': ProductAttributePkId,
                'ProductAttributeValuePkId': ProductAttributeValuePkId
            }
            attrsmodify={
                'AttributeName': AttributeName,
                'ProductAttributeCurrentPrice': ProductAttributeCurrentPrice,
                'ProductAttributeValue': ProductAttributeValue,
                'ProductAttributeOriginalPrice': ProductAttributeOriginalPrice,
                'ProductAttributePkId': ProductAttributePkId,
                'ProductAttributeValuePkId': ProductAttributeValuePkId
            }
            ProductAttrs.push(attrs);
            ProductAttrsmodify.push(attrsmodify);

        })
        var ProductAttrsData = {
            'ProductOriginalPrice': ProductOriginalPrice,
            'ProductCurrentPrice': ProductCurrentPrice,
            'ProductQty': ProductQty,
            'ProductAttrs': ProductAttrsmodify,
            'ProucetImagePaths': imgArr
        }
        console.log(ProductAttrsData);
////////
//     总成本费用
        cost = cost*ProductQty;

        var totalmoney = $('#productProperties footer .salesNum em').html();
        var singlemoney = $('#productProperties footer .pricePerPcs em').html();
        var groupcombox = {
            'ProductCurrentPrice': ProductCurrentPrice,
            'ProductQty': ProductQty,
            'TotalGroupCurrentPrice': totalmoney,
            'TotalGroupCurrentSinglePrice': singlemoney,
            'img': imgArr,
            'attr': ProductAttrs
        }
        ///////
        //1是修改
        if($(this).data('state')==1){
            var $item = $('#quotation-modify .quotation-modify-body .quotation-group >.modify');
            //内容清空
            $item.find('.pic-container').html('');
            $item.find('.product-detail').html('');
            //

            $item.data('cost',cost);
            $item.data('ProductAttrsData',ProductAttrsData);
            $item.find('.quotation-head .modify').data('groupcombox',groupcombox);
            $item.find('.total-money .money-count').html(totalmoney);
            imgArr.length==0 && $item.find('.pic-container').html('未添加图片');
            $.each(imgArr,function(i,img){
                $item.find('.pic-container').append(
                    '<div class="pic-border" style="background-image: url('+ img +')"></div>'
                );
                //$item.find('.pic-container .pic-border').css('background-image','url('+ img +')');
            })
            $item.find('.product-detail').append(
                '<div class="product-attr">'
                +'<span>Unit price: </span>'
                +'<span></span>'
                +'<span class="money fund-change">'+ numModify(ProductCurrentPrice) +'</span>'
                +'</div>'
            );
            $.each(ProductAttrs,function(j,attr){
                $item.find('.product-detail').append(
                    '<div class="product-attr">'
                    +'<span>'+ attr.AttributeName +': </span>'
                    +'<span>'+ attr.ProductAttributeValue +'</span>'
                    +'<span class="money fund-change">'+ attr.UsdProductAttributeCurrentPrice +'</span>'
                    +'</div>'
                );

            })
            $item.find('.product-detail').append(
                '<div class="product-attr">'
                +'<span>quantity: </span>'
                +'<span>'+ ProductQty +'</span>'
                +'<span class="money fund-change">'+ totalmoney +'</span>'
                +'</div>'
            );
            $("#productProperties").addClass('hidden');
            $('#quotation-modify').removeClass('hidden');
        }
        //0是新建
        else if($(this).data('state')==0){
            if($('#productProperties .quantityOfPro .qtyTitle .changeQtyBtn .firstShow').html()=='请输入产品数量'){
                vp.ui.pop('icon-err','产品数量不能为空');
                return false;
            }
            var $quotation = $('#quotation-modify').find('#quotation-modify-exmple .quotation-item').clone();
            $quotation.data('cost',cost);
            $quotation.data('ProductAttrsData',ProductAttrsData);
            console.error(ProductAttrsData)
            $quotation.find('.quotation-head .modify').data('groupcombox',groupcombox);
            imgArr.length==0 && $quotation.find('.pic-container').html('未添加图片');
            $.each(imgArr,function(i,img){
                $quotation.find('.pic-container').append(
                    '<div class="pic-border" style="background-image: url('+ img +')"></div>'
                );
                //$quotation.find('.pic-container .pic-border').css('background-image','url('+ img +')');
            })
            $quotation.find('.product-detail').append(
                '<div class="product-attr">'
                +'<span>Unit price: </span>'
                +'<span></span>'
                +'<span class="money fund-change">'+ numModify(ProductCurrentPrice) +'</span>'
                +'</div>'
            );
            $.each(ProductAttrs,function(j,attr){
                $quotation.find('.product-detail').append(
                    '<div class="product-attr">'
                    +'<span>'+ attr.AttributeName +': </span>'
                    +'<span>'+ attr.ProductAttributeValue +'</span>'
                    +'<span class="money fund-change">'+ attr.UsdProductAttributeCurrentPrice +'</span>'
                    +'</div>'
                );

            })
            $quotation.find('.product-detail').append(
                '<div class="product-attr">'
                +'<span>quantity: </span>'
                +'<span>'+ ProductQty +'</span>'
                +'<span class="money fund-change">'+ totalmoney +'</span>'
                +'</div>'
            );

            $quotation.find('.total-money .money-count').html(totalmoney);
            $('#quotation-modify').find('.quotation-modify-body .quotation-group').append($quotation);
            $("#productProperties").addClass('hidden');
            $('#quotation-modify').removeClass('hidden');

        }
        //console.log(ProductAttrs);
        getTransFee(true);
        $('#productProperties .propertyList .prodProperty .detailList li').removeClass('choosed');


    })
////共用函数
    function getData(status){
        var ProductAttrsGroup = [];
        var QuotationName = $('#quotation-modify .order-name .order-word textarea').val();

        //var ProductNo = $('#quotation-modify .quotation-modify-body .product-order-num .product-num .productno').html();
        var ProductName = $('#quotation-modify .quotation-modify-body .product-order-num .product-name').html();
        var CustomInfo = '';
        if($('#quotation-modify .note-modify').hasClass('isShow')){
            CustomInfo = $('#quotation-modify .note-modify .note-word').html();
        }


        var ProductPkId = $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('ProductPkId');
        var pkId = $('#quotation-modify').find('.quotation-modify-body .order-download .download').data('pkId');
        var OrderQuotationPkId = $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('OrderQuotationPkId');
        $('#quotation-modify').find('.quotation-modify-body .quotation-group .quotation-item').each(function(){
            var ProductAttrsData = $(this).data('ProductAttrsData');
            // console.log(ProductAttrsData)
            // $.each(ProductAttrsData.ProductAttrs,function(i,obj){
            //     if(obj.UsdProductAttributeCurrentPrice){
            //         obj.ProductAttributeCurrentPrice = obj.UsdProductAttributeCurrentPrice;
            //         delete obj.UsdProductAttributeCurrentPrice;
            //     }
            //     if(obj.UsdProductAttributeOriginalPrice){
            //         obj.ProductAttributeOriginalPrice = obj.UsdProductAttributeOriginalPrice;
            //         delete obj.UsdProductAttributeOriginalPrice;
            //     }
            // })
            console.log(ProductAttrsData);
            ProductAttrsGroup.push(ProductAttrsData);

        })
        var Logistics = {};
        if($('#quotation-modify .quotation-modify-body .freight-way').hasClass('isShow')){
            Logistics = $('#quotation-modify .quotation-modify-body .freight-way .freight-head .modify').data('Logisticsmodify');
        }

        console.info(Logistics);
        var LogisticsModify = {};
        for(var key in Logistics){
            if(key == 'UsdFreightOriginalPrice'){
                LogisticsModify['FreightOriginalPrice'] = Logistics[key];
            }
            else if(key == 'UsdFreightCurrentPrice'){
                LogisticsModify['FreightCurrentPrice'] = Logistics[key];
            }
            else{
                LogisticsModify[key] = Logistics[key];
            }
        }
        console.info(LogisticsModify);
        var OrderQuotation = {
            //"CurrentCurrency": 1,
            "QuotationName": QuotationName,
            "ProductPkId": ProductPkId,
            "ProductName": ProductName,
            //"ProductNo": ProductNo,
            "CustomInfo": CustomInfo,
            "ProductAttrsGroup": ProductAttrsGroup,
            //"OrderQuotationPkId": pkId,
            "Logistics": LogisticsModify
        }
        if(status=='update'){
            OrderQuotation.OrderQuotationPkId = OrderQuotationPkId ;
        }
        if($('#quotation-modify .money-change-border .icon-maoduo41').hasClass('hidden')){
            OrderQuotation.CurrentCurrency = 0 ;
        }
        else{
            OrderQuotation.CurrentCurrency = 1 ;
        }
        console.log(costRate);
        //人民币切换
        if (OrderQuotation.CurrentCurrency == 0) {
            //货运信息
            if (OrderQuotation.Logistics.FreightCurrentPrice) {
                OrderQuotation.Logistics.FreightCurrentPrice = OrderQuotation.Logistics.FreightCurrentPrice/rate;
            }
            if (OrderQuotation.Logistics.FreightOriginalPrice) {
                OrderQuotation.Logistics.FreightOriginalPrice = OrderQuotation.Logistics.FreightOriginalPrice/rate;
            }
            //报价组合
            $.each(OrderQuotation.ProductAttrsGroup,function(i,obj) {
                obj.ProductCurrentPrice = obj.ProductCurrentPrice/rate;
                obj.ProductOriginalPrice = obj.ProductOriginalPrice/rate;
                $.each(obj.ProductAttrs,function(j,ob) {
                    ob.ProductAttributeCurrentPrice = ob.ProductAttributeCurrentPrice/rate;
                    ob.ProductAttributeOriginalPrice = ob.ProductAttributeOriginalPrice/rate;
                })
            })
        }
        return OrderQuotation;
    }

//二次保存定价单
    $('#quotation-modify').on('click','.quotation-foot .save-modify',function(){
        if(!isPass()){
            return false;
        }
        var OrderQuotation = getData('update');
        console.log(OrderQuotation);
        console.log(JSON.stringify(OrderQuotation));
        $.ajax("https://www.mooddo.com/v1/Order/UpdateOrderQuotations", {
            type:"post",
            headers:{vp_token:vp.core.token},
            data:JSON.stringify(OrderQuotation),
            contentType:"application/json",
            success: function(data){
                //console.log(data);
                if(data.IsSuccess){
                    $('#Mask-pop').show();
                    $('#Mask-pop .save-quotation').show();
                    $('#quotation .quotation-body-border .quotation-list-item').html('');
                    $('#quotation .quotation-body-border .no-data').hide();
                    quotationpage = 1;
                    getOrderQuotations(10,quotationpage);
                }
            }
        })
    })

    function isPass(){
        if($('#quotation-modify .quotation-group .quotation-item').length==0){
            vp.ui.pop('icon-err','报价组合信息不能为空','请添加产品报价组合的信息');
            return false;
        }
        var obj = getSale();
        var totalsale = obj.totalsale;
        var cost = obj.cost;
        //console.log(totalsale-1.2*cost);
        console.log(obj)
        var num = setNum();
        console.log(totalsale/num);
        console.log(1.2*cost/num);
        if(parseFloat((totalsale/num).toFixed(2))<parseFloat((1.2*cost/num).toFixed(2))){
            console.log(parseFloat((totalsale/num).toFixed(2)))
            console.log(parseFloat((1.2*cost/num).toFixed(2)))
            $('#Mask-pop .quotation-tips .content .money').html((1.2*cost).toFixed(2));
            $('#Mask-pop').show();
            $('#Mask-pop .quotation-tips').show();
            return false;
        }
        return true;
    }
//另存定价单
    $('#quotation-modify').on('click','.quotation-foot .save-as',function(){
        if(!isPass()){
            return false;
        }
        var OrderQuotation = getData('create');
        $('#Mask-pop .saveas-quotation .save').data('OrderQuotation',OrderQuotation)
        //弹出另存为弹窗
        $('#Mask-pop').show();
        $('#Mask-pop .saveas-quotation').show();
        $('#Mask-pop .saveas-quotation .quotation-input input').val(OrderQuotation.QuotationName);

    })

//提交订单
    $('#quotation-modify').on('click','.quotation-foot .submit-order',function(){
        if(!isPass()){
            return false;
        }
        if(!$('#quotation-modify .freight-way').hasClass('isShow')){
            vp.ui.pop('icon-err','货运信息不能为空','请添加货运信息');
            return false;
        }
        if(!$('#quotation-modify .note-modify').hasClass('isShow')){
            vp.ui.pop('icon-err','请输入自定义信息','如具体的货运地址，客户信息和备注说明等');
            return false;
        }
        var OrderQuotation = getData('update');
        //console.log(JSON.stringify(OrderQuotation));
        $.ajax("https://www.mooddo.com/v1/Order/UpdateOrderQuotations", {
            type:"post",
            headers:{vp_token:vp.core.token},
            data:JSON.stringify(OrderQuotation),
            contentType:"application/json",
            success: function(data){
                //console.log(data);
                if(data.IsSuccess){
                    $.ajax("https://www.mooddo.com/v1/Order/CreateOrderByQuotation", {
                        type:"post",
                        headers:{vp_token:vp.core.token},
                        data:JSON.stringify({
                            "OrderQuotationPkId": [
                                OrderQuotation.OrderQuotationPkId
                            ]
                        }),
                        contentType:"application/json",
                        success: function(data){
                            console.log(data);
                            if(data.IsSuccess){
                                $('#order-success').removeClass('hidden');
                                $('#quotation-modify').addClass('hidden');
                                $('#order-success .success-tip .tips p').html(data.Data);
                                $('#Bill .bill-body .trading-list').html('');
                                $('#Bill .bill-body .no-data').hide();
                                $('#deal-order .deal-order-border .order-list').html('');
                                $('#deal-order .deal-order-border .no-data').hide();
                                orderpage = 1;
                                getOrders(10,1,true,true);
                            }
                        }
                    })
                }
            }
        })
    })

//单据名聚焦事件
    $('#quotation-modify').on('focus','.order-name .order-word textarea',function(){

    })
    $('#productProperties').on('focus','#product-num-input',function(){
        var $ele = $(this).parent();
        //windowResize($ele);
    })

    function windowResize($ele){
        $(window).resize(function(){
            if(!($('#productProperties').hasClass('hidden'))){

                var $foot = $('#productProperties footer').height();
                //var viewTop = $('#productProperties article').scrollTop(),            // 可视区域顶部
                var viewBottom = window.innerHeight - $foot;  // 可视区域底部

                var elementTop = $ele.offset().top, // $element是保存的input
                    elementBottom = elementTop + $ele.height()-viewBottom;

                $('#productProperties article').scrollTop( elementBottom );
            }
        })
    }
    $(window).resize(function(){
//alert(333)

        if(!($('#productProperties').hasClass('hidden'))){
            // var viewTop = $('#productProperties article').scrollTop(),            // 可视区域顶部
            //     viewBottom = viewTop + window.innerHeight;  // 可视区域底部
            //
            // var elementTop = $('#quotation-modify .order-name .order-word').offset().top, // $element是保存的input
            //     elementBottom = elementTop + $('#quotation-modify .order-name .order-word').height()-viewBottom;
            // $('#productProperties article').scrollTop( elementBottom );
        }

        // var viewTop = $('#quotation-modify .quotation-modify-body').scrollTop(),            // 可视区域顶部
        //     viewBottom = viewTop + window.innerHeight;  // 可视区域底部
        //
        // var elementTop = $('#quotation-modify .order-name .order-word').offset().top, // $element是保存的input
        //     elementBottom = elementTop + $('#quotation-modify .order-name .order-word').height()-viewBottom;
        //
        // //console.error(elementBottom)
        // //console.log($('#money-count').height())
        // $('#quotation-modify .quotation-modify-body').scrollTop( elementBottom );
    })

//货运信息
    $('#quotation-modify').on('click','.modify-group .add-address',function(){
        billReset();
        $('#quotation-modify').addClass('hidden');
        $('#freight-information').removeClass('hidden');
        //$('#freight-information').find('.foot .save').data('source','new');
        $('#freight-information').find('.destination .country-select').html('请选择国家');
        $('#freight-information .freight-body .freight-way-border .IEway .freight-left .icon-right').removeClass('selected');
        $('#freight-information .freight-body .freight-way-border .IPway .freight-left .icon-right').addClass('selected');
        $('#freight-information .freight-body .freight-way-border .IEway').addClass('abled');
        $('#freight-information .freight-body .freight-way-border .IPway').addClass('abled');
        setNum();
        $('#freight-information').find('.freight-fee .fee-way #fee-modify .fee-count').html(0);
        $('#freight-information').find('.freight-fee .fee-standard .content .standard-money').html(0);
        $('#freight-information').find('.foot .freight-total-fee .money').html(0);
    })
//货运信息返回
    $('#freight-information').on('click','.message-head .cancel',function(){
        $('#quotation-modify').removeClass('hidden');
        $('#freight-information').addClass('hidden');
    })
//货运信息选择国家
    $('#freight-information').on('click','.freight-body .destination .country-select',function(){
        $('#freight-information').addClass('hidden');
        $('#Country-list').removeClass('hidden');
    })
//选择国家返回货运信息
    $('#Country-list').on('click','.country-head .cancel',function(){
        $('#freight-information').removeClass('hidden');
        $('#Country-list').addClass('hidden');
        if($('#Country-list .country-head').data('ismodify')){
            var countryList = $('#Country-list .country-head').data('countryList');
            countryRender(countryList);
        }
    })
//选择国家到搜索国家
    $('#Country-list').on('click','.country-head .icon-search',function(){
        $('#search-country').removeClass('hidden');
        $('#Country-list').addClass('hidden');
    })
//搜索国家返回到选择国家页
    $('#search-country').on('click','.search-head .icon-mail_fanhui',function(){
        $('#search-country').addClass('hidden');
        $('#Country-list').removeClass('hidden');
    })
//搜索国家搜索按钮跳转到选择国家页
    $('#search-country').on('click','.search-head .search',function(){
        //$('#search-country').addClass('hidden');
        //$('#Country-list').removeClass('hidden');
    })
//选择国家页选择按钮
    $('#Country-list').on('click','.country-list-table .country-item',function(){
        var country = $(this).find('.district .china-country').html();
        var countryEng = $(this).find('.district .english-country').html();
        var IPsupport = $(this).find('.IP').attr('data-support');
        var IEsupport = $(this).find('.IE').attr('data-support');
        var pkid = $(this).data('pkid');


        if(IPsupport =='false'){
            $('#freight-information .freight-body .freight-way-border .IPway').removeClass('abled');
        }else{
            $('#freight-information .freight-body .freight-way-border .IPway').addClass('abled');
        }
        if(IEsupport =='false'){
            $('#freight-information .freight-body .freight-way-border .IEway').removeClass('abled');
            //使选项从IE调到IP
            $('#freight-information .freight-body .freight-way-border .IEway .freight-left .icon-right').removeClass('selected');
            $('#freight-information .freight-body .freight-way-border .IPway .freight-left .icon-right').addClass('selected');
        }else{
            $('#freight-information .freight-body .freight-way-border .IEway').addClass('abled');
        }
        //设置选择的国家
        $('#freight-information .freight-body .destination .country-select').html(country);
        $('#freight-information .freight-body .destination .country-select').data('countryEng',countryEng);
        $('#freight-information .freight-body .destination .country-select').data('CountryPkId',pkid);
        $('#freight-information').removeClass('hidden');
        $('#Country-list').addClass('hidden');
        //////
        if($('#Country-list .country-head').data('ismodify')){
            var countryList = $('#Country-list .country-head').data('countryList');
            countryRender(countryList);
        }

        getTransFee(false);
    })
//货运费用实时计算
    function getTransFee(isProduct){
        var CountryPkId = $('#freight-information .freight-body .destination .country-select').data('CountryPkId');
        //产品pkid
        var ProductPkId = $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('ProductPkId');
        var DeliveryMethod = '';
        if($('#freight-information .freight-way-border .list .freight-left .selected').closest('.list').hasClass('IPway')){
            DeliveryMethod = 'IP'
        }else{
            DeliveryMethod = 'IE'
        }
        var quantity = setNum();
        if(CountryPkId){
            getFee(CountryPkId,ProductPkId,quantity,DeliveryMethod,isProduct);
        }
        else{
            getSale();
        }
    }

//货运方式的选择
    $('#freight-information').on('click','.freight-body .freight-way-border .list',function(){

        if($(this).hasClass('abled')){

            $(this).find('.icon-right').addClass('selected');
            $(this).siblings('.list').find('.icon-right').removeClass('selected');
            getTransFee(false);
        }
    })

//货运费用修改
    $('#freight-information').on('click','.freight-fee .modify',function(){
        //$(this).find('.fee-count').hide();
        $(this).find('#fee-input').show();
        $(this).find('#fee-input').focus();

    })
    $('#freight-information').on('blur','#fee-input',function(){
        $('#freight-information').one('click',function(e){
            if($(e.target).closest('#fee-modify').length==0){
                //$('#fee-modify').find('.fee-count').show();
                $('#fee-modify').find('#fee-input').hide();
                //console.log()
                if($('#fee-input').val()&&numModify($('#fee-input').val())>=0){
                    $('#fee-modify').find('.fee-count').html(numModify($('#fee-input').val()));
                    $('#freight-information .foot .money').html(numModify($('#fee-input').val()));
                }
                $('#fee-input').val('');
            }
        })
    })
//货运信息删除
    $('#quotation-modify').on('click','.freight-way .freight-head .delete',function(){
        $(this).closest('.freight-way').removeClass('isShow');
        $(this).closest('.freight-way').hide();
        $('#quotation-modify').find('.modify-group .add-address').show();
        getSale();

    })
//货运信息修改
    $('#quotation-modify').on('click','.freight-way .freight-head .modify',function(){
        billReset();
        console.log($(this).data('Logistics'));
        var Logistics = $(this).data('Logistics');
        var transway = $(this).data('transway');
        console.log(transway);
        //国家pkid
        var CountryPkId = Logistics.CountryPkId;
        $('#freight-information').find('.destination .country-select').data('CountryPkId',CountryPkId);

        //产品pkid
        var ProductPkId = $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('ProductPkId');
        $('#freight-information .message-head').data('ProductPkId',ProductPkId);

        $('#quotation-modify').addClass('hidden');
        $('#freight-information').removeClass('hidden');
        //修改状态为
        $('#freight-information').find('.foot .save').data('source','modify');
        //数据填充
        $('#freight-information').find('.destination .country-select').html(Logistics.CountryNameChn);
        if(Logistics.DeliveryMethod == 'IP'){
            $('#freight-information').find('.freight-way-border .IPway .icon-right').addClass('selected');
            $('#freight-information').find('.freight-way-border .IEway .icon-right').removeClass('selected');
        }
        else{
            $('#freight-information').find('.freight-way-border .IPway .icon-right').removeClass('selected');
            $('#freight-information').find('.freight-way-border .IEway .icon-right').addClass('selected');
        }
        //IP ie 是否支持渲染
        if(transway.IsSAcceptIE){
            $('#freight-information').find('.freight-way-border .IEway').addClass('abled');
        }
        else{
            $('#freight-information').find('.freight-way-border .IEway').removeClass('abled');
        }
        if(transway.IsSAcceptIP){
            $('#freight-information').find('.freight-way-border .IPway').addClass('abled');
        }
        else{
            $('#freight-information').find('.freight-way-border .IPway').removeClass('abled');
        }
        //设置货运信息数量
        var quantity = setNum();

        getFee(CountryPkId,ProductPkId,quantity,Logistics.DeliveryMethod);
        $('#freight-information').find('.freight-fee .fee-way #fee-modify .fee-count').html(numModify(Logistics.UsdFreightCurrentPrice));
        $('#freight-information').find('.freight-fee .fee-standard .content .standard-money').html(numModify(Logistics.UsdFreightOriginalPrice*costRate));
        $('#freight-information').find('.foot .freight-total-fee .money').html(numModify(Logistics.UsdFreightCurrentPrice));
    })
//设置货运信息数量
    function setNum(){
        var totalcount = 0;
        $('#quotation-modify').find('.quotation-modify-body .quotation-group .quotation-item').each(function(){
            //console.log($(this).data('ProductAttrsData').ProductQty)
            totalcount += parseInt($(this).data('ProductAttrsData').ProductQty);
        })
        $('#freight-information').find('.product-num .num-count').html(totalcount);
        return totalcount;
    }

//货运信息保存
    $('#freight-information').on('click','.foot .save',function(){
        if($('#freight-information .destination .country-select').html() == '请选择国家'){

            vp.ui.pop('icon-err','货运国家不能为空','请选择货运国家');
            return false;
        }
        var countryEng = $('#freight-information .destination .country-select').data('countryEng');
        var CountryNameChn = $('#freight-information .destination .country-select').html();
        var $ele = $('#freight-information .freight-way-border .list .freight-left .selected').closest('.list');
        var DeliveryMethod = '';
        if($ele.hasClass('IPway')){
            DeliveryMethod = 'IP';
        }else{
            DeliveryMethod = 'IE';
        }
        var cost = 0;
        var FreightCurrentPrice = $('#freight-information').find('.freight-fee #fee-modify .fee-count').html();
        var FreightOriginalPrice = $('#freight-information').find('.freight-fee .fee-standard .standard-money').html()/costRate;
        cost = (FreightOriginalPrice).toFixed(2);
        console.log(cost)
        var ToalFreight = $('#freight-information').find('.foot .money').html();
        var ProductQty = $('#freight-information').find('.product-num .num-count').html();
        var IsSAcceptIP= false;
        var IsSAcceptIE = false;
        var CountryPkId = $('#freight-information .destination .country-select').data('CountryPkId');
        if($('#freight-information .freight-way-border .IPway').hasClass('abled')){
            IsSAcceptIP = true;
        }else{
            IsSAcceptIP = false;
        }
        if($('#freight-information .freight-way-border .IEway').hasClass('abled')){
            IsSAcceptIE = true;
        }else{
            IsSAcceptIE = false;
        }

        var transway = {
            'IsSAcceptIP': IsSAcceptIP,
            'IsSAcceptIE': IsSAcceptIE
        }

        var Logistics = {
            'CountryNameChn': CountryNameChn,
            'DeliveryMethod': DeliveryMethod,
            'FreightCurrentPrice': FreightCurrentPrice,
            'FreightOriginalPrice': FreightOriginalPrice,
            'ToalFreight': ToalFreight,
            'ProductQty': ProductQty,
            'CountryPkId': CountryPkId
        }
        var Logisticsmodify = {
            'DeliveryMethod': DeliveryMethod,
            'FreightCurrentPrice': FreightCurrentPrice,
            'FreightOriginalPrice': FreightOriginalPrice,
            'CountryPkId': CountryPkId
        }
        console.log(Logistics)
        //数据储存
        $('#quotation-modify').find('.freight-way .freight-head .modify').data('Logistics',Logistics);
        $('#quotation-modify').find('.freight-way .freight-head .modify').data('Logisticsmodify',Logisticsmodify);
        $('#quotation-modify').find('.freight-way .freight-head .modify').data('transway',transway);
        //数据修改
        $('#quotation-modify').find('.freight-way .freight-detail .country .country-detail').html(countryEng);
        $('#quotation-modify').find('.freight-way .freight-detail .freight-way-detail .transway .way').html(DeliveryMethod);
        if(DeliveryMethod == 'IP'){
            $('#quotation-modify').find('.freight-way .freight-detail .freight-way-detail .freight-way-tip span').html('国际优先快递服务');
        }
        else{
            $('#quotation-modify').find('.freight-way .freight-detail .freight-way-detail .freight-way-tip span').html('国际经济快递服务');
        }
        $('#quotation-modify').find('.freight-way .total-money .money').html(ToalFreight);
        $('#quotation-modify').removeClass('hidden');
        $('#freight-information').addClass('hidden');
        $('#quotation-modify').find('.freight-way').show();
        $('#quotation-modify').find('.modify-group .add-address').hide();

        $('#quotation-modify').find('.freight-way').addClass('isShow');
        $('#quotation-modify').find('.freight-way').data('cost',cost);

        getSale();
    })

//获得原始货运费用

    function getFee(CountryPkId,ProductPkId,ProductQty,DeliveryMethod,isProduct){
        var url = {
            local:"data/testDemo/data.json",
            type:"post",
            server:"/v1/Logistics/GetLogisticsFreeInfo",
            debug:false,
            loadFlag:false,
        };
        var data = {
            "CountryPkId":CountryPkId,
            "ProductPkId":ProductPkId,
            "ProductQty":ProductQty,
            "DeliveryMethod":DeliveryMethod
        }
        vp.core.ajax(url,data,function(data){
            console.log(data);
            var UsdTotalFreight = data.Data.UsdTotalFreight;
            console.log(UsdTotalFreight)
            $('#freight-information .freight-fee .fee-standard .standard-money').html(numModify(UsdTotalFreight*1.2));
            $('#freight-information .freight-fee .fee-way #fee-modify .fee-count').html(numModify(UsdTotalFreight*1.2));
            $('#freight-information .foot .freight-total-fee .money').html(numModify(UsdTotalFreight*1.2));


            if(isProduct){
                $('#quotation-modify .freight-way .freight-body .total-money .money').html(numModify(UsdTotalFreight*1.2));
                $('#quotation-modify').find('.freight-way').data('cost',numModify(UsdTotalFreight));
                var Logistics = $('#quotation-modify .quotation-modify-body .freight-way .freight-head .modify').data('Logisticsmodify');
                Logistics.FreightCurrentPrice = numModify(UsdTotalFreight*1.2);
                Logistics.FreightOriginalPrice = numModify(UsdTotalFreight);
                $('#quotation-modify .quotation-modify-body .freight-way .freight-head .modify').data('Logisticsmodify',Logistics);
                getSale();
            }

        },function(data){
            console.log(data);
        })
    }
//获取国家列表
    function getCountryList() {
        var url = {
            local:"data/testDemo/data.json",
            type:"get",
            server:"/v1/Logistics/GetLogisticsCountry",
            debug:false,
            loadFlag:true,
        };
        var data = {};
        vp.core.ajax(url,data,function(data){
            //console.log(data);
        },function(data){
            //console.log(data)
            var countryList = data.Data;
            $('#Country-list .country-head').data('countryList',countryList);
            countryRender(countryList);

        },function(data){
            console.log(data)
        })
    }

    getCountryList();


//保存报价单成功后弹窗事件
    $('#Mask-pop').on('click','.save-quotation .close',function(){
        $('#Mask-pop').hide();
        $('#Mask-pop .save-quotation').hide();
    })
//发送邮件
    $('#Mask-pop').on('click','.save-quotation .send-email',function(){
        $('#Mask-pop').hide();
        $('#Mask-pop .save-quotation').hide();
        //do something
    })
//下载报价单
    $('#Mask-pop').on('click','.save-quotation .download-quotation',function(){
        $('#Mask-pop').hide();
        $('#Mask-pop .save-quotation').hide();
        //do something
        $('#quotation-modify .quotation-modify-body .order-download .download').click();

    })


//另存为报价单弹窗保存
    $('#Mask-pop').on('click','.saveas-quotation .save',function(){
        if(!$('#Mask-pop .saveas-quotation .quotation-input input').val()){
            vp.ui.pop('icon-err','报价单名称不能为空','请填写报价单名称');
            return false;
        }
        var OrderQuotation = $(this).data('OrderQuotation');
        OrderQuotation.QuotationName = $('#Mask-pop .saveas-quotation .quotation-input input').val();
        $.ajax("https://www.mooddo.com/v1/Order/CreateOrderQuotation", {
            type:"post",
            headers:{vp_token:vp.core.token},
            data:JSON.stringify(OrderQuotation),
            contentType:"application/json",
            success: function(data){
                console.log(data);
                if(data.IsSuccess){
                    $('#Mask-pop').show();
                    $('#Mask-pop .save-quotation').show();
                    $('#quotation-modify .quotation-modify-body .order-download .download').data('OrderQuotationPkId',data.Data);
                    $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('OrderQuotationPkId',data.Data);
                    $('#quotation .quotation-body-border .quotation-list-item').html('');
                    $('#quotation .quotation-body-border .no-data').hide();
                    quotationpage = 1;
                    getOrderQuotations(10,quotationpage);
                }
            }
        })
        $('#Mask-pop').hide();
        $('#Mask-pop .saveas-quotation').hide();
        //do something
    })
//另存为报价单弹窗取消
    $('#Mask-pop').on('click','.saveas-quotation .cancel',function(){
        $('#Mask-pop').hide();
        $('#Mask-pop .saveas-quotation').hide();
        //do something
    })
//清空单据名称

    $('#Mask-pop').on('click','.saveas-quotation .quotation-input .icon-err',function(){
        $(this).prev().val('');
        //do something
    })

//实时计算销售额和提成
    function getSale(){
        var totalsale = 0;
        var cost = 0;
        $('#quotation-modify .quotation-group .quotation-item').each(function(){
            totalsale += parseFloat($(this).find('.total-money .money-count').html());
            cost += parseFloat($(this).data('cost'));
            console.info(cost)
        })
        if($('#quotation-modify').find('.freight-way').hasClass('isShow')){
            totalsale += parseFloat($('#quotation-modify .freight-way .total-money .money').html());
            cost += parseFloat($('#quotation-modify').find('.freight-way').data('cost'));
            console.error(cost);
        }
        $('#quotation-modify .profit .sale-volume .sale-num .money-count').html(numModify(totalsale));
        var salecost = ((totalsale*0.9664 - cost)*0.675).toFixed(2);
        $('#quotation-modify .profit .withdraw-deposit .deposit-num .money-count').html(numModify(salecost));
        return {'totalsale': totalsale,'cost': cost};
    }

//订单保存成功页完成按钮
    $('#order-success').on('click','.success-title .complete',function(){
        $('#order-success').addClass('hidden');
        $('#quotation-modify').removeClass('hidden');
    })

//报价过低修改报价按钮
    $('#Mask-pop').on('click','.quotation-tips .modify-quotation',function(){
        $('#Mask-pop').hide();
        $('#Mask-pop .quotation-tips').hide();
    })

//国家渲染
    function countryRender(countryList){
        var $country = $('#Country-list').find('#country-item-example .country-item');
        $('#Country-list').find('.country-list-table').html('');
        $.each(countryList,function(i,country){
            var $countrymodify = $country.clone();
            $countrymodify.data('pkid',country.PkId);
            $countrymodify.find('.district .china-country').html(country.CountryNameChn);
            $countrymodify.find('.district .english-country').html(country.CountryNameEn);
            $countrymodify.find('.IP').attr('data-support',country.IsSAcceptIP);
            $countrymodify.find('.IE').attr('data-support',country.IsSAcceptIE);
            if(country.IsSAcceptIP){
                $countrymodify.find('.IP span').removeClass('icon-maoduo0').addClass('icon-maoduo1');
            }
            else{
                $countrymodify.find('.IP span').addClass('icon-maoduo0').removeClass('icon-maoduo1');
            }

            if(country.IsSAcceptIE){
                $countrymodify.find('.IE span').removeClass('icon-maoduo0').addClass('icon-maoduo1');
            }
            else{
                $countrymodify.find('.IE span').addClass('icon-maoduo0').removeClass('icon-maoduo1');
            }
            $('#Country-list').find('.country-list-table').append($countrymodify);
        })
    }
//搜索国家搜索按钮
    $('#search-country').on('click','.search-head .search',function(){
        var text = $('#search-country .search-input input').val();
        var countryList = $('#Country-list .country-head').data('countryList');
        //console.log(countryList)
        var countryGroup = [];
        if(text){
            $('#Country-list .country-head').data('ismodify',true);
            $.each(countryList,function(i,country){
                if(country.CountryNameChn.indexOf(text)>-1|| country.CountryNameEn.indexOf(text)>-1){
                    countryGroup.push(country);
                }
            })
            countryRender(countryGroup);
            //console.info(countryGroup);
            $('#search-country').addClass('hidden');
            $('#Country-list').removeClass('hidden');
        }
        else{
            vp.ui.pop('icon-err','搜索国家不能为空','请输入国家');
        }
    })
//单据名称修改
    $('#quotation-modify').on('focus','#order-name-input',function(){

        $(this).data('text',$(this).val());
    })
    $('#quotation-modify').on('blur','#order-name-input',function(){

        if(!$(this).val()){

            $(this).val($(this).data('text'));
            return false;
        }
    })
//订单页切换币种
    $('#Order-Detail').on('click','.quotation-title .money-change-border',function(){
        if($(this).find('.icon-maoduo41').hasClass('hidden')){
            //人民币切换美元
            $('#Order-Detail').find('.symol-switch').html('$');
            $('#Order-Detail').find('.fund-change').each(function(){
                $(this).html($(this).data('usdmoney'));
            })
            $(this).find('.icon-maoduo41').removeClass('hidden');
            $(this).find('.icon-maoduo42').addClass('hidden');
        }
        else{
            //美元切换人民币
            $('#Order-Detail').find('.symol-switch').html('&yen;')
            $('#Order-Detail').find('.fund-change').each(function(){
                $(this).html($(this).data('rmbmoney'));
            })
            $(this).find('.icon-maoduo42').removeClass('hidden');
            $(this).find('.icon-maoduo41').addClass('hidden');
        }

    })

//修改标价单页切换币种
    $('#quotation-modify').on('click','.quotation-title .money-change-border',function(){
        if($(this).find('.icon-maoduo41').hasClass('hidden')){
            //人民币切换美元
            $('#quotation-modify').find('.symol-switch').html('$');
            $('#quotation-modify').find('.fund-change').each(function(){
                $(this).html(($(this).html()*rate).toFixed(2));
            })
            $(this).find('.icon-maoduo41').removeClass('hidden');
            $(this).find('.icon-maoduo42').addClass('hidden');
        }
        else{
            //美元切换人民币
            $('#quotation-modify').find('.symol-switch').html('&yen;')
            $('#quotation-modify').find('.fund-change').each(function(){
                $(this).html(($(this).html()/rate).toFixed(2));
            })
            $(this).find('.icon-maoduo42').removeClass('hidden');
            $(this).find('.icon-maoduo41').addClass('hidden');
        }

    })
//汇率接口
    function getRate(){
        var url = {
            local:"data/testDemo/data.json",
            type:"get",
            server:"/v1/System/GetRMBToUSDExchangeRate",
            debug:false,
            loadFlag:true
        };
        var data = {};
        vp.core.ajax(url,data,function(data){
            //成功
            //console.log(data);
            rate = data.Data;
        })
    }

//币种重置
    function　billReset(){
        var $this = $('#quotation-modify .quotation-title');
        if($this.find('.icon-maoduo41').hasClass('hidden')){
            //人民币切换美元
            $('#quotation-modify').find('.symol-switch').html('$');
            $('#quotation-modify').find('.fund-change').each(function(){
                $(this).html(($(this).html()*rate).toFixed(2));
            })
            $this.find('.icon-maoduo41').removeClass('hidden');
            $this.find('.icon-maoduo42').addClass('hidden');
        }
    }
    //限制最多两行文本
    function limitText(ele) {

        $(ele).css('white-space','nowrap');
        console.info(ele.offsetWidth)
        console.info(ele.scrollWidth)
        if (ele.offsetWidth >= ele.scrollWidth) {
            console.info(111)
            $(ele).css('line-height','2rem');
        }
        else if (ele.offsetWidth < ele.scrollWidth && ele.scrollWidth <= 2 * ele.offsetWidth) {
            console.info(222)
            $(ele).css('line-height','.8rem');
            $(ele).css('white-space','inherit');
        }
        else if (ele.offsetWidth * 2 < ele.scrollWidth) {
            $(ele).css('line-height','.8rem');
            $(ele).css('white-space','inherit');
        }
    }
    //判断手机类型android ios
    function getPhoneType() {
        var pattern_phone = new RegExp("iphone","i");
        var pattern_android = new RegExp("Android","i");
        var userAgent = navigator.userAgent.toLowerCase();
        var isAndroid = pattern_android.test(userAgent);
        var isIphone = pattern_phone.test(userAgent);
        if(isAndroid){
            return 'Android';
        }
        else if(isIphone){
            return 'Iphone';
        }
        else{
            return 'other';
        }
    }

    /*
     currentpage =1;
     $.ajax("https://www.mooddo.com/v1/Order/GetOrderQuotations?pageSize="+ 10 +"&pageIndex="+1, {
     type:"post",
     success: function(data) {
     console.log(data);
     var pagecount = data.PageCount;
     var quotationList = data.Data;
     var $quotation = $('#qw1').find('#example-quotation .quotation-item');
     //去掉加载动态图
     $('.load_animation').addClass('hidden');
     if(quotationList.length>0){
     $.each(quotationList,function(i,quotation){
     //PkId
     var PkId = quotation.PkId;

     //单据名
     var quotationname = quotation.QuotationName;
     //产品名
     var productname = quotation.ProductName;
     var ordersale = quotation.RmbOrderTotalSales;
     var imgurl = quotation.ProductMainImagePath;
     var $quotationmodify = $quotation.clone();

     //数据填充
     $quotationmodify.attr('data-pkid',PkId);
     $quotationmodify.find('.item-head .item-name').html(quotationname);
     $quotationmodify.find('.item-body .pic-border').css('background-image','url('+ imgurl +')');
     $quotationmodify.find('.item-body .product-introduce .product .product-name').html(productname);
     $quotationmodify.find('.item-body .product-introduce .price .money-num').html(ordersale);
     $('#qw1 .quotation-list-item').append($quotationmodify);
     })
     $('#qw1').find('.loading_more').removeClass('hidden');
     if(pagecount==currentpage){
     $('#qw1').find('.loading_more .No-more').removeClass('hidden');
     $('#qw1').find('.loading_more .See-more').addClass('hidden');

     }
     }
     }
     })
     var quotationPkidList = [];
     $('#qw1').on('click','.quotation-body-border .quotation-list-item .quotation-item',function(){
     $(this).find('.item-head .pic-border .icon-right-null').toggleClass('selected');
     if($(this).find('.item-head .pic-border .icon-right-null').hasClass('selected')){
     quotationPkidList.push($(this).attr('data-pkid'));
     }
     else{
     quotationPkidList.removearr($(this).attr('data-pkid'));
     }
     })

     Array.prototype.removearr = function(id){
     var index = this.indexOf(id);
     if(index>-1){
     this.splice(index,1);
     }
     }
     */
    /**********************************************************************************************/
    $('body').on('click','.bottom-list',function(){
        var $this=$(this);
        var ClassName=$this.attr('class').split(' ')[1];
        switch(ClassName){
            case 'customer':
                router.navigate("customer");
                break;
            case 'email':
                router.navigate("email");
                break;
            case 'mime':
                router.navigate("mine/");
                break;
            case 'receipts':
                router.navigate("bill");
                break;
            case 'product':
                router.navigate("product");
                break;
        }
    })
    //报价说明
    $('#quotation-modify').on('click','.quotation-foot .quotation_note', function() {
        $('#quotation_way').removeClass('hidden');
    })
    //报价说明返回
    $('#quotation_note_pop').on('click','.header .icon-mail_fanhui', function() {
        $('#quotation_way').addClass('hidden');
    })

    //报价方法
    $('#productProperties').on('click','.quotation-tip', function() {
        $('#quotation_way').removeClass('hidden');
    })
    //报价方法返回
    $('#quotation_way').on('click','.header .icon-mail_fanhui', function() {
        $('#quotation_way').addClass('hidden');
    })
    //货运报价说明
    $('#freight-information').on('click','.freight-tips', function() {
        $('#freight_description').removeClass('hidden');
    })
    //货运报价说明返回
    $('#freight_description').on('click','.header .icon-mail_fanhui', function() {
        $('#freight_description').addClass('hidden');
    })

    //货运报价说明
    $('#order-track').on('click','.order-stop .order-description', function() {
        $('#order_tracking').removeClass('hidden');
    })
    //货运报价说明返回
    $('#order_tracking').on('click','.header .icon-mail_fanhui', function() {
        $('#order_tracking').addClass('hidden');
    })
    //单据使用说明
    $('#Bill').on('click','.bill-body .bill-tips',function(){
        $('#order_description').removeClass('hidden')
    })
    //单据使用说明返回
    $('#order_description').on('click','.header .icon-mail_fanhui',function(){
        $('#order_description').addClass('hidden')
    })

    //自定义信息说明
    $('#own-information').on('click','.own-description',function(){
        $('#own_description').removeClass('hidden')
    })
    //自定义信息返回
    $('#own_description').on('click','.header .icon-mail_fanhui',function(){
        $('#own_description').addClass('hidden');
    })
}

billRegist();
vp.registerFn.billRegist = billRegist;


