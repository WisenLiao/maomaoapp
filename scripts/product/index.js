;(function(){
function productModule() {
    vp.ui.isTrue = true;
    var product_module = $('#product_module'), //产品首页
        product_details = $('#product_details'),  //产品详细页
        customer_details = $('#customer_details'),  //客户详情
        public_sea_customer = $('#public-sea-customer'),  //客户详情公海页
        product_search = $('#product_search'),  //搜索页
        creatOrderName = $('#creatOrderName'),  //创建订单
        customContent = $('#customContent'),    //自定义内容
        $pagesize = 6,  //一页显示的数据
        NETReg=/[\w-]+\.(com|net|org|co|us|me|biz|in|cm|ac|ae|aero|af|ag|al|am|as|asia|at|au|ax|az|ba|be|bg|bi|bj|br|br.com|bt|by|bz|ca|cat|cc|cd|ch|ck|cl|cn|cn.com|co.nl|coop|cx|cy|cz|de|dk|dm|dz|edu|ee|eg|es|eu|eu.com|fi|fo|fr|gb|gb.com|gb.net|qc.com|ge|gl|gm|gov|gr|gs|hk|hm|hn|hr|hu|hu.com|id|ie|il|info|int|io|iq|ir|is|it|je|jobs|jp|ke|kg|kr|la|li|lt|lu|lv|ly|ma|mc|md|mil|mk|mobi|ms|mt|mu|mx|my|name|nf|ng|nl|no|no.com|nu|nz|co.nz|pl|pr|pro|pt|pw|ro|ru|sa|sa.com|sb|sc|se|se.com|se.net|sg|sh|si|sk|sm|st|so|su|tc|tel|tf|th|tj|tk|tm|tn|to|tp|tr|travel|tw|tz|ua|uk|uk.com|uk.net|ac.uk|gov.uk|us.com|uy|uy.com|uz|va|vc|ve|vg|ws|xxx|yu|za.com|arpa|com.cn|gov.cn|net.cn|org.cn|co.in|net.in|org.in|ind.in|firm.in|museum|co.uk|org.uk|wang|top|ren|com.de|ec|fm|gg|gy|mg|pe|sx|tl|tv|site|website|web|home|space|online|press|tech|store|music|shop|gw|mp|ml|cf|ga|gq|mo|news|mo.cn|com.au|net.au|org.au|asn.au|com.ai|net.ai|org.ai|off.ai|ar.com|com.ar|co.at|or.at|ae.org|com.af|net.af|org.af|africa.com|com.ag|net.ag|co.ag|nom.ag|ai|com.bd|net.bd|org.bd|bh|com.bh|com.bi|org.bn|bo|com.bo|com.br|org.br|bs|com.bs|com.bz|net.bz|co.bz|org.cc|edu.cc|cg|ci|com.ci|net.ci|co.ci|com.cm|net.cm|co.cm|com.co|net.co|nom.co|cr|co.cr|or.cr|cu|com.cu|com.cy|tm.cy|de.com|dj|co.dm|do|com.do|net.do|org.do|com.ec|net.ec|info.ec|pro.ec|med.ec|fin.ec|com.ee|com.eg|com.es|net.es|org.es|com.fj|net.fj|org.fj|info.fj|biz.fj|pro.fi|name.fj|com.fr|gd|com.ge|gf|net.gg|org.gg|co.gg|gh|com.gh|gi|com.gi|com.gl|net.gl|org.gl|edu.gl|co.gl|gp|com.gp|net.gp|mobi.gp|gr.com|com.gr|net.gr|org.gr|gt|com.gt|net.gt|org.gt|ind.gt|com.gy|net.gy|co.gy|com.hn|net.hn|org.hn|com.hr|ht|com.ht|net.ht|org.ht|hu.net|co.id|im|com.im|net.im|org.im|co.im|org.il|co.il|net.ir|org.ir|co.ir|net.je|org.je|co.je|com.jm|net.jm|org.jm|jo|com.jo|jp.net|jpn.com|info.ke|mobi.ke|com.ke|or.ke|me.ke|ne.ke|com.kg|net.kg|org.kg|com.kh|net.kh|org.kh|edu.kh|ki|com.ki|net.ki|org.ki|info.ki|mobi.ki|biz.ki|tel.ki|phone.ki|co.kr|kr.com|com.kw|net.kw|kz|com.kz|com.lb|lc|lk|org.lk|edu.lk|com.lv|net.lv|org.lv|com.ly|net.ly|org.ly|net.ma|org.ma|co.ma|com.mg|net.mg|org.mg|co.mg|com.mk|com.mm|net.mm|mn|com.ms|net.ms|org.ms|com.mt|net.mt|org.mt|com.mu|net.mu|org.mu|co.mu|ac.mu|mw|com.mx|com.my|net.my|org.my|name.my|org.mz|co.mz|na|com.na|co.na|com.ng|org.ng|edu.ng|mobi.ng|name.ng|com.nf|net.nf|info.nf|co.no|com.np|nr|com.nr|net.nr|org.nr|info.nr|biz.nr|net.nz|com.om|co.om|pa|com.pa|net.pa|org.pa|com.pe|net.pe|org.pe|com.pg|net.pg|ph|com.ph|net.ph|org.ph|pk|com.pk|net.pk|com.pl|net.pl|org.pl|edu.pl|info.pl|biz.pl|mail.pl|medial.pl|shop.pl|travel.pl|targi.pl|sos.pl|waw.pl|auto.pl|aero.pl|aid.pl|tm.pl|gsm.pl|rel.pl|pc.pl|priv.pl|tourism.pl|warszawa|realestate|pm|pn|net.pn|org.pn|co.pn|com.pr|net.pr|org.pr|ps|com.ps|net.ps|org.ps|com.pt|qa|com.qa|re|com.ro|rs|org.rs|co.rs|ru.com|rw|com.sa|com.sb|net.sb|org.sb|com.sc|net.sc|org.sc|sd|tm.se|com.sg|net.sg|org.sg|edu.sg|sl|com.sl|net.sl|org.sl|sr|com.sv|co.th|com.tj|com.tl|net.tl|org.tl|com.tn|com.tr|org.tr|info.tr|biz.tr|gen.tr|tt|com.tt|net.tt|org.tt|co.tt|info.tt|biz.tt|pro.tt|com.ua|kiev.ua|ug|org.ug|co.ug|or.ug|ac.ug|go.ug|ne.ug|sc.ug|ltd.uk|me.uk|plc.uk|us.org|com.uy|com.uz|co.uz|com.ve|co.ve|vn|vu|wf|com.ye|yt|co.za|co.zw|ad|cv|mv|sn|mz|sv|nc|sy|an|ne|sz|ao|aq|jm|td|ni|tg|aw|eh|np|er|kh|nt|bb|et|km|bd|kn|fj|kp|om|bf|fk|kw|ky|pf|fx|pg|bm|lb|lr|um|ls|bv|bw|py|gn|vi|mh|gu|mm|ye|zm|mq|sj|zr|cs|ar|bl|bn|bq|cw|mf|mr|za|zw|singles|bike|plumbing|clothing|camera|estate|construction|contractors|kitchen|land|ventures|holdings|equipment|gallery|graphics|lighting|photography|today|technology|directory|enterprises|diamonds|voyage|tips|cab|domains|shoes|limo|careers|recipes|photos|menu|academy|computer|center|management|systems|company|uno|builders|training|email|solutions|support|camp|glass|repair|education|institute|farm|watch|marketing|bargains|boutique|coffee|florist|house|solar|cheap|zone|cool|works|codes|viajes|holiday|expert|international|agency|events|rentals|productions|community|catering|cards|vacations|foundation|cleaning|properties|tools|industries|parts|dating|partners|cruises|flights|villas|tienda|condos|maison|exposed|supplies|supply|fish|associates|media|vision|services|exchange|lease|capital|engineering|report|world|toys|town|gripe|wtf|fail|limited|care|digital|direct|place|deals|cash|discount|fitness|church|life|guide|gifts|immo|university|financial|clinic|surgery|dental|finance|insure|claims|tax|fund|furniture|healthcare|restaurant|pizza|reisen|city|gratis|schule|business|network|work|beer|surf|casa|tires|money|coach|memorial|legal|delivery|energy|college|tours|golf|plus|love|run|chat|style|school|bingo|tennis|apartments|football|green|degree|engineer|software|market|mortgage|dentist|flowers|wedding|garden|fashion|ngo|ong|soy|fit|sale|video|black|adult|porn|design|casino|band|gives|vet|republican|vote|voto|army|navy|game|dog|mba|cafe|express|team|show|sarl|hockey|taxi|theater|jewelry|coupons|soccer|fyi|best|poker|nagoya|gold|rehab|wales|cymru|irish|kiwi|markets|one|live|studio|yoga|rent|lgbt|airforce|rip|ski|amsterdam|archi|bio|wine|vin|xyz|physio|car|cars|auto|family|trading|cloud|mom|group|ltd|salon|gmbh|vip|fans|security|protection|feedback|theatre|stream|tube|gdn|earth|miami|guru|xin|lol|pub|club|bid|loan|red|win|link|date|party|click|trade|science|wiki|pics|photo|help|gift|rocks|social|lawyer)\b(\.(cn|hk|uk|jp|tw))*/g;
    var ints = null;
    var int = null;
    //产品页面    记录全局变量
    modulepkid = '0',  //记录选择的分类id
    modulepages = 1,
    //搜索页记录全局变量
    searchval = '',//当前搜索内容
    searchpages = 1, //当前页码
    //产品首页
    layout_module = {
        currencySwitch : true,  //首页货币切换 为true代表当前为美元
        //布局
        layoutFun:function(){
            var me = this;
            $('.liaowei').on('click',function(e){
            	$("#product-add").remove();
                e.preventDefault();
                product_module.addClass('hidden');
				var newPage = $product_add_page.clone(true)
				newPage.removeClass("hidden");
				newPage.insertBefore("#quotation-modify");
				prodAddFn.run();
            })
            $('#product-add').find('.back').on('click',function(e){
                e.preventDefault();
                $('#product-add').find('.cancel-add-pop').removeClass('hidden');
                // $("#product-add").remove();
                // product_module.removeClass('hidden');
            })
            //添加滚动
            vp.ui.scroll(product_module,product_module.find('.product-display'));
            //货币切换
            product_module.find('.switch-money').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                if(me.currencySwitch){
                    _this.removeClass('icon-maoduo41').addClass('icon-maoduo42');
                    product_module.find('.money').each(function(){
                        $(this).text('¥'+$(this).attr('data-rmb'));  //切换成人民币
                        me.currencySwitch=false;
                    })
                }else{
                    _this.removeClass('icon-maoduo42').addClass('icon-maoduo41');
                    product_module.find('.money').each(function(){
                        $(this).text('$'+$(this).attr('data-doller'));  //切换成人民币
                        me.currencySwitch=true;
                    })
                }
            });
            //轮播图跳转至产品详情
            var $switchbox = product_module.children('.product-display').find('.carousel-box');  //轮播图容器
            $switchbox.on('click','li',function(e){
                var _this = $(this);
                e.preventDefault();
                product_module.addClass('hidden');
                product_details.removeClass('hidden');
                layout_Customer.pageno = 1; //初始化相关客户页码
                layout_Introduce.detailsFun(_this.attr('data-pkid'));
            })
        },
        //跳至搜索页
        searchFun:function(){
            product_module.find('.icon-search').on('click',function(e){
                e.preventDefault();
                product_module.addClass('hidden');
                product_search.removeClass('hidden');
            })
        },
        //产品分类选择
        SelectionFun:function(){
            var $box = product_module.find('.classify_alls'); //box
            var range = 1.6;   //基准距离
            var state = 0;     //下滑列表控制
            var sizepx = $('html').css('fontSize').replace(/px/,'');  //基本尺寸
            var sortsbox = product_module.find('.assortment-all'), //下拉列表
            $productsbox = $('.products-box');
            //下滑
            product_module.find('#slide').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                if(state == 0){
                    _this.addClass('tops');
                    product_module.find('.black_module').show();
                    state = 1;
                }else{
                    _this.removeClass('tops');
                    product_module.find('.black_module').hide();
                    state = 0;
                }
                product_module.find('.assortment-all').stop().slideToggle(200);
            });
            //下拉导航
            sortsbox.on('click','.sorts',function(e){
                e.preventDefault();
                var _this = $(this);
                state = 0;
                product_module.find('#slide').toggleClass('tops');
                _this.addClass('red-select');
                _this.siblings().removeClass('red-select');
                product_module.find('.assortment-all').stop().slideUp(200);
                product_module.find('.black_module').hide();
                $box.find('li').eq(_this.index()).children('.sort-underline').removeClass('hidden');
                $box.find('li').eq(_this.index()).siblings().children('.sort-underline').addClass('hidden');
                var $index = _this.index()-1;
                var Left = range * $index * sizepx;
                if(_this.index()==0){
                    Left=0;
                }
                $box.animate({
                    'scrollLeft':Left,
                },500)
                $productsbox.children().remove();
                layout_module.products_all('1', _this.attr('data-pkid'),layout_module.currencySwitch);
                modulepkid = _this.attr('data-pkid');  //记录id
                modulepages = 1;     //页码归零
            });
            //滑动导航
            $box.on('click','li',function(e){
                e.preventDefault();
                var _this = $(this);
                _this.children('.sort-underline').removeClass('hidden');
                _this.siblings().children('.sort-underline').addClass('hidden');
                sortsbox.find('li').eq(_this.index()).addClass('red-select').siblings().removeClass('red-select');;
                if(state == 1){
                    product_module.find('#slide').removeClass('tops');
                    product_module.find('.black_module').hide();
                    product_module.find('.assortment-all').stop().slideUp(200);
                    state = 0;
                }
                var $index = _this.index()-1;
                var Left = range * $index * sizepx;
                if(_this.index()==0){
                    Left=0;
                }
                $box.animate({
                    'scrollLeft':Left,
                },500)
                $productsbox.children().remove();
                layout_module.products_all('1', _this.attr('data-pkid'),layout_module.currencySwitch);
                modulepkid = _this.attr('data-pkid');  //记录id
                modulepages = 1;     //页码归零
            });
            //遮罩层
            product_module.find('.black_module').on('click',function(e){
                product_module.find('.assortment-all').stop().slideUp(200);
                product_module.find('.black_module').hide();
                product_module.find('.classify_alls').show();
                product_module.find('.alls').addClass('hidden');
                product_module.find('#slide').toggleClass('tops');
                state = 0;
            });
        },
        //轮播
        CarouselFun:function(){
            var _lubo = product_module.find('.carousel'); //最外层容器
            var _box = product_module.find('.carousel-box');     //图片容器
            var _cirBox='.cir_box';
            var cirOn='cir_on';
            var _cirOn='.cir_on';
            var cirlen=_box.children('li').length; //圆点的数量  图片的数量
            var luboTime=5500; //轮播时间
            var switchTime=500;//图片切换时间
            clearInterval(int);
            cir();
            //根据图片的数量来生成圆点
            function cir(){
                _lubo.append('<ul class="cir_box"></ul>');
                var cir_box=product_module.find('.cir_box');
                for(var i=0; i<cirlen;i++){
                    cir_box.append('<li style="" value="'+i+'"></li>');
                }
                var cir_dss=cir_box.width();
                cir_box.css({
                    left:'50%',
                    marginLeft:-cir_dss/2,
                    zIndex:10
                });
                cir_box.children('li').eq(0).addClass(cirOn);
            }
            //定时器
            int=setInterval(clock,luboTime);
            function clock(){
                var cir_box=product_module.find(_cirBox);
                var onLen=product_module.find(_cirOn).val();
                _box.children('li').eq(onLen).stop(false,false).animate({
                    opacity:0,
                    zIndex:1
                },switchTime);
                if(onLen==cirlen-1){
                    onLen=-1;
                }
                _box.children('li').eq(onLen+1).stop(false,false).animate({
                    opacity:1,
                    zIndex:10
                },switchTime);
                cir_box.children('li').eq(onLen+1).addClass(cirOn).siblings().removeClass(cirOn);
            }
            //触摸事件
            _box.off('touchstart');
            var startPos = null,
                isScrolling = 0,
                endPos = null;
            _box.on('touchstart',function(e){
                    startPos = null,
                    isScrolling = 0,
                    endPos = null;
                clearInterval(int);
                var _this = $(this);
                var touch = event.targetTouches[0];     //touches数组对象获得屏幕上所有的touch，取第一个touch
                startPos = {x:touch.pageX,y:touch.pageY,time:+new Date};    //取第一个touch的坐标值
            });
            _box.off('touchmove');
            _box.on('touchmove',function(e){
                var touch = event.targetTouches[0];
                if(event.targetTouches.length > 1 || event.scale && event.scale !== 1) return;
                endPos = {x:touch.pageX - startPos.x,y:touch.pageY - startPos.y};
                isScrolling = Math.abs(endPos.x) < Math.abs(endPos.y) ? 1:0;
                if(isScrolling === 0){
                    e.preventDefault();      //阻止触摸事件的默认行为，即阻止滚屏
                }
            });
            _box.off('touchend');
            _box.on('touchend',function(e){
                int=setInterval(clock,luboTime);
                if(endPos==null){return;}
                var duration = +new Date - startPos.time;    //滑动的持续时间
                if(isScrolling === 0){ //当为水平滚动时
                    if(Number(duration) > 10){
                        //右
                        if(endPos.x > 20){
                            var cir_box=product_module.find(_cirBox);
                            var onLen=product_module.find(_cirOn).val();
                            _box.children('li').eq(onLen).stop(false,false).animate({
                                opacity:0,
                                zIndex:1
                            },250);
                            if(onLen==0){
                                onLen=cirlen;
                            }
                            _box.children('li').eq(onLen-1).stop(false,false).animate({
                                opacity:1,
                                zIndex:10
                            },250);
                            cir_box.children('li').eq(onLen-1).addClass(cirOn).siblings().removeClass(cirOn);
                        //左
                        }else if(endPos.x < -20){
                            var cir_box=product_module.find(_cirBox);
                            var onLen=product_module.find(_cirOn).val();
                            _box.children('li').eq(onLen).stop(false,false).animate({
                                opacity:0,
                                zIndex:1
                            },250);
                            if(onLen==cirlen-1){
                                onLen=-1;
                            }
                            _box.children('li').eq(onLen+1).stop(false,false).animate({
                                opacity:1,
                                zIndex:10
                            },250);
                            cir_box.children('li').eq(onLen+1).addClass(cirOn).siblings().removeClass(cirOn);
                        }
                    }
                }
                e.preventDefault();
            });
        },
        //获取产品详情 跳转
        detailsFun:function(){
            var product_box = product_module.find('.product-display');
            product_box.on('click','.product',function(e){
                e.preventDefault();
                var _this = $(this);
                product_module.addClass('hidden');
                product_details.removeClass('hidden');
                layout_Introduce.detailsFun(_this.attr('data-pkid'))
                layout_Customer.pageno = 1; //初始化相关客户页码
                product_details.find('.siwdth-money').removeClass('icon-maoduo42').addClass('icon-maoduo41')
            })
        },
        //获取更多
        get_moreFun:function(){
            var $box = product_module.find('.products-box'), //box
                nomore = product_module.find('.No-more'),  //没有更多
                me = this,
                seemore = product_module.find('.See-more');  //获取更多
            seemore.on('click',function(e){
                e.preventDefault();
                modulepages+=1;
                layout_module.products_all(modulepages,modulepkid,me.currencySwitch);
            })
        },
        //获取产品列表
        products_all:function(page,pkid,moneytype) {
            var me = this,
                $box = product_module.find('.products-box'), //box
                nomore = product_module.find('.No-more'),  //没有更多
                seemore = product_module.find('.See-more');  //获取更多
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Product/GetProductList",
                debug:false,
                loadFlag:true,
            };
            var data = {pageNo:page,pageSize:$pagesize,condition:{keyword:'',categoryId:pkid} };
            vp.core.ajax(url,data,function(data){
                console.log("产品首页列表");
                console.log(data);
                //没有更多或者查看更多
                for (var i = 0; i < data.Data.Data.length; i++) {
                    $box.append(
                        '<div class="product" data-pkid="'+data.Data.Data[i].PkId+'">' +
                        '<div class="product-photo">' +
                        '<img src="' + data.Data.Data[i].ImageLink + '" alt="">' +
                        '</div>' +
                        '<p class="title">' + data.Data.Data[i].Name + '</p>' +
                        '<span class="money" data-doller='+numModify(data.Data.Data[i].USDPrice*1.2)+' data-rmb='+numModify(data.Data.Data[i].RMBPrice*1.2)+'>'+istype(moneytype)+'</span>' +
                        '</div>'
                    )
                }
                if(data.Data.RecordTotalCount==$pagesize){
                    nomore.removeClass('hidden');
                    seemore.addClass('hidden');
                }else{
                    if (data.Data.Data.length < $pagesize) {
                        nomore.removeClass('hidden');
                        seemore.addClass('hidden');
                    } else {
                        seemore.removeClass('hidden');
                        nomore.addClass('hidden');
                    };
                }
                function istype(type){
                    if(type){
                        return '$'+numModify(data.Data.Data[i].USDPrice*1.2);
                    }else{
                        return '¥'+numModify(data.Data.Data[i].RMBPrice*1.2);
                    }
                }
            },function(data){

            },function(){

            });
        },
        //获取导航栏产品类目分 默认 搜索 筛选  //轮播图获取
        get_classify:function(){
            var $header = product_module.find('.header');
            var $box_H = $header.find('.classify_alls');  //滑动选择栏
                $box_S = $header.find('.assortment-all');   //下拉选择栏
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Product/GetCategories",
                debug:false,
                loadFlag:true,
            };
            var data = {parentPkId:0};
            vp.core.ajax(url,data,function(data){
                //成功
                console.log(data)
                for(var i=0; i<data.Data.length; i++){
                    $box_H.append(
                        '<li data-pkid="'+data.Data[i].PkId+'">'+data.Data[i].Name+'<span class="sort-underline hidden"></span></li>'
                    )
                    $box_S.append(
                        '<li class="sorts" data-pkid="'+data.Data[i].PkId+'">'+data.Data[i].Name+'</li>'
                    )
                }
            },function(err){
            },function(tem){
            });
            //轮播图
            carouselbox = product_module.find('.carousel-box');
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Product/GetBanners",
                debug:false,
                loadFlag:true,
            };
            var data = {};
            vp.core.ajax(url,data,function(data){
                //成功
                console.log(data)
                for(var i = 0; i<data.Data.length; i++){
                    carouselbox.append(
                        '<li data-pkid='+data.Data[i].ProductPkId+'><img src='+data.Data[i].DisplayLink+' alt=""></li>'
                    )
                }
                layout_module.CarouselFun();  //轮播
            },function(err){
            },function(tem){
            });
        },
    },
    //搜索页
    layout_search = {
        //布局
        layoutFun:function(){
            //后退
            product_search.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                product_search.addClass('hidden');
                product_module.removeClass('hidden');
            })
            var products = product_search.find('.products-box');
            //跳转至产品详情
            products.on('click','.product',function(e){
                e.preventDefault();
                var _this = $(this);
                product_search.addClass('hidden');
                product_details.removeClass('hidden');
                layout_Introduce.detailsFun(_this.attr('data-pkid'))
                layout_Customer.pageno = 1; //初始化相关客户页码
                layout_Introduce.moneyswitch=true; //初始化币种
            });
            //添加滚动置顶
            vp.ui.scroll(product_search,product_search.find('.main-contents'));
        },
        //搜索框 执行
        searchFun:function(){
            var input = product_search.find('input[name="products"]'),
            ascertain = product_search.find('.head-right');  //确定
            var loading = product_search.find('.loading'), //查看更多
                seemore = loading.find('.See-more'), //查看更多
                nomore = loading.find('.No-more');  //没有更多
            var $box = product_search.find('.products-box'); //box
            input.on('keyup change',function(e){
                if (e.which == 13) {
                    ascertain.click(); //执行搜索
                }
                if($(this).val().trim()!==""){
                    ascertain.addClass('select-right');
                }else{
                    ascertain.removeClass('select-right');
                }
            })
            ascertain.on('click',function(e){
                e.preventDefault();
                if(!$(this).hasClass('select-right')){return;}
                $box.children().remove();
                layout_search.products_all('1',input.val().trim()); //执行搜索
                searchval = input.val().trim(); //记录搜索内容
                searchpages = 1;     //页码归1
            })
        },
        //获取更多
        get_moreFun:function(){
            var $box = product_search.find('.products-box'), //box
                nomore = product_search.find('.No-more'),  //没有更多
                seemore = product_search.find('.See-more');  //获取更多
            seemore.on('click',function(e){
                e.preventDefault();
                searchpages+=1;
                layout_search.products_all(searchpages,searchval);
            })
        },
        //获取搜索列表
        products_all:function(page,val){
            var $box = product_search.find('.products-box'), //box
                nomore = product_search.find('.No-more'),  //没有更多
                seemore = product_search.find('.See-more');  //获取更多
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Product/GetProductList",
                debug:false,
                loadFlag:true,
            };
            var data = {pageNo:page,pageSize:$pagesize,condition:{keyword:val}};
            vp.core.ajax(url,data,function(data){
                //成功
                console.log("搜索页列表");
                console.log(data)
                //没有更多或者查看更多
                for(var i=0 ; i<data.Data.Data.length ; i++){
                    $box.append(
                        '<div class="product" data-pkid='+data.Data.Data[i].PkId+'>'+
                        '<div class="product-photo">'+
                        '<img src="'+ data.Data.Data[i].ImageLink +'" alt="">'+
                        '</div>'+
                        '<p class="title">'+ data.Data.Data[i].Name +'</p>'+
                        '<span class="money" data-rmb='+numModify(data.Data.Data[i].RMBPrice*1.2)+' data-doller='+numModify(data.Data.Data[i].USDPrice*1.2)+'>$'+ numModify(data.Data.Data[i].USDPrice*1.2)+' </span>'+
                        '</div>'
                    )
                }
                product_search.find('.loading').removeClass('hidden');
                if(data.Data.RecordTotalCount == $pagesize){
                    seemore.removeClass('hidden');
                    nomore.addClass('hidden');
                }else{
                    if(data.Data.Data.length < $pagesize ){
                        nomore.removeClass('hidden');
                        seemore.addClass('hidden');
                    }else{
                        seemore.removeClass('hidden');
                        nomore.addClass('hidden');
                    };
                }
            },function(err){
            },function(tem){
            });
        },
    },
    //产品详情页   赋值id到客户详情
    layout_Introduce = {
        moneyswitch:true,  //为true为人民币
        //布局
        layoutFun:function(){
            //相关客户跳转
            product_details.find('.Related-customers').on('click',function(e){
                product_details.find('.customers').click();
            })
            product_details.find('.customers').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                product_details.addClass('hidden');
                customer_details.removeClass('hidden');
                customer_details.find('.products-box').children().remove();
                layout_Customer.SeemoreFun($(this).attr('data-pkid'),1); //查看相关客户
                layout_Customer.pageno=1;
            })
            //后退到首页
            product_details.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                product_details.addClass('hidden');
                product_module.removeClass('hidden');
            })
            //币种切换
            var  me = this;
            product_details.find('.siwdth-money').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                if(me.moneyswitch){
                    _this.removeClass('icon-maoduo41').addClass('icon-maoduo42');
                    product_details.find('.Product-price').text('¥'+product_details.find('.Product-price').attr('data-rmb'));
                    me.moneyswitch = false;
                }else{
                    _this.removeClass('icon-maoduo42').addClass('icon-maoduo41');
                    product_details.find('.Product-price').text('$'+product_details.find('.Product-price').attr('data-doller'));
                    me.moneyswitch = true;
                }
            })
        },
        //接收详情数据
        detailsFun:function(pkid){
            var keybox = product_details.find('.keys'),//关键词
            price = product_details.find('.Product-price'),  //金额
            name = product_details.find('.Product-title'),   //名字
            //number = product_details.find('.Product-number'),   //编号
            picture = product_details.find('.picture-box'),   //轮播图
            graphic = product_details.find('.graphic-box');  //图文详情
            product_details.find('.cir_box').remove();
            product_details.find('.siwdth-money').removeClass('.icon-maoduo42').addClass('.icon-maoduo41')
            layout_Introduce.moneyswitch=true; //初始化币种
            picture.children().remove();
            keybox.children().remove();
            name.text("");
            picture.children().remove();
            graphic.children().remove();
            //相关客户  开始报价绑定pkid
            product_details.find('.customers').attr('data-pkid',pkid);
            product_details.find('.Price-right').attr('data-pkid',pkid);
            customer_details.find('.customers').attr('data-pkid',pkid);
            customer_details.find('.Price-right').attr('data-pkid',pkid);
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Product/GetProdctByPkId",
                debug:false,
                loadFlag:true,
            };
            var data = {pkId:pkid};
            vp.core.ajax(url,data,function(data){
                //成功
                //产品轮播展示图
                console.log('产品详情列表');
                console.log(data);
                for(var i=0;i<data.Data.Images.length; i++){
                    picture.append(
                        '<li><img src="'+data.Data.Images[i].DispalyLink+'" alt=""></li>'
                    )
                }
                layout_Introduce.IntroduceFun();
                //产品编号
                // number.text( data.Data.ProductNo );
                //产品名
                name.text( data.Data.Name );
                //金额
                price.attr('data-rmb', numModify(data.Data.RMBPrice*1.2));
                price.attr('data-doller', numModify(data.Data.USDPrice*1.2));
                price.text('$'+numModify(data.Data.USDPrice*1.2));
                //关键词
                for(var i=0; i<data.Data.Keywords.length; i++){
                    keybox.append(
                        '<span class="key">'+data.Data.Keywords[i]+'</span>'
                    )
                }
                //图文详情
                if(data.Data.Description!=null||undefined){
                    var photodetails = data.Data.Description.replace("http://",'https://');
                    graphic.append(
                        photodetails
                    )
                }
            },function(err){
            },function(tem){
            });
        },
        //详情页轮播
        IntroduceFun:function(){
            var _lubo = product_details.find('.Product-picture'); //最外层容器
            var _box = product_details.find('.picture-box');     //图片容器
            var _cirBox='.cir_box';
            var cirOn='cir_on';
            var _cirOn='.cir_on';
            var cirlen=_box.children('li').length; //圆点的数量  图片的数量
            var luboTime=5000; //轮播时间
            var switchTime=500;//图片切换时间
            clearInterval(ints);
            cir();
            //根据图片的数量来生成圆点
            function cir(){
                _lubo.append('<ul class="cir_box"></ul>');
                var cir_box=product_details.find(_cirBox);
                for(var i=0; i<cirlen;i++){
                    cir_box.append('<li style="" value="'+i+'"></li>');
                }
                var cir_dss=cir_box.width();
                cir_box.css({
                    left:'50%',
                    marginLeft:-cir_dss/2,
                    zIndex:15
                });
                cir_box.children('li').eq(0).addClass(cirOn);
            }
            //定时器
            ints=setInterval(clock,luboTime);
            function clock(){
                var cir_box=product_details.find(_cirBox);
                var onLen=product_details.find(_cirOn).val();
                _box.children('li').eq(onLen).stop(false,false).animate({
                    opacity:0,
                    zIndex:1
                },switchTime);
                if(onLen==cirlen-1){
                    onLen=-1;
                }
                _box.children('li').eq(onLen+1).stop(false,false).animate({
                    opacity:1,
                    zIndex:10
                },switchTime);
                cir_box.children('li').eq(onLen+1).addClass(cirOn).siblings().removeClass(cirOn);
            }
            //触摸事件
            var startPos = null,
                isScrolling = 0,
                endPos = null;
            _box.off('touchstart');
            _box.on('touchstart',function(e){
                startPos = null,
                isScrolling = 0,
                endPos = null;
                clearTimeout(ints);
                var touch = event.targetTouches[0];     //touches数组对象获得屏幕上所有的touch，取第一个touch
                startPos = {x:touch.pageX,y:touch.pageY,time:+new Date};    //取第一个touch的坐标值
            });
            _box.off('touchmove');
            _box.on('touchmove',function(e){
                var touch = event.targetTouches[0];
                if(event.targetTouches.length > 1 || event.scale && event.scale !== 1) return;
                endPos = {x:touch.pageX - startPos.x,y:touch.pageY - startPos.y};
                isScrolling = Math.abs(endPos.x) < Math.abs(endPos.y) ? 1:0;
                if(isScrolling === 0){
                    e.preventDefault();      //阻止触摸事件的默认行为，即阻止滚屏
                }
            })
            _box.off('touchend');
            _box.on('touchend',function(e){
                ints=setInterval(clock,luboTime);
                if(endPos==null){return;}
                var duration = +new Date - startPos.time;    //滑动的持续时间
                if(isScrolling === 0){ //当为水平滚动时
                    if(Number(duration) > 10){
                        //右
                        if(endPos.x > 20){
                            var cir_box=product_details.find(_cirBox);
                            var onLen=product_details.find(_cirOn).val();
                            _box.children('li').eq(onLen).stop(false,false).animate({
                                opacity:0,
                                zIndex:1
                            },250);
                            if(onLen==0){
                                onLen=cirlen;
                            }
                            _box.children('li').eq(onLen-1).stop(false,false).animate({
                                opacity:1,
                                zIndex:10
                            },250);
                            cir_box.children('li').eq(onLen-1).addClass(cirOn).siblings().removeClass(cirOn);
                            //左
                        }else if(endPos.x < -20){
                            var cir_box=product_details.find(_cirBox);
                            var onLen=product_details.find(_cirOn).val();
                            _box.children('li').eq(onLen).stop(false,false).animate({
                                opacity:0,
                                zIndex:1
                            },250);
                            if(onLen==cirlen-1){
                                onLen=-1;
                            }
                            _box.children('li').eq(onLen+1).stop(false,false).animate({
                                opacity:1,
                                zIndex:10
                            },250);
                            cir_box.children('li').eq(onLen+1).addClass(cirOn).siblings().removeClass(cirOn);
                        }
                    }
                }
                e.preventDefault();
            });
        },
    },
    //相关客户
    layout_Customer = {
        //布局
        pageno : 1,
        layoutFun:function(){
            //跳转到产品详细
            customer_details.find('.product-details').on('click',function(e){
                e.preventDefault();
                customer_details.addClass('hidden');
                product_details.removeClass('hidden');
            })
            //后退到首页
            customer_details.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                customer_details.addClass('hidden');
                product_module.removeClass('hidden');
            })
            //跳转至公海查看详情
            var box = customer_details.find('.products-box');
            box.on('click','.products-customer',function(e){
                e.preventDefault();
                customer_details.find('.them-customer-is').removeClass('them-customer-is');
                $(this).addClass('them-customer-is');
                customer_details.addClass('hidden');
                public_sea_customer.find('.fullcontach').addClass('hidden');
                public_sea_customer.removeClass('hidden');
                layout_customerdetails.getCustomers($(this).attr('data-url'));
            })
            public_sea_customer.find('.prev.lf').on('touchstart touchend',function(e){
                if(e.type=="touchstart"){
                    $(this).addClass('Ace-select');
                }
                if(e.type=="touchend"){
                    $(this).removeClass('Ace-select');
                }
            })
            public_sea_customer.find('.next.lf').on('touchstart touchend',function(e){
                if(e.type=="touchstart"){
                    $(this).addClass('Ace-select');
                }
                if(e.type=="touchend"){
                    $(this).removeClass('Ace-select');
                }
            })
            //下一个
            public_sea_customer.find('.next.lf').on('click',function(){
                var them = customer_details.find('.them-customer-is');
                if(them.next().length==0){
                    return;
                }
                them.next().addClass('them-customer-is');
                them.removeClass('them-customer-is');
                layout_customerdetails.getCustomers(them.next().attr('data-url'))
            })
            //上一个
            public_sea_customer.find('.prev.lf').on('click',function(){
                var them = customer_details.find('.them-customer-is');
                if(them.prev().length==0){
                    return;
                }
                them.prev().addClass('them-customer-is');
                them.removeClass('them-customer-is');
                layout_customerdetails.getCustomers(them.prev().attr('data-url'))
            })
        },
        //获取
        SeemoreFun:function(pkid,pageno){
            var nomore = customer_details.find('.No-more'),  //没有更多
                seemore = customer_details.find('.See-more');  //获取更多
            var me = this;
            var $box = customer_details.find('.products-box');
            var $pagesize = 8;
            console.log(pkid,pageno);
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Product/GetProdctRelatedCustomer",
                debug:false,
                loadFlag:true,
            };
            var data = {ProductId:pkid,pageSize:$pagesize,pageIndex:pageno};
            vp.core.ajax(url,data,function(data){
                //成功
                console.log(data)
            },function(data){
                console.log(data);
                for(var i=0; i<data.Data.length; i++){
                    $box.append(
                        '<div class="products-customer" data-pkid='+data.Data[i].ProductPkId+' data-url='+data.Data[i].Domain+'>'+
                        '<div class="customer-name"><span class="first-name">'+data.Data[i].CompanyName.charAt(0) +'</span></div>'+
                        '<div class="customer-details">'+
                        '<div class="title"><span class="titles">'+ data.Data[i].CompanyName +'</span><span class="icon-to-right"></span></div>'+
                        '<p class="introduce">'+data.Data[i].CompanyIntroduction+'</p>'+
                        '</div>'
                    )
                }
                customer_details.find('.loading').removeClass('hidden');
                if(data.RecordTotalCount == $pagesize){
                    nomore.removeClass('hidden');
                    seemore.addClass('hidden');
                }else{
                    if (data.Data.length < $pagesize) {
                        nomore.removeClass('hidden');
                        seemore.addClass('hidden');
                    } else {
                        seemore.removeClass('hidden');
                        nomore.addClass('hidden');
                    };
                }
            },function(tem){
            });
        },
        //加载更多
        loadmoreFun:function(){
            var nomore = customer_details.find('.No-more'),  //没有更多
                seemore = customer_details.find('.See-more');  //获取更多
            seemore.on('click',function(e){
                e.preventDefault();
                layout_Customer.pageno+=1;
                console.log(layout_Customer.pageno )
                layout_Customer.SeemoreFun(customer_details.find('.customers').attr('data-pkid'),layout_Customer.pageno);
            })
        },
    },
    //相关客户跳至公海查看详情
    layout_customerdetails = {
        layoutFun:function(){
            var means = public_sea_customer.find('.data-box'),   //基本资料
                historys = public_sea_customer.find('.history-box');//跟进记录
            //后退
            public_sea_customer.find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                public_sea_customer.addClass('hidden');
                customer_details.removeClass('hidden');
            });
            //基本资料，跟进记录切换
            public_sea_customer.find('.nav-box').children('.data').on('click',function(e){
                e.preventDefault();
                $(this).addClass('active').siblings().removeClass('active');
                means.removeClass('hidden');
                historys.addClass('hidden');
            });
            public_sea_customer.find('.nav-box').children('.history').on('click',function(e){
                e.preventDefault();
                $(this).addClass('active').siblings().removeClass('active');
                historys.removeClass('hidden');
                means.addClass('hidden');
            })
            //公海信息转到私海
            public_sea_customer.find('.bottom').find('.send').on('click',function(e){
                e.preventDefault();
                var _this = $(this);
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Sea/AddPrivateSeaBySeaPublic",
                    debug:false,
                    loadFlag:true,
                };
                var data = {Domain:_this.attr('data-url')};
                vp.core.ajax(url,data,function(data){
                    console.log(data);
                    vp.ui.pop('icon-right','添加成功');
                },function(data){
                    vp.ui.pop('icon-err',vp.ui.showMessage(data.Code))
                },function(tem){
                });
            })
            //点击是否收起关键词
            public_sea_customer.on('click','.fullcontach .key-box .seeMoreMessage',function(){
                var $this=$(this);
                $this.toggleClass('active');
                if($this.hasClass('active')){
                    $this.html('点击收起');
                    $this.siblings('.text').addClass('active');
                }else{
                    $this.html('查看更多');
                    $this.siblings('.text').removeClass('active');
                }
            })
            public_sea_customer.on('click','.content .data-box .message .more',function(){
                var $this=$(this);
                $this.toggleClass('active');
                if($this.hasClass('active')){
                    $this.html('点击收起');
                    $('#public-sea-customer .content .data-box .message .text').addClass('showcontent');
                }else{
                    $this.html('查看更多');
                    $('#public-sea-customer .content .data-box .message .text').removeClass('showcontent');
                }
            });
        },
        //获取客户信息
        getCustomer:function(urls){
            var historybox = public_sea_customer.find('.history-box'), //跟进记录
                databox = public_sea_customer.find('.data-box'),
                titles = databox.children('.message').children('.name'), //标题
                messages= databox.children('.message').children('.text'),  //介绍
                unfolded = databox.children('.message').children('.more'),  //展开收起
                websites = databox.children('.company-message').children('.text').children('.website'), //网址
                customersall = databox.children('.company-message').children('.linkman').children('.num'), //联系人数
                countrys = databox.children('.company-message').children('.country').children('.address'),  //国家
                emailseas = historybox.children('.mail-times').children('.num'),  //邮件累计查看次数
                validcustomers = historybox.children('.linkman').eq(0).children('.num'), //有效联系人数
                salesmans = historybox.children('.linkman').eq(1).children('.num'),  //跟进业务员数量
                lasttimers = historybox.children('.prev-sendtime').children('.num'),  //最后发送时间
                fisttiemrs = historybox.children('.prev-sendtime').children('.num'),  //最后收到邮件时间
                completes = public_sea_customer.find('.bottom').find('.send');       //转到私海
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Sea/GetSeaDetailByDomain",
                debug:false,
                loadFlag:true,
            };
            var data = {domain:urls,seaType:0};
            vp.core.ajax(url,data,function(data){
                //成功
                console.log(data);
                //基本资料
                titles.text( data.Data.Title ); //标题
                messages.text( data.Data.Descripte );  //介绍
                websites.text( data.Data.Domain )//地址
                if(data.Data.ContactsNumber>9){
                    data.Data.ContactsNumber = parseInt(data.Data.ContactsNumber/10);
                }
                customersall.text( data.Data.ContactsNumber+"0+" )//联系人数
                //countrys.text() //国家信息
                //跟进记录
                emailseas.text( data.Data.FollowUpRecord.EmailReadCount ) //邮件累计查看次数
                validcustomers.text( data.Data.FollowUpRecord.ActiveNunber ) //有效联系人
                salesmans.text( data.Data.FollowUpRecord.UsedUserCount )//跟进业务员数量
                lasttimers.text( data.Data.FollowUpRecord.LastSendEmailTime ) //最后发送时间
                fisttiemrs.text( data.Data.FollowUpRecord.LastReceiveEmailTime ) //最后收到邮件时间
                //转到私海添加url
                completes.attr('data-url',data.Data.Domain); //添加跳转地址
            },function(data){
                console.log(data);
            },function(tem){
            });
        },
        getCustomers:function(urls) {
            console.log(urls);
            var them = customer_details.find('.them-customer-is');
            if(them.next().length==0){
                public_sea_customer.find('.next.lf').addClass('disabled');
            }else{
                public_sea_customer.find('.next.lf').removeClass('disabled');
            }
            if(them.prev().length==0){
                public_sea_customer.find('.prev.lf').addClass('disabled');
            }else{
                public_sea_customer.find('.prev.lf').removeClass('disabled');
            }
            getCustomerFullcontact(urls);
            var historybox = public_sea_customer.find('.history-box'), //跟进记录
                databox = public_sea_customer.find('.data-box'),
                titles = databox.children('.message').children('.name'), //标题
                messages= databox.children('.message').children('.text'),  //介绍
                unfolded = databox.children('.message').children('.more'),  //展开收起
                websites = databox.children('.company-message').children('.text').children('.website'), //网址
                customersall = databox.children('.company-message').children('.linkman').children('.num'), //联系人数
                countrys = databox.children('.company-message').children('.country').children('.address'),  //国家
                emailseas = historybox.children('.mail-times').children('.num'),  //邮件累计查看次数
                validcustomers = historybox.children('.linkman').eq(0).children('.num'), //有效联系人数
                salesmans = historybox.children('.linkman').eq(1).children('.num'),  //跟进业务员数量
                lasttimers = historybox.children('.prev-sendtime').children('.num'),  //最后发送时间
                fisttiemrs = historybox.children('.prev-sendtime').children('.num'),  //最后收到邮件时间
                completes = public_sea_customer.find('.bottom').find('.send');       //转到私海
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Sea/GetSeaDetailByDomain",
                debug:false,
                loadFlag:true,
            };
            var data = {domain:urls,seaType:0};
            vp.core.ajax(url,data,function(data){
                //基本资料
                console.log(data)
                titles.text( data.Data.Title ); //标题
                messages.text( data.Data.Descripte );  //介绍
                websites.text( data.Data.Domain )//地址
                customersall.text( returnNumBerType(data.Data.ContactsNumber) )//联系人数
                //countrys.text() //国家信息
                //跟进记录
                if(data.Data.FollowUpRecord!=null){
                    emailseas.text( data.Data.FollowUpRecord.EmailReadCount ) //邮件累计查看次数
                    validcustomers.text( data.Data.FollowUpRecord.ActiveNunber ) //有效联系人
                    salesmans.text( data.Data.FollowUpRecord.UsedUserCount )//跟进业务员数量
                    lasttimers.text( data.Data.FollowUpRecord.LastSendEmailTime ) //最后发送时间
                    fisttiemrs.text( data.Data.FollowUpRecord.LastReceiveEmailTime ) //最后收到邮件时间
                }
                //转到私海添加url
                completes.attr('data-url',data.Data.Domain); //添加跳转地址
            },function(data){
                console.log(data);
            },function(tem){
            });
            function getCustomerFullcontact(website) {
                    var pubSeaDetail = $('#public-sea-customer');
                    var url = {
                        local: "data/testDemo/data.json",
                        type: "get",
                        server: "/v1/Sea/GetCompanyByFC",
                        debug: false,
                        loadFlag: false
                    };
                    var data = {domain: website};
                    vp.core.ajax(url, data, function (e) {
                        //成功
                        console.log(e)
                        var data = e.Data;
                        if (data != null && data.status == 200) {
                            var Imglogo = data.logo;
                            pubSeaDetail.find('.fullcontach').removeClass('hidden'); //详细信息
                            if (Imglogo != null) {//详细信息图片 显示or隐藏
                                pubSeaDetail.find('.fullcontach .xaingxi-message .photo-box').removeClass('hidden');
                                pubSeaDetail.find('.fullcontach .xaingxi-message .photo-box img').attr('src', Imglogo.replace('https', 'http'));
                            } else {
                                pubSeaDetail.find('.fullcontach .xaingxi-message .photo-box').addClass('hidden');
                            }
                            var organization = data.organization;
                            if (organization != null) {
                                //公司名
                                var companyName = organization.name;
                                if (companyName != null) {
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .company').removeClass('hidden');
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .company .text').html(companyName);
                                } else {
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .company').addClass('hidden');
                                }
                                //员工数
                                var Ycount = organization.approxEmployees;
                                if (Ycount > 0) {
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .count').removeClass('hidden');
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .count .text').removeClass('hidden')
                                        .html(returnNumBerType(Ycount));
                                } else {
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .count').addClass('hidden');
                                }
                                //成立时间
                                var time = data.organization.founded;
                                if (time != null) {
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .time').removeClass('hidden');
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .time .text').html(time);
                                } else {
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .time').addClass('hidden');
                                }
                                //概述
                                var overview = data.organization.overview;
                                if (overview != null) {
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .gaishu').removeClass('hidden');
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .gaishu .text').html($.trim(overview));
                                } else {
                                    pubSeaDetail.find('.fullcontach .xaingxi-message .ulbox .gaishu').addClass('hidden');
                                }
                                //相关网址
                                var links = organization.links;
                                if (links && links.length > 0) {
                                    var clone = pubSeaDetail.find('.fullcontach .xiangguan-net .modalLi .li').clone();
                                    pubSeaDetail.find('.fullcontach .xiangguan-net').removeClass('hidden');
                                    pubSeaDetail.find('.fullcontach .xiangguan-net .ulbox').empty();
                                    for (var i = 0; i < links.length; i++) {
                                        var item = clone.clone();
                                        var url = links[i].url.match(NETReg);
                                        item.find('.pingtai').html(links[i].label);
                                        item.find('.web').html('www.' + url);
                                        pubSeaDetail.find('.fullcontach .xiangguan-net .ulbox').append(item);
                                    }
                                } else {
                                    pubSeaDetail.find('.fullcontach .xiangguan-net').addClass('hidden');
                                }
                                //关键词
                                var keyWorld = organization.keywords;
                                if (keyWorld && keyWorld.length > 0) {
                                    pubSeaDetail.find('.fullcontach .key-box .text').html('');
                                    keyWorld = keyWorld.join('、');
                                    pubSeaDetail.find('.fullcontach .key-box .text').html(keyWorld);
                                    pubSeaDetail.find('.fullcontach .key-box').removeClass('hidden');
                                } else {
                                    pubSeaDetail.find('.fullcontach .key-box').addClass('hidden');
                                }
                                //详细信息全部为空隐藏
                                if (Imglogo == null && companyName == null && Ycount < 1 && time == null && overview == null) {
                                    pubSeaDetail.find('.fullcontach .xaingxi-message').addClass('hidden');
                                }
                            }
                            //社交信息
                            var socialProfiles = data.socialProfiles;
                            if (socialProfiles && socialProfiles.length > 0) {
                                var clone = pubSeaDetail.find('.fullcontach .shejiaoModal').clone();
                                var len = socialProfiles.length;
                                var item = '';
                                pubSeaDetail.find('.fullcontach .shejiao-message').remove();
                                clone.removeClass('hidden').removeClass('shejiaoModal').addClass('shejiao-message');
                                for (var i = 0; i < len; i++) {
                                    item = clone.clone();
                                    item.find('.title .name').html(socialProfiles[i].typeName);
                                    item.find('.fullBox .name').html(socialProfiles[i].username);
                                    item.find('.fullBox .text').html(socialProfiles[i].bio);
                                    item.find('.net .text').html(socialProfiles[i].url.match(NETReg));
                                    item.find('.shejiao-bottom .box1 .num').html(socialProfiles[i].following);
                                    item.find('.shejiao-bottom .box2 .num').html(socialProfiles[i].followers);
                                    pubSeaDetail.find('.fullcontach .shejiaoModal').before(item);
                                }
                            } else {
                                pubSeaDetail.find('.fullcontach .shejiao-message').addClass('hidden');
                            }
                            //排名
                            var traffic = data.traffic;
                            if (traffic != null) {
                                var topCountryRanking = traffic.topCountryRanking;
                                var ranking = traffic.ranking;
                                var len = '';
                                var clone = '';
                                if ($.trim(topCountryRanking) == '' && ($.trim(ranking) == '')) {
                                    pubSeaDetail.find('.fullcontach .paiming').addClass('hidden');
                                } else {
                                    pubSeaDetail.find('.fullcontach .paiming .modalPaiMing').siblings('.list').remove();
                                    if ((topCountryRanking && topCountryRanking.length > 0) && (ranking && ranking.length > 0)) {
                                        for (var i = 0; i < topCountryRanking.length; i++) {
                                            for (var j = 0; j < ranking.length; j++) {
                                                if (topCountryRanking[i].locale == ranking[j].ranking) {
                                                    topCountryRanking.splice(i, 1)
                                                }
                                            }
                                        }
                                        len = topCountryRanking.length;
                                        clone = pubSeaDetail.find('.fullcontach .paiming .modalPaiMing .list').clone();
                                        for (var k = 0; k < len; k++) {
                                            var item = clone.clone();
                                            item.find('.name').html(topCountryRanking[k].locale);
                                            item.find('.num').html(topCountryRanking[k].rank);
                                            pubSeaDetail.find('.fullcontach .paiming .modalPaiMing').before(item);
                                        }
                                        pubSeaDetail.find('.fullcontach .paiming').removeClass('hidden');
                                    } else if ((topCountryRanking && topCountryRanking.length == 0) || (ranking && ranking.length == 0)) {
                                        topCountryRanking = topCountryRanking.concat(ranking);
                                        len = topCountryRanking.length;
                                        clone = pubSeaDetail.find('.fullcontach .paiming .modalPaiMing .list').clone();
                                        for (var k = 0; k < len; k++) {
                                            var item = clone.clone();
                                            item.find('.name').html(topCountryRanking[k].locale);
                                            item.find('.num').html(topCountryRanking[k].rank);
                                            pubSeaDetail.find('.fullcontach .paiming .modalPaiMing').before(item);
                                        }
                                        pubSeaDetail.find('.fullcontach .paiming').removeClass('hidden');
                                    } else {
                                        pubSeaDetail.find('.fullcontach .paiming').addClass('hidden');
                                    }
                                }
                            } else {
                                pubSeaDetail.find('.fullcontach .paiming').addClass('hidden');
                            }
                        }
                    }, function (e) {

                    }, function () {

                    });
                }
        }
    },
    //自定义信息
    layout_customContent = {
        layoutFun:function(){
            //取消按钮
            $("#customContent").find("header .cancelBtn").on("click",function(){
                product_generatedPriceSheet.removeClass("hidden").siblings("#customContent").addClass("hidden");
            })
            //确定按钮
            $("#customContent").find("header .sureBtn").on("click",function(){
                product_generatedPriceSheet.removeClass("hidden").siblings("#customContent").addClass("hidden");
                product_generatedPriceSheet.find("li.addCustomBtn").addClass("hidden");
                var $customText =  $("#customContent article textarea").val();
                $("#generatedPriceSheet").find("article .customInforList").removeClass("hidden").find("p").html($customText);
            })
        }
    },
    //弹窗说明与条款
    layout_windowPop = {
        layoutFun:function(){
            var product_box = $('#product_box');
            var pops = $('#quotation_way');  //报价说明
            product_box.find('.quotation_note').on('click',function(e){
                e.preventDefault();
                pops.removeClass('hidden');
            })
            //关闭条款框;
            product_box.find('.illustrate').find('.icon-mail_fanhui').on('click',function(e){
                e.preventDefault();
                $(this).closest('.illustrate').addClass('hidden');
            })
        },
    };
    //产品首页
    layout_module.layoutFun();    //布局
    layout_module.get_classify();  //获取导航栏产品类目分默认搜索筛选
    layout_module.searchFun();    //搜索
    //layout_module.CarouselFun();  //轮播
    layout_module.SelectionFun(); //分类选择
    layout_module.get_moreFun();  //获取更多
    layout_module.detailsFun();  //获取产品详情
    layout_module.products_all(1,"0",layout_module.currencySwitch);  //获取产品列表
    //搜索页
    layout_search.layoutFun();  //布局
    layout_search.get_moreFun();  //获取更多
    layout_search.searchFun();  //搜索框
    //产品详情页
    layout_Introduce.layoutFun();  //布局
    //客户详情
    layout_Customer.layoutFun(); //布局
    //客户详情跳至公海查看
    layout_customerdetails.layoutFun();
    //layout_Customer.SeemoreFun(); //查看更多
    layout_Customer.loadmoreFun();  //加载更多
    //自定义信息
    layout_customContent.layoutFun();
    //弹窗说明和条款
    layout_windowPop.layoutFun();
    //保留两位处理
    // function numModify(data){
    //     return Math.floor(data * 100) / 100;
    // }
    function returnNumBerType(v){
        if(v==0){
            return 0
        }else if(v<10){
            return v;
        }else{
            v=parseInt(v/10);
            return v+'0+';
        }
    }
    function sWitch(element){
        var endPos = null;
        var startPos = null;
        var isScrolling = null;
        element.on('touchstart',function(e){
            var touch = event.targetTouches[0];
            startPos = {x: touch.pageX, y: touch.pageY, time: +new Date};
        });
        element.on('touchmove',function(e){
            var touch = event.targetTouches[0];
            if(event.targetTouches.length > 1 || event.scale && event.scale !== 1) return;
            endPos = { x:touch.pageX - startPos.x,y:touch.pageY - startPos.y};
            isScrolling = Math.abs(endPos.x) < Math.abs(endPos.y) ? 1:0;  //1为y轴滑动，0为x轴滑动
            if(isScrolling === 0){e.preventDefault();}      //阻止触摸事件的默认行为，即阻止滚屏
        });
        element.on('touchend',function(e){
            e.preventDefault();
            var duration = +new Date - startPos.time;    //滑动的持续时间
            if(isScrolling === 0){ //当为水平滚动时
                if(Number(duration) > 10){
                    if(endPos.x > 20) {
                        //右滑
                    }
                    else if(endPos.x < -20) {
                        //左滑
                    }
                }
            }
        });
    }
    //footer栏跳转
    product_module.on('click','#footer-bottom .bottom-list',function(){
        var $this=$(this);
        var ClassName=$this.attr('class').split(' ')[1];
        switch(ClassName){
            case 'customer':
                router.navigate("customer/");
                break;
            case 'email':
                router.navigate("email/");
                break;
            case 'mime':
                router.navigate("mine/");
                break;
            case 'receipts':
                router.navigate("bill/");
                break;
            case 'product':
                router.navigate("product/");
                break;
        }
    })
    //小廖
    var prodAddFn = {
        run:function () {
        	var $prodAddDiv = $("#product-add");
            //添加产品步骤切换
            var $liBtn = $prodAddDiv.find(".product-add-foot li.li-btn");
            var $productAddBody = $prodAddDiv.find(".product-add-body");
            $liBtn.on("click",function () {
                var index = $(this).index();
                if(index==6){
                    $("#product-add-next-step").addClass("hidden").siblings("span").removeClass('hidden');
                }else{
                    $("#product-add-next-step").removeClass("hidden").siblings("span").addClass("hidden");
                }
                $productAddBody.eq(index/2).removeClass("hidden").siblings(".product-add-body").addClass("hidden");
            })
            //选择分类描述
            $prodAddDiv.find(".add-descriptions-page").on("click",".backDesc",function () {
                $prodAddDiv.find(".add-descriptions-page").addClass("hidden");
            });

            //添加图片
            var addprodPicNum = 0;
            var $addProdpic = $("#product-add .prod-add-pic");
			
            //产品图片上传↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            $('#add-prodpic-btn').change(function (e) {
//              showPreview_2(this);
				vp.ui.uploadPicture($(this)[0],function(res){
					    var param = {
                            "FileName": res.origin.name,
                            "Base64Str": res.base64
                        };
					 	var url = {
                            local:"data/testDemo/data.json",
                            type:"post",
                            server:"/v1/Product/UploadImage",
                            debug:false,
                            loadFlag:true,
                        };

                        vp.core.ajax(url,param,function(data){
                            //成功
                            if (!data.IsSuccess) return;
                            var DisplayLink = data.Data.DisplayLink;
                            var OssPath = data.Data.OssPath;
                            var $newImg = $('<li><span class="iconBtn"><i class="icon-maoduo0"></i></span><img class="thumbnail" localfilepath="' + res.origin.name + '" osspath="' + OssPath +'" src="' + DisplayLink + '"/></li>');
                            $newImg.insertBefore($addProdpic.find(".add-prodpic-btn"));
                            addprodPicNum++;
                            $addProdpic.find(".numofPic").html("" + addprodPicNum + "/9")
                            if(addprodPicNum == 9){
                                $addProdpic.find(".add-prodpic-btn").hide();
                            }
                        },function(err){
                        },function(tem){
                        });
				})
            });
           
            
            //产品图片上传↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

            $addProdpic.on("click",".iconBtn",function(){
                $(this).parent("li").remove();
                addprodPicNum--;
                $addProdpic.find(".numofPic").html("" + addprodPicNum + "/9")
                if(addprodPicNum<9){
                    $addProdpic.find(".add-prodpic-btn").show();
                }
            })
            //其他图片上传↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            var pic_index = 0;
            $('#add-descPic-btn')[0].onchange = function(e){
                showPreview(this);
            }
            function showPreview(source) {
                var file = source.files[0];
                console.log(file)
                var name = file.name;
                //只允许上传图片
                if (!/image\/\w+/.test(file.type)) {
                    //alert("请确保文件为图像类型");
                    return;
                }
                //检查浏览器支持性
                if(window.FileReader) {
                    var fr = new FileReader();  //实例化
                    fr.onloadend = function(e) {
                        $('#photo_module').removeClass('hidden');
                        new AlloyCrop({
                            image_src:e.target.result,
                            box:$('#photo_module').get(0),
                            circle:false, // optional parameters , the default value is false
                            width: 400,
                            height: 400,
                            ok:function (base64, canvas) {
                                sss(name,base64);
                                $('#photo_module').addClass('hidden');
                            },
                            cancel:function(){
                                $('#photo_module').addClass('hidden');
                            },
                            ok_text: "完成",
                            cancel_text: "取消"
                        });
                    };
                    fr.readAsDataURL(file);  //读取为dataURL  readAsBinaryString//二进制
                }
            }
            function sss(name,base64){
                var param = {
                    "FileName": name,
                    "Base64Str": base64
                };
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Product/UploadImage",
                    debug:false,
                    loadFlag:true,
                };
                vp.core.ajax(url,param,function(data){
                    //成功
                    console.log(data);
                    if (!data.IsSuccess) return;
                    var DisplayLink = data.Data.DisplayLink;
                    var OssPath = data.Data.OssPath;
                    var $newImg = $('<li><div class="li-tag"></div><div class="li-main"><img localfilepath="' + name + '" osspath="' + OssPath + ' " src="' + DisplayLink +'"><span class="icon icon-node-cir"></span></div><textarea class="li-text noChinese" placeholder="请描述产品的特点，英文书写"></textarea></li>');
                    // var $newImg = $('<div class="img-layer"><span class="icon-maoduo0"></span><img class="thumbnail" localfilepath="' + res.origin.name + '" osspath="' + OssPath +'" src="' + DisplayLink + '"/></div>');
                    $newImg.find("span").on("click",function(){
                        $prodAddDiv.find(".more-options-pop").removeClass("hidden");
                        pic_index = $(this).parents("li").index();
                    })
                    $(".prod-discription-ul-hidden").append($newImg);
                    autosizeTextarea();
                    var ul_html = document.getElementsByClassName("prod-discription-ul-hidden")[0];
                    ul_html.scrollTop = ul_html.scrollHeight;
                },function(err){
                },function(tem){
                });
            }
            
            //其它图片上传↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

            var $descriptionLis = $prodAddDiv.find(".add-descriptions-page li");
            $descriptionLis.on("click",function () {
                var imgLi = $(this).find("img").clone(true);
                $prodAddDiv.find(".prod-discription-ul-hidden li").eq(pic_index-1).find(".li-tag").html(imgLi);
                $prodAddDiv.find(".add-descriptions-page").addClass("hidden");
            })
            //非中文验证
            $("#product-add").on("keyup",".noChinese",function () {
                var value = $(this)[0].value;
                var newValue = value.replace(/[\u4e00-\u9fa5]/g,'');
                $(this).val(newValue);
            })
            //数字验证
            $("#product-add").on("keyup",".onlyNumber",function () {
                var value = $(this)[0].value;
                var newValue = value.replace(/[^\d\.]/g,'');
                $(this).val(newValue);
            })
            //...........................
            var $keywordsDiv = $("#product-add .prod-keywords");
            var $keywordsAddBtn = $keywordsDiv.find(".icon-add-cir");
            $keywordsAddBtn.on("click",function () {
                $h_li=$('<li><input class="noChinese" type="text" placeholder="请输入产品英文关键词"/><span class="icon icon-maoduo103"></span></li>');
                $keywordsDiv.find("ul").append($h_li);
            })
            $keywordsDiv.on("click","li span",function () {
                $(this).parent("li").remove();
            })
            //属性设置
            var $attrsDiv = $("#product-add .prod-custom-attr");
            $attrsDiv.on("click","p.add-attr-btn",function () {
                var $h_li = $('<li class="prod-val-li"><span>属性值</span><input class="noChinese" type="text" /><span>基础价</span><input class="onlyNumber" type="text" /><i class="icon icon-maoduo103"></i></li>');
                $(this).prev("ul").append($h_li);
            })
            $attrsDiv.on("click","li.prod-val-li i",function () {
                $(this).parent("li").remove();
            })
            $attrsDiv.on("click","li.prod-attr-li p span",function () {
                $(this).parent("p").parent("li").remove();

            })
            var $prodAttrLi = $attrsDiv.find("li.prod-attr-li").eq(0).clone(true);
            $prodAttrLi.find("input").val("");
            $prodAttrLi.find("p.attr-name").append($('<span class="icon icon-maoduo103"></span>'))
            var $attrsAddBtn = $("#product-add div.add-attrs-btn p");
            $attrsAddBtn.on("click",function () {
                var $Li = $prodAttrLi.clone(true);
                $attrsDiv.find(".prod-attr-ul").append($Li);
            })
            //textarea自适应高度
            function autosizeTextarea() {
                var pic_add_textarea = document.getElementsByClassName("li-text");
                for(var k = 0; k < pic_add_textarea.length; k++){
                    autosize(pic_add_textarea[k]);
                };
            }

            //弹窗操作
            var $moreOptionsPop = $prodAddDiv.find(".more-options-pop");
            var $cancelAddPop = $prodAddDiv.find(".cancel-add-pop");
            var $helpOtherPop = $prodAddDiv.find(".help-other-pop");
            //添加产品分类描述
            $moreOptionsPop.on("click",".choose-tag",function () {
                $prodAddDiv.find(".add-descriptions-page").removeClass("hidden");
                $moreOptionsPop.addClass("hidden");
            });
            $moreOptionsPop.on("click",".delete-pic",function () {
                console.log(pic_index);
                $prodAddDiv.find(".prod-discription-ul-hidden li").eq(pic_index-1).remove();
                $moreOptionsPop.addClass("hidden");
            });
            $moreOptionsPop.on("click",".cancel-option",function () {
                $moreOptionsPop.addClass("hidden");
            });
            //保存

            //取消添加
            $cancelAddPop.on("click",".ok-btn",function () {
                $prodAddDiv.remove();
                product_module.removeClass('hidden');
            })
            $cancelAddPop.on("click",".cancel-btn",function () {
                $cancelAddPop.addClass("hidden");
            });
            
            //点击上传产品
            $("#product-add").find(".save-add-prod").on("click",function () {

                //数据验证
                var isDataRight = true;
                $prodAddDiv.find(".cantNull").each(function () {
                    if($(this).val()==""){
                        isDataRight = false;
                        return false;
                    }
                });
                $prodAddDiv.find(".noNullInput").find("input").each(function () {
                    if($(this).val()==""){
                        isDataRight = false;
                        return false;
                    }
                });

                if($prodAddDiv.find(".cantNoImg img").length == 0){
                    isDataRight = false;
                }

                if(isDataRight == false){
                    vp.ui.pop("icon-err","请按格式填写信息");
                }else{
                    //调用提交接口
                    uploadNewProduct();
                    return false
                }

            })
            function uploadNewProduct(){
                //接口，提交
                //获取页面数据
                var isParamRight = true;
                var Name = $prodAddDiv.find(".prod-add-name textarea").val(),
                    CategoryPkId = 0,
                    Price = parseFloat($prodAddDiv.find(".prod-add-price input").val()).toFixed(2),
                    Weight = parseFloat($prodAddDiv.find(".prod-weight input").val()).toFixed(2),
                    Length = parseFloat($prodAddDiv.find(".prod-length input").val()).toFixed(2),
                    Width = parseFloat($prodAddDiv.find(".prod-width input").val()).toFixed(2),
                    Height = parseFloat($prodAddDiv.find(".prod-height input").val()).toFixed(2),
                    Keywords = [],
                    Images = [],
                    Attributes = [],
                    Supplier = {},
                    ApplicationSceneDescription = null,
                    LogoPrintableDescription = null,
                    FunctionalDescription = null,
                    ColorDescription = null,
                    PackagingDescription = null,
                    ParametricDescription = null;
                var $descriptionLis = $prodAddDiv.find(".prod-discription-ul").find("li");
                for(var i=0;i<$descriptionLis.length;i++){
                    var $li = $descriptionLis.eq(i);
                    var image = $li.find("img").attr("osspath");
                    var description = $li.find("textarea").val();
                    if(image && description!=""){
                        if($li.hasClass("ApplicationSceneDescription")){
                            ApplicationSceneDescription = {"Image":image,"Description":description};
                        }
                        else if($li.hasClass("FunctionalDescription")){
                            FunctionalDescription = {"Image":image,"Description":description};
                        }
                        else if($li.hasClass("LogoPrintableDescription")){
                            LogoPrintableDescription = {"Image":image,"Description":description};
                        }
                        else if($li.hasClass("ColorDescription")){
                            ColorDescription = {"Image":image,"Description":description};
                        }
                        else if($li.hasClass("PackagingDescription")){
                            PackagingDescription = {"Image":image,"Description":description};
                        }
                        else if($li.hasClass("ParametricDescription")){
                            ParametricDescription = {"Image":image,"Description":description};
                        }
                        continue;
                    }else if(!image && description==""){

                    }else{
                        isParamRight = false;
                        vp.ui.pop("icon-err","请填写图片相关的描述内容");
                    }
                }


                //keywords
                $prodAddDiv.find(".prod-keywords li").each(function () {
                    var val = $(this).find("input").val();
                    if(val!=""){
	                    Keywords.push(val);                    	
                    }
                })
                //images
                $prodAddDiv.find(".prod-add-pic li:not(.add-prodpic-btn)").each(function () {
                    var obj = {};
                    var ossPath = $(this).find("img").attr("osspath"),
                        localFilePath = $(this).find("img").attr("localfilepath");
                    obj["OssPath"] = ossPath;
                    obj["LocalFilePath"] = localFilePath;
                    Images.push(obj);
                })
                //attributes
                $prodAddDiv.find(".prod-custom-attr .prod-attr-li").each(function () {
                    var attributeName = $(this).find("p.attr-name input").val(),
                        attributeValues = [];
                    $(this).find("li").each(function () {
                        var valsObj = {}
                        var attributeValueName = $(this).find(".noChinese").val(),
                            attributeValuePrice =$(this).find(".onlyNumber").val();
                        valsObj["AttributeValueName"] = attributeValueName;
                        valsObj["AttributeValuePrice"] = attributeValuePrice;
                        attributeValues.push(valsObj);
                    })
                    var attrsObj = {
                        "AttributeName":attributeName,
                        "AttributeValues":attributeValues
                    }
                    Attributes.push(attrsObj);
                })
                //supplier
                Supplier = {
                    "CompanyName":$prodAddDiv.find(".add-supplier-btn .companyName").val(),
                    "ContactPerson":$prodAddDiv.find(".add-supplier-btn .contactPerson").val(),
                    "PhoneNo":$prodAddDiv.find(".add-supplier-btn .phoneNo").val(),
                    "Email":$prodAddDiv.find(".add-supplier-btn .email").val(),
                    "WeChat":$prodAddDiv.find(".add-supplier-btn .wechat").val(),
                    "Remark":$prodAddDiv.find(".add-supplier-btn .remark").val()
                };
                var param = {
                    "Name":Name,
                    "CategoryPkId":CategoryPkId,
                    "Price":Price,
                    "Weight":Weight,
                    "Length":Length,
                    "Width":Width,
                    "Height":Height,
                    "Keywords":Keywords,
                    "Images":Images,
                    "Attributes":Attributes,
                    "Supplier":Supplier,
                    "ApplicationSceneDescription":ApplicationSceneDescription,
                    "LogoPrintableDescription":LogoPrintableDescription,
                    "FunctionalDescription":FunctionalDescription,
                    "ColorDescription":ColorDescription,
                    "PackagingDescription":PackagingDescription,
                    "ParametricDescription":ParametricDescription
                };
                var url = {
                    local:"data/testDemo/data.json",
                    type:"post",
                    server:"/v1/Product/UploadUserProduct",
                    debug:false,
                    loadFlag:true,
                };
                if(isParamRight){
                    console.log(param);
                    vp.core.ajax(url,param,function(data){
                        //成功
                        $("#product-add").remove();
                		product_module.removeClass('hidden');
                		vp.ui.pop("icon-right","上传产品成功")
                    },function(err){
                    },function(tem){
                    });
                }

            }
        }
    }
    //添加产品页面全局变量
    var $product_add_page = $("#product-add").clone(true);
    //报价单----------------------------------------------------------------------------------------------
    //货运费用实时计算
    function getTransFee(isproduct){
        var CountryPkId = $('#freight-information .freight-body .destination .country-select').data('CountryPkId');
        //产品pkid
        var ProductPkId = $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('ProductPkId');
        var DeliveryMethod = '';
        if($('#freight-information .freight-way-border .list .freight-left .selected').closest('.list').hasClass('IPway')){
            DeliveryMethod = 'IP'
        }else{
            DeliveryMethod = 'IE'
        }
        var quantity = setNum();
        if(CountryPkId){
            getFee(CountryPkId,ProductPkId,quantity,DeliveryMethod,isproduct);
        }
        else{
            getSale();
        }
    }
    //设置货运信息数量
    function setNum(){
        var totalcount = 0;
        $('#quotation-modify').find('.quotation-modify-body .quotation-group .quotation-item').each(function(){
            //console.log($(this).data('ProductAttrsData').ProductQty)
            totalcount += parseInt($(this).data('ProductAttrsData').ProductQty);
        })
        $('#freight-information').find('.product-num .num-count').html(totalcount);
        return totalcount;
    }
    //获得原始货运费用
    function getFee(CountryPkId,ProductPkId,ProductQty,DeliveryMethod,isproduct){
        var url = {
            local:"data/testDemo/data.json",
            type:"post",
            server:"/v1/Logistics/GetLogisticsFreeInfo",
            debug:false,
            loadFlag:false,
        };
        var data = {
            "CountryPkId":CountryPkId,
            "ProductPkId":ProductPkId,
            "ProductQty":ProductQty,
            "DeliveryMethod":DeliveryMethod
        }
        vp.core.ajax(url,data,function(data){
            console.log(data);
            var UsdTotalFreight = data.Data.UsdTotalFreight;
            //console.error(UsdTotalFreight)
            $('#freight-information .freight-fee .fee-standard .standard-money').html(numModify(UsdTotalFreight*1.2));
            $('#freight-information .freight-fee .fee-way #fee-modify .fee-count').html(numModify(UsdTotalFreight*1.2));
            $('#freight-information .foot .freight-total-fee .money').html(numModify(UsdTotalFreight*1.2));

            if(isproduct){

                $('#quotation-modify .freight-way .freight-body .total-money .money').html(numModify(UsdTotalFreight*1.2));
                $('#quotation-modify').find('.freight-way').data('cost',numModify(UsdTotalFreight));
                var Logistics = $('#quotation-modify .quotation-modify-body .freight-way .freight-head .modify').data('Logistics');
                Logistics.FreightCurrentPrice = numModify(UsdTotalFreight*1.2);
                Logistics.FreightOriginalPrice = numModify(UsdTotalFreight);
                $('#quotation-modify .quotation-modify-body .freight-way .freight-head .modify').data('Logistics',Logistics);

            }
            getSale();
        },function(data){
            console.log(data);
        })
    }
    //国家渲染
    function countryRender(countryList){
        var $country = $('#Country-list').find('#country-item-example .country-item');
        $('#Country-list').find('.country-list-table').html('');
        $.each(countryList,function(i,country){
            var $countrymodify = $country.clone();
            $countrymodify.data('pkid',country.PkId);
            $countrymodify.find('.district .china-country').html(country.CountryNameChn);
            $countrymodify.find('.district .english-country').html(country.CountryNameEn);
            $countrymodify.find('.IP').attr('data-support',country.IsSAcceptIP);
            $countrymodify.find('.IE').attr('data-support',country.IsSAcceptIE);
            if(country.IsSAcceptIP){
                $countrymodify.find('.IP span').removeClass('icon-maoduo0').addClass('icon-maoduo1');
            }
            else{
                $countrymodify.find('.IP span').addClass('icon-maoduo0').removeClass('icon-maoduo1');
            }

            if(country.IsSAcceptIE){
                $countrymodify.find('.IE span').removeClass('icon-maoduo0').addClass('icon-maoduo1');
            }
            else{
                $countrymodify.find('.IE span').addClass('icon-maoduo0').removeClass('icon-maoduo1');
            }
            $('#Country-list').find('.country-list-table').append($countrymodify);
        })
    }
    //实时计算销售额和提成
    function getSale(){
        var totalsale = 0;
        var cost = 0;
        $('#quotation-modify .quotation-group .quotation-item').each(function(){
            totalsale += parseFloat($(this).find('.total-money .money-count').html());
            cost += parseFloat($(this).data('cost'));
            console.info(cost)
        })
        if($('#quotation-modify').find('.freight-way').hasClass('isShow')){
            totalsale += parseFloat($('#quotation-modify .freight-way .total-money .money').html());
            cost += parseFloat($('#quotation-modify').find('.freight-way').data('cost'));
            console.error(cost);
        }
        $('#quotation-modify .profit .sale-volume .sale-num .money-count').html(numModify(totalsale));
        var salecost = ((totalsale*0.9664 - cost)*0.675).toFixed(2);
        $('#quotation-modify .profit .withdraw-deposit .deposit-num .money-count').html(numModify(salecost));
        return {'totalsale': totalsale,'cost': cost};
    }
    //获取国家列表
    function getCountryList() {
        var url = {
            local:"data/testDemo/data.json",
            type:"get",
            server:"/v1/Logistics/GetLogisticsCountry",
            debug:false,
            loadFlag:true,
        };
        var data = {};
        vp.core.ajax(url,data,function(data){
            //console.log(data);
        },function(data){
            //console.log(data)
            var countryList = data.Data;
            if(!countryList){
                return false;
            }
            $('#Country-list .country-head').data('countryList',countryList);
            countryRender(countryList);

        },function(data){
            console.log(data)
        })
    }
    getCountryList();
    //获取汇率
    var rate =0;
    getRate();
    function getRate(){
        var url = {
            local:"data/testDemo/data.json",
            type:"get",
            server:"/v1/System/GetRMBToUSDExchangeRate",
            debug:false,
            loadFlag:true
        };
        var data = {};
        vp.core.ajax(url,data,function(data){
            //成功
            //console.log(data);
            rate = data.Data;
        })
    }
    //验证是否能提交
    function isPass(){
        if($('#quotation-modify .quotation-group .quotation-item').length==0){
            vp.ui.pop('icon-err','报价组合信息不能为空','请添加产品报价组合的信息');
            return false;
        }
        var obj = getSale();
        var totalsale = obj.totalsale;
        var cost = obj.cost;
        //console.log(totalsale-1.2*cost);
        console.log(numModify(1.2*cost));
        console.log(totalsale);
        var num = setNum();
        if(parseFloat((totalsale/num).toFixed(2))<parseFloat((1.2*cost/num).toFixed(2))){

            $('#Mask-pop .quotation-tips .content .money').html((1.2*cost).toFixed(2));
            $('#Mask-pop').show();
            $('#Mask-pop .quotation-tips').show();
            return false;
        }
        return true;
    }
    ////共用函数
    function getData(status){
        var ProductAttrsGroup = [];
        var QuotationName = $('#quotation-modify .order-name .order-word textarea').val();

        var ProductNo = $('#quotation-modify .quotation-modify-body .product-order-num .product-num .productno').html();
        var ProductName = $('#quotation-modify .quotation-modify-body .product-order-num .product-name').html();
        var CustomInfo = '';
        if($('#quotation-modify .note-modify').hasClass('isShow')){
            CustomInfo = $('#quotation-modify .note-modify .note-word').html();
        }


        var ProductPkId = $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('ProductPkId');
        var pkId = $('#quotation-modify').find('.quotation-modify-body .order-download .download').data('pkId');
        var OrderQuotationPkId = $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('OrderQuotationPkId');
        $('#quotation-modify').find('.quotation-modify-body .quotation-group .quotation-item').each(function(){
            ProductAttrsGroup.push($(this).data('ProductAttrsData'));

        })
        var Logistics = {};
        if($('#quotation-modify .quotation-modify-body .freight-way').hasClass('isShow')){
            Logistics = $('#quotation-modify .quotation-modify-body .freight-way .freight-head .modify').data('Logistics');
        }

        //console.info(Logistics);
        var LogisticsModify = {};
        for(var key in Logistics){
            if(key =='CountryPkId'||key == 'DeliveryMethod'||key == 'FreightOriginalPrice'||key == 'FreightCurrentPrice'){
                LogisticsModify[key] = Logistics[key];
            }
        }
        console.info(LogisticsModify);
        var OrderQuotation = {
            //"CurrentCurrency": 1,
            "QuotationName": QuotationName,
            "ProductPkId": ProductPkId,
            "ProductName": ProductName,
            "ProductNo": ProductNo,
            "CustomInfo": CustomInfo,
            "ProductAttrsGroup": ProductAttrsGroup,
            //"OrderQuotationPkId": pkId,
            "Logistics": LogisticsModify
        }
        if(status=='update'){
            OrderQuotation.OrderQuotationPkId = OrderQuotationPkId ;
        }
        if($('#quotation-modify .money-change-border .icon-maoduo41').hasClass('hidden')){
            OrderQuotation.CurrentCurrency = 0 ;
        }
        else{
            OrderQuotation.CurrentCurrency = 1 ;
        }
        //人民币切换
        if (OrderQuotation.CurrentCurrency == 0) {
            //货运信息
            if (OrderQuotation.Logistics.FreightCurrentPrice) {
                OrderQuotation.Logistics.FreightCurrentPrice = OrderQuotation.Logistics.FreightCurrentPrice/rate;
            }
            if (OrderQuotation.Logistics.FreightOriginalPrice) {
                OrderQuotation.Logistics.FreightOriginalPrice = OrderQuotation.Logistics.FreightOriginalPrice/rate;
            }

            //报价组合
            $.each(OrderQuotation.ProductAttrsGroup,function(i,obj) {
                obj.ProductCurrentPrice = obj.ProductCurrentPrice/rate;
                obj.ProductOriginalPrice = obj.ProductOriginalPrice/rate;
                $.each(obj.ProductAttrs,function(j,ob) {
                    ob.ProductAttributeCurrentPrice = ob.ProductAttributeCurrentPrice/rate;
                    ob.ProductAttributeOriginalPrice = ob.ProductAttributeOriginalPrice/rate;
                })
            })
        }
        return OrderQuotation;
    }
    //报价组合页面修改
    function quotationGroupReset(){
        $('#productProperties footer .sureBtn').data('state',0);
        $('#productProperties .propertyList .prodProperty .detailList li').removeClass('choosed');
        $('#productProperties .propertyList .prodProperty .detailList li:first-child').addClass('choosed');
        //初始化产品数量
        $('#productProperties .quantityOfPro .qtyTitle .changeQtyBtn .firstShow').html('请输入产品数量');
        $('#productProperties .quantityOfPro .qtyTitle .changeQtyBtn #product-num').html('');
        //初始化价格
        $('#productProperties .quantityOfPro .prodBasePrice .changeBtn em').html($('#productProperties .quantityOfPro .qtyInputer em').html());
        $("#productProperties .propertyList .propertyHeader .changeBtn em").each(function(){
            $(this).html($(this).closest('.prodProperty').find('.detailList li:first-child .detailPrice b').html());
        })
        //初始化图片框
        $("#productProperties .productPicList .picUl").html('<li><img class="default" src="images/quotation_add.png" alt=""></li>');
        //$("#productProperties").removeClass('hidden');
        //$('#quotation-modify').addClass('hidden');
    }
    //修改报价单数据重置
    function quotationReset(){
        $('#quotation-modify').find('.quotation-modify-body .quotation-group').html('');
        $('#quotation-modify').find('.quotation-modify-body .modify-group .add-address').show();
        $('#quotation-modify').find('.quotation-modify-body .modify-group .add-note').show();
        $('#quotation-modify').find('.quotation-modify-body .note-modify').hide();
        $('#quotation-modify').find('.quotation-modify-body .freight-way').hide();
        $('#quotation-modify').find('.quotation-modify-body .freight-way').removeClass('isShow');
        $('#quotation-modify').find('.quotation-modify-body .profit .sale-volume .sale-num .money-count').html(0);
        $('#quotation-modify').find('.quotation-modify-body .profit .withdraw-deposit .deposit-num .money-count').html(0);
        layout_pictureBank.reset();
        $('#quotation-modify').find('.quotation-foot .border').hide();
        $('#quotation-modify').find('.quotation-foot .save-quotation').show();
        $('#quotation-modify').find('.quotation-foot .save-quotation').removeClass('abled');
    }
    //数字处理
    function numModify(data){
        return (Math.floor(data * 1000) / 1000).toFixed(2);
    }
    //币种切换
    function billChange(){
        var $this = $('#quotation-modify .quotation-title');
        if($this.find('.icon-maoduo41').hasClass('hidden')){
            //人民币切换美元
            $('#quotation-modify').find('.symol-switch').html('$');
            $('#quotation-modify').find('.fund-change').each(function(){
                $(this).html(($(this).html()*rate).toFixed(2));
            })
            $this.find('.icon-maoduo41').removeClass('hidden');
            $this.find('.icon-maoduo42').addClass('hidden');
        }
        else{
            //美元切换人民币
            $('#quotation-modify').find('.symol-switch').html('&yen;')
            $('#quotation-modify').find('.fund-change').each(function(){
                $(this).html(($(this).html()/rate).toFixed(2));
            })
            $this.find('.icon-maoduo42').removeClass('hidden');
            $this.find('.icon-maoduo41').addClass('hidden');
        }
    }
    //币种重置
    function　billReset(){
        var $this = $('#quotation-modify .quotation-title');
        if($this.find('.icon-maoduo41').hasClass('hidden')){
            //人民币切换美元
            $('#quotation-modify').find('.symol-switch').html('$');
            $('#quotation-modify').find('.fund-change').each(function(){
                $(this).html(($(this).html()*rate).toFixed(2));
            })
            $this.find('.icon-maoduo41').removeClass('hidden');
            $this.find('.icon-maoduo42').addClass('hidden');
        }
    }
    //限制最多两行文本
    function limitText(ele) {

        $(ele).css('white-space','nowrap');
        if (ele.offsetWidth >= ele.scrollWidth) {
            //console.info(111)
            $(ele).css('line-height','2rem');
        }
        else if (ele.offsetWidth < ele.scrollWidth && ele.scrollWidth <= 2 * ele.offsetWidth) {
            //console.info(222)
            $(ele).css('line-height','.8rem');
            $(ele).css('white-space','inherit');
        }
        else if (ele.offsetWidth * 2 < ele.scrollWidth) {
            $(ele).css('line-height','.8rem');
            $(ele).css('white-space','inherit');
        }
    }
    //获取手机类型
    function GetPhoneType() {
        var pattern_phone = new RegExp("iphone","i");
        var pattern_android = new RegExp("Android","i");
        var userAgent = navigator.userAgent.toLowerCase();
        var isAndroid = pattern_android.test(userAgent);
        var isIphone = pattern_phone.test(userAgent);
        if(isAndroid){
            return 'Android';
        }
        else if(isIphone){
            return 'Iphone';
        }
        else{
            return 'other';
        }
    }
    var quotationgroup = {
        costRate: 1.2,
        eventBind: function(){
            var _this = this;
            //产品报价
            $('#product_details').on('click','.footer .Price-right',function(){
                $('#product_details').addClass('hidden');
                $('#quotation-modify').removeClass('hidden');
                $('#quotation-modify').find('.quotation-title .back-border').data('source','#product_details');
                var pkId = $(this).attr('data-pkid');
                console.log(pkId);
                _this.getQuotationDetail(pkId);
            })
            $('#customer_details').on('click','.footer .Price-right',function(){
                $('#customer_details').addClass('hidden');
                $('#quotation-modify').removeClass('hidden');
                $('#quotation-modify').find('.quotation-title .back-border').data('source','#customer_details');
                var pkId = $(this).attr('data-pkid');
                _this.getQuotationDetail(pkId);
            })
            //报价单返回到产品模块
            $('#quotation-modify').on('click','.quotation-title .back-border',function(){
                var ele = $(this).data('source');

                $(ele).removeClass('hidden');
                //$('#customer_details').addClass('hidden');
                $('#quotation-modify').addClass('hidden');
                //报价组合详情页重置
                quotationGroupReset();
                quotationReset();
                billReset();
            })
            //添加产品组合
            $('#quotation-modify').on('click','.modify-group .add-product',function(){
                //用于区分产品组合保存时是新建还是修改
                quotationGroupReset();
                billReset();
                layout_pictureBank.reset();
                $("#productProperties").removeClass('hidden');
                $('#quotation-modify').addClass('hidden');
            })
            //保存报价组合
            $('#productProperties').on('click','footer .sureBtn',function(){

                $('#productProperties').click();

                var ProductNo = $('#productProperties .aboutProduct .productno').html();
                var ProductName = $('#productProperties .aboutProduct .product-name').html();
                var imgArr = [];
                var cost = 0;
                if(!$('#productProperties .productPicList .picUl li img:first-child').hasClass('default')){

                    $('#productProperties .productPicList .picUl li img').each(function(i,img){

                        imgArr.push(img.src);
                    })
                }
                var ProductQty = $('#productProperties #product-num').html();
                var ProductOriginalPrice = $('#productProperties .quantityOfPro .qtyInputer .basePricePerPcs em').html()/_this.costRate;
                //计算成本价格
                cost = ProductOriginalPrice;
                var ProductCurrentPrice = $('#productProperties .quantityOfPro .prodBasePrice .changeBtn em').html();
                var ProductAttrs = [];
                $('#productProperties .propertyList .prodProperty').each(function(i,ele){
                    var attrs = {};
                    var AttributeName = $(ele).find('.propertyHeader .propertyName').html();
                    var ProductAttributeCurrentPrice = $(ele).find('.propertyHeader .changeBtn em').html();
                    var ProductAttributeValue = $(ele).find('.detailList .choosed em').html();
                    var ProductAttributeOriginalPrice = $(ele).find('.detailList .choosed .detailPrice b').html()/_this.costRate;
                    cost += ProductAttributeOriginalPrice;
                    var ProductAttributePkId = $(ele).find('.detailList .choosed').data('attr-id');
                    var ProductAttributeValuePkId = $(ele).find('.detailList .choosed').data('id');
                    attrs={
                        'AttributeName': AttributeName,
                        'ProductAttributeCurrentPrice': ProductAttributeCurrentPrice,
                        'ProductAttributeValue': ProductAttributeValue,
                        'ProductAttributeOriginalPrice': ProductAttributeOriginalPrice,
                        'ProductAttributePkId': ProductAttributePkId,
                        'ProductAttributeValuePkId': ProductAttributeValuePkId
                    }
                    ProductAttrs.push(attrs);

                })
                var ProductAttrsData = {
                    'ProductOriginalPrice': ProductOriginalPrice,
                    'ProductCurrentPrice': ProductCurrentPrice,
                    'ProductQty': ProductQty,
                    'ProductAttrs': ProductAttrs,
                    'ProucetImagePaths': imgArr
                }

//////////     总成本费用
                cost = cost*ProductQty;

                var totalmoney = $('#productProperties footer .salesNum em').html();
                var singlemoney = $('#productProperties footer .pricePerPcs em').html();
                var groupcombox = {
                    'ProductCurrentPrice': ProductCurrentPrice,
                    'ProductQty': ProductQty,
                    'TotalGroupCurrentPrice': totalmoney,
                    'TotalGroupCurrentSinglePrice': singlemoney,
                    'img': imgArr,
                    'attr': ProductAttrs
                }
                ///////
                //1是修改
                if($(this).data('state')==1){
                    var $item = $('#quotation-modify .quotation-modify-body .quotation-group >.modify');
                    //内容清空
                    $item.find('.pic-container').html('');
                    $item.find('.product-detail').html('');
                    //

                    $item.data('cost',cost);
                    $item.data('ProductAttrsData',ProductAttrsData);
                    $item.find('.quotation-head .modify').data('groupcombox',groupcombox);
                    $item.find('.total-money .money-count').html(totalmoney);
                    imgArr.length==0 && $item.find('.pic-container').html('未添加图片');
                    $.each(imgArr,function(i,img){
                        console.log(img)
                        $item.find('.pic-container').append(
                            '<div class="pic-border" style="background-image: url('+ img +')"></div>'
                        );
                        //$item.find('.pic-container .pic-border').css('background-image','url('+ img +')');
                    })
                    $item.find('.product-detail').append(
                        '<div class="product-attr">'
                        +'<span>Unit price: </span>'
                        +'<span></span>'
                        +'<span class="money fund-change">'+ numModify(ProductCurrentPrice) +'</span>'
                        +'</div>'
                    );
                    $.each(ProductAttrs,function(j,attr){
                        $item.find('.product-detail').append(
                            '<div class="product-attr">'
                            +'<span>'+ attr.AttributeName +': </span>'
                            +'<span>'+ attr.ProductAttributeValue +'</span>'
                            +'<span class="money fund-change">'+ attr.ProductAttributeCurrentPrice +'</span>'
                            +'</div>'
                        );

                    })
                    $item.find('.product-detail').append(
                        '<div class="product-attr">'
                        +'<span>quantity: </span>'
                        +'<span>'+ ProductQty +'</span>'
                        +'<span class="money fund-change">'+ totalmoney +'</span>'
                        +'</div>'
                    );
                    $("#productProperties").addClass('hidden');
                    $('#quotation-modify').removeClass('hidden');
                }
                //0是新建
                else if($(this).data('state')==0){
                    if($('#productProperties .quantityOfPro .qtyTitle .changeQtyBtn .firstShow').html()=='请输入产品数量'){
                        vp.ui.pop('icon-err','产品数量不能为空');
                        return false;
                    }
                    var $quotation = $('#quotation-modify').find('#quotation-modify-exmple .quotation-item').clone();
                    $quotation.data('cost',cost);
                    $quotation.data('ProductAttrsData',ProductAttrsData);
                    $quotation.find('.quotation-head .modify').data('groupcombox',groupcombox);
                    imgArr.length==0 && $quotation.find('.pic-container').html('未添加图片');
                    $.each(imgArr,function(i,img){
                        console.log(img)
                        $quotation.find('.pic-container').append(
                            '<div class="pic-border" style="background-image: url('+ img +')"></div>'
                        );
                        //$quotation.find('.pic-container .pic-border').css('background-image','url('+ img +')');
                    })
                    $quotation.find('.product-detail').append(
                        '<div class="product-attr">'
                        +'<span>Unit price: </span>'
                        +'<span></span>'
                        +'<span class="money fund-change">'+ numModify(ProductCurrentPrice) +'</span>'
                        +'</div>'
                    );
                    $.each(ProductAttrs,function(j,attr){
                        $quotation.find('.product-detail').append(
                            '<div class="product-attr">'
                            +'<span>'+ attr.AttributeName +': </span>'
                            +'<span>'+ attr.ProductAttributeValue +'</span>'
                            +'<span class="money fund-change">'+ attr.ProductAttributeCurrentPrice +'</span>'
                            +'</div>'
                        );

                    })
                    $quotation.find('.product-detail').append(
                        '<div class="product-attr">'
                        +'<span>quantity: </span>'
                        +'<span>'+ ProductQty +'</span>'
                        +'<span class="money fund-change">'+ totalmoney +'</span>'
                        +'</div>'
                    );

                    $quotation.find('.total-money .money-count').html(totalmoney);
                    $('#quotation-modify').find('.quotation-modify-body .quotation-group').append($quotation);
                    $("#productProperties").addClass('hidden');
                    $('#quotation-modify').removeClass('hidden');
                    $('#quotation-modify .quotation-foot .save-quotation').addClass('abled');
                }
                //console.log(ProductAttrs);

                $('#productProperties .propertyList .prodProperty .detailList li').removeClass('choosed');
                getTransFee(true);
                getSale();


            })

            //修改产品组合按钮
            $('#quotation-modify').on('click','.quotation-item .quotation-head .modify',function(){
                //币种重置
                billReset();
                //给修改的组合做标记
                $(this).closest('.quotation-item').addClass('modify').siblings().removeClass('modify');
                //console.log($(this).data('groupcombox'));
                $('#productProperties footer .sureBtn').data('state',1);
                var data = $(this).data('groupcombox');
                console.log(data);

                //图片填充
                if(data.img.length>0){
                    $('#productProperties .productPicList .picUl').html('');
                }else{
                    $('#productProperties .productPicList .picUl').html('<li><img class="default" src="images/quotation_add.png" alt=""></li>');
                }

                $.each(data.img,function(i,img){
                    $('#productProperties .productPicList .picUl').append(
                        '<li><img src="'+ img +'"/></li>'
                    )
                })

                //修改报价组合时，数据修改
                $('#productProperties .qtyTitle .changeQtyBtn #product-num').html(data.ProductQty);
                $('#productProperties .prodBasePrice .changeBtn em').html(data.ProductCurrentPrice);
                $('#productProperties footer .pricePerPcs em').html(data.TotalGroupCurrentSinglePrice);
                $('#productProperties footer .salesNum em').html(data.TotalGroupCurrentPrice);
                $("#productProperties").removeClass('hidden');
                $('#quotation-modify').addClass('hidden');
                $('#productProperties article .propertyList .prodProperty').each(function(i,ele){
                    //console.log(ele)

                    console.log(data);
                    var attrbuteid = data.attr[i].ProductAttributeValuePkId;

                    $(ele).find('.propertyHeader .changeBtn em').html(data.attr[i].ProductAttributeCurrentPrice);
                    if($(ele).find('.detailList').find('[data-id="'+ attrbuteid +'"]').length>0){
                        $(ele).find('.detailList').find('[data-id="'+ attrbuteid +'"]').addClass('choosed');
                    }
                    else{
                        $(ele).find('.detailList').find('.cl:first-child').addClass('choosed');
                    }
                    //console.error($(ele).find('.detailList').find('[data-id="'+ ProductAttributePkId +'"]'))
                })
                /////////

            })
            //产品组合删除按钮
            $('#quotation-modify').on('click','.quotation-item .quotation-head .group-delete',function(){
                $(this).closest('.quotation-item').remove();
                if($('#quotation-modify .quotation-modify-body .quotation-group .quotation-item').length==0){
                    $('#quotation-modify .quotation-foot .save-quotation').removeClass('abled');
                }
                //getSale();
                getTransFee(true);
            })
            //货运信息
            $('#quotation-modify').on('click','.modify-group .add-address',function(){
                billReset();
                $('#quotation-modify').addClass('hidden');
                $('#freight-information').removeClass('hidden');
                //$('#freight-information').find('.foot .save').data('source','new');
                $('#freight-information').find('.destination .country-select').html('请选择国家');
                $('#freight-information .freight-body .freight-way-border .IEway .freight-left .icon-right').removeClass('selected');
                $('#freight-information .freight-body .freight-way-border .IPway .freight-left .icon-right').addClass('selected');
                $('#freight-information .freight-body .freight-way-border .IEway').addClass('abled');
                $('#freight-information .freight-body .freight-way-border .IPway').addClass('abled');
                setNum();
                $('#freight-information').find('.freight-fee .fee-way #fee-modify .fee-count').html(0);
                $('#freight-information').find('.freight-fee .fee-standard .content .standard-money').html(0);
                $('#freight-information').find('.foot .freight-total-fee .money').html(0);
            })
            //货运信息返回
            $('#freight-information').on('click','.message-head .cancel',function(){
                $('#quotation-modify').removeClass('hidden');
                $('#freight-information').addClass('hidden');
            })
            //货运信息选择国家
            $('#freight-information').on('click','.freight-body .destination .country-select',function(){
                $('#freight-information').addClass('hidden');
                $('#Country-list').removeClass('hidden');
            })
            //选择国家返回货运信息
            $('#Country-list').on('click','.country-head .cancel',function(){
                $('#freight-information').removeClass('hidden');
                $('#Country-list').addClass('hidden');
                if($('#Country-list .country-head').data('ismodify')){
                    var countryList = $('#Country-list .country-head').data('countryList');
                    countryRender(countryList);
                }
            })
            //选择国家到搜索国家
            $('#Country-list').on('click','.country-head .icon-search',function(){
                $('#search-country').removeClass('hidden');
                $('#Country-list').addClass('hidden');
            })
            //搜索国家返回到选择国家页
            $('#search-country').on('click','.search-head .icon-mail_fanhui',function(){
                $('#search-country').addClass('hidden');
                $('#Country-list').removeClass('hidden');
            })
            //搜索国家搜索按钮跳转到选择国家页
            $('#search-country').on('click','.search-head .search',function(){
                //$('#search-country').addClass('hidden');
                //$('#Country-list').removeClass('hidden');
            })
            //选择国家页选择按钮
            $('#Country-list').on('click','.country-list-table .country-item',function(){
                var country = $(this).find('.district .china-country').html();
                var countryEng = $(this).find('.district .english-country').html();
                var IPsupport = $(this).find('.IP').attr('data-support');
                var IEsupport = $(this).find('.IE').attr('data-support');
                var pkid = $(this).data('pkid');


                if(IPsupport =='false'){
                    $('#freight-information .freight-body .freight-way-border .IPway').removeClass('abled');
                }else{
                    $('#freight-information .freight-body .freight-way-border .IPway').addClass('abled');
                }
                if(IEsupport =='false'){
                    $('#freight-information .freight-body .freight-way-border .IEway').removeClass('abled');
                    //使选项从IE调到IP
                    $('#freight-information .freight-body .freight-way-border .IEway .freight-left .icon-right').removeClass('selected');
                    $('#freight-information .freight-body .freight-way-border .IPway .freight-left .icon-right').addClass('selected');
                }else{
                    $('#freight-information .freight-body .freight-way-border .IEway').addClass('abled');
                }
                //设置选择的国家
                $('#freight-information .freight-body .destination .country-select').html(country);
                $('#freight-information .freight-body .destination .country-select').data('countryEng',countryEng);
                $('#freight-information .freight-body .destination .country-select').data('CountryPkId',pkid);
                $('#freight-information').removeClass('hidden');
                $('#Country-list').addClass('hidden');
                //////
                if($('#Country-list .country-head').data('ismodify')){
                    var countryList = $('#Country-list .country-head').data('countryList');
                    countryRender(countryList);
                }

                getTransFee(false);
            })
            //搜索国家搜索按钮
            $('#search-country').on('click','.search-head .search',function(){
                var text = $('#search-country .search-input input').val();
                var countryList = $('#Country-list .country-head').data('countryList');
                //console.log(countryList)
                var countryGroup = [];
                if(text){
                    $('#Country-list .country-head').data('ismodify',true);
                    $.each(countryList,function(i,country){
                        if(country.CountryNameChn.indexOf(text)>-1|| country.CountryNameEn.indexOf(text)>-1){
                            countryGroup.push(country);
                        }
                    })
                    countryRender(countryGroup);
                    //console.info(countryGroup);
                    $('#search-country').addClass('hidden');
                    $('#Country-list').removeClass('hidden');
                }
                else{
                    vp.ui.pop('icon-err','搜索国家不能为空','请输入国家');
                    return false;
                }

            })

            //货运方式的选择
            $('#freight-information').on('click','.freight-body .freight-way-border .list',function(){

                if($(this).hasClass('abled')){

                    $(this).find('.icon-right').addClass('selected');
                    $(this).siblings('.list').find('.icon-right').removeClass('selected');
                    getTransFee(false);
                }
            })

            //货运费用修改
            $('#freight-information').on('click','.freight-fee .modify',function(){
                $(this).find('.fee-count').hide();
                $(this).find('#fee-input').show();
                $(this).find('#fee-input').focus();

            })
            $('#freight-information').on('blur','#fee-input',function(){
                $('#freight-information').one('click',function(e){
                    if($(e.target).closest('#fee-modify').length==0){
                        $('#fee-modify').find('.fee-count').show();
                        $('#fee-modify').find('#fee-input').hide();
                        if($('#fee-input').val()){
                            $('#fee-modify').find('.fee-count').html(numModify($('#fee-input').val()));
                            $('#freight-information .foot .money').html(numModify($('#fee-input').val()));
                        }
                        $('#fee-input').val('');

                    }
                })
            })
            //货运信息删除
            $('#quotation-modify').on('click','.freight-way .freight-head .delete',function(){
                $(this).closest('.freight-way').removeClass('isShow');
                $(this).closest('.freight-way').hide();
                $('#quotation-modify').find('.modify-group .add-address').show();
                getSale();
            })
            //货运信息修改
            $('#quotation-modify').on('click','.freight-way .freight-head .modify',function(){
                billReset();
                console.log($(this).data('Logistics'));
                getTransFee(false);
                var Logistics = $(this).data('Logistics');
                var transway = $(this).data('transway');
                //国家pkid
                var CountryPkId = Logistics.CountryPkId;
                $('#freight-information').find('.destination .country-select').data('CountryPkId',CountryPkId);

                //产品pkid
                var ProductPkId = $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('ProductPkId');
                $('#freight-information .message-head').data('ProductPkId',ProductPkId);

                $('#quotation-modify').addClass('hidden');
                $('#freight-information').removeClass('hidden');
                //修改状态为
                $('#freight-information').find('.foot .save').data('source','modify');
                //数据填充
                $('#freight-information').find('.destination .country-select').html(Logistics.CountryNameChn);
                if(Logistics.DeliveryMethod == 'IP'){
                    $('#freight-information').find('.freight-way-border .IPway .icon-right').addClass('selected');
                    $('#freight-information').find('.freight-way-border .IEway .icon-right').removeClass('selected');
                }
                else{
                    $('#freight-information').find('.freight-way-border .IPway .icon-right').addClass('selected');
                    $('#freight-information').find('.freight-way-border .IEway .icon-right').removeClass('selected');
                }
                //IP ie 是否支持渲染
                if(transway.IsSAcceptIE){
                    $('#freight-information').find('.freight-way-border .IEway').addClass('abled');
                }
                else{
                    $('#freight-information').find('.freight-way-border .IEway').removeClass('abled');
                }
                if(transway.IsSAcceptIP){
                    $('#freight-information').find('.freight-way-border .IPway').addClass('abled');
                }
                else{
                    $('#freight-information').find('.freight-way-border .IPway').removeClass('abled');
                }
                //设置货运信息数量
                setNum();

                //getFee(CountryPkId,ProductPkId,quantity,Logistics.DeliveryMethod);
                $('#freight-information').find('.freight-fee .fee-way #fee-modify .fee-count').html(Logistics.FreightCurrentPrice);
                $('#freight-information').find('.freight-fee .fee-standard .content .standard-money').html(numModify(Logistics.FreightOriginalPrice*_this.costRate));
                $('#freight-information').find('.foot .freight-total-fee .money').html(Logistics.FreightCurrentPrice);
            })


            //货运信息保存
            $('#freight-information').on('click','.foot .save',function(){
                if($('#freight-information .destination .country-select').html() == '请选择国家'){
                    vp.ui.pop('icon-err','货运国家不能为空','请选择货运国家');
                    return false;
                }
                var countryEng = $('#freight-information .destination .country-select').data('countryEng');
                var CountryNameChn = $('#freight-information .destination .country-select').html();
                var $ele = $('#freight-information .freight-way-border .list .freight-left .selected').closest('.list');
                var DeliveryMethod = '';
                if($ele.hasClass('IPway')){
                    DeliveryMethod = 'IP';
                }else{
                    DeliveryMethod = 'IE';
                }
                var cost = 0;
                var FreightCurrentPrice = $('#freight-information').find('.freight-fee #fee-modify .fee-count').html();
                var FreightOriginalPrice = $('#freight-information').find('.freight-fee .fee-standard .standard-money').html()/_this.costRate;
                cost = (FreightOriginalPrice).toFixed(2);
                console.log(cost)
                var ToalFreight = $('#freight-information').find('.foot .money').html();
                var ProductQty = $('#freight-information').find('.product-num .num-count').html();
                var IsSAcceptIP= false;
                var IsSAcceptIE = false;
                var CountryPkId = $('#freight-information .destination .country-select').data('CountryPkId');
                if($('#freight-information .freight-way-border .IPway').hasClass('abled')){
                    IsSAcceptIP = true;
                }else{
                    IsSAcceptIP = false;
                }
                if($('#freight-information .freight-way-border .IEway').hasClass('abled')){
                    IsSAcceptIE = true;
                }else{
                    IsSAcceptIE = false;
                }

                var transway = {
                    'IsSAcceptIP': IsSAcceptIP,
                    'IsSAcceptIE': IsSAcceptIE
                }

                var Logistics = {
                    'CountryNameChn': CountryNameChn,
                    'DeliveryMethod': DeliveryMethod,
                    'FreightCurrentPrice': FreightCurrentPrice,
                    'FreightOriginalPrice': FreightOriginalPrice,
                    'ToalFreight': ToalFreight,
                    'ProductQty': ProductQty,
                    'CountryPkId': CountryPkId
                }
                console.log(Logistics)
                //数据储存
                $('#quotation-modify').find('.freight-way .freight-head .modify').data('Logistics',Logistics)
                $('#quotation-modify').find('.freight-way .freight-head .modify').data('transway',transway)
                //数据修改
                $('#quotation-modify').find('.freight-way .freight-detail .country .country-detail').html(countryEng);
                $('#quotation-modify').find('.freight-way .freight-detail .freight-way-detail .transway .way').html(DeliveryMethod);
                if(DeliveryMethod == 'IP'){
                    $('#quotation-modify').find('.freight-way .freight-detail .freight-way-detail .freight-way-tip span').html('国际优先快递服务');
                }
                else{
                    $('#quotation-modify').find('.freight-way .freight-detail .freight-way-detail .freight-way-tip span').html('国际经济快递服务');
                }
                $('#quotation-modify').find('.freight-way .total-money .money').html(ToalFreight);
                $('#quotation-modify').removeClass('hidden');
                $('#freight-information').addClass('hidden');
                $('#quotation-modify').find('.freight-way').show();
                $('#quotation-modify').find('.modify-group .add-address').hide();

                $('#quotation-modify').find('.freight-way').addClass('isShow');
                $('#quotation-modify').find('.freight-way').data('cost',cost);

                getSale();
            })



            //自定义信息删除
            $('#quotation-modify').on('click','.note-modify .note-title .name-delete',function(){
                $(this).closest('.note-modify').hide();
                $(this).closest('.note-modify').removeClass('isShow');
                console.log($(this).closest('.quotation-modify-body'))
                $(this).closest('.quotation-modify-body').find('.modify-group .add-note').show();
            })
            //添加自定义信息
            $('#quotation-modify').on('click','.modify-group .add-note',function(){
                $('#quotation-modify').addClass('hidden');
                $('#own-information').removeClass('hidden');

            })
            //自定义信息修改
            $('#quotation-modify').on('click','.note-modify .note-title .name-modify',function(){
                $('#quotation-modify').addClass('hidden');
                $('#own-information').removeClass('hidden');
            })
            //自定义信息取消按钮
            $('#own-information').on('click','.stop-head .cancel',function(){
                $('#quotation-modify').removeClass('hidden');
                $('#own-information').addClass('hidden');
                //$('#quotation-modify').find('.modify-group .add-note').hide();
                //$('#quotation-modify').find('.note-modify').show();
            })
            //自定义信息确定
            $('#own-information').on('click','.stop-head .submit',function(){
                if($('#own-information .reason-border textarea').val()){
                    $('#quotation-modify').removeClass('hidden');
                    $('#own-information').addClass('hidden');
                    $('#quotation-modify').find('.quotation-modify-body .note-modify .note-word').html($('#own-information .reason-border textarea').val());
                    $('#quotation-modify').find('.modify-group .add-note').hide();
                    $('#quotation-modify').find('.note-modify').show();
                    $('#quotation-modify .note-modify').addClass('isShow');
                }
                else{
                    vp.ui.pop('icon-err','自定义信息不能为空','请填写自定义信息');
                }

            })


            //创建报价单
            $('#quotation-modify').on('click','.quotation-foot .save-quotation',function(){
                if(!isPass()){
                    return false;
                }
                var OrderQuotation = getData('new');
                console.log(JSON.stringify(OrderQuotation));
                $.ajax("https://www.mooddo.com/v1/Order/CreateOrderQuotation", {
                    type:"post",
                    headers:{vp_token:vp.core.token},
                    data:JSON.stringify(OrderQuotation),
                    contentType:"application/json",
                    success: function(data){
                        //console.log(data);
                        if(data.IsSuccess){
                            $('#Mask-pop').show();
                            $('#Mask-pop .save-quotation').show();
                            $('#quotation-modify .quotation-modify-body .order-download .download').data('OrderQuotationPkId',data.Data);
                            $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('OrderQuotationPkId',data.Data);
                        }
                    }
                })
            })
            //二次保存定价单
            $('#quotation-modify').on('click','.quotation-foot .save-modify',function(){
                if(!isPass()){
                    return false;
                }
                var OrderQuotation = getData('update');
                console.log(JSON.stringify(OrderQuotation));
                $.ajax("https://www.mooddo.com/v1/Order/UpdateOrderQuotations", {
                    type:"post",
                    headers:{vp_token:vp.core.token},
                    data:JSON.stringify(OrderQuotation),
                    contentType:"application/json",
                    success: function(data){
                        //console.log(data);
                        if(data.IsSuccess){
                            $('#Mask-pop').show();
                            $('#Mask-pop .save-quotation').show();
                        }
                    }
                })
            })


            //另存定价单
            $('#quotation-modify').on('click','.quotation-foot .save-as',function(){
                if(!isPass()){
                    return false;
                }
                var OrderQuotation = getData('create');
                $('#Mask-pop .saveas-quotation .save').data('OrderQuotation',OrderQuotation)
                //弹出另存为弹窗
                $('#Mask-pop').show();
                $('#Mask-pop .saveas-quotation').show();
                $('#Mask-pop .saveas-quotation .quotation-input input').val(OrderQuotation.QuotationName);

            })

            //提交订单
            $('#quotation-modify').on('click','.quotation-foot .submit-order',function(){
                if(!isPass()){
                    return false;
                }
                if(!$('#quotation-modify .freight-way').hasClass('isShow')){
                    vp.ui.pop('icon-err','货运信息不能为空','请添加货运信息');
                    return false;
                }
                if(!$('#quotation-modify .note-modify').hasClass('isShow')){
                    vp.ui.pop('icon-err','请输入自定义信息','如具体的货运地址，客户信息和备注说明等');
                    return false;
                }
                var OrderQuotation = getData('update');
                //console.log(JSON.stringify(OrderQuotation));
                $.ajax("https://www.mooddo.com/v1/Order/UpdateOrderQuotations", {
                    type:"post",
                    headers:{vp_token:vp.core.token},
                    data:JSON.stringify(OrderQuotation),
                    contentType:"application/json",
                    success: function(data){
                        //console.log(data);
                        if(data.IsSuccess){
                            $.ajax("https://www.mooddo.com/v1/Order/CreateOrderByQuotation", {
                                type:"post",
                                headers:{vp_token:vp.core.token},
                                data:JSON.stringify({
                                    "OrderQuotationPkId": [
                                        OrderQuotation.OrderQuotationPkId
                                    ]
                                }),
                                contentType:"application/json",
                                success: function(data){
                                    console.log(data);
                                    if(data.IsSuccess){
                                        $('#order-success').removeClass('hidden');
                                        $('#quotation-modify').addClass('hidden');
                                        $('#order-success .success-tip .tips p').html(data.Data);
                                    }
                                }
                            })
                        }
                    }
                })
            })

            //保存报价单成功后弹窗事件
            $('#Mask-pop').on('click','.save-quotation .close',function(){
                $('#Mask-pop').hide();
                $('#Mask-pop .save-quotation').hide();
                $('#quotation-modify .order-download').show();
                $('#quotation-modify .quotation-foot .border').show();
                $('#quotation-modify .quotation-foot .save-quotation').hide();

            })
            //发送邮件
            $('#Mask-pop').on('click','.save-quotation .send-email',function(){
                $('#Mask-pop').hide();
                $('#Mask-pop .save-quotation').hide();
                //do something
            })
            //下载报价单
            $('#Mask-pop').on('click','.save-quotation .download-quotation',function(){
                $('#Mask-pop').hide();
                $('#Mask-pop .save-quotation').hide();
                $('#quotation-modify .quotation-modify-body .order-download .download').click();
                $('#Mask-pop .save-quotation').hide();
                $('#quotation-modify .order-download').show();
                $('#quotation-modify .quotation-foot .border').show();
                $('#quotation-modify .quotation-foot .save-quotation').hide();
                //do something
            })

            //报价单下载
            $('#quotation-modify').on('click','.quotation-modify-body .order-download .download',function(){
                var pkId = $(this).data('OrderQuotationPkId');
                var currency = 0;
                if($('#quotation-modify .quotation-title .money-change-border .icon-maoduo41').hasClass('hidden')){
                    currency = 0;
                }
                else{
                    currency = 1;
                }
                var type = GetPhoneType();
                console.log(type)
                if(type =='Iphone'){
                    urltoken = encodeURIComponent (vp.core.token);
                    window.open('https://www.mooddo.com/v1/Order/DownLoadQuotations?OrderQuotationPkId='+ pkId +'&CurrentCurrency='+currency+'&vp_token='+urltoken)
                }
                else if(type == 'Android') {
                    $.ajax('https://www.mooddo.com/v1/Order/GetQuotationsPath?CurrentCurrency='+ currency +'&OrderQuotationPkId=' + pkId,{
                        type:"get",
                        headers:{vp_token:vp.core.token},
                        contentType:"application/json",
                        success: function(data){
                            console.log(data);
                            if(data){
                                download(data);
                            }
                        }
                    })
                }
            })
            //另存为报价单弹窗保存
            $('#Mask-pop').on('click','.saveas-quotation .save',function(){
                if(!$('#Mask-pop .saveas-quotation .quotation-input input').val()){
                    vp.ui.pop('icon-err','报价单名称不能为空','请填写报价单名称');
                    return false;
                }
                var OrderQuotation = $(this).data('OrderQuotation');
                OrderQuotation.QuotationName = $('#Mask-pop .saveas-quotation .quotation-input input').val();
                $.ajax("https://www.mooddo.com/v1/Order/CreateOrderQuotation", {
                    type:"post",
                    headers:{vp_token:vp.core.token},
                    data:JSON.stringify(OrderQuotation),
                    contentType:"application/json",
                    success: function(data){
                        console.log(data);
                        $('#Mask-pop').show();
                        $('#Mask-pop .save-quotation').show();
                        $('#quotation-modify .quotation-modify-body .order-download .download').data('OrderQuotationPkId',data.Data);
                        $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('OrderQuotationPkId',data.Data);
                    }
                })
                $('#Mask-pop').hide();
                $('#Mask-pop .saveas-quotation').hide();
                //do something
            })
            //另存为报价单弹窗取消
            $('#Mask-pop').on('click','.saveas-quotation .cancel',function(){
                $('#Mask-pop').hide();
                $('#Mask-pop .saveas-quotation').hide();
                //do something
            })
            //清空单据名称

            $('#Mask-pop').on('click','.saveas-quotation .quotation-input .icon-err',function(){
                $(this).prev().val('');
                //do something
            })

            //订单保存成功页完成按钮
            $('#order-success').on('click','.success-title .complete',function(){
                $('#order-success').addClass('hidden');
                $('#quotation-modify').removeClass('hidden');
            })

            //报价过低修改报价按钮
            $('#Mask-pop').on('click','.quotation-tips .modify-quotation',function(){
                $('#Mask-pop').hide();
                $('#Mask-pop .quotation-tips').hide();
            })

            //单据名称修改
            $('#quotation-modify').on('focus','#order-name-input',function(){

                $(this).data('text',$(this).val());
            })
            $('#quotation-modify').on('blur','#order-name-input',function(){

                if(!$(this).val()){

                    $(this).val($(this).data('text'));
                    return false;
                }
            })

            //修改标价单页切换币种
            $('#quotation-modify').on('click','.quotation-title .money-change-border',function(){
                billChange();
            })

            //货运报价说明
            $('#freight-information').on('click','.freight-tips', function() {
                $('#freight_description').removeClass('hidden');
            })
            //货运报价说明返回
            $('#freight_description').on('click','.header .icon-mail_fanhui', function() {
                $('#freight_description').addClass('hidden');
            })
            //自定义信息说明
            $('#own-information').on('click','.own-description',function(){
                $('#own_description').removeClass('hidden')
            })
            //自定义信息返回
            $('#own_description').on('click','.header .icon-mail_fanhui',function(){
                $('#own_description').addClass('hidden');
            })
        },
        getQuotationDetail: function(pkId){
            var _this = this;
            var url = {
                local:"data/testDemo/data.json",
                type:"get",
                server:"/v1/Product/GetProdctByPkId",
                debug:false,
                loadFlag:true
            };
            var data = {pkId: pkId};
            vp.core.ajax(url,data,function(data){
                console.log(data);
                _this.quotationRender(data);

            },function(data){

            },function(data){

            })
        },
        quotationRender: function(data) {
            var _this = this;
            var quotation = data.Data,
                productno = quotation.ProductNo,
                productname = quotation.Name,
                pkId = quotation.PkId,
                images = quotation.Images,
                baseprice = quotation.USDPrice,
                attribute = quotation.Attributes,
                productPkId = quotation.Attributes[0].ProductPkId;

            $('#quotation-modify').find('.quotation-modify-body .product-order-num').data('ProductPkId',productPkId);
            //quotationgroup = quotation.
            $('#quotation-modify .product-order-num .product-num .productno').html(productno);
            $('#quotation-modify .product-order-num .product-name').html(productname);
            limitText($('#quotation-modify').find('.product-order-num .product-name')[0]);
            $('#quotation-modify #order-name-input').html(productname);

            //储存下载报价单pkId
            $('#quotation-modify').find('.quotation-modify-body .order-download .download').data('pkId', pkId);
            //$('#quotation-modify').find('.quotation-modify-body .product-order-num').data('ProductPkId', ProductPkId);
            $('#quotation-modify .quotation-modify-body .profit .sale-volume .sale-num .money-count').html(0);
            $('#quotation-modify .quotation-modify-body .profit .withdraw-deposit .deposit-num .money-count').html(0);

            //报价组合数据填充

            $('#productProperties article .aboutProduct .productno').html(productno);
            $('#productProperties article .aboutProduct .product-name').html(productname);
            $('#productProperties article .quantityOfPro .prodBasePrice .changeBtn em').html(numModify(baseprice*_this.costRate));
            $('#productProperties article .quantityOfPro .qtyInputer .basePricePerPcs em').html(numModify(baseprice*_this.costRate));
            ////
            $('#pictureBank article .aboutPic .productno').html(productno);
            $('#pictureBank article .aboutPic .product-name').html(productname);
            //图片银行图片更新
            $('#pictureBank article .picLists .cl').html('');
            $.each(images,function(i,img){
                $('#pictureBank article .picLists .cl').append(
                    '<li><span class="icon-right"></span> <img src="'+ img.DispalyLink +'" alt=""></li>'
                )
            })
            ///
            var $attrList = $('#productProperties').find('article #attribute-exmple .prodProperty');
            $('#productProperties article').find('.propertyList').html('');
            $.each(attribute,function(i,attr){
                var $attrListModify = $attrList.clone();
                $attrListModify.find('.propertyHeader .propertyName').html(attr.AttributeName);
                $attrListModify.find('.propertyHeader .changeBtn em').html(numModify(attr.Values[0].AttributeValueUSDPrice*_this.costRate));
                $.each(attr.Values,function(j,obj){
                    $attrListModify.find('.detailList').append(
                        '<li class="cl" data-id="'+ obj.PkId +'" data-attr-id="'+ attr.PkId +'">'
                        +'<span class="icon icon-right"></span><em>'+ obj.AttributeValueName +'</em><span class="detailPrice">+$<b>'+ numModify(obj.AttributeValueUSDPrice*_this.costRate) +'</b></span>'
                        +'</li>'
                    )
                })

                $('#productProperties article').find('.propertyList').append($attrListModify);
            })
            layout_productProperties.layoutFun();
        }
    }

    quotationgroup.eventBind();

    var layout_productProperties = {
        layoutFun:function(){

            //初始化
            //header按钮
            $("#productProperties").find("header .cancelBtn").on("click",function(){
                $("#productProperties").addClass('hidden');
                $('#quotation-modify').removeClass('hidden');
                //选项选择重置
                $('#productProperties .propertyList .prodProperty .detailList .cl').removeClass('choosed');
                $("#productProperties .productPicList .picUl").html('');
            })
            $("#productProperties").find("footer .sureBtn").on("click",function(){
                //$("#productProperties").addClass('hidden');
                //$('#quotation-modify').removeClass('hidden');
            })



            //产品数量修改
            $('#productProperties').on('click','.quantityOfPro .qtyTitle .changeQtyBtn',function(){
                //$(this).find('.fee-count').hide();
                $(this).find('input').show();
                $(this).find('input').focus();

            })
            $('#productProperties').on('blur','#product-num-input',function(){
                $('#productProperties').one('click',function(e){
                    if($(e.target).closest('#product-num-border').length==0){
                        //$('#fee-modify').find('.fee-count').show();
                        $('#product-num-border').find('#product-num-input').hide();
                        if($('#product-num-input').val()&&parseInt($('#product-num-input').val())>0&&parseInt($('#product-num-input').val())<100000000){
                            $('#product-num-border').find('#product-num').html(parseInt($('#product-num-input').val()));
                            $('#product-num-border .firstShow').html('修改数量');
                            countSinglePrice();
                        }
                        $('#product-num-input').val('');
                    }
                })
            })



            //单项属性选择
            //var $propertyLists = $("#productProperties .propertyList");
            $("#productProperties").on('click','.propertyList .detailList li',function(e){
                e.stopPropagation();
                $("#productProperties").click();
                //$propertyLists.find("li").on("click",function(){
                var thisPrice = $(this).find("b").html();
                $(this).addClass("choosed").siblings("li").removeClass("choosed");
                $(this).parents(".prodProperty").find(".changeBtn em").html(thisPrice)
                countSinglePrice();
            })

            //修改报价

            $("#productProperties").on("click",'.changeBtn',function(e){
                $(this).find('input').show();
                $(this).find('input').focus();
            })
            $("#productProperties").on('blur','.changeBtn input',function(){
                var $ele = $(this).closest('.changeBtn');
                $('#productProperties').one('click',function(e){
                    if(!$(e.target).closest('.changeBtn').is($ele)){
                        $ele.find('input').hide();
                        if($ele.find('input').val()&&parseFloat($ele.find('input').val())>=0&&parseFloat($ele.find('input').val())<100000000){
                            $ele.find('em').html(parseFloat($ele.find('input').val()));

                            countSinglePrice();
                        }
                        $ele.find('input').val('');
                    }

                })

                countSinglePrice();

            })


            //计算价格
            function countSinglePrice(){
                var $changeBtn = $("#productProperties .changeBtn");
                //计算单价
                var singlePrice = 0;
                var $em = $changeBtn.find("em");
                $em.each(function(i,ele){

                    singlePrice = singlePrice + parseFloat($(this).html());
                });
                $("#productProperties .pricePerPcs em").html(singlePrice.toFixed(2));
                //计算总价
                var productQuantity = $('#product-num').html();
                if(!isNaN(productQuantity)){
                    var saleMoney = singlePrice * productQuantity;
                    $("#productProperties .salesNum em").html(saleMoney.toFixed(2));

                }
            }
            countSinglePrice();
            //添加图片
            //var $productPicList = $("#productProperties article .productPicList");
            $('#productProperties').on('click','article .productPicList .picUl li',function(){
                //$productPicList.find("li").on("click",function(){
                $("#productProperties").addClass("hidden").siblings("#pictureBank").removeClass("hidden");
            })
        }
    };

    var layout_pictureBank = {
        picNum: 0,
        reset: function(){
            $("#pictureBank .picLists p em").html(0);
            $('#pictureBank .picLists li span').removeClass('spanShow');
            this.picNum = 0;
        },
        layoutFun:function(){
            //图片选择
            var $li = $("#pictureBank .picLists li"),
                $choosedNum = $("#pictureBank .picLists p em"),
                _this = this;
            $('#pictureBank').on('click','.picLists li',function(){
                //$li.on("click",function(){
                var $span = $(this).find("span");
                if($span.is(":visible") && _this.picNum <= 3){
                    $span.removeClass("spanShow");
                    _this.picNum--;
                }else if(_this.picNum < 3){
                    $span.addClass("spanShow");
                    _this.picNum++;
                }
                $choosedNum.html(_this.picNum);
            })
            //取消按钮
            $("#pictureBank").find("header .cancelBtn").on("click",function(){
                $("#productProperties").removeClass("hidden").siblings("#pictureBank").addClass("hidden");
            })
            //确定按钮
            $("#pictureBank").find("header .sureBtn").on("click",function(){
                $("#productProperties").removeClass("hidden").siblings("#pictureBank").addClass("hidden");
                //插入图片
                var $selectedImgs = $("#pictureBank .picLists li").find(".spanShow").siblings("img");
                var $srcArr = [];
                $selectedImgs.each(function(){
                    var src = $(this).attr("src");
                    $srcArr.push(src);
                })
                //console.log($srcArr);
                //var $insertImg = $("<li><img></img></li>");
                var imgLists = "";
                for(var i=0; i<$srcArr.length; i++){
                    imgLists = imgLists + '<li><img src="' + $srcArr[i] + '"/></li>';
                }
                console.error(imgLists)
                if(imgLists){
                    $("#productProperties").find(".productPicList .picUl").html(imgLists);
                }
                else{
                    $("#productProperties").find(".productPicList .picUl").html('<li><img class="default" src="images/quotation_add.png" alt=""></li>');
                }

                //$("#pictureBank .picLists li .icon-right-cir").removeClass('spanShow');
                //$choosedNum.html(0);
                //添加图片事件绑定
                var $productPicList = $("#productProperties article .productPicList");
                $productPicList.find("li").on("click",function(){
                    $("#productProperties").addClass("hidden").siblings("#pictureBank").removeClass("hidden");
                })
            })
        }
    }
    layout_pictureBank.layoutFun();
}
    productModule();
    vp.registerFn.productModule = productModule;
    //报价单
})(window)

