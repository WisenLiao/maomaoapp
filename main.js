;(function(w) {
	console.log("进入...mian.js");
	var path = "";
    requirejs.config({
        baseUrl: "./",
        urlArgs: "0.1.0",
		waitSeconds:0,
        paths: {
			initRouter: 	path+"libs/initRouter",
			fortext: 		path+"libs/fortext"
        },
        shim: {

        }
    });
	require(["libs/vp","libs/initScirpt"],function () {
		require(["app"],function (app) {
			w.app = new app().$mount("#app-Container");
			require(["router"], function(r) {
				w.router = r;
			});
		})
	});
})(window);