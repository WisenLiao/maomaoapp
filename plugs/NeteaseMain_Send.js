﻿var nim = {},
    accid = "",
    message = "",
    appKey = "",
    $connection = $(".connection");
$(function () {
    $.post("/Setting/GetNeteaseApiToken", function (data) {
        if (data.Type == 0) {
            $connection.text("正在连接中...").css("color", "yellow");
            accid = data.Accid;
            message = data.Message;
            appKey = data.appKey;
            nim = NIM.getInstance({
                appKey: data.appKey,
                account: data.Accid,
                token: data.Message,
                onconnect: onConnect,
                onwillreconnect: onWillReconnect,
                ondisconnect: onDisconnect,
                onerror: onError,
                oncustomsysmsg: onCustomSysMsg
            });
        }
        else {
            $msgInfo.text(data.Message).css("color", "Red");
        }
    });
    $.post("/Setting/GetPushRecord",{Key:"",Order:"",PageSize:20,PageNo:1}, function (res) {
        if (res.Type == 0) {
            var obj = $.parseJSON(res.Data);
            console.log(obj);
            var str = "";
            for (var i = 0; i < obj.length; i++) {
                var className = "";
                var color = "#ACACAC";
                if (obj[i].isread == "0"){
                    color = "#333";
                    className = "isread";
                }
                str += '<div class="message ' + className + '" data-id="' + obj[i].id + '">';
                str += '<div class="type">';
                str += '<img src="'+GetSystemTypeimg(obj[i].noticetype)+'">'+'<span>' + GetSystemType(obj[i].noticetype) + '</span>';
                str += '</div>';
                str += '<div class="text">';
                if (obj[i].Url == "")
                    str += '<span>' + unescape(obj[i].Message) + '</span><span class="del">删除</span>';
                else
                    str += '<span class="tohref"><a href="' + obj[i].link + '" target="_blank" style="color:'+color+'" class="colorStyle">' + obj[i].title + '</a></span><span class="del">删除</span>';
                str += '</div>';
                str += '<div class="addtime">';
                str += '<span>' + obj[i].addtime + '</span>';
                str += '</div>';
                str += '</div>';
            }
            $("#messages").html(str);
        }
    });
    //判断已读
    $("#messages").on("click", ".tohref", function () {
        var _this = $(this),
            id = _this.closest(".message").data("id");
        $.post("/Setting/UpdPushRead", { msg: '{Id:'+id+'}' }, function (data) {
            console.log(data);
            if (data.Type == 0)
                _this.closest(".message").removeClass("isread");
                _this.children('a').css('color','#ACACAC')
        });
    });

    $("#messages").on("click", ".del", function () {
        var _this = $(this);
        var id = _this.closest(".message").data("id");
        var bl = confirm("确定要删除吗？");
        if (bl) {
            $.post("/Setting/DelNetease", { msg: id }, function (data) {
                if (data.Type == 0)
                    _this.closest(".message").remove();
            });
        }
    });

    // $("#messages").on("mouseover", ".message", function () {
    //     var _this = $(this);
    //     _this.find(".del").show();
    // });
    //
    // $("#messages").on("mouseout", ".message", function () {
    //     var _this = $(this);
    //     _this.find(".del").hide();
    // });

    $('.header-notice .btn-see').on('click',function(){
        window.open('/Content/notification/notice.html');
    })
});
var data = {};
function onConnect() {
    $connection.text("连接成功").css("color", "green");
}
function onError(error) {
    $connection.text("连接失败").css("color", "red");
}
function onDisconnect(error) {
    $connection.text("连接丢失").css("color", "red");
}
function onWillReconnect(obj) {
    $connection.text("即将重连").css("color", "red");
}
function onCustomSysMsg(sysMsg) {
    console.log(sysMsg.content);
    var obj = $.parseJSON(sysMsg.content);
    console.log(obj.NoticeType == 2, obj.NoticeType == 3);
    if (obj.NoticeType != 2) {
        var str = "";
        str += '<div class="message isread" data-id="' + obj.Id + '">';
        str += '<div class="type">';
        str += '<img src="'+GetSystemTypeimg(obj.NoticeType)+'">'+'<span>' + GetSystemType(obj.NoticeType) + '</span>';
        str += '</div>';
        str += '<div class="text">';
        if (obj.Url == "")
            str += '<span>' + unescape(obj.Message) + '</span><span class="del">删除</span>';
        else
            str += '<span class="tohref"><a href="' + obj.Url + '" target="_blank">' + unescape(obj.Message) + '</a></span><span class="del">删除</span>';
        str += '</div>';
        str += '<div class="addtime">';
        str += '<span>' + obj.AddTime + '</span>';
        str += '</div>';
        str += '</div>';
        $('#messages .message').eq(-1).remove();
        $("#messages").children('.message').eq(1).after(str); //在版本与活动通知后添加
        //$("#SystemMsg").text("有信息推送过来呐");
    }
	else if (obj.EventType == 4) {
            //$("#SystemMsg").text("系统有推送刷新页面的，可以把Message的参数显示在页面。这是探钱手的接口");
			//console.log(sysMsg);
			console.log(obj.Message)
			var url = $('.magic-detail .search-input').data('website');
			if(obj.Message[0].RootUrl ==url){
				//console.info(123123123)
			
				if(firstLoad){
					//MatchResult(obj.Message);
					//var url = $('.magic-detail .search-input').data('website');
					if(obj.Message[0].IsCompleted){
						if(obj.Message[0].TotalCount>0){
							isTemp = 0;
							GetMagicHandPaging(url,1,0,0,searchNum)
							GetMagicHandPrize(url,searchNum+1,false)
							//lastMatchRender();
						}
						else if(obj.Message[0].TotalCount==0){
							$('.magic-detail .search-detail .search-blank').show();
							$('.magic-detail .search-detail .search-list-border').hide();
						}
					}
					else{
						if(obj.Message[0].TotalCount>0){
							GetMagicHandPaging(url,1,0,0,searchNum);
						}
					}
				}else{
					if(obj.Message[0].IsCompleted){
						//newReturnNum =obj.Message.length;
						if(obj.Message[0].TotalCount>0){
							GetMagicHandPrize(url,searchNum+1,false);
							isTemp = 0;
							$('.result-return-tips p').html('本次匹配已结束，又找到了新数据，<span>点击刷新</span>。注：将按照近似度重新排序。');
							$('.result-return-tips').show();
							//lastMatchRender();
							$('.result-return-tips').addClass('tipshow');
						}
						else{
							$('.magic-detail .search-detail .search-blank').show();
							$('.magic-detail .search-detail .search-list-border').hide();
						}
						
					}
					else{
						//newReturnNum +=obj.Message.length;
						//newDataReturn();
						if(obj.Message[0].TotalCount>0){
							$('.result-return-tips').show();
							$('.result-return-tips').addClass('tipshow');
						}
						
					}
					//$('.result-return-tips').addClass('tipshow');
					//var $positionMatch = $('.magic-detail .search-detail .search-target');
					//$($positionMatch.find('.match-times')[searchNum-1]).find('.position-match >span').html(returnData.length);
				}
				firstLoad = false;
			}
        }
    // if (obj.NoticeType == 2 || obj.NoticeType == 3) {
    //     if (obj.EventType == 1) {
    //         $("#SystemMsg").text("系统有推送刷新/通知页面的，这是支付推送消息，请进入支付的下一个页面");
    //     }
    //     else if (obj.EventType == 2) {
    //         $("#SystemMsg").text("系统有推送刷新页面的，可以把Message的参数显示在页面。搜索结果页面");
    //     }
    //     else if (obj.EventType == 3) {
    //         $("#SystemMsg").text("系统有推送刷新页面的，可以把Message的参数显示在页面。Bing与Yahoo对比");
    //     }
    //     else if (obj.EventType == 100) {
    //         $("#SystemMsg").text("无参数");
    //     }
    // }
}

function getLocalTime(v) {
    v = v * 1;
    var dateObj = new Date(v);
    if (dateObj.format('yyyy') == "NaN") { console.log("时间戳格式不正确"); /*alert("时间戳格式不正确");*/return; }
    return dateObj.getFullYear() + '/' + (dateObj.getMonth() + 1) + '/' + dateObj.getDate() + ' ' + dateObj.getHours() + ':' + dateObj.getMinutes() + ':' + dateObj.getSeconds();
}
function GetSystemType(t) {
    switch (t) {
        case "1":
            {
                return "系统通知";
            }
            break;
        case "2":
            {
                return "系统刷新";
            }
            break;
        case "3":
            {
                return "系统刷新/系统通知";
            }
            break;
        case "4":
            {
                return "活动通知";
            }
            break;
        case "5":
            {
                return "版本通知";
            }
            break;
    }
}
function GetSystemTypeimg(t) {
    switch (t) {
        case "1":
        {
            return "/Content/i/notice3.png";
        }
            break;
        case "2":
        {
            return "/Content/i/notice3.png";
        }
            break;
        case "3":
        {
            return "/Content/i/notice3.png";
        }
            break;
        case "4":
        {
            return "/Content/i/notice2.png";
        }
            break;
        case "5":
        {
            return "/Content/i/notice1.png";
        }
            break;
    }
}

Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S+": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};