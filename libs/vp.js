define([],
    function()
    {
        "use strict";
        console.log("进入...vp.js")
        window.vp = window.vp || {};
        var obj = {
            ui:{
                loadtimer:350,   //小蜜蜂加载最低显示时间;
                isTrue:false,
                timepop:null,
                loading:function (arg) {
                    if(arg){
                		$(".load_animation p").html(arg);
                	}
					$('.load_animation').removeClass('hidden');
                },
                removeLoading:function(){
                    $('.load_animation').addClass('hidden');
                },
                error:function () {

                },
                pop:function(className,Msg,Msg2){
                    $('.login-window').remove();
                    $('.pop-prompt').remove();
                    if(arguments.length == 2){
                        var content = '<div class="login-window"><span class="'+className+'"></span><p>'+Msg+'</p></div>'
                        $('body').append(content);
                        $('.login-window:last').each(function(){
                            var $width = $(this).width();
                            var $rem = $('html').css('fontSize').replace('px','');
                            $(this).css({
                                left:$rem*5 - $width/2,
                            })
                            $(this).animate({
                                top:1.333*$rem,
                                opacity:'1',
                                width:$width
                            },300,timeback($(this)))
                        })
                    }
                    if(arguments.length == 3){
                        var content = '<div class="pop-prompt"><span class="'+className+'"></span><div class="title"><h2>'+Msg+'</h2><p>'+Msg2+'</p></div></div>'
                        $('body').append(content);
                        timeback($('.pop-prompt:last'))
                    }
                    function timeback(_this){
                        vp.ui.timepop = null;
                        vp.ui.timepop = setTimeout(function() {
                            _this.remove();
                        },3500);
                    }
                },
                showMessage:function(codeNum){
                    var messageDic = {
                        "500":"网络差请重试",
                        "0": "请求成功",
                        "10001": "用户不存在",
                        "10002": "用户被锁定",
                        "10003": "用户手机号为空",
                        "10004": "用户手机号错误",
                        "10005": "用户手机号已存在",
                        "10006": "用户手机号不存在",
                        "10011": "用户验证码验证超时",
                        "10012": "用户验证码验证错误",
                        "10013": "用户验证码验证无效",
                        "10014": "用户验证码已被使用",
                        "10021": "用户登录密码过长",
                        "10022": "用户登录密码过短",
                        "10023": "用户登录密码为空",
                        "10024": "用户登录密码错误",
                        "10025": "用户登录密码比对错误",
                        "10026": "用户新密码与确认密码不一致",
                        "10027": "用户新密码与旧密码一致",
                        "10031": "用户账号名为空",
                        "10032": "用户账号名过长",
                        "10033": "用户账号名已分配",
                        "10034": "用户账号名可使用",
                        "10041": "用户成功注册但是登陆失败",
                        "10042": "用户注册失败",
                        "10051": "用户Token创建失败",
                        "10052": "用户Token校验失败",
                        "10053": "用户Token非法",
                        "10054": "用户Token已过期",
                        "10071": "用户已登陆",
                        "10072": "用户未登陆",
                        "10073": "用户登录失败",
                        "10081": "用户验证码发送失败",
                        "10082": "用户验证码发送成功",
                        "10083": "用户验证码发送超出最大发送次数",
                        "10091": "用户图片验证码生成失败",
                        "10092": "用户图片验证码校验失败",
                        "10093": "用户图片验证码校验成功",
                        "10094": "用户图片验证码无效",
                        "10111": "用户昵称更新成功",
                        "10112": "用户昵称更新失败",
                        "10113": "用户昵称为空",
                        "10114": "用户昵称过长",
                        "10121": "用户上传base64图片上传成功",
                        "10122": "用户上传base64图片上传失败",
                        "10123": "用户上传base64图片格式错误",
                        "10124": "用户上传base64图片数据为空",
                        "10131": "用户身份证图片上传成功",
                        "10132": "用户身份证图片上传是失败",
                        "10133": "用户身份证号错误",
                        "10134": "用户身份证号为空",
                        "10141": "用户真实姓名错误",
                        "10142": "用户已实名验证",
                        "10143": "用户未实名验证",
                        "10151": "用户银行卡号为空",
                        "10152": "用户开户银行为空",
                        "10153": "用户开户姓名为空",
                        "10154": "用户当前并没有添加银行卡",
                        "10155": "用户添加银行卡正在使用",
                        "10156": "用户添加银行卡正在审核",
                        "10157": "用户当前已有银行卡申请审核",
                        "10171": "用户当前余额不足",
                        "10172": "用户提现金额格式有误",
                        "10173": "用户提现金额单笔不能低于1元",
                        "10174": "用户提现金额单笔不能高于一百万元",
                        "10175": "用户提现时间距离上次提现不足1天",
                        "10176": "用户提现货币单位不支持",
                        "10181": "用户偏好为空",
                        "10191": "用户反馈内容为空",
                        "10192": "用户反馈内容提交失败",
                        "15001": "后台提现状态错误",
                        "15002": "后台该提现操作已完成不允许再次修改",
                        "15003": "后台提现状态为等待审核不需要再次修改",
                        "20001": "存在当前用户帐号下",
                        "20002": "存在别的用户帐号下",
                        "20003": "添加到死海失败",
                        "20004": "联系人数量不等于0不允许添加到死海",
                        "20005": "大海添加到私海失败",
                        "20006": "公司添加到私海失败",
                        "20007": "参数有误",
                        "20008": "添加失败",
                        "20009": "放弃失败",
                        "30021": "未添加收件人",
                        "30041": "邮件发送失败",
                        "30101": "请选择客户",
                        "30102": "联系人数量为零",
                        "30301": "附件上传失败",
                        "40001": "产品名称为空",
                        "40002": "产品描述为空",
                        "40003": "产品上传图片数为0",
                        "40004": "产品属性个数为0",
                        "40005": "产品属性值个数为0",
                        "40006": "产品价格不大于0",
                        "40007": "产品关键词个数不大于0",
                        "40008": "产品上传的图片不合法",
                        "40010": "主键不合法",
                        "40011": "分类名为空或空格",
                        "40012": "产品关键词不正确",
                        "40013": "产品属性名称为空字符串",
                        "40014": "产品属性值名称为空",
                        "40015": "产品属性值价格小于0",
                        "40016": "相关客户网址输入不正确",
                        "50000": "不支持此种订单状态",
                        "50001": "销售额小于成本X1点2倍",
                        "70000": "分页页码或分页大小不大于0",
                        "70001": "上传的文件有错误",
                        "70002": "上传图片失败",
                        "-1": "请求失败"
                    };
                    return messageDic[codeNum];
                },
                selectCustomer:function(id){
                    var str='';
                    str+='<div class="slect-Box0" id="slect-Box0">';
                    str+=   '<div class="content">';
                    str+=       '<h3 class="title">请选择群发类别</h3>';
                    str+=       '<div class="list li0">';
                    str+=           '<span class="icon-person-sea icon lf"></span>';
                    str+=           '<div class="box lf">';
                    str+=               '<p class="name">选择客户</p>';
                    str+=               '<p class="text">选择多个客户用于开发阶段的群发</p>';
                    str+=           '</div>';
                    str+=       '</div>';
                    str+=       '<div class="list li1">';
                    str+=           '<span class="icon-person-sea icon lf"></span>';
                    str+=           '<div class="box lf">';
                    str+=               '<p class="name">选择联系人</p>';
                    str+=               '<p class="text">选择多个客户中的多个联系人进行群发</p>';
                    str+=           '</div>';
                    str+=       '</div>';
                    str+=   '</div>';
                    str+='</div>';
                    $('#'+id).append(str);
                },
                scroll:function(box,contantet){
                    box.append(
                        '<div class="scrolltop-all">'+
                            '<i class="icon-maoduo33"></i>'+
                            '<p class="text_scroll">顶部</p>'+
                        '</div>'
                    )
                    var scrolltop = box.find('.scrolltop-all');
                    //回到顶部
                    contantet.on('scroll',function(e){
                        var _this = $(this);
                        if(_this.scrollTop()>600){
                            scrolltop.show();
                        }else{
                            scrolltop.hide();
                        }
                    });
                    scrolltop.on('click',function(e){
                        e.preventDefault();
                        contantet.animate({
                                scrollTop: 0
                            },
                            500);
                        return false;
                    })
                },
                uploadPictures:function(_this,imgshow,callback){
                    var callbackFun = false;
                    if(arguments.length==3){callbackFun=true};
                    lrz(_this.files[0])
                        .then(function (rst) {
                            // 处理成功会执行
                            imgshow.attr('src',rst.base64);
                            if(callbackFun){
                                callback(rst);
                            }
                        })
                        .catch(function (err) {
                            // 处理失败会执行
                        })
                        .always(function () {
                            // 不管是成功失败，都会执行
                        });
                },
                uploadPicture:function(_this,callback){
                    lrz(_this.files[0])
                        .then(function (rst) {
                            // 处理成功会执行
                            
                                callback(rst);
                            
                        })
                        .catch(function (err) {
                            // 处理失败会执行
                        })
                        .always(function () {
                            // 不管是成功失败，都会执行
                        });
                },
            },
            registerObj:{

            },
            registerFn:{

            },
            core:{
                //api:"http://192.168.1.240:1020",
                //api:"http://test1130.veryvp.com",
                api:"https://www.mooddo.com",
//				api:"http://192.168.1.183:2320",
                token:window.localStorage['token'],
                debug: false,
                ajax: function (url, params, success, error, fail) {
                    var reqUrl,currentTime=null,endTime=null;
                    if(!params){
                        params = {};
                    }
                    if(url&&url.debug){
                        reqUrl = vp.core.api+url.local+ '?rnd=' + new Date().getTime();
                        $.ajax(reqUrl,
                            {
                                data: JSON.stringify(params),
                                success:success,
                                error:error,
                                complete: function (XMLHttpRequest, textStatus) {
                                    var test = $.extend(true, {}, params);
                                    console.log(reqUrl + "\n\n [本地发送]\n" + JSON.stringify(test) + "\n\n [本地返回]\n" + XMLHttpRequest.responseText);
                                }
                            }
                        );
                        return;
                    }else{
                        if(url && url.server){
                            reqUrl = vp.core.api+url.server
                        }else{
                            reqUrl = vp.core.api+url
                        }
                    }
                    var options = {
                        type: 'POST',
                        cache: false,
                        headers:{vp_token:vp.core.token},
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(params),
                        dataType: 'json',
                        timeout: 19900,
                        success: function (data, textStatus, jqXHR) {
                            if(data.IsSuccess){
                                if(data&&data.Data&&data.Data.Token){
                                    vp.core.token = data.Data.Token;
                                }
                                if (typeof success === 'function') {
                                    success(data);
                                }
                            }else{
                                if(vp.ui.isTrue){
                                    if(data.Code==401){
                                        vp.ui.pop('icon-err','登陆失效，请重新登录');
                                        router.navigate("login/");
                                        vp.ui.removeLoading();
                                        return;
                                    }
                                }
                                if (typeof error === 'function') {
                                    var msg = data && data.Message || "加载失败";
                                    error(data);
                                }else{

                                }
                            }
                            endTime = new Date().getTime();
                            if(endTime-currentTime<vp.ui.loadtimer){
                                setTimeout(function(){
                                    vp.ui.removeLoading()
                                    },vp.ui.loadtimer-(endTime-currentTime)
                                )
                            }else{
                                vp.ui.removeLoading();
                            }
                        },
                        error: function (XMLHttpRequest, textStatus) {
                            if (typeof fail === 'function') {
                                var msg =  "加载失败,网络异常";
                                vp.ui.pop('icon-err',msg);
                                fail();
                            }else{
                                var msg =  "加载失败,网络异常";
                                vp.ui.pop('icon-err',msg);
                            }
                            endTime = new Date().getTime();
                            if(endTime-currentTime<vp.ui.loadtimer){
                                setTimeout(function(){
                                    vp.ui.removeLoading()
                                },vp.ui.loadtimer-(endTime-currentTime)
                                )
                            }else{
                                vp.ui.removeLoading();
                            }
                        },
                        complete: function (XMLHttpRequest, textStatus) {
                            if (vp.core.debug) {
                                var test = $.extend(true, {}, params);
                                console.log(url + "\n\n [发送]\n" + JSON.stringify(test) + "\n\n [返回]\n" + XMLHttpRequest.responseText);
                            }
                        }
                    };
                    if(url && url.type){
                        options.type = url.type;
                        if(options.type == "get" || options.type == "GET"){
                            options.data = params;
                        }
                    }
                    //显示小蜜蜂
                    if( url&&url.loadFlag){
                        vp.ui.loading();
                        currentTime = new Date().getTime();
                    }
                    var ajax = $.ajax(reqUrl, options);
                },
                //初始化
                init: function (callback){

                },
            },
        };
        vp.ui = obj.ui;
        vp.core = obj.core;
        vp.registerFn = obj.registerFn;
        vp.registerObj = obj.registerObj;
    });