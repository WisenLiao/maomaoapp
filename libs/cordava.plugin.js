//注册插件;
cordova.define('com.app.plugin.OpenFile', function(require, exports, module){
    var exec = require("cordova/exec");
    /**
     * Constructor.
     *
     */
    function OpenFilePllugin()
    {

    };

    OpenFilePllugin.prototype.openFile = function (fileUrl, params,successCallback, errorCallback)
    {
        if (errorCallback == null)
        {
            errorCallback = function (){
            };
        }
        if (typeof errorCallback != "function")
        {
            console.log("OpenFilePllugin failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function")
        {
            console.log("OpenFilePllugin  failure: success callback parameter must be a function");
            return;
        }
        if (!errorCallback)
            successCallback = params;
        exec(successCallback, errorCallback, 'OpenFile', 'openFile',[fileUrl, params]);

    };
    var openFilePllugin = new OpenFilePllugin();
    module.exports = openFilePllugin;
});

//显示进度条
function showUploadingProgress(progressEvt) {
    if (progressEvt.lengthComputable) {
        navigator.notification.progressValue(Math
            .round((progressEvt.loaded / progressEvt.total) * 100));
    }
}

//打开文件;
function OpenFile(path){
    //   alert('OpenFile');
    var openFilePlugin=cordova.require('com.app.plugin.OpenFile');
    openFilePlugin.openFile(path, {},
        function (result) {

        }, function (e) {
            navigator.notification.alert(e,null,"提示","确定");
        });
}

//下载文件
function download(path,ext) {
    try {
        path = encodeURI(path);
        var pos = path.lastIndexOf("/");
        var fileName = path.substring(pos + 1);
        //无后缀
        if (ext) {
            fileName = fileName + ext;
        }
        setTimeout(function () {
            var fileTransfer = new FileTransfer();
            var uri = encodeURI(path);
            var fileURL = "/mnt/sdcard/download/" + fileName;
            fileTransfer.onprogress = showUploadingProgress;
            navigator.notification.progressStart("", "当前下载进度");
            fileTransfer.download(
                uri,
                fileURL,
                function (entry) {
                    navigator.notification.progressStop();
                    navigator.notification.confirm("是否打开文件?", function (button) {
                        if( button==1 ) {
                            OpenFile(fileURL);
                        }
                        else{
                            window.event.returnValue = false;
                        }
                    }, "提示", ["确定","取消"])
                },
                function (e) {
                    navigator.notification.progressStop();
                    navigator.notification.alert(e,null,"提示","确定");
                }
            );
        }, 100);
    }
    catch(e){
        navigator.notification.alert(e,null,"提示","确定");
    }

}